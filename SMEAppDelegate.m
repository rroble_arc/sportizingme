//
//  SMEAppDelegate.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/6/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEAppDelegate.h"

#import "IIViewDeckController.h"


#import "SMEWorkOutVC.h"

#import "SMEAthleteMenuVC.h"

#import "SMEIntroVC.h"

#import "SMETimeManager.h"

#import "SMESponsorMenuVC.h"
#import "SMEFindAthleteVC.h"
#import "ARCFacebookManager.h"







@implementation SMEAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    fetching = NO;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
 self.offersArray = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4", nil];

    SMEIntroVC *introVC = [[SMEIntroVC alloc]initWithNibName:@"SMEIntroVC" bundle:nil];
    
    UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:introVC];
    
    self.window.rootViewController = nc;
    [self.window makeKeyAndVisible];
    return YES;
}
#pragma mark CLLOcationDelegate
-(void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"newLocation : %@",newLocation);
    //NSLog(@"oldLocation : %@",oldLocation);
    
    BOOL isInBackground = NO;
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        isInBackground = YES;
    }
    
    // Handle location updates as normal, code omitted for brevity.
    // The omitted code should determine whether to reject the location update for being too
    // old, too close to the previous one, too inaccurate and so forth according to your own
    // application design.
    
    if (isInBackground)
    {
        //NSLog(@"newLocation : %@",newLocation);
        //NSLog(@"oldLocation : %@",oldLocation);

       
        [self sendBackgroundLocationToServer:newLocation];
    }
    else
    {
        // ...
    }
   
}
-(void) startFetch{
    if(!fetching){
         fetching = YES;
        [self.fetch stopFetching];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"athlete_id", nil];
    NSString * urlString = @"http://www.i-italy.org/webservices/json/getMagazineFrontPage";
    
    
   self.fetch = [[ARCFetcher alloc]init];
    
    [self.fetch startFetchWithURLString:urlString postParams:nil userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
       
        if(error){
             fetching = NO;
            NSLog(@"error : %@",error);
        }else{
             fetching = NO;
            NSLog(@"result : %@",result);
        }
    } progressCallback:nil];
    }
}
-(void) sendBackgroundLocationToServer:(CLLocation *)location
{
    // REMEMBER. We are running in the background if this is being executed.
    // We can't assume normal network access.
    // bgTask is defined as an instance variable of type UIBackgroundTaskIdentifier
    
    // Note that the expiration handler block simply ends the task. It is important that we always
    // end tasks that we have started.
    
    bgTask = [[UIApplication sharedApplication]
              beginBackgroundTaskWithExpirationHandler:
              ^{
                  [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                   }];
    
                  
                       // Here you put the task for tm.seconds to be passed all the infos to be passed on web... to terminate he's training etc.
                        // And upon coming back to foreground fetch something if ok pa ba iyang training etc.
    
                   
                        [self startFetch];
                    
    
    
                  // ANY CODE WE PUT HERE IS OUR BACKGROUND TASK
                  
                  // For example, I can do a series of SYNCHRONOUS network methods (we're in the background, there is
                  // no UI to block so synchronous is the correct approach here).
                  
                  // ...
                  
                  // AFTER ALL THE UPDATES, close the task
                  
                  if (bgTask != UIBackgroundTaskInvalid)
                  {
                      [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                       bgTask = UIBackgroundTaskInvalid;
                       }
}
 
#pragma mark Method
-(void)createDeck{
    self.leftVC = [[SMEAthleteMenuVC alloc]initWithNibName:@"SMEAthleteMenuVC" bundle:nil];
    
    SMEWorkOutVC *centerController = [[SMEWorkOutVC alloc] initWithNibName:@"SMEWorkOutVC" bundle:nil];
    self.centerVC = [[UINavigationController alloc] initWithRootViewController:centerController];
    
    self.deckAthlete =  [[IIViewDeckController alloc] initWithCenterViewController:self.centerVC leftViewController:self.leftVC];
    self.deckAthlete.leftSize = 60;
    self.deckAthlete.openSlideAnimationDuration = 0.25f; // In seconds
    self.deckAthlete.closeSlideAnimationDuration = 0.25f;
    self.deckAthlete.bounceDurationFactor = 1.25f;
    self.deckAthlete.bounceOpenSideDurationFactor = 1.25f;
    
    [self.deckAthlete setPanningMode:IIViewDeckNoPanning];
}
-(void)createDeckSponsor{
    self.leftVC = [[SMESponsorMenuVC alloc]initWithNibName:@"SMESponsorMenuVC" bundle:nil];
    
    SMEFindAthleteVC *centerController = [[SMEFindAthleteVC alloc] initWithNibName:@"SMEFindAthleteVC" bundle:nil];
    self.centerVC = [[UINavigationController alloc] initWithRootViewController:centerController];
    
    self.deckSponsor =  [[IIViewDeckController alloc] initWithCenterViewController:self.centerVC leftViewController:self.leftVC];
    
    self.deckSponsor.openSlideAnimationDuration = 0.25f; // In seconds
    self.deckSponsor.closeSlideAnimationDuration = 0.25f;
    self.deckSponsor.bounceDurationFactor = 1.25f;
    self.deckSponsor.bounceOpenSideDurationFactor = 1.25f;
    
    [self.deckSponsor setPanningMode:IIViewDeckNoPanning];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

   
    SMETimeManager *tm =[SMETimeManager sharedManager];
    if(tm.hasStarted){

         [self.locationManager startMonitoringSignificantLocationChanges];
        [tm.timer invalidate];
        
   
        NSDate *date = [NSDate date];
        NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
        [ud setObject:date forKey:@"date"];
        [ud setObject:[NSNumber numberWithLong:tm.seconds] forKey:@"seconds"];
    
        [ud synchronize];
    }
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
  
    SMETimeManager *tm =[SMETimeManager sharedManager];
    if(tm.hasStarted){

        [self.locationManager stopMonitoringSignificantLocationChanges];
        [self.locationManager startUpdatingLocation];
        [tm startTimer];
    }
    
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //[self.locationManager stopMonitoringSignificantLocationChanges];
   // [self.locationManager startUpdatingLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{

    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    [ud setObject:nil forKey:@"date"];
    [ud setObject:0 forKey:@"seconds"];
    
    [ud synchronize];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
  // attempt to extract a token from the url
  ARCFacebookManager *fbm = [ARCFacebookManager sharedManager];
  
  BOOL canOpen =  [fbm.fb_session handleOpenURL:url];
  
  
  return canOpen;
}

@end
