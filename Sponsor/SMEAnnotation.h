//
//  SMEAnnotation.h
//  Sportizingme
//
//  Created by Shlby Puerto on 6/26/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>

@interface SMEAnnotation : NSObject<MKAnnotation>
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

// Title and subtitle for use by selection UI.
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic, strong)NSMutableDictionary *dict;


#pragma mark - init
- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;
- (id)initWithLatitude:(CLLocationDegrees)latitude
             longitude:(CLLocationDegrees)longitude
                 title:(NSString*)title
              subtitle:(NSString*)subtitle;

#pragma mark - other property
// <you can add here more properties and method>
- (void)showNSLogDetails;

@end
