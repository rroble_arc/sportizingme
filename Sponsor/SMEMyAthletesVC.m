//
//  SMEMyAthletesVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEMyAthletesVC.h"
#import "IIViewDeckController.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "Reachability.h"
#import "SMELanguage.h"
#import "SMEMyAthleteCell.h"
#import "ARCFetcher.h"
#import "SMESponsorUserManager.h"
#import "SVProgressHUD.h"

@interface SMEMyAthletesVC ()

@end

@implementation SMEMyAthletesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];

    [self.TB setSelectedItem:self.currentTB];
  [self setupNav];
  [self temp];
  
  // change the line separator color
  UIColor *separatorColor = [UIColor colorWithRed:194.0/255.0 green:224.0/255.0 blue:191.0/255.0 alpha:1.0];
  self.currentTableView.separatorColor = separatorColor;
  self.pastTableView.separatorColor = separatorColor;
  self.savedTableView.separatorColor = separatorColor;  
  
  [self handlerInfiniteScrolling];
  [self setLanguage];
}
-(void)viewWillAppear:(BOOL)animated{
    [self fetch];
}
#pragma mark - Methods
-(void)fetch{
    [SVProgressHUD show];
 
    NSString * urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/sponsor/my/athletes?token=%@",[[SMESponsorUserManager sharedManager]token]];
    
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:nil userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        [SVProgressHUD dismiss];
        if(error){
            NSLog(@"error %@",result);
        }else{
            NSLog(@"result %@",result);
        }
    } progressCallback:nil];
}
- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - handle infinite scroll
- (void)handlerInfiniteScrolling
{
  // add the infinite scrolling
  // CURRENT
  [self.currentTableView addInfiniteScrollingWithActionHandler:^{
    // append data to data source, insert new cells at the end of table view
    // call [tableView.infiniteScrollingView stopAnimating] when done
    [self performSelector:@selector(increaseCurrentArray) withObject:nil afterDelay:3.0];
  }];
  
  // PAST
  [self.pastTableView addInfiniteScrollingWithActionHandler:^{
    // append data to data source, insert new cells at the end of table view
    // call [tableView.infiniteScrollingView stopAnimating] when done
    [self performSelector:@selector(increasePastArray) withObject:nil afterDelay:3.0];
  }];
  
  // SAVED
  [self.savedTableView addInfiniteScrollingWithActionHandler:^{
    // append data to data source, insert new cells at the end of table view
    // call [tableView.infiniteScrollingView stopAnimating] when done
    [self performSelector:@selector(increaseSavedArray) withObject:nil afterDelay:3.0];
  }];
}

#pragma mark - Set Language
-(void)setLanguage{
  SMELanguage *lang = [SMELanguage sharedManager];
  [self.btnCurrent setTitle:[lang sponsorMyAthletesBtnCurrent] forState:UIControlStateNormal];
  [self.btnPast setTitle:[lang sponsorMyAthletesBtnPast] forState:UIControlStateNormal];
  [self.btnSaved setTitle:[lang sponsorMyAthletesBtnSaved] forState:UIControlStateNormal];
    
    [self.currentTB setTitle:[lang sponsorMyAthletesBtnCurrent]];
    [self.pastTB setTitle:[lang sponsorMyAthletesBtnPast]];
    [self.savedTB setTitle:[lang sponsorMyAthletesBtnSaved]];
}

#pragma mark - temp function
- (void)increaseCurrentArray{
  NSInteger total = self.currentArray.count;
  
  for (NSInteger i=0; i<10; i++) {
    NSString *str = [NSString stringWithFormat:@"current%d",i+total];
    [self.currentArray addObject:str];
  }
  [self.currentTableView reloadData];
  [self.currentTableView.infiniteScrollingView stopAnimating];
}
- (void)increasePastArray{
  NSInteger total = self.pastArray.count;
  
  for (NSInteger i=0; i<10; i++) {
    NSString *str = [NSString stringWithFormat:@"past%d",i+total];
    [self.pastArray addObject:str];
  }
  [self.pastTableView reloadData];
  [self.pastTableView.infiniteScrollingView stopAnimating];
}
- (void)increaseSavedArray{
  NSInteger total = self.savedArray.count;
  
  for (NSInteger i=0; i<10; i++) {
    NSString *str = [NSString stringWithFormat:@"saved%d",i+total];
    [self.savedArray addObject:str];
  }
  [self.savedTableView reloadData];
  [self.savedTableView.infiniteScrollingView stopAnimating];
}


#pragma mark Methods
-(void)temp{
  self.currentArray = [NSMutableArray arrayWithObjects:@"current0",@"current1",@"current2", nil];
  self.pastArray = [NSMutableArray arrayWithObjects:@"past0",@"past1",@"past2", nil];
  self.savedArray = [NSMutableArray arrayWithObjects:@"saved0",@"saved1",@"saved2",@"saved3", nil];
}

-(void)setupNav{
  [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
  UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
  [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
  [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
  
  
  self.navigationItem.leftBarButtonItem = btnBar;
  
  [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
}
#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  
  
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
  if(tableView == self.currentTableView)
    return [self.currentArray count];
  else if(tableView == self.pastTableView)
    return [self.pastArray count];
  else if(tableView == self.savedTableView)
    return [self.savedArray count];
  
  return  0;
  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Cell";
  SMEMyAthleteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
  if(cell==nil){
    //cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [[SMEMyAthleteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
  }
  
  
  
  
  if(tableView == self.currentTableView){
    cell.nameLabel.text = [self.currentArray objectAtIndex:indexPath.row];
    //cell.sportLabel.text = [self.currentArray objectAtIndex:indexPath.row];
  }
  else if(tableView == self.pastTableView){
    cell.nameLabel.text = [self.pastArray objectAtIndex:indexPath.row];
    //cell.sportLabel.text = [self.pastArray objectAtIndex:indexPath.row];
  }
  else if(tableView == self.savedTableView){
    cell.nameLabel.text = [self.savedArray objectAtIndex:indexPath.row];
    //cell.sportLabel.text = [self.savedArray objectAtIndex:indexPath.row];
  }
  
  
  return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return SMEMYATHLETECELL_HEIGHT;
}

#pragma mark TabBar Delegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if(item == self.currentTB){
        [self.currentTableView setHidden:NO];
        [self.pastTableView setHidden:YES];
        [self.savedTableView setHidden:YES];
    }else if(item == self.pastTB){
        [self.currentTableView setHidden:YES];
        [self.pastTableView setHidden:NO];
        [self.savedTableView setHidden:YES];
    }else if(item == self.savedTB){
        [self.currentTableView setHidden:YES];
        [self.pastTableView setHidden:YES];
        [self.savedTableView setHidden:NO];
    }
}

#pragma mark - Button Action
- (IBAction)currentAction:(id)sender {
  [self.currentTableView setHidden:NO];
  [self.pastTableView setHidden:YES];
  [self.savedTableView setHidden:YES];
}

- (IBAction)pastAction:(id)sender {
  [self.currentTableView setHidden:YES];
  [self.pastTableView setHidden:NO];
  [self.savedTableView setHidden:YES];
}
- (IBAction)savedAction:(id)sender {
  [self.currentTableView setHidden:YES];
  [self.pastTableView setHidden:YES];
  [self.savedTableView setHidden:NO];
}
@end
