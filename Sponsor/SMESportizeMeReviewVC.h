//
//  SMESportizeMeReviewVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMESportizeMeReviewVC : UIViewController
- (IBAction)sendAction:(id)sender;
//

@property (weak, nonatomic) IBOutlet UILabel *lblAthleteName;
@property (weak, nonatomic) IBOutlet UILabel *lblTimesWeek;
@property (weak, nonatomic) IBOutlet UILabel *lblHourWeek;
@property (weak, nonatomic) IBOutlet UILabel *lblGymName;
@property (weak, nonatomic) IBOutlet UILabel *lblWeeks;
@property (weak, nonatomic) IBOutlet UIImageView *imgAD;

@property (weak, nonatomic) IBOutlet UITextField *txtCash;


//!
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblTimes;
@property (weak, nonatomic) IBOutlet UILabel *lblFor1;
@property (weak, nonatomic) IBOutlet UILabel *lblHour;
@property (weak, nonatomic) IBOutlet UILabel *lblGym;
@property (weak, nonatomic) IBOutlet UILabel *lblFor2;
@property (weak, nonatomic) IBOutlet UILabel *lblWeek;
@property (weak, nonatomic) IBOutlet UILabel *lblWear;
@property (weak, nonatomic) IBOutlet UILabel *lblPay;

@property (strong, nonatomic) UIImage *imageAD;
@property (strong, nonatomic) NSString *no_of_trainings;
@property (strong, nonatomic) NSMutableArray *sportsArray;

@property (strong, nonatomic) NSMutableDictionary *dictAthlete;
@end
