//
//  SMEAnnotation.m
//  Sportizingme
//
//  Created by Shlby Puerto on 6/26/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEAnnotation.h"



@implementation SMEAnnotation
#pragma mark - init
- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate{
    self = [super init];
    if (self) {
        //
        _coordinate = coordinate;
    }
    return self;
}

- (id)initWithLatitude:(CLLocationDegrees)latitude
             longitude:(CLLocationDegrees)longitude
                 title:(NSString*)title
              subtitle:(NSString*)subtitle
{
    self = [super init];
    if (self) {
        // initialize here
        _coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        _title = title;
        _subtitle = subtitle;
    }
    return self;
}

#pragma mark - other property
- (void)showNSLogDetails{
    NSLog(@"%@",self.title);
    NSLog(@"%@",self.subtitle);
    NSLog(@"lat: %f",self.coordinate.latitude);
    NSLog(@"lon: %f",self.coordinate.longitude);
}
@end
