//
//  SMEMapAthleteVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
#import "SMEAnnotation.h"



@interface SMEMapAthleteVC : UIViewController<UITabBarDelegate>

@property (strong, nonatomic) NSMutableArray *tableArray;

@property (weak, nonatomic) IBOutlet UIView *listView;
@property (weak, nonatomic) IBOutlet UIView *mapListView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)listAction:(id)sender;
- (IBAction)mapAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnList;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;
@property (weak, nonatomic) IBOutlet UITabBarItem *listTB;
@property (weak, nonatomic) IBOutlet UITabBarItem *mapTB;
@property (weak, nonatomic) IBOutlet UITabBar *TB;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
