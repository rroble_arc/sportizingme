//
//  SMEMyAthletesVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEMyAthletesVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *currentTableView;
@property (weak, nonatomic) IBOutlet UITableView *pastTableView;
@property (weak, nonatomic) IBOutlet UITableView *savedTableView;

@property (strong, nonatomic) NSMutableArray *currentArray;
@property (strong, nonatomic) NSMutableArray *pastArray;
@property (strong, nonatomic) NSMutableArray *savedArray;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrent;
@property (weak, nonatomic) IBOutlet UIButton *btnPast;
@property (weak, nonatomic) IBOutlet UIButton *btnSaved;

- (IBAction)currentAction:(id)sender;
- (IBAction)pastAction:(id)sender;
- (IBAction)savedAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITabBarItem *currentTB;
@property (weak, nonatomic) IBOutlet UITabBarItem *pastTB;
@property (weak, nonatomic) IBOutlet UITabBarItem *savedTB;
@property (weak, nonatomic) IBOutlet UITabBar *TB;

@end
