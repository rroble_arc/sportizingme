//
//  SMEFindAthleteVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "SMESponsorUserManager.h"
#import "SMEFindAthleteVC.h"
#import "IIViewDeckController.h"
#import "ARCFetcher.h"
#import "SMEMapAthleteVC.h"
#import "SMELanguage.h"
#import "SVProgressHUD.h"

@interface SMEFindAthleteVC ()

@end

@implementation SMEFindAthleteVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setLanguage];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.btnFind setTitle:[lang sponsorFindAthleteBtnFind] forState:UIControlStateNormal];
    [self.btnAdvanced setTitle:[lang sponsorFindAthleteBtnAdvanced] forState:UIControlStateNormal];
    [self.txtLocation setPlaceholder:[lang sponsorFindAthleteTxtLocation]];
    [self.txtSports setPlaceholder:[lang sponsorFindAthleteTxtSports]];
    
}
#pragma mark Methods
-(void)setupNavigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
    [self.navigationController.navigationBar setTintColor:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)findAction:(id)sender {
    
    [SVProgressHUD show];
    
    NSMutableArray *sportsArray = [NSMutableArray array];
    
  
    [sportsArray addObject:@"RUN"];
    [sportsArray addObject:@"CYC"];

  
        NSMutableDictionary *postDict;
        NSString * urlString;
     
            
            postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"1.22222",@"longitude",@"2.3333",@"latitude", nil];
            
            for(int i = 0;i <[sportsArray count];i++){
                NSString *key = [NSString stringWithFormat:@"sports[%d]",i];
                [postDict setValue:[sportsArray objectAtIndex:i] forKey:key];
            }
            
            urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/sponsor/find/athletes?token=%@",[[SMESponsorUserManager sharedManager]token]];
            
            ARCFetcher *fetch = [[ARCFetcher alloc]init];
            
            [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
                [SVProgressHUD dismiss];
                if(error){
                    NSLog(@"result %@",result);
                }else{
                     NSLog(@"result %@",result);
                    if([result isKindOfClass:[NSArray class]]){
                    SMEMapAthleteVC *mvc = [[SMEMapAthleteVC alloc]initWithNibName:@"SMEMapAthleteVC" bundle:nil];
                    mvc.tableArray = result;
                    [self.navigationController pushViewController:mvc animated:YES];
                    }
                   
                    
                }
                
            } progressCallback:nil];
    
    
    
}
#pragma mark - Methods

@end
