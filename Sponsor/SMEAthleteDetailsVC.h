//
//  SMEAthleteDetailsVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEAthleteDetailsVC : UIViewController
- (IBAction)sportizeMeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSportize;
@property (weak, nonatomic) IBOutlet UILabel *lblWhere;
@property (weak, nonatomic) IBOutlet UILabel *lblSports;
//
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgGYM;
@property (weak, nonatomic) IBOutlet UIImageView *imgRUN;
@property (weak, nonatomic) IBOutlet UIImageView *imgCYC;
@property (weak, nonatomic) IBOutlet UIImageView *imgMMA;
@property (weak, nonatomic) IBOutlet UIImageView *imgSOC;
@property (weak, nonatomic) IBOutlet UITextView *txtTrain;

@property (strong, nonatomic) NSMutableDictionary *dictAthlete;

//!
@end
