//
//  SMESportizeMeSent.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMESportizeMeSent.h"
#import "IIViewDeckController.h"
#import "SMELanguage.h"

@interface SMESportizeMeSent ()

@end

@implementation SMESportizeMeSent

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setLanguage];
    [self setSent];
}
-(void)setSent{
    self.lblAthleteName.text = [self.dictAthlete objectForKey:@"name"];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.lblOffer setText:[lang sponsorSentOffer]];
    [self.lblReview setText:[lang sponsorSentReview]];
    [self.lblAccept setText:[lang sponsorSentAccept]];
    [self.lblPack setText:[lang sponsorSentPack]];
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    self.title = [[SMELanguage sharedManager]sponsorSentTitle];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_bg"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
