//
//  SMESportizeMeReviewVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "SVProgressHUD.h"
#import "NSData+Base64.h"
#import "SMESponsorUserManager.h"
#import "SMESportizeMeReviewVC.h"
#import "SMESportizeMeSent.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface SMESportizeMeReviewVC ()
{
    CGFloat animatedDistance;
}
@end

@implementation SMESportizeMeReviewVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.title = [[SMELanguage sharedManager] sponsorSendOfferTitle];
      [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_bg"] forBarMetrics:UIBarMetricsDefault];
    
    [self setupNavigationBar];
    [self setLanguage];
    [self setReview];
}
-(void)setReview{
    self.lblAthleteName.text = [self.dictAthlete objectForKey:@"name"];
    self.lblTimesWeek.text = self.no_of_trainings;
    self.lblHourWeek.text = @"1";
    self.lblWeeks.text = @"1";
    self.lblGymName.text = @"SOME GYM";
    [self.imgAD setImage:self.imageAD];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.btnSend setTitle:[lang sponsorReviewBtnSend] forState:UIControlStateNormal];
    [self.lblAgree setText:[lang sponsorReviewAgree]];
    [self.lblTimes setText:[lang sponsorReviewTimes]];
    [self.lblFor1 setText:[lang sponsorReviewFor]];
    [self.lblFor2 setText:[lang sponsorReviewFor]];
    [self.lblHour setText:[lang sponsorReviewHour]];
    [self.lblGym setText:[lang sponsorReviewGym]];
    [self.lblWeek setText:[lang sponsorReviewWeek]];
    [self.lblWear setText:[lang sponsorReviewWear]];
    [self.lblPay setText:[lang sponsorReviewPay]];
    
}

-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_bg"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendAction:(id)sender {
    
    [SVProgressHUD show];
    NSMutableDictionary *postDict;
    NSString * urlString;
                      
        NSData *pictureData = [[NSData alloc]init];
        pictureData = UIImagePNGRepresentation(self.imageAD);
    NSString * base64string = [pictureData base64EncodedString];
    base64string = [base64string stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.lblTimesWeek.text,@"number_of_trainings_per_week",self.lblWeeks.text,@"number_of_weeks",self.lblHourWeek.text,@"hours_of_training",[self.dictAthlete objectForKey:@"id"],@"athlete_id",self.txtCash,@"payment",base64string,@"ads_to_display", nil];
    
    for(int i = 0;i <[self.sportsArray count];i++){
        NSString *key = [NSString stringWithFormat:@"sports[%d]",i];
        [postDict setValue:[self.sportsArray objectAtIndex:i] forKey:key];
    }
    
    urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/sponsor/sportize?token=%@",[[SMESponsorUserManager sharedManager]token]];
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        [SVProgressHUD dismiss];
        if(error){
            NSLog(@"result %@",result);
        }else{
            NSLog(@"result %@",result);
            if([result objectForKey:@"error"]==nil){
                SMESportizeMeSent *smvc = [[SMESportizeMeSent alloc]initWithNibName:@"SMESportizeMeSent" bundle:nil];
                smvc.dictAthlete = self.dictAthlete;
                [self.navigationController pushViewController:smvc animated:YES];
            }else{
                
            }
            
    
        }
        
    } progressCallback:nil];

    /*
   
     */
}
#pragma mark - Methods
-(void)fetch{
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"athlete_id", nil];
    NSString * urlString = @"";
    
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        if(error){
            
        }else{
            
        }
    } progressCallback:nil];
}
#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
   
    
    return YES;
}
@end
