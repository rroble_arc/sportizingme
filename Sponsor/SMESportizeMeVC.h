//
//  SMESportizeMeVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMESportizeMeVC : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
- (IBAction)reviewAction:(id)sender;
- (IBAction)downloadAction:(id)sender;

@property (strong, nonatomic) UIImagePickerController *imagePicker;

@property (weak, nonatomic) IBOutlet UIImageView *imgAD;
@property (strong, nonatomic) UIImage* mainImage;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellTraining;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSports;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnReview;
@property (weak, nonatomic) IBOutlet UIButton *btnDownload;
@property (weak, nonatomic) IBOutlet UILabel *lblAD;
//
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtSports;
@property (weak, nonatomic) IBOutlet UILabel *lblSports;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
- (IBAction)sportsAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnGYM;
@property (weak, nonatomic) IBOutlet UIButton *btnRUN;
@property (weak, nonatomic) IBOutlet UIButton *btnCYC;
@property (weak, nonatomic) IBOutlet UIButton *btnMMA;
@property (weak, nonatomic) IBOutlet UIButton *btnSOC;
//!
@property (strong, nonatomic) NSMutableDictionary *dictAthlete;
@end
