//
//  SMESponsorMenuVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMESponsorMenuVC.h"
#import "IIViewDeckController.h"
#import "SMEIntroVC.h"
#import "SMEAppDelegate.h"
#import "SMELanguage.h"
//Views
#import "SMEFindAthleteVC.h"
#import "SMEMyCompanyVC.h"
#import "SMEDashboardVC.h"
#import "SMEMyAccountSponsor.h"
#import "SMESponsorSettings.h"
#import "SMEMyAthletesVC.h"
//!

@interface SMESponsorMenuVC ()
@property (strong, nonatomic) NSMutableArray *tableArray;
@end

@implementation SMESponsorMenuVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.tableArray = [[NSMutableArray alloc]initWithObjects:@"My Athletes",@"My Company",@"Dashboard",@"Find Athletes!",@"My Account",@"Settings",@"Sign Out", nil];
    [self setupNav];
    //[self updateLanguage];
    [self setLanguage];
}
-(void)updateLanguage{
    [self.tableArray replaceObjectAtIndex:0 withObject:[[SMELanguage sharedManager] sponsorMenuCellMyAthletes]];
    [self.tableArray replaceObjectAtIndex:1 withObject:[[SMELanguage sharedManager] sponsorMenuCellMyCompany]];
    [self.tableArray replaceObjectAtIndex:2 withObject:[[SMELanguage sharedManager] sponsorMenuCellDashboard]];
    [self.tableArray replaceObjectAtIndex:3 withObject:[[SMELanguage sharedManager] sponsorMenuCellFindAthletes]];
    [self.tableArray replaceObjectAtIndex:4 withObject:[[SMELanguage sharedManager] sponsorMenuCellMyAccount]];
    [self.tableArray replaceObjectAtIndex:5 withObject:[[SMELanguage sharedManager] sponsorMenuCellSettings]];
    [self.tableArray replaceObjectAtIndex:6 withObject:[[SMELanguage sharedManager] sponsorMenuCellSignOut]];
    
    
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.lblMyAthletes setText:[lang sponsorMenuCellMyAthletes]];
    [self.lblCompany setText:[lang sponsorMenuCellMyCompany]];
    [self.lblDashboard setText:[lang sponsorMenuCellDashboard]];
    [self.lblAthletes setText:[lang sponsorMenuCellFindAthletes]];
    [self.lblAccount setText:[lang sponsorMenuCellMyAccount]];
    [self.lblSettings setText:[lang sponsorMenuCellSettings]];
    [self.lblSignout setText:[lang sponsorMenuCellSignOut]];
}
-(void)setupNav{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
     return [self.tableArray count]+2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    
    if(row == 0){
        return self.cellMyAthletes;
    }else if(row == 1){
        return self.cellCompany;
    }else if(row ==2){
        return self.cellDashboard;
    }else if(row == 3){
        return self.cellAthletes;
    }else if(row ==4){
        return self.cellAccount;
    }else if(row ==5){
        return self.cellSettings;
    }else if(row ==6){
        return self.cellSignout;
    }else if(row == 7){
        return self.cellBlank;
    }else if(row == 8){
        return self.cellBlank2;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 3 || indexPath.row == 8){
        return 176;
    }
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        SMEMyAthletesVC *vc = [[SMEMyAthletesVC alloc]initWithNibName:@"SMEMyAthletesVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
        
    }
    else if(indexPath.row == 1){
        SMEMyCompanyVC *vc = [[SMEMyCompanyVC alloc]initWithNibName:@"SMEMyCompanyVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
        
    }
    else if(indexPath.row == 2){
        SMEDashboardVC *vc = [[SMEDashboardVC alloc]initWithNibName:@"SMEDashboardVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
    }
    else if(indexPath.row == 3){
        SMEFindAthleteVC *vc = [[SMEFindAthleteVC alloc]initWithNibName:@"SMEFindAthleteVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
    }
    else if(indexPath.row == 4){
        SMEMyAccountSponsor *vc = [[SMEMyAccountSponsor alloc]initWithNibName:@"SMEMyAccountSponsor" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
    }
    else if(indexPath.row == 5){
        SMESponsorSettings
 *vc = [[SMESponsorSettings alloc]initWithNibName:@"SMESponsorSettings" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
    }else if(indexPath.row == 6){
        SMEIntroVC *vc = [[SMEIntroVC alloc]initWithNibName:@"SMEIntroVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        
        SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.window.rootViewController = nc;
        [appDelegate.window makeKeyAndVisible];
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    }

@end
