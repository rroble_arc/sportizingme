//
//  SMEFindAthleteVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEFindAthleteVC : UIViewController

- (IBAction)findAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAdvanced;
@property (weak, nonatomic) IBOutlet UIButton *btnFind;
@property (weak, nonatomic) IBOutlet UITextField *txtLocation;
@property (weak, nonatomic) IBOutlet UITextField *txtSports;

@end
