//
//  SMEMyAthleteCell.m
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/16/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEMyAthleteCell.h"
#import "ARCAccessoryDisclosureIndicator.h"

CGFloat const SMEMYATHLETECELL_HEIGHT = 70;

@implementation SMEMyAthleteCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
    self.accessoryView = [ARCAccessoryDisclosureIndicator accessoryWithColor:[UIColor colorWithRed:140.0/255.0 green:198.0/255.0 blue:63.0/255.0 alpha:1.0]];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setupView];
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
  [super setSelected:selected animated:animated];
  
  // Configure the view for the selected state
}

#pragma mark - setupView
- (void)setupView
{
  // load the nib
  [[NSBundle mainBundle] loadNibNamed:@"SMEMyAthleteCell"
                                owner:self options:nil];
  [self.contentView addSubview:self.pView];  
}

@end
