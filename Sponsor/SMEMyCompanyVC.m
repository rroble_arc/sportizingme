//
//  SMEMyCompanyVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEMyCompanyVC.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"
#import "IIViewDeckController.h"
@interface SMEMyCompanyVC ()

@end

@implementation SMEMyCompanyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNav];
    [self setLanguage];
}
#pragma mark - Methods
-(void)fetch{
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"athlete_id", nil];
    NSString * urlString = @"";
    
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        if(error){
            
        }else{
            
        }
    } progressCallback:nil];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    
    [self.lblPresentation setText:[lang sponsorCompanyAd]];
    [self.lblMyAds setText:[lang sponsorCompanyPresentation]];
}
-(void)setupNav{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
