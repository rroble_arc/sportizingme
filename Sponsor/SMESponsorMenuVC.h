//
//  SMESponsorMenuVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMESponsorMenuVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
//!
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAthletes;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellBlank2;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDashboard;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMyAthletes;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSettings;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellBlank;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSignout;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellCompany;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAccount;
//!
@property (weak, nonatomic) IBOutlet UILabel *lblDashboard;
@property (weak, nonatomic) IBOutlet UILabel *lblMyAthletes;

@property (weak, nonatomic) IBOutlet UILabel *lblAthletes;

@property (weak, nonatomic) IBOutlet UILabel *lblAccount;

@property (weak, nonatomic) IBOutlet UILabel *lblCompany;

@property (weak, nonatomic) IBOutlet UILabel *lblSettings;
@property (weak, nonatomic) IBOutlet UILabel *lblSignout;
//

//!
@end
