//
//  SMEAthleteDetailsVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEAthleteDetailsVC.h"
#import "SMESportizeMeVC.h"
#import "SMELanguage.h"
#import "UIImageView+WebCache.h"

@interface SMEAthleteDetailsVC ()

@end

@implementation SMEAthleteDetailsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setLanguage];
    [self setAthlete];
}
-(void)setAthlete{
    [self.imgView setImageWithURL:[NSURL URLWithString:[self.dictAthlete objectForKey:@"link_image"]]];
    NSArray *sports = [self.dictAthlete objectForKey:@"sports"];

    if([sports containsObject:@"GYM"])
        [self.imgGYM setAlpha:1]; else [self.imgGYM setAlpha:.3];
    
    if([sports containsObject:@"RUN"])
        [self.imgRUN setAlpha:1]; else [self.imgRUN setAlpha:.3];
    
    if([sports containsObject:@"CYC"])
        [self.imgCYC setAlpha:1]; else [self.imgCYC setAlpha:.3];
    
    if([sports containsObject:@"MMA"])
        [self.imgMMA setAlpha:1]; else [self.imgMMA setAlpha:.3];
    
    if([sports containsObject:@"SOC"])
        [self.imgSOC setAlpha:1]; else [self.imgSOC setAlpha:.3];
    
    
    self.txtTrain.text = [self.dictAthlete objectForKey:@"training_location"];

}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.btnSportize setTitle:[lang sponsorAthleteDetailsBtnSportize] forState:UIControlStateNormal];
    [self.lblWhere setText:[lang sponsorAthleteDetailsLblWhere]];
    [self.lblSports setText:[lang sponsorAthleteDetailsLblSports]];
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
   
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sportizeMeAction:(id)sender {
    SMESportizeMeVC *smvc = [[SMESportizeMeVC alloc]initWithNibName:@"SMESportizeMeVC" bundle:nil];
    smvc.dictAthlete = self.dictAthlete;
    [self.navigationController pushViewController:smvc animated:YES];
}

@end
