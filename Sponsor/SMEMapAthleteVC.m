//
//  SMEMapAthleteVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEMapAthleteVC.h"
#import "SMEAthleteDetailsVC.h"
#import "SMELanguage.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "SMEMyAthleteCell.h"

@interface SMEMapAthleteVC ()
@property (nonatomic, strong)NSMutableArray *mapAnnotations;
@end

@implementation SMEMapAthleteVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
  // change the line separator color
    [self.TB setSelectedItem:self.mapTB];
    
  UIColor *separatorColor = [UIColor colorWithRed:194.0/255.0 green:224.0/255.0 blue:191.0/255.0 alpha:1.0];
  self.tableView.separatorColor = separatorColor;
  
    //self.tableArray = [NSMutableArray arrayWithObjects:@"list0",@"list1",@"list2", nil];
    [self setLanguage];
    [self setupNavigationBar];
  [self handlerInfiniteScrolling];
    [self refreshAnnotations];

}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_bg"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.btnList setTitle:[lang sponsorMapAthleteBtnList] forState:UIControlStateNormal];
    [self.btnMap setTitle:[lang sponsorMapAthleteBtnMap] forState:UIControlStateNormal];
    
    [self.listTB setTitle:[lang sponsorMapAthleteBtnList]];
    [self.mapTB setTitle:[lang sponsorMapAthleteBtnMap]];
}


#pragma mark - handle infinite scrolling
- (void)handlerInfiniteScrolling
{
  // add the infinite scrolling
  [self.tableView addInfiniteScrollingWithActionHandler:^{
    // append data to data source, insert new cells at the end of table view
    // call [tableView.infiniteScrollingView stopAnimating] when done
    [self performSelector:@selector(increaseTableArray) withObject:nil afterDelay:3.0];
  }];
}

#pragma mark - temp function
- (void)increaseTableArray{
  NSInteger total = self.tableArray.count;
  
  for (NSInteger i=0; i<10; i++) {
    NSString *str = [NSString stringWithFormat:@"list%d",i+total];
    //[self.tableArray addObject:str];
  }
  [self.tableView reloadData];
  [self.tableView.infiniteScrollingView stopAnimating];
}


#pragma mark - 
#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.tableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SMEMyAthleteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil){
        cell = [[SMEMyAthleteCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
  
    //cell.textLabel.text = [self.tableArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = [[self.tableArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSArray *sports = [[self.tableArray objectAtIndex:indexPath.row] objectForKey:@"sports"];
    NSMutableString *sportString =  [NSMutableString string];
    for(int i = 0; i<[sports count];i++)
    {
        [sportString appendString:[NSString stringWithFormat:@" %@",[sports objectAtIndex:i]]];
    }
    cell.sportLabel.text = sportString;
    
    
    return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SMEAthleteDetailsVC *details = [[SMEAthleteDetailsVC alloc]initWithNibName:@"SMEAthleteDetailsVC" bundle:nil];
    details.dictAthlete = [self.tableArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:details animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return SMEMYATHLETECELL_HEIGHT;
}


#pragma mark TabBar Delegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if(item == self.listTB){
        [self.listView setHidden:NO];
        [self.mapListView setHidden:YES];
    }else if(item == self.mapTB){
        [self.listView setHidden:YES];
        [self.mapListView setHidden:NO];
    }
}
#pragma mark - Button Action
- (IBAction)listAction:(id)sender {
    [self.listView setHidden:NO];
    [self.mapListView setHidden:YES];
}

- (IBAction)mapAction:(id)sender {
    [self.listView setHidden:YES];
    [self.mapListView setHidden:NO];
}
#pragma mark - MAP
#pragma mark - MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[SMEAnnotation class]])
    {
        //CDMapAnnotation *an = annotation;
        
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView*    annotationView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        
        if (!annotationView)
        {
            // If an existing pin view was not available, create one.
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                             reuseIdentifier:@"CustomPinAnnotation"];
            annotationView.canShowCallout = YES;
            
            
            // Add a detail disclosure button to the callout.
            UIButton* rightButton = [UIButton buttonWithType:
                                     UIButtonTypeDetailDisclosure];
            [rightButton addTarget:self action:@selector(showAnnotationDetails)
                  forControlEvents:UIControlEventTouchUpInside];
            annotationView.rightCalloutAccessoryView = rightButton;
            
            UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
            [iv setContentMode:UIViewContentModeScaleAspectFit];
            [iv setImage:[UIImage imageNamed:@"Man.png"]];
            annotationView.leftCalloutAccessoryView = iv;
        }
        else
            annotationView.annotation = annotation;
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    //NSLog(@"didSelectAnnotationView");
    NSLog(@"sdfsdf");
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    //NSLog(@"region did change");
}

#pragma mark -
- (void)removeAnnotations{
    NSArray *anns = self.mapView.annotations;
    for (NSInteger i = 0; i < anns.count; i++) {
        if([[anns objectAtIndex:i] isKindOfClass:[MKUserLocation class]]){
            //NSLog(@"user location ni ayaw e remove");
        }
        else{
            [self.mapView removeAnnotation:[anns objectAtIndex:i]];
        }
    }
}

- (void)refreshAnnotations{
    [self removeAnnotations]; // we must remove the previous to avoid duplicates
    NSLog(@"tableArray %@",self.tableArray);
    self.mapAnnotations = [NSMutableArray arrayWithCapacity:self.tableArray.count];
    
    for (NSMutableDictionary *locDict in self.tableArray) {
        NSLog(@"locDict %@",locDict);
        //CLLocationDegrees lat = [[locDict objectForKey:@"latitude"] floatValue];
        CLLocationDegrees lat = 44.698555;
        CLLocationDegrees lon = 8.522644;
        //CLLocationDegrees lon = [[locDict objectForKey:@"longitude"] floatValue];
        NSString *title = [locDict objectForKey:@"name"];
        NSString *subtitle = [locDict objectForKey:@"name"];
        
        SMEAnnotation *ann = [[SMEAnnotation alloc] initWithLatitude:lat
                                                         longitude:lon
                                                             title:title
                                                          subtitle:subtitle];
        ann.dict = locDict;
        // add to the array
        [self.mapAnnotations addObject:ann];
    }
    [self.mapView addAnnotations:self.mapAnnotations];
}

- (void)showAnnotationDetails{
    //NSLog(@"show details");
    SMEAnnotation *ann = (SMEAnnotation*) [[self.mapView selectedAnnotations] lastObject];// we know that this has only 1 value
    SMEAthleteDetailsVC *details = [[SMEAthleteDetailsVC alloc]initWithNibName:@"SMEAthleteDetailsVC" bundle:nil];
    details.dictAthlete = ann.dict;
    [self.navigationController pushViewController:details animated:YES];
    
}
@end
