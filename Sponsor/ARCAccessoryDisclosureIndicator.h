//
//  ARCAccessoryDisclosureIndicator.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/16/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARCAccessoryDisclosureIndicator : UIControl

@property (nonatomic, strong) UIColor *accessoryColor;
@property (nonatomic, strong) UIColor *highlightedColor;

+ (ARCAccessoryDisclosureIndicator *)accessoryWithColor:(UIColor *)color;

@end
