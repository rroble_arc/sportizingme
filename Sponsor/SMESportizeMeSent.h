//
//  SMESportizeMeSent.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMESportizeMeSent : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblAthleteName;
@property (strong, nonatomic) NSMutableDictionary *dictAthlete;
@property (weak, nonatomic) IBOutlet UILabel *lblOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblReview;
@property (weak, nonatomic) IBOutlet UILabel *lblAccept;
@property (weak, nonatomic) IBOutlet UILabel *lblPack;

@end
