//
//  ARCAccessoryDisclosureIndicator.m
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/16/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "ARCAccessoryDisclosureIndicator.h"

@implementation ARCAccessoryDisclosureIndicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


// creating the object
+ (ARCAccessoryDisclosureIndicator *)accessoryWithColor:(UIColor *)color{
  ARCAccessoryDisclosureIndicator *acc = [[ARCAccessoryDisclosureIndicator alloc] initWithFrame:CGRectMake(0, 0, 11.0, 15.0)];
  acc.accessoryColor = color;
  return acc;
  
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
  // Drawing code
  // (x,y) is the tip of the arrow
	CGFloat x = CGRectGetMaxX(self.bounds)-3.0;;
	CGFloat y = CGRectGetMidY(self.bounds);
	const CGFloat R = 4.5;
	CGContextRef ctxt = UIGraphicsGetCurrentContext();
	CGContextMoveToPoint(ctxt, x-R, y-R);
	CGContextAddLineToPoint(ctxt, x, y);
	CGContextAddLineToPoint(ctxt, x-R, y+R);
	CGContextSetLineCap(ctxt, kCGLineCapSquare);
	CGContextSetLineJoin(ctxt, kCGLineJoinMiter);
	CGContextSetLineWidth(ctxt, 3);
  
  if (self.highlighted)
	{
		[self.highlightedColor setStroke];
	}
	else
	{
		[self.accessoryColor setStroke];
	}
  
	CGContextStrokePath(ctxt);
}

- (void)setHighlighted:(BOOL)highlighted
{
	[super setHighlighted:highlighted];
  
	[self setNeedsDisplay];
}

- (UIColor *)accessoryColor
{
	if (!_accessoryColor)
	{
		return [UIColor blackColor];
	}
  
	return _accessoryColor;
}

- (UIColor *)highlightedColor
{
	if (!_highlightedColor)
	{
		return [UIColor whiteColor];
	}
  
	return _highlightedColor;
}

@end
