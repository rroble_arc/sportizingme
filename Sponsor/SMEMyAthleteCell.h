//
//  SMEMyAthleteCell.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/16/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>
extern CGFloat const SMEMYATHLETECELL_HEIGHT;


@interface SMEMyAthleteCell : UITableViewCell

// content parent view
@property (strong, nonatomic) IBOutlet UIView *pView;
@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;


@end
