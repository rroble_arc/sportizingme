//
//  SMESportizeMeVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMESportizeMeVC.h"
#import "SMESportizeMeReviewVC.h"
#import "SMELanguage.h"
@interface SMESportizeMeVC ()
{
    BOOL GYM;
    BOOL RUN;
    BOOL CYC;
    BOOL MMA;
    BOOL SOC;
}
@end

@implementation SMESportizeMeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    GYM = NO;
    RUN = NO;
    CYC = NO;
    MMA = NO;
    SOC = NO;
    
    self.title = [[SMELanguage sharedManager] sponsorSendOfferTitle];
    [self setupNavigationBar];
    [self setLanguage];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.btnReview setTitle:[lang sponsorSportizeMeBtnReview] forState:UIControlStateNormal];
    [self.btnDownload setTitle:[lang sponsorSportizeMeBtnDownload] forState:UIControlStateNormal];
    [self.lblAD setText:[lang sponsorSportizeMeLblAD]];
    [self.lblNumber setText:[lang sponsorSportizeMeNumber]];
    [self.lblSports setText:[lang sponsorSportizeMeSports]];
}

-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_bg"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
    -(void)back{
        [self.navigationController popViewControllerAnimated:YES];
    }
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return self.cellTraining;
    }
    else if(indexPath.row == 1){
        return self.cellSports;
    }else if (indexPath.row ==2){
        return self.cellAddress;
    }
   
    return nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(self.txtNumber.text.length >0){
        [self.lblNumber setHidden:YES];
    }else{
        [self.lblNumber setHidden:NO];
    }
    if(self.txtSports.text.length >0){
        [self.lblSports setHidden:YES];
    }else{
        [self.lblSports setHidden:NO];
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(self.txtNumber.text.length >0){
        [self.lblNumber setHidden:YES];
    }else{
        [self.lblNumber setHidden:NO];
    }
    if(self.txtSports.text.length >0){
        [self.lblSports setHidden:YES];
    }else{
        [self.lblSports setHidden:NO];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)reviewAction:(id)sender {
    NSMutableArray *sportsArray = [NSMutableArray array];
    
    if(GYM) [sportsArray addObject:@"GYM"];
    if(RUN) [sportsArray addObject:@"RUN"];
    if(CYC) [sportsArray addObject:@"CYC"];
    if(MMA) [sportsArray addObject:@"MMA"];
    if(SOC) [sportsArray addObject:@"SOC"];
    
    SMESportizeMeReviewVC *smvc = [[SMESportizeMeReviewVC alloc]initWithNibName:@"SMESportizeMeReviewVC" bundle:nil];
    smvc.dictAthlete = self.dictAthlete;
    smvc.no_of_trainings = self.txtNumber.text;
    smvc.sportsArray = sportsArray;
    smvc.imageAD = self.mainImage;
    [self.navigationController pushViewController:smvc animated:YES];
}

- (IBAction)downloadAction:(id)sender {
    [self startPhotoLibray];
}
- (IBAction)sportsAction:(id)sender {
    if(sender == self.btnGYM){
        if(GYM)GYM=NO;else GYM=YES;
    }else if(sender == self.btnRUN){
        if(RUN)RUN=NO;else RUN=YES;
    }else if(sender == self.btnCYC){
        if(CYC)CYC=NO;else CYC=YES;
    }else if(sender == self.btnMMA){
        if(MMA)MMA=NO;else MMA=YES;
    }else if(sender == self.btnSOC){
        if(SOC)SOC=NO;else SOC=YES;
    }
}

#pragma mark Picture
- (void)startCamera{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        self.imagePicker = [[UIImagePickerController alloc] init];
        [self.imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        self.imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //self.imagePicker.cameraOverlayView = self.overlayView;
        [self.imagePicker setDelegate:self];
        //[self.imagePicker setShowsCameraControls:NO];
        [self presentViewController:self.imagePicker animated:YES completion:^{
            
        }];
        
    }
    else{
        NSLog(@"UIImagePickerControllerSourceTypeCamera unavailable");
        // for testing just show the photo library
        [self startPhotoLibray];
    }
}

- (void)startPhotoLibray{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        [picker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        [picker setDelegate:self];
        
        [self presentViewController:picker
                           animated:YES
                         completion:^{
                             // present complete
                         }];
    }
    else{
        NSLog(@"UIImagePickerControllerSourceTypeSavedPhotosAlbum unavailable");
    }
}
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES
                               completion:^{
                                   
                               }];
    
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    
    [picker dismissViewControllerAnimated:YES
                               completion:^{
                                   // dismiss finish
                               }];
    CGRect visibleRect;
    visibleRect.origin.x = (image.size.height - image.size.width) / 1.5;
    visibleRect.origin.y = 0;
    visibleRect.size.width = image.size.width;
    visibleRect.size.height = image.size.width;
    
    CGImageRef cr = CGImageCreateWithImageInRect([image CGImage], visibleRect);
	
    UIImage *newimage =  [[UIImage alloc] initWithCGImage:cr
                                                    scale: 3.5
                                              orientation:image.imageOrientation];
    
    newimage = [self imageByRotatingImage:newimage fromImageOrientation:newimage.imageOrientation];
    self.mainImage = newimage;
    [self.imgAD setImage:self.mainImage];
  
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(UIImage*)imageByRotatingImage:(UIImage*)initImage fromImageOrientation:(UIImageOrientation)orientation
{
    CGImageRef imgRef = initImage.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = orientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            return initImage;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    // Create the bitmap context
    CGContextRef    context = NULL;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (bounds.size.width * 4);
    bitmapByteCount     = (bitmapBytesPerRow * bounds.size.height);
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        return nil;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    CGColorSpaceRef colorspace = CGImageGetColorSpace(imgRef);
    context = CGBitmapContextCreate (bitmapData,bounds.size.width,bounds.size.height,8,bitmapBytesPerRow,
                                     colorspace,kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorspace);
    
    if (context == NULL)
        // error creating context
        return nil;
    
    CGContextScaleCTM(context, -1.0, -1.0);
    CGContextTranslateCTM(context, -bounds.size.width, -bounds.size.height);
    
    CGContextConcatCTM(context, transform);
    
    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(context, CGRectMake(0,0,width, height), imgRef);
    
    CGImageRef imgRef2 = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    free(bitmapData);
    UIImage * image = [UIImage imageWithCGImage:imgRef2 scale:initImage.scale orientation:UIImageOrientationUp];
    CGImageRelease(imgRef2);
    self.mainImage = image;
    return image;
}

@end
