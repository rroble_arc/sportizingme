//
//  SMEAppDelegate.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/6/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "IIViewDeckController.h"

#import "ARCFetcher.h"

@class SMEViewController;

@interface SMEAppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>{
    //CLLocationManager *locationManager;
    UIBackgroundTaskIdentifier bgTask;
    BOOL fetching;
}
@property (strong, nonatomic) ARCFetcher *fetch;
@property (strong, nonatomic) NSMutableArray *offersArray;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SMEViewController *viewController;

@property (strong, nonatomic) UINavigationController *centerVC;
@property (strong, nonatomic) UIViewController *leftVC;

@property (strong, nonatomic)IIViewDeckController* deckAthlete;
@property (strong, nonatomic)IIViewDeckController* deckSponsor;



-(void)createDeck;
-(void)createDeckSponsor;



@end
