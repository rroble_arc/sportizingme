<?php

namespace Arcanys\SecurityBundle;

use Arcanys\SecurityBundle\DependencyInjection\Security\CompilerPass\AuthenticatorCompilerPass;
use Arcanys\SecurityBundle\DependencyInjection\Security\Factory\WebserviceTokenFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ArcanysSecurityBundle extends Bundle
{

    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new WebserviceTokenFactory());
        
        $container->addCompilerPass(new AuthenticatorCompilerPass());
    }

}
