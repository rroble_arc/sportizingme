<?php

namespace Arcanys\SecurityBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface UserSessionInterface
{

    /**
     * Set user
     *
     * @param UserInterface $user
     * @return UserSessionInterface
     */
    public function setUser(UserInterface $user);

    /**
     * Get user
     *
     * @return UserInterface
     */
    public function getUser();

    /**
     * Set last access date
     *
     * @param \DateTime $lastAccessDate
     * @return UserSessionInterface
     */
    public function setLastAccessDate(\DateTime $lastAccessDate);

    /**
     * Get last access date
     *
     * @return \DateTime 
     */
    public function getLastAccessDate();

    /**
     * Set token
     *
     * @param string $token
     * @return UserSessionInterface
     */
    public function setToken($token);

    /**
     * Get token
     * 
     * @return string 
     */
    public function getToken();

    /**
     * Get plain token
     * 
     * @return string
     */
    public function getPlainToken();

    /**
     * Set plain token
     * 
     * @param string $plainToken
     * @return UserSessionInterface
     */
    public function setPlainToken($plainToken);
}
