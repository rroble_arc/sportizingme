<?php

namespace Arcanys\SecurityBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface UserInterface extends BaseUserInterface
{

    /**
     * @param UserSessionInterface $session
     * @return UserInterface
     */
    public function addSession(UserSessionInterface $session);

    /**
     * @return array Session tokens
     */
    public function getSessions();

    /**
     * @param array $sessions
     */
    public function setSessions($sessions);

    /**
     * @return UserSessionInterface
     */
    public function getSession();
}
