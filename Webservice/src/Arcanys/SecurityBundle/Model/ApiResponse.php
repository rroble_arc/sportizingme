<?php

namespace Arcanys\SecurityBundle\Model;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ApiResponse extends JsonResponse
{

    public static function error($message = 'error', array $data = array())
    {
        $result = array(
            'error' => true,
            'message' => $message,
            'data' => $data,
        );
        return new static($result, 500);
    }

    public static function success($message = 'success', array $data = array())
    {
        if (is_array($message)) {
            $data = $message;
            $message = 'success';
        }
        $result = array(
            'error' => false,
            'message' => $message,
            'data' => $data,
        );
        return new static($result);
    }

}
