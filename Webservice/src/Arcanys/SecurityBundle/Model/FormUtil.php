<?php

namespace Arcanys\SecurityBundle\Model;

use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class FormUtil
{

    /**
     * @var Form
     */
    protected $form;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Form $form = null, Request $request = null, Logger $logger = null)
    {
        $this->form = $form;
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * @return Form
     */
    public function getForm()
    {
        return $this->form;
    }

    public function setForm(Form $form)
    {
        $this->form = $form;
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return FormUtil
     */
    public static function with(Form $form, Request $request, Logger $logger)
    {
        return new static($form, $request, $logger);
    }

    public function getFormErrors()
    {
        $errors = null;
        if ($this->form) {
            $errors = $this->getErrors($this->form);
            if ($this->logger) {
                $this->logger->info('Form errors: ' . var_export($errors, true), array($this));
                $extra = $this->form->getExtraData();
                $this->logger->info('Extra data (' . count($extra) . '): ' . var_export($extra, true), array($this));
                if ($this->request) {
                    $request = array(
                        'attributes' => $this->request->attributes->all(),
                        'query' => $this->request->query->all(),
                        'request' => $this->request->request->all(),
                    );
                    $this->logger->info('Request : ' . var_export($request, true), array($this));
                }
            }
        }
        return ApiResponse::error('Invalid form fields', $errors);
    }

    public function getErrors(Form $form)
    {
        $errors = array();
        // field errors
        foreach ($form->all() as $name => $child) {
            if ($child->all()) {
                $childErrors = static::getErrors($child);
            } else {
                $childErrors = array();
                foreach ($child->getErrors() as $error) {
                    $childErrors[] = $error->getMessage();
                }
            }
            if ($childErrors) {
                $errors[$name] = $childErrors;
            }
        }
        // form errors
        $otherErrors = array();
        foreach ($form->getErrors() as $error) {
            $otherErrors[] = $error->getMessage();
        }
        if ($otherErrors) {
            $otherErrors['extra'] = $form->getExtraData();
            $errors['form'] = $otherErrors;
        }

        if ($errors) {
            $errors = array('fields' => $errors);
        }
        return $errors;
    }

}
