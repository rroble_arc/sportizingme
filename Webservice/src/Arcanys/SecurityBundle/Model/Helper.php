<?php

namespace Arcanys\SecurityBundle\Model;

use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
abstract class Helper
{

    /**
     * @param integer $length
     * @return string
     */
    public static function randomString($length = 10)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

    public static function encodePassword(EncoderFactory $factory, $plain, $class, $salt = null)
    {
        if (is_object($class)) $class = get_class($class);
        if (is_object($salt)) $salt = get_class($salt);
        $encoder = $factory->getEncoder($class);
        $encoded = $encoder->encodePassword($plain, $salt);
        return $encoded;
    }

}
