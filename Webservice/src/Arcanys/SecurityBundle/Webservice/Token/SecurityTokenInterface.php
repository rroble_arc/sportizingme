<?php

namespace Arcanys\SecurityBundle\Webservice\Token;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface SecurityTokenInterface extends TokenInterface
{
    
}
