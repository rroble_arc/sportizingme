<?php

namespace Arcanys\SecurityBundle\Webservice\Token;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface BasicLoginTokenInterface extends SecurityTokenInterface
{
}
