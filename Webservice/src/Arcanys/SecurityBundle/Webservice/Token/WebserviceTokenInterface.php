<?php

namespace Arcanys\SecurityBundle\Webservice\Token;

use Arcanys\SecurityBundle\Model\UserSessionInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface WebserviceTokenInterface extends SecurityTokenInterface
{

    /**
     * @return string
     */
    public function getToken();

    /**
     * @param string $token
     */
    public function setToken($token);

    /**
     * @return array
     */
    public function toArray();

    /**
     * @return UserSessionInterface
     */
    public function getSession();

    /**
     * @param UserSessionInterface $session
     */
    public function setSession(UserSessionInterface $session = null);
}
