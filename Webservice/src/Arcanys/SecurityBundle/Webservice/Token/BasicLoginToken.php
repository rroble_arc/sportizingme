<?php

namespace Arcanys\SecurityBundle\Webservice\Token;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class BasicLoginToken extends AbstractToken implements BasicLoginTokenInterface
{

    protected $credentials;

    public function __construct(Request $request, $usernameKey = 'username', $passwordKey = 'password')
    {
        $this->credentials = $request->get($passwordKey);
        $user = $request->get($usernameKey);
        if ($user) $this->setUser($user);
    }

    public function getCredentials()
    {
        return $this->credentials;
    }

}
