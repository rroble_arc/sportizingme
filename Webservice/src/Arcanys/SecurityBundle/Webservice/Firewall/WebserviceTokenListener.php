<?php

namespace Arcanys\SecurityBundle\Webservice\Firewall;

use Arcanys\SecurityBundle\Webservice\Authentication\Authenticator\AuthenticatorInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class WebserviceTokenListener implements ListenerInterface
{

    const LAST_ERROR_MESSAGE = '_LAST_LOGIN_MESSAGE';

    protected $securityContext;
    protected $authenticationManager;
    protected $tokenKey;
    protected $authenticator;

    public function __construct(SecurityContextInterface $securityContext, 
            AuthenticationManagerInterface $authenticationManager, 
            AuthenticatorInterface $authenticator)
    {
        $this->securityContext = $securityContext;
        $this->authenticationManager = $authenticationManager;
        $this->authenticator = $authenticator;
    }

    /**
     * {@inheritDoc}
     */
    public function handle(GetResponseEvent $event)
    {
        if ($this->authenticator->isTokenAuthenticationNeeded()) {
            $token = $this->authenticator->createWebserviceToken();
            $this->authenticate($token);
        }
        
        if ($this->authenticator->isBasicAuthenticationNeeded()) {
            $token = $this->authenticator->createBasicToken();
            $this->authenticate($token);
        }
    }
    
    private function authenticate(TokenInterface $token)
    {
        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->securityContext->setToken($authToken);
        } catch (AuthenticationException $failed) {
            // silencio or log ?
        }
    }

}
