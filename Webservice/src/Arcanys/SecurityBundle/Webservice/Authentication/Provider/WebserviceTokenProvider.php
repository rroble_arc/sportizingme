<?php

namespace Arcanys\SecurityBundle\Webservice\Authentication\Provider;

use Arcanys\SecurityBundle\Webservice\Authentication\Authenticator\AuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class WebserviceTokenProvider implements AuthenticationProviderInterface
{

    /**
     * @var AuthenticatorInterface
     */
    protected $authenticator;

    public function __construct(AuthenticatorInterface $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    /**
     * {@inheritDoc}
     */
    public function authenticate(TokenInterface $token)
    {
        return $this->authenticator->authenticate($token);
    }

    /**
     * @param TokenInterface $token
     * @return boolean
     */
    public function supports(TokenInterface $token)
    {
        return $this->authenticator->supports($token);
    }

}
