<?php

namespace Arcanys\SecurityBundle\Webservice\Authentication\Authenticator;

use Arcanys\SecurityBundle\Webservice\Token\BasicLoginToken;
use Arcanys\SecurityBundle\Webservice\Token\BasicLoginTokenInterface;
use Arcanys\SecurityBundle\Webservice\Token\SecurityTokenInterface;
use Arcanys\SecurityBundle\Webservice\Token\WebserviceToken;
use Arcanys\SecurityBundle\Webservice\Token\WebserviceTokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class DefaultAuthenticator implements AuthenticatorInterface
{

    /**
     * @var EncoderFactoryInterface
     */
    protected $encoderFactory;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $usernameKey;

    /**
     * @var string
     */
    protected $passwordKey;

    /**
     * @var string
     */
    protected $tokenKey;

    /**
     * @var integer
     */
    protected $lifetime;

    public function __construct(EncoderFactoryInterface $encoderFactory, Request $request, $usernameKey, $passwordKey, $tokenKey, $lifetime)
    {
        $this->encoderFactory = $encoderFactory;
        $this->request = $request;
        $this->usernameKey = $usernameKey;
        $this->passwordKey = $passwordKey;
        $this->tokenKey = $tokenKey;
        $this->lifetime = $lifetime; // in seconds
    }

    /**
     * @inheritDoc
     */
    public function isBasicAuthenticationNeeded()
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function isTokenAuthenticationNeeded()
    {
        return null !== $this->getToken();
    }

    /**
     * @inheritDoc
     */
    public function getToken()
    {
        return $this->request->get($this->tokenKey);
    }

    public function authenticate(TokenInterface $token)
    {
        if ($token instanceof BasicLoginTokenInterface) {
            return $this->authenticateBasic($token);
        } else if ($token instanceof WebserviceTokenInterface) {
            return $this->authenticateToken($token);
        }

        throw new AuthenticationException('The token authentication failed.');
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof SecurityTokenInterface;
    }

    public function authenticateBasic(BasicLoginTokenInterface $token)
    {
        throw new AuthenticationException('Please implement your basic authentication.');
    }

    public function authenticateToken(WebserviceTokenInterface $token)
    {
        throw new AuthenticationException('Please implement your token authentication.');
    }

    public function createBasicToken()
    {
        return new BasicLoginToken($this->request, $this->usernameKey, $this->passwordKey);
    }

    public function createWebserviceToken()
    {
        return new WebserviceToken($this->getToken());
    }

}
