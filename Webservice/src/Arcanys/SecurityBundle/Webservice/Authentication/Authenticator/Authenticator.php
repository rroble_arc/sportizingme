<?php

namespace Arcanys\SecurityBundle\Webservice\Authentication\Authenticator;

use Arcanys\SecurityBundle\Webservice\Token\BasicLoginTokenInterface;
use Arcanys\SecurityBundle\Webservice\Token\WebserviceTokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class Authenticator implements AuthenticatorInterface
{

    /**
     * @var array
     */
    protected $authenticators;

    /**
     * @var string
     */
    protected $authenticator;

    public function __construct()
    {
        $this->authenticators = array();
    }

    public function addAuthenticator(AuthenticatorInterface $auth, $alias)
    {
        $this->authenticators[$alias] = $auth;
    }

    /**
     * @return AuthenticatorInterface
     */
    private function getAuthenticator()
    {
        if (isset($this->authenticators[$this->authenticator])) {
            return $this->authenticators[$this->authenticator];
        }
    }

    public function isBasicAuthenticationNeeded()
    {
        foreach ($this->authenticators as $alias => $auth) {
            if ($auth->isBasicAuthenticationNeeded()) {
                $this->authenticator = $alias;
                return true;
            }
        }
    }

    public function createBasicToken()
    {
        if ($this->getAuthenticator()) {
            return $this->getAuthenticator()->createBasicToken();
        }
    }

    public function authenticateBasic(BasicLoginTokenInterface $token)
    {
        if ($this->getAuthenticator()) {
            return $this->getAuthenticator()->authenticateBasic($token);
        }
    }

    public function isTokenAuthenticationNeeded()
    {
        foreach ($this->authenticators as $alias => $auth) {
            if ($auth->isTokenAuthenticationNeeded()) {
                $this->authenticator = $alias;
                return true;
            }
        }
    }

    public function createWebserviceToken()
    {
        if ($this->getAuthenticator()) {
            return $this->getAuthenticator()->createWebserviceToken();
        }
    }

    public function authenticateToken(WebserviceTokenInterface $token)
    {
        if ($this->getAuthenticator()) {
            return $this->getAuthenticator()->authenticateToken($token);
        }
    }

    public function getToken()
    {
        if ($this->getAuthenticator()) {
            return $this->getAuthenticator()->getToken();
        }
    }

    public function authenticate(TokenInterface $token)
    {
        if ($this->getAuthenticator()) {
            return $this->getAuthenticator()->authenticate($token);
        }
    }

    public function supports(TokenInterface $token)
    {
        if ($this->getAuthenticator()) {
            return $this->getAuthenticator()->supports($token);
        }
    }

}
