<?php

namespace Arcanys\SecurityBundle\Webservice\Authentication\Authenticator;

use Arcanys\SecurityBundle\Webservice\Token\BasicLoginTokenInterface;
use Arcanys\SecurityBundle\Webservice\Token\WebserviceTokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface AuthenticatorInterface
{

    /**
     * @param TokenInterface $token
     */
    public function authenticate(TokenInterface $token);

    /**
     * @param TokenInterface $token
     * @return boolean
     */
    public function supports(TokenInterface $token);

    /**
     * @return BasicLoginTokenInterface
     */
    public function createBasicToken();

    /**
     * @return WebserviceTokenInterface
     */
    public function createWebserviceToken();

    /**
     * Checks if there is a need to authenticate basic.
     * 
     * @return boolean
     */
    public function isBasicAuthenticationNeeded();

    /**
     * Checks if there is a need to authenticate token.
     * 
     * @return boolean
     */
    public function isTokenAuthenticationNeeded();

    /**
     * @return string
     */
    public function getToken();

    /**
     * @param BasicLoginTokenInterface $token
     */
    public function authenticateBasic(BasicLoginTokenInterface $token);

    /**
     * @param WebserviceTokenInterface $token
     */
    public function authenticateToken(WebserviceTokenInterface $token);
}
