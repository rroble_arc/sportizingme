<?php

namespace Arcanys\SecurityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ArcanysSecurityExtension extends Extension
{

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $container->setParameter('token_login.security.authentication.listener.username_key', $config['username_key']);
        $container->setParameter('token_login.security.authentication.listener.password_key', $config['password_key']);
        $container->setParameter('token_login.security.authentication.listener.token_key', $config['token_key']);
        $container->setParameter('token_login.security.authentication.listener.token_lifetime', $config['token_lifetime']);
    }

}
