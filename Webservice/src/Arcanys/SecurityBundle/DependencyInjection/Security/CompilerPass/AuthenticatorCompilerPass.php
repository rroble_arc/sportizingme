<?php

namespace Arcanys\SecurityBundle\DependencyInjection\Security\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AuthenticatorCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('arcanys_security.authenticator')) {
            return;
        }

        $definition = $container->getDefinition('arcanys_security.authenticator');

        $taggedServices = $container->findTaggedServiceIds('arcanys_security.authenticator');
        foreach ($taggedServices as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                $definition->addMethodCall(
                        'addAuthenticator', array(new Reference($id), $attributes['alias'])
                );
            }
        }
    }

}
