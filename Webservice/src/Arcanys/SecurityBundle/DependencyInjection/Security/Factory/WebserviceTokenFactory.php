<?php

namespace Arcanys\SecurityBundle\DependencyInjection\Security\Factory;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class WebserviceTokenFactory implements SecurityFactoryInterface
{

    /**
     * {@inheritDoc}
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.token_login.' . $id;
        $container->setDefinition($providerId, new DefinitionDecorator('token_login.security.authentication.provider'));

        $listenerId = 'security.authentication.listener.token_login.' . $id;
        $container->setDefinition($listenerId, new DefinitionDecorator('token_login.security.authentication.listener'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * {@inheritDoc}
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * {@inheritDoc}
     */
    public function getKey()
    {
        return 'token_login';
    }

    /**
     * {@inheritDoc}
     */
    public function addConfiguration(NodeDefinition $node)
    {
        
    }

}
