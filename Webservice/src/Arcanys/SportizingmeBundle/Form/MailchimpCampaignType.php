<?php

namespace Arcanys\SportizingmeBundle\Form;

use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\Cache;
use Doctrine\Common\Cache\PhpFileCache;
use Hype\MailchimpBundle\Mailchimp\MailChimp;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class MailchimpCampaignType extends AbstractType
{

    /**
     * @var MailChimp
     */
    protected $mailchimp;
    
    /**
     * @var Cache
     */
    private $cache;
    
    /**
     * @var float
     */
    private $cacheLifetime;

    public function __construct(MailChimp $mailchimp, $cacheDir, $cacheLifetime /* in minutes */)
    {
        $this->mailchimp = $mailchimp;
        if (function_exists('apc_fetch')) {
            $this->cache = new ApcCache();
        } else {
            if (!file_exists($cacheDir)) {
                mkdir($cacheDir, 0777, true);
            }
            $this->cache = new PhpFileCache($cacheDir);
        }
        $this->cacheLifetime = $cacheLifetime * 60*60; // to seconds
    }

    public function getParent()
    {
        return 'choice';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $choices = $this->getCampaigns();
        $resolver->setDefaults(array('choices' => $choices));
    }

    public function getName()
    {
        return 'mailchimp_campaign_type';
    }
    
    private function getCampaigns()
    {
        $campaigns = $this->cache->fetch($ccachekey = 'mailchimp_campaigns');
        if (!$campaigns) {
            $campaigns = array();
            $campaignData = $this->mailchimp->getCampaign()->get();
            $data = (array) $campaignData['data'];
            foreach ($data as $campaign) {
                $campaigns['campaign-'.$campaign['id']] = $campaign['title'];
            }
            $this->cache->save($ccachekey, $campaigns, $this->cacheLifetime);
        }
        
        $result = array('Campaigns' => $campaigns);
        
        $templates = $this->cache->fetch($tcachekey = 'mailchimp_templates');
        if (!$templates) {
            $templates = array();
            $templateData = $this->mailchimp->getTemplate()->listAll(array(
                'user' => true,
                'gallery' => true,
            ));
            foreach ((array) $templateData['user'] as $customTemplate) {
                $templates['Custom Templates']['template-'.$customTemplate['id'].'-user'] = $customTemplate['name'];
            }
            foreach ((array) $templateData['gallery'] as $galleryTemplate) {
                $templates['MailChimp Templates']['template-'.$galleryTemplate['id'].'-gallery'] = $galleryTemplate['name'];
            }
            $this->cache->save($tcachekey, $templates, $this->cacheLifetime);
        }
        return array_merge($result, $templates);
    }

}
