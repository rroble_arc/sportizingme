<?php

namespace Arcanys\SportizingmeBundle\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AthleteFilterType extends AbstractType
{

    /**
     * @var Request
     */
    protected $request;
    
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function __construct(Request $request, ManagerRegistry $registry)
    {
        $this->request = $request;
        $this->manager = $registry->getManager();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = array();
        foreach ($this->request->request->all() as $data)
            break;
        $filters = (array) @$data['filters'];
        $country = $this->getCountry(@$filters['country']);
        $region = $this->getRegion(@$filters['region']);
        $city = $this->getCity(@$filters['city']);
        $gym = $this->getGym(@$filters['gym']);
        
        $builder
                ->add('country', 'entity', array(
                    'class' => 'ArcanysSportizingmeBundle:Worlddb\Country',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'All',
                        'class' => 'span8 edit-country',
                    ),
                ))
                ->add('region', 'entity', array(
                    'class' => 'ArcanysSportizingmeBundle:Worlddb\Region',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'All',
                        'class' => 'span8 edit-region',
                    ),
                    'query_builder' => function(EntityRepository $er) use($country) {
                        $qb = $er->createQueryBuilder('r')->orderBy('r.name')->setMaxResults(0);
                        if ($country) {
                            $qb->where($qb->expr()->eq('r.country', $qb->expr()->literal($country->getCode())))
                                    ->setMaxResults(500);
                        }
                        return $qb;
                    },
                ))
                ->add('city', 'entity', array(
                    'class' => 'ArcanysSportizingmeBundle:Worlddb\City',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'All',
                        'class' => 'span8 edit-city',
                    ),
                    'query_builder' => function(EntityRepository $er) use($region) {
                        $qb = $er->createQueryBuilder('c')->orderBy('c.name')->setMaxResults(0);
                        if ($region) {
                            $qb->where($qb->expr()->eq('c.region', $qb->expr()->literal($region->getCode())))
                                    ->setMaxResults(500);
                        }
                        return $qb;
                    },
                ))
                ->add('gym', 'entity', array(
                    'class' => 'ArcanysSportizingmeBundle:Gym',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'All',
                        'class' => 'span8 edit-gym',
                    ),
                    'query_builder' => function(EntityRepository $er) use($country, $region, $city) {
                        $qb = $er->createQueryBuilder('g')->leftJoin('g.address', 'a')->orderBy('g.name');
                        if ($city) {
                            $qb->where($qb->expr()->eq('a.city', $qb->expr()->literal($city->getName())));
                        } else if ($region) {
                            $qb->where($qb->expr()->eq('a.state', $qb->expr()->literal($region->getName())));
                        } else if ($country) {
                            $qb->where($qb->expr()->eq('a.country', $qb->expr()->literal($country->getName())));
                        }
                        return $qb;
                    },
                ))
                ->add('athletes', 'entity', array(
                    'class' => 'ArcanysSportizingmeBundle:User\Athlete',
                    'multiple' => true,
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'All',
                        'class' => 'span8 edit-athletes',
                    ),
                    'query_builder' => function(EntityRepository $er) use($country, $region, $city, $gym) {
                        $qb = $er->createQueryBuilder('a')->orderBy('a.firstname')->addOrderBy('a.lastname')
                                ->leftJoin('a.address', 'aa')
                                ->leftJoin('a.trainingLocation', 't')
                                ->leftJoin('t.address', 'ta');
                        if ($gym) {
                            $qb->where($qb->expr()->eq('t.id', $gym->getId()));
                        } else if ($city) {
                            $name = $qb->expr()->literal($city->getName());
                            $qb->where($qb->expr()->eq('aa.city', $name))
                               ->orWhere($qb->expr()->eq('ta.city', $name));
                        } else if ($region) {
                            $name = $qb->expr()->literal($region->getName());
                            $qb->where($qb->expr()->eq('aa.state', $name))
                               ->orWhere($qb->expr()->eq('ta.state', $name));
                        } else if ($country) {
                            $name = $qb->expr()->literal($country->getName());
                            $qb->where($qb->expr()->eq('aa.country', $name))
                               ->orWhere($qb->expr()->eq('ta.country', $name));
                        }
                        return $qb;
                    },
                ))
                ->add('submit', 'hidden', array(
                    'attr' => array('class' => 'fake-submit'),
                ))
        ;
        }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Arcanys\SportizingmeBundle\Model\AthleteFilter',
        ));
    }

    public function getName()
    {
        return 'athlete_filter_type';
    }

    protected function getCountry($id)
    {
        if ($id) {
            return $this->manager->getRepository('ArcanysSportizingmeBundle:Worlddb\Country')
                    ->find($id);
        }
    }

    protected function getRegion($id)
    {
        if ($id) {
            return $this->manager->getRepository('ArcanysSportizingmeBundle:Worlddb\Region')
                    ->find($id);
        }
    }

    protected function getCity($id)
    {
        if ($id) {
            return $this->manager->getRepository('ArcanysSportizingmeBundle:Worlddb\City')
                    ->find($id);
        }
    }

    protected function getGym($id)
    {
        if ($id) {
            return $this->manager->getRepository('ArcanysSportizingmeBundle:Gym')
                    ->find($id);
        }
    }
    
}
