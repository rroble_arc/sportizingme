jQuery(function($) {
    
    // activate datepicker
    $(".datepicker").datepicker({
        format: "dd/mm/yyyy"
    });
    
    // go back to the history
    $(".abort-back").on("click", function() {
        history.back();
        return false;
    });
    
    // reload/redirect when pagination (per page) is changed
    $("#pagination-per-page").on("change", function() {
        var url = $(this).val();
        location.replace(url);
    });
    
    // Goto the first action button when a row is double-clicked.
    $("#panel-main td").on("dblclick", function() {
        var tr = $(this).parent(), td = $("td:last", tr);
        var link = $("a.action-button:first", td).attr("href");
        if (link != undefined) {
            location.href = link;
        }
    });
    
    // activate tooltips on action button
    $("#panel-main .action-button").tooltip();

});
