<?php

namespace Arcanys\SportizingmeBundle\Model;

use ArrayIterator;
use Countable;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;
use IteratorAggregate;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class Pagination implements IteratorAggregate, Countable
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var QueryBuilder
     */
    protected $qb;
    
    protected $limit = 10;
    protected $offset = 0;
    protected $page = 1;
    protected $pages = 0;

    public function __construct(Request $request, QueryBuilder $qb)
    {
        $this->request = $request;
        $this->qb = $qb;
        $this->initRequest();
        $this->initCounter();
        $this->qb->setFirstResult($this->offset);
        $this->qb->setMaxResults($this->limit);
    }
    
    protected function initRequest()
    {
        $this->limit = $this->request->get('limit', 10);
        $this->page = $this->request->get('page', 1);
        $currentPage = intval($this->page) < 1 ? 1 : intval($this->page);
        $this->offset = ($currentPage - 1) * $this->limit;
    }
    
    protected function initCounter()
    {
        $qb = clone $this->qb;
        $alias = null;
        foreach ($qb->getDQLPart('from') as $from) {
            $alias = $from->getAlias();break;
        }
        $qb->select("COUNT({$alias})");
        $total = $qb->getQuery()->getSingleResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
        $this->pages = (int)ceil($total/$this->limit);
    }

    public function getLimit()
    {
        return $this->limit;
    }
    
    public function getOffset()
    {
        return $this->offset;
    }
    
    public function getIterator()
    {
        return new ArrayIterator($this->paginate());
    }

    public function count()
    {
        return count($this->paginate());
    }
    
    public function paginate($limit = 10)
    {
        if ($limit <= 0) $limit = 10;
        
        $pages = array();
        $mid = (int) ceil($limit/2);
        // left
        while ($mid-->0) {
            $page = $this->page - $mid;
            if ($page > 0) $pages[] = $page;
        }
        // right
        while (!isset($pages[$limit-1])) {
            $next = ++$page;
            if ($next > $this->pages) break;
            $pages[] = $next;
        }
        // left (filler)
        $first = isset($pages[0]) ? $pages[0] : null;
        while (!isset($pages[$limit-1])) {
            $pre = --$first;
            if ($pre <= 0) break;
            array_unshift($pages, $pre);
        }
        
        return $pages;
    }
    
    public function getPages()
    {
        return $this->pages;
    }

    public function getRoute()
    {
        return $this->request->get('_route');
    }

    public function getParams($mergeQuery = true)
    {
        $params = $this->request->get('_route_params');
        if ($mergeQuery) {
            $params = array_merge($params, $this->request->query->all());
        }
        return $params;
    }
    
    public function getPage()
    {
        return $this->page;
    }
    
    public function isFirst()
    {
        return $this->page == 1;
    }
    
    public function isLast()
    {
        return $this->page == $this->pages;
    }
    
    public function getNext()
    {
        return $this->page + 1;
    }
    
    public function getPrevious()
    {
        return $this->page - 1;
    }
    
    public function getResult()
    {
        return $this->qb->getQuery()->getResult();
    }
        
}
