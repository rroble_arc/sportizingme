<?php

namespace Arcanys\SportizingmeBundle\Model;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ApiResponse
{

    protected $error;
    protected $message;
    protected $data;

    public function __construct($error, $message = null, array $data = array())
    {
        $this->error = (bool) $error;
        $this->message = (string) $message;
        $this->data = $data;
    }

    public function build()
    {
        return new JsonResponse(array(
            'error' => $this->error,
            'message' => $this->message,
            'data' => $this->data,
        ));
    }
    
    public static function error($message = 'error', array $data = array(), $build = true)
    {
        $response = new static(true, $message, $data);
        return $build ? $response->build() : $response;
    }

    public static function success($message = 'success', array $data = array(), $build = true)
    {
        if (is_array($message)) {
            $data = $message;
            $message = 'success';
        }
        $response = new static(false, $message, $data);
        return $build ? $response->build() : $response;
    }
    
}
