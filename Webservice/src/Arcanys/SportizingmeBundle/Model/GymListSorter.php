<?php

namespace Arcanys\SportizingmeBundle\Model;

use Arcanys\SportizingmeBundle\Entity\Gym;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GymListSorter implements LocationInterface
{

    protected $longitude;
    protected $latitude;

    public function __construct($longitude, $latitude)
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    public function getLongitude()
    {
        return (float) $this->longitude;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = (float) $longitude;
    }

    public function getLatitude()
    {
        return (float) $this->latitude;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = (float) $latitude;
    }
    
    public function sort(Gym $gym1, Gym $gym2)
    {
        $dist1 = $gym1->getDistance($this);
        $dist2 = $gym2->getDistance($this);
        if ($dist1 == $dist2) {
            return 0;
        }
        return ($dist1 < $dist2) ? -1 : 1;
    }

}
