<?php

namespace Arcanys\SportizingmeBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface LocationInterface
{

    /**
     * @return float
     */
    public function getLatitude();

    /**
     * @return float
     */
    public function getLongitude();
}
