<?php

namespace Arcanys\SportizingmeBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
abstract class Helper
{

    public static function getHashtags($hashtags, $process = false, $prefix = null)
    {
        if ($process) {
            $tags = str_replace(array('#', '@'), array(' ', ' '), $hashtags);
            $arr1 = explode(' ', $tags);
            $arr2 = array_filter($arr1, function($str) {
                        return trim($str) != '';
                    });
            $arr3 = array_map(function($str) use($prefix) {
                        if ($prefix) {
                            return $prefix . $str;
                        }
                        return '#' . $str . ' @' . $str;
                    }, $arr2);
            return implode(' ', $arr3);
        }
        return $hashtags;
    }

}
