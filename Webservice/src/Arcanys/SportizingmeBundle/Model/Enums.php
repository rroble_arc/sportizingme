<?php

namespace Arcanys\SportizingmeBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
abstract class Enums
{

    const SPORT_GYM = 'GYM',
            SPORT_RUN = 'RUN',
            SPORT_CYCLE = 'CYC',
            SPORT_MMA = 'MMA',
            SPORT_SOCCER = 'SOC';
    const WEIGHT_POUNDS = 'lbs',
            WEIGHT_KILO = 'kg';
    const HEIGHT_CM = 'cm',
            HEIGHT_INCHES = 'in';
    const STATUS_PENDING = 'pending',
            STATUS_ACCEPTED = 'accepted',
            STATUS_DECLINED = 'declined',
            STATUS_DONE = 'done';
    
    const DEAL_BASE_RATE = 3; // in $

    const TYPE_CHECKIN = 'checkin',
            TYPE_CHECKOUT = 'checkout',
            TYPE_GYM = 'gym',
            TYPE_SPORTIZING = 'sportizing';
    
    public static function getFeedTypes()
    {
        $array = array(
            static::TYPE_CHECKIN,
            static::TYPE_CHECKOUT,
            static::TYPE_GYM,
            static::TYPE_SPORTIZING,
        );
        return array_combine($array, $array);
    }

    /**
     * @return array
     */
    public static function getSports()
    {
        $array = array(
            static::SPORT_GYM,
            static::SPORT_RUN,
            static::SPORT_CYCLE,
            static::SPORT_MMA,
            static::SPORT_SOCCER,
        );
        return array_combine($array, $array);
    }

    /**
     * @return array
     */
    public static function getWeightUnits()
    {
        $array = array(
            static::WEIGHT_POUNDS,
            static::WEIGHT_KILO,
        );
        return array_combine($array, $array);
    }

    /**
     * @return array
     */
    public static function getHeightUnits()
    {
        $array = array(
            static::HEIGHT_CM,
            static::HEIGHT_INCHES,
        );
        return array_combine($array, $array);
    }

    /**
     * @return array
     */
    public static function getShirtSizes()
    {
        $array = array('SM', 'S', 'M', 'L', 'XL', 'XXL');
        return array_combine($array, $array);
    }

    public static function getStatusList()
    {
        $array = array(
            static::STATUS_ACCEPTED,
            static::STATUS_DECLINED,
            static::STATUS_PENDING,
            static::STATUS_DONE,
        );
        return array_combine($array, $array);
    }

    /**
     * @return array
     */
    public static function getGenders()
    {
        $array = array('M', 'F');
        return array_combine($array, $array);
    }
    
    public static function predefinedEmailTemplates()
    {
        $list = array(
            'Athlete registration',
            'Forgot password',
            'Reset password',
            'Sponsorship offer',
            'Change password',
        );
        return array_combine($list, $list);
    }

}
