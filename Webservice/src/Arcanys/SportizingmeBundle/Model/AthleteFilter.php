<?php

namespace Arcanys\SportizingmeBundle\Model;

use Arcanys\SportizingmeBundle\Entity\Gym;
use Arcanys\SportizingmeBundle\Entity\Worlddb\City;
use Arcanys\SportizingmeBundle\Entity\Worlddb\Country;
use Arcanys\SportizingmeBundle\Entity\Worlddb\Region;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AthleteFilter
{

    /**
     * @var Country
     */
    protected $country;

    /**
     * @var Region
     */
    protected $region;

    /**
     * @var City
     */
    protected $city;

    /**
     * @var Gym
     */
    protected $gym;

    /**
     * @var array
     */
    protected $athletes;

    /**
     * @var string
     */
    protected $submit;

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(Country $country = null)
    {
        $this->country = $country;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion(Region $region = null)
    {
        $this->region = $region;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity(City $city = null)
    {
        $this->city = $city;
    }

    public function getGym()
    {
        return $this->gym;
    }

    public function setGym(Gym $gym = null)
    {
        $this->gym = $gym;
    }

    public function getAthletes()
    {
        return $this->athletes;
    }

    public function setAthletes($athletes)
    {
        $this->athletes = $athletes;
    }

    public function getSubmit()
    {
        return $this->submit;
    }

    public function setSubmit($submit)
    {
        $this->submit = $submit;
    }

}
