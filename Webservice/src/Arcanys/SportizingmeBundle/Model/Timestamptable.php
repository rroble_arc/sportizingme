<?php

namespace Arcanys\SportizingmeBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface Timestamptable
{

    /**
     * @return \DateTime
     */
    public function getDateCreated();

    /**
     * @param \DateTime $dateUpdated
     */
    public function setDateCreated(\DateTime $dateCreated);

    /**
     * @return \DateTime
     */
    public function getDateUpdated();

    /**
     * @param \DateTime $dateUpdated
     */
    public function setDateUpdated(\DateTime $dateUpdated = null);
}
