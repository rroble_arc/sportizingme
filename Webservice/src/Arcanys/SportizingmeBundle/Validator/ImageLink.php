<?php

namespace Arcanys\SportizingmeBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Annotation
 */
class ImageLink extends Constraint
{

    public $message = 'The URL "%string%" is not a valid image.';
    
    public function validatedBy()
    {
        return 'image_link';
    }

}
