<?php

namespace Arcanys\SportizingmeBundle\Validator;

use Buzz\Browser;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImageLinkValidator extends ConstraintValidator
{

    /**
     * @var Browser
     */
    protected $browser;

    public function __construct(Browser $browser)
    {
        $this->browser = $browser;
        $this->browser->getClient()->setVerifyPeer(false);
    }

    public function validate($value, Constraint $constraint)
    {
        $valid = false;
        $result = $this->browser->head($value);
        if ($result->isSuccessful()) {
            $type = $result->getHeader('Content-Type');
            $valid = strpos($type, 'image') !== false;
        }
        if (!$valid) {
            $this->context->addViolation($constraint->message, array('%string%' => $value));
        }
    }

}
