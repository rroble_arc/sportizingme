<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Arcanys\SportizingmeBundle\Model\Enums;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AthleteAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias() . '.trainingLocation', 'trainingLocation');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Profile')
                ->add('firstname')
                ->add('lastname')
                ->add('email')
                ->add('dateOfBirth', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'class' => 'datepicker',
                        'data-date-format' => 'dd/mm/yyyy',
                    ),
                ))
                ->add('password', 'repeated', array(
                    'required' => $this->getSubject()->getId() === null,
                    'type' => 'password',
                    'first_options' => array(
                        'label' => 'Password',
                        'always_empty' => true,
                        'attr' => array(
                            'class' => 'span5',
                            'autocomplete' => 'off',
                    )),
                    'second_options' => array(
                        'label' => 'Confirm password',
                        'always_empty' => true,
                        'attr' => array(
                            'class' => 'span5',
                            'autocomplete' => 'off',
                    )),
                ))
                ->add('gender', 'choice', array('choices' => Enums::getGenders()))
                ->add('personalIncome')
                ->add('school', 'sonata_type_model', array(
                    'btn_add' => 'New School',
                    'required' => false,
                ))
                ->add('company', 'sonata_type_model', array(
                    'btn_add' => 'New Company',
                    'required' => false,
                ))
                ->add('maxEducationLevel')
                ->add('paypal')
            ->end()
                
            ->with('Photos')
                ->add('coverPhoto', 'image_type', array(
                    'required' => false,
                ))
                ->add('profilePicture', 'image_type', array(
                    'required' => false,
                ))
            ->end()
                
            ->with('Athlete')
                ->add('shortPresentation')
                ->add('sports', 'choice', array(
                    'choices' => Enums::getSports(),
                    'multiple' => true,
                ))
                ->add('trainingLocation', 'sonata_type_model', array(
                    'btn_add' => 'New Training Location',
                    'empty_value' => '',
                ))
                ->add('trainingFrequency')
                ->add('weight')
                ->add('weightUnit', 'choice', array('choices' => Enums::getWeightUnits()))
                ->add('height')
                ->add('heightUnit', 'choice', array('choices' => Enums::getHeightUnits()))
                ->add('trainingShirtSize', 'choice', array('choices' => Enums::getShirtSizes()))
            ->end()
                
            ->with('Address')
                ->add('address', 'sonata_type_admin', array(
                    'delete' => false,
                    'btn_add' => false,
                    'required' => false,
                ))
            ->end()
                
            ->with('Facebook')
                ->add('facebookId', null, array('required' => false))
                ->add('facebookLink')
                ->add('facebookEmail')
                ->add('facebookFriends')
                ->add('facebookPhoto', 'image_type', array(
                    'required' => false,
                ))
            ->end()
                
            ->with('Twitter')
                ->add('twitterLink')
                ->add('twitterUsername')
                ->add('twitterFollowers')
                ->add('twitterPhoto', 'image_type', array(
                    'required' => false,
                ))
            ->end()
                
            ->with('Earnings')
                ->add('cash', null, array(
                    'attr' => array('disabled' => 'disabled')
                ))
            ->end()
        ;

        $formMapper->setHelps(array(
            'twitterPhoto' => $this->preview('twitterPhoto'),
            'facebookPhoto' => $this->preview('facebookPhoto'),
            'profilePicture' => $this->preview('profilePicture'),
            'coverPhoto' => $this->preview('coverPhoto'),
        ));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('firstname')
                ->add('lastname')
                ->add('shortPresentation')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('firstname')
                ->add('lastname')
                ->add('trainingLocation.name', null, array('label' => 'Training Location'))
                ->add('trainingFrequency')
                ->add('cash')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(
//                            'template' => 'ArcanysSportizingmeBundle:Admin:athlete_show.html.twig',
                        ),
                    )
                ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Athlete')
                ->add('name')
                ->add('gender')
                ->add('dateOfBirth')
                ->add('shortPresentation')
                ->add('personalIncome')
                ->add('maxEducationLevel')
                ->add('sportsStr', null, array('label' => 'Sports'))
                ->add('trainingLocation')
                ->add('trainingFrequency')
                ->add('weightStr', null, array('label' => 'Weight'))
                ->add('heightStr', null, array('label' => 'Height'))
                ->add('trainingShirtSize')
                ->add('email')
                ->add('paypal')
                ->add('cash')
            ->end()
        ;
    }

}
