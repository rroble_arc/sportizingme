<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GymUserAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name')
                ->add('email')
                ->add('password', 'repeated', array(
                    'required' => $this->getSubject()->getId() === null,
                    'type' => 'password',
                    'first_options' => array(
                        'label' => 'Password',
                        'always_empty' => true,
                        'attr' => array(
                            'class' => 'span5',
                            'autocomplete' => 'off',
                    )),
                    'second_options' => array(
                        'label' => 'Confirm password',
                        'always_empty' => true,
                        'attr' => array(
                            'class' => 'span5',
                            'autocomplete' => 'off',
                    )),
                ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('email')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('email')
        ;
    }

}
