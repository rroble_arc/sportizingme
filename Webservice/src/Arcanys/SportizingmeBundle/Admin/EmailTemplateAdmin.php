<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Arcanys\SportizingmeBundle\Model\Enums;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class EmailTemplateAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Email Template')
                ->add('name', 'choice', array(
                    'choices' => Enums::predefinedEmailTemplates(),
                    'translation_domain' => 'Sportizingme',
                    'empty_value' => '',
                ))
                ->add('subject', 'text')
                ->add('campaign', 'mailchimp_campaign_type', array(
                    'required' => false,
                    'empty_value' => 'None',
                    'attr' => array(
                        'class' => 'edit-campaign',
                    ),
                    'label' => 'MailChimp Template',
                ))
                ->add('htmlContent', null, array(
                    'attr' => array(
                        'rows' => '20',
                        'class' => 'span11 editor edit-html-content',
                    ),
                ))
                ->add('plainTextContent', null, array(
                    'attr' => array(
                        'rows' => '15',
                        'class' => 'span12 edit-plain-text-content',
                    ),
                ))
                ->add('submit', 'hidden', array('attr' => array('class' => 'fake-submit')))
            ->end()
            
            ->with('Help', array('translation_domain' => 'Sportizingme'))
                ->add('help', 'text', array(
                    'mapped' => false, 
                    'required' => false,
                    'attr' => array('class' => 'hidden'),
                    'label' => ' ',
                ))
            ->end()
        ;
        $formMapper->setHelps(array(
            'help' => 'email_template.help'
        ));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('subject')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('subject')
        ;
    }
    
    public function fakeSubmission()
    {
        $this->formOptions['validation_groups'] = 'fake_submit';
    }

}
