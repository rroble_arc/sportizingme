<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class NotificationAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Notification')
                ->add('athlete')
                ->add('message')
                ->add('content')
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('athlete')
                ->add('message')
                ->add('content')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('athlete')
                ->add('message')
                ->add('content')
                ->add('dateCreated', null, array('label' => 'Date'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(
                        ),
                    )
                ))
        ;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create')
                ->remove('edit');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Notification')
                ->add('athlete')
                ->add('message')
                ->add('content')
                ->add('dateCreated', null, array('label' => 'Date'))
            ->end()
        ;
    }


}
