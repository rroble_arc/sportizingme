<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImageAdmin extends Admin
{
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Image')
                ->add('link', 'url', array('required' => false))
                ->add('file', 'file', array(
                    'required' => false,
                    'data_class' => null,
                ))
                ->add('type', 'hidden')
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('link')
                ->add('file')
                ->add('type')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('link')
                ->add('file')
                ->add('type')
        ;
    }

}
