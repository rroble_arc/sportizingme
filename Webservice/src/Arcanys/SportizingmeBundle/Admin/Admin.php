<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as BaseAdmin;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
abstract class Admin extends BaseAdmin
{

    protected function preview($field)
    {
        $getter = 'get' . ucfirst($field);
        $subject = $this->getSubject();
        if (method_exists($subject, $getter)) {
            $image = call_user_func(array($subject, $getter));
            if ($image && ($link = $image->getLink())) {
                return '<img src="' . $link . '" class="admin-preview img-polaroid offset2" width="200" />';
            }
        }
    }

}
