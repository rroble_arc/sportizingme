<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class WorkoutAdmin extends Admin
{
    
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.athlete', 'athlete');
        $query->leftJoin($query->getRootAlias().'.gym', 'gym');
        $query->leftJoin($query->getRootAlias().'.offer', 'offer');
        $query->leftJoin('offer.sponsor', 'sponsor');
        $query->leftJoin('sponsor.company', 'company');
        return $query;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Workout')
                ->add('athlete')
                ->add('gym')
                ->add('offer')
                ->add('caption')
                ->add('start', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy hh:mm',
                    'attr' => array(
                        'data-date-format' => 'dd/mm/yyyy',
                        'class' => 'datepicker',
                    ),
                ))
                ->add('end', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy hh:mm',
                    'attr' => array(
                        'data-date-format' => 'dd/mm/yyyy',
                        'class' => 'datepicker',
                    ),
                    'required' => false,
                ))
                ->add('earning', null, array(
                    'attr' => array('disabled' => 'disabled')
                ))
            ->end()
            ->with('Picture')
                ->add('picture', 'image_type', array(
                    'required' => false,
                ))
            ->end()
        ;
        
        $formMapper->setHelps(array(
            'picture' => $this->preview('picture'),
        ));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('athlete')
                ->add('gym')
                ->add('offer')
                ->add('caption')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('athlete.firstname', null, array('label' => 'Athlete'))
                ->add('gym.name', null, array('label' => 'Gym'))
                ->add('offer.sponsor.company.name', null, array('label' => 'Sponsor'))
                ->add('caption')
                ->add('start')
                ->add('end')
                ->add('earning')
        ;
    }

}
