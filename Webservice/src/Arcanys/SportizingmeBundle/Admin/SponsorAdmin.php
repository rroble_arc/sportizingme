<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SponsorAdmin extends Admin
{
    
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.company', 'company');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('User')
                ->add('email')
                ->add('password', 'repeated', array(
                    'required' => $this->getSubject()->getId() === null,
                    'type' => 'password',
                    'first_options' => array(
                        'label' => 'Password',
                        'always_empty' => true,
                        'attr' => array(
                            'class' => 'span5',
                            'autocomplete' => 'off',
                    )),
                    'second_options' => array(
                        'label' => 'Confirm password',
                        'always_empty' => true,
                        'attr' => array(
                            'class' => 'span5',
                            'autocomplete' => 'off',
                    )),
                ))
            ->end()
            ->with('Sponsor')
                ->add('company', 'sonata_type_admin', array(
                    'delete' => false,
                    'btn_add' => false,
                ))
                ->add('paypal')
                ->add('hashtags')
            ->end()
            ->with('Address')
                ->add('address', 'sonata_type_admin', array(
                    'delete' => false,
                    'btn_add' => false,
                ))
            ->end()
            ->with('Logo')
                ->add('logo', 'image_type', array(
                    'required' => false,
                ))
            ->end()
        ;
        
        $formMapper->setHelps(array(
            'logo' => $this->preview('logo'),
        ));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('email')
                ->add('company')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('company.name', null, array('label' => 'Name'))
                ->add('email')
                ->add('paypal')
        ;
    }

}
