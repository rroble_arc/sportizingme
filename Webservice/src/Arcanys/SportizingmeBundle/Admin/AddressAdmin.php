<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AddressAdmin extends Admin
{
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Address')
                ->add('street')
                ->add('number')
                ->add('city')
                ->add('state')
                ->add('country')
                ->add('zipcode')
                ->add('latitude', null, array('attr' => array('class' => 'edit-latitude')))
                ->add('longitude', null, array('attr' => array('class' => 'edit-longitude')))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('streetName')
                ->add('streetNumber')
                ->add('city')
                ->add('state')
                ->add('country')
                ->add('postalCode')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('streetName')
                ->add('streetNumber')
                ->add('city')
                ->add('state')
                ->add('country')
                ->add('postalCode')
        ;
    }

}
