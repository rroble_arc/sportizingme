<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class OfferAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.sponsor', 'sponsor');
        $query->leftJoin($query->getRootAlias().'.athlete', 'athlete');
        $query->leftJoin('sponsor.company', 'company');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId() === null) {
            $formMapper
                ->with('Sponsorship Offer')
                    ->add('filters', 'athlete_filter_type', array(
                        'required' => false,
                        'attr' => array('class' => 'athlete-filters span7'),
                    ))
                    ->add('title', null, array(
                        'attr' => array('class' => 'span7'),
                    ))
                    ->add('message', null, array(
                        'attr' => array(
                            'rows' => 20,
                            'class' => 'span7',
                        ),
                    ))
                ->end();
        } else {
            $formMapper
                ->with('Sponsorship Offer')
                    ->add('athlete', null, array('required' => true))
                    ->add('title')
                    ->add('message', null, array(
                        'attr' => array(
                            'rows' => 20,
                            'class' => 'span7',
                        )))
                ->end();
        }
        $formMapper
            ->with('Details')
                ->add('sponsor')
                ->add('startDate', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'class' => 'datepicker',
                        'data-date-format' => 'dd/mm/yyyy',
                    ),
                ))
                ->add('endDate', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'data-date-format' => 'dd/mm/yyyy',
                        'class' => 'datepicker'
                    ),
                ))
                ->add('trainingsPerWeek')
                ->add('numberOfWeeks')
                ->add('hoursPerSession')
                ->add('payment')
                ->add('rate')
            ->end()
            ->with('Pictures')
                ->add('ads', 'image_type', array(
                    'label' => 'Workout photo',
                ))
                ->add('front', 'image_type', array(
                    'label' => 'Front (Shirt)',
                ))
                ->add('back', 'image_type', array(
                    'label' => 'Back (Shirt)',
                ))
            ->end()
        ;
        
        $formMapper->setHelps(array(
            'ads' => $this->preview('ads'),
            'front' => $this->preview('front'),
            'back' => $this->preview('back'),
        ));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('sponsor')
                ->add('athlete')
                ->add('status')
                ->add('trainingsPerWeek')
                ->add('numberOfWeeks')
                ->add('hoursPerSession')
                ->add('payment')
                ->add('rate')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('sponsor.company.name', null, array('label' => 'Sponsor'))
                ->add('athlete.firstname', null, array('label' => 'Athlete'))
                ->add('status')
                ->add('trainingsPerWeek')
                ->add('numberOfWeeks')
                ->add('hoursPerSession')
                ->add('payment')
                ->add('rate')
        ;
    }
    
    public function fakeSubmission()
    {
        $this->formOptions['validation_groups'] = 'fake_submit';
    }

}
