<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Arcanys\SportizingmeBundle\Model\Enums;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class FeedAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.athlete', 'athlete');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId() === null) {
            $formMapper
                ->with('Feed')
                    ->add('filters', 'athlete_filter_type', array(
                        'required' => false,
                        'attr' => array('class' => 'athlete-filters span7'),
                    ))
                    ->add('message', null, array(
                        'attr' => array(
                            'rows' => 20,
                            'class' => 'span7',
                        ),
                    ))
                ->end();
        } else {
            $formMapper
                ->with('Feed')
                    ->add('athlete', null, array('required' => true))
                    ->add('message', null, array(
                        'attr' => array('rows' => 20),
                    ))
                ->end()
            ;
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('athlete')
                ->add('title', 'doctrine_orm_string', array('label' => 'Type'), 'choice', array(
                    'choices' => Enums::getFeedTypes(),
                ))
                ->add('message')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('athlete.firstname', null, array('label' => 'Athlete'))
                ->add('message')
        ;
    }
    
    public function fakeSubmission()
    {
        $this->formOptions['validation_groups'] = 'fake_submit';
    }

}
