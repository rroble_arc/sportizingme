<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class MessageAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.athlete', 'athlete');
        $query->where($query->getRootAlias().' INSTANCE OF Arcanys\SportizingmeBundle\Entity\Message\Message');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId() === null) {
            $formMapper
                ->with('Message')
                    ->add('filters', 'athlete_filter_type', array(
                        'required' => false,
                        'attr' => array('class' => 'athlete-filters span7'),
                    ))
                    ->add('title', null, array(
                        'attr' => array('class' => 'span7'),
                    ))
                    ->add('message', null, array(
                        'attr' => array(
                            'rows' => 20,
                            'class' => 'span7',
                        ),
                    ))
                ->end();
        } else {
            $formMapper
                ->with('Message')
                    ->add('athlete', null, array('required' => true))
                    ->add('title')
                    ->add('message', null, array(
                        'attr' => array('rows' => 20),
                    ))
                ->end();
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('athlete')
                ->add('title')
                ->add('message')
                ->add('read')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('athlete.firstname', null, array('label' => 'Athlete'))
                ->add('title')
                ->add('message')
                ->add('read')
        ;
    }
    
    public function fakeSubmission()
    {
        $this->formOptions['validation_groups'] = 'fake_submit';
    }

}
