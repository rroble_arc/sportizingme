<?php

namespace Arcanys\SportizingmeBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GymAdmin extends Admin
{
    
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.address', 'address');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Gym')
                ->add('name')
                ->add('hashtags')
                ->add('approved')
            ->end()
                
            ->with('Photo')
                ->add('image', 'image_type', array(
                    'required' => false,
                ))
            ->end()
                
            ->with('Address')
                ->add('address', 'sonata_type_admin', array(
                    'delete' => false,
                    'btn_add' => false,
                ))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('address')
                ->add('hashtags')
                ->add('approved')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('address.street', null, array('label' => 'Address'))
                ->add('hashtags')
                ->add('approved')
        ;
    }

}
