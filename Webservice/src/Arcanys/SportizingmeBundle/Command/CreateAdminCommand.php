<?php

namespace Arcanys\SportizingmeBundle\Command;

use Arcanys\SportizingmeBundle\Entity\User\Admin;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class CreateAdminCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('sport:admin:create');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $em ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $admin = new Admin();
        $admin->setName('super_admin');
        $admin->setEmail('super_admin');
        $admin->setPassword('welcome');
        $admin->addRole(Admin::ROLE_SUPER_ADMIN);
        
        $em->persist($admin);
        $em->flush();
        
        $output->writeln('Admin has been created successfully!');
    }

}
