<?php

namespace Arcanys\SportizingmeBundle\Command;

use Arcanys\SportizingmeBundle\Entity\Email\Template;
use Arcanys\SportizingmeBundle\Model\Enums;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class UpdateEmailTemplatesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('sport:emails:update')
        ;
    }
    
    private function getTemplate($name, $repo)
    {
        $template = $repo->findOneByName($name);
        if (!$template) {
            $template = new Template();
        }
        return $template;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container = $this->getContainer();
        /* @var $em ObjectManager */
        $em = $container->get('doctrine')->getManager();
        $repo = $em->getRepository('ArcanysSportizingmeBundle:Email\Template');
        $translator = $container->get('translator');

        $templates = Enums::predefinedEmailTemplates();
        foreach ($templates as $name) {
            $template = $this->getTemplate($name, $repo);
            $template->setName($name)
                    ->setSubject($translator->trans($name.'.subject', array(), 'mails'))
                    ->setHtmlContent($translator->trans($name.'.body', array(), 'mails'));
            $em->persist($template);
        }
        
        $em->flush();
        $output->writeln('Updated email templates!');
    }

}
