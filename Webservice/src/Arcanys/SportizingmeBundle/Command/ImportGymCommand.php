<?php

namespace Arcanys\SportizingmeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImportGymCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('sport:gym:import')
                ->setDescription('Import gyms')
                ->addOption('truncate', 't', InputOption::VALUE_NONE, 'Set this parameter to truncate gym table')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        if ($input->getOption('truncate')) {
            $output->writeln('Removing existing gyms...');
            $gyms = $em->getRepository('ArcanysSportizingmeBundle:Gym')->findAll();
            foreach ($gyms as $gym) {
                $em->remove($gym);
            }
        }
        
        $importer = new GymImporter($em);
        $bundle = $this->getApplication()->getKernel()->getBundle('ArcanysSportizingmeBundle');
        $output->writeln('Importing gyms...');
        $count = $importer->import($bundle->getPath() . '/Resources/data/gyms');
        $output->writeln(sprintf('Gyms imported sucessfully! "%s" gyms were imported', $count));
    }

}
