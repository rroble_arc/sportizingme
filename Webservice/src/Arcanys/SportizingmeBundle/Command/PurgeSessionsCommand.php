<?php

namespace Arcanys\SportizingmeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class PurgeSessionsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('sport:purge:sessions')
                ->setDescription('Delete expired session tokens')
                ->addArgument('lifetime', InputOption::VALUE_REQUIRED, 'Set the session lifetime', 86400) // default 24hrs
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lifetime = $input->getArgument('lifetime');

        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $sessions = $em->getRepository('ArcanysSportizingmeBundle:User\Session')
                ->findAll();
        $count = 0;
        foreach ($sessions as $session) {
            if ($session->getToken() == 'expired') {
                $em->remove($session);
                $count++;
            }
            else {
                $interval = new \DateInterval('PT' . $lifetime . 'S');
                $date = $session->getLastAccessDate()->add($interval);
                if ($date < new \DateTime()) {
                    $em->remove($session);
                    $count++;
                }
            }
        }

        $em->flush();
        $output->writeln(sprintf('Sessions purged sucessfully! "%s" session tokens were purged', $count));
    }

}
