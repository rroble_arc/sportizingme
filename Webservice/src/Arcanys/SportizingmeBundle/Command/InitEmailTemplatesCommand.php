<?php

namespace Arcanys\SportizingmeBundle\Command;

use Arcanys\SportizingmeBundle\Entity\Email\Template;
use Arcanys\SportizingmeBundle\Model\Enums;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class InitEmailTemplatesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('sport:emails:init')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container = $this->getContainer();
        /* @var $em ObjectManager */
        $em = $container->get('doctrine')->getManager();
        $translator = $container->get('translator');

        $mails = Enums::predefinedEmailTemplates();
        foreach ($mails as $mail) {
            $template = new Template();
            $template->setName($mail)
                    ->setSubject($translator->trans($mail.'.subject', array(), 'mails'))
                    ->setHtmlContent($translator->trans($mail.'.body', array(), 'mails'));
            $em->persist($template);
        }
        
        $em->flush();
        $output->writeln('Initialized email templates!');
    }

}
