<?php

namespace Arcanys\SportizingmeBundle\Command;

use Arcanys\SportizingmeBundle\Entity\Address;
use Arcanys\SportizingmeBundle\Entity\Gym;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Finder\Finder;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GymImporter
{

    /**
     * @var ObjectManager
     */
    protected $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }
    
    private function findGym($name)
    {
        $gym = $this->em->getRepository('ArcanysSportizingmeBundle:Gym')
                ->findOneBy(array('name' => $name));
        if ($gym) {
            return $gym;
        }
        $gym = new Gym();
        $gym->setName($name);
        return $gym;
    }

    public function import($path)
    {
        $finder = new Finder();
        $finder->files()->in($path);

        $count = 0;
        foreach ($finder as $file) {
            echo basename($file);
            $data = $this->readCsv($file);
            echo ' - ',count($data), PHP_EOL;
            foreach ($data as $row) {
                $gym = $this->findGym($row[0]);
                $address = $gym->getAddress();
                if (!$address) {
                    $address = new Address();
                    $gym->setAddress($address);
                }
                $address->setNumber($row[1]);
                $address->setStreet($row[2]);
                $address->setZipcode($row[3]);
                $address->setCity($row[4]);
                $address->setState($row[5]);
                $address->setCountry($row[6]);
                $address->setLatitude($row[7]);
                $address->setLongitude($row[8]);
                $gym->setHashtags($row[9]);
                $gym->setApproved(true);
                
                $this->em->persist($gym);
                $count++;
            }
        }

        $this->em->flush();
        return $count;
    }

    protected function readCsv($csv)
    {
        $rows = array();
        $ignoreFirstLine = true;
        $ignoreEmpty = true;
        if (($handle = fopen($csv->getRealPath(), "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) {
                    continue;
                }
                if ($ignoreEmpty && trim(implode($data)) == '') {
                    continue;
                }
                $rows[] = array_map('trim', $data);
            }
            fclose($handle);
        }
        return $rows;
    }

}
