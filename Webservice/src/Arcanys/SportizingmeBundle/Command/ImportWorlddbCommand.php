<?php

namespace Arcanys\SportizingmeBundle\Command;

use Arcanys\SportizingmeBundle\Entity\Worlddb\City;
use Arcanys\SportizingmeBundle\Entity\Worlddb\Country;
use Arcanys\SportizingmeBundle\Entity\Worlddb\Region;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImportWorlddbCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('sport:worlddb:import')
                ->setDescription('Import worlddb')
                ->addOption('truncate', 't', InputOption::VALUE_OPTIONAL, 'Set this parameter to truncate gym table', true)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        if ($input->getOption('truncate')) {
            $output->writeln('Removing existing worlddb data...');

            $connection = $em->getConnection();
            $platform = $connection->getDatabasePlatform();
            foreach (array('worlddb_country', 'worlddb_region', 'worlddb_city') as $name) {
                $connection->executeUpdate($platform->getTruncateTableSQL($name, true));
            }
            $em->flush();
        }
        $bundle = $this->getApplication()->getKernel()->getBundle('ArcanysSportizingmeBundle');
        $base = $bundle->getPath() . '/Resources/data/worlddb/';

        $output->writeln('Importing countries...');
        $countries = $this->readCsv($base.'countries.csv');
        foreach ($countries as $row) {
            $country = new Country();
            $country->setCode($row[0]);
            $country->setName($row[1]);
            $country->setLatitude($row[2]);
            $country->setLongitude($row[3]);
            $em->persist($country);
        }
        $em->flush();
        $output->writeln(sprintf('"%s" countries were imported', count($countries)));
        
        $output->writeln('Importing regions...');
        $regions = $this->readCsv($base.'regions.csv');
        foreach ($regions as $row) {
            $region = new Region();
            $region->setCountry($row[0]);
            $region->setCode($row[1]);
            $region->setName($row[2]);
            $region->setLatitude($row[3]);
            $region->setLongitude($row[4]);
            $em->persist($region);
        }
        $em->flush();
        $output->writeln(sprintf('"%s" regions were imported', count($regions)));
        
        ini_set('memory_limit', '512M');
        $output->writeln('Importing cities...');
        $cities = $this->readCsv($base.'cities.csv');
        foreach ($cities as $row) {
            $city = new City();
            $city->setCountry($row[0]);
            $city->setRegion($row[1]);
            $city->setName($row[2]);
            $city->setLatitude($row[3]);
            $city->setLongitude($row[4]);
            $em->persist($city);
        }
        $em->flush();
        $output->writeln(sprintf('"%s" cities were imported', count($cities)));
    }

    protected function readCsv($csvfile, $ignoreFirstLine = false)
    {
        $rows = array();
        $ignoreEmpty = true;
        if (($handle = fopen($csvfile, "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle)) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) {
                    continue;
                }
                if ($ignoreEmpty && trim(implode($data)) == '') {
                    continue;
                }
                $rows[] = array_map('trim', $data);
            }
            fclose($handle);
        }
        return $rows;
    }

}
