<?php

namespace Arcanys\SportizingmeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AutoCheckoutCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('sport:workout:autocheckout');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $workouts = $em->getRepository('ArcanysSportizingmeBundle:Workout')
                ->findOpenSessions('-2hours');
        foreach ($workouts as $workout) {
            $workout->setEnd(new \DateTime());
            $em->persist($workout);
        }
        $em->flush();
        $output->writeln(sprintf('Auto-checkout sucessful! "%s" open workouts were auto-checked out', count($workouts)));
    }

}
