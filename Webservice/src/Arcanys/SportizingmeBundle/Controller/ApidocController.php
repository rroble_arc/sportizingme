<?php

namespace Arcanys\SportizingmeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Apidoc controller.
 *
 * @Route("/apidoc")
 */
class ApidocController extends Controller
{

    /**
     * @Route("/{id}", name="api_doc", defaults={"id":null})
     * @Template()
     */
    public function apiAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $respository = $em->getRepository('ArcanysSportizingmeBundle:Apidoc');
        if ($id) {
            $doc = $respository->find($id);
        } else {
            $doc = $respository->findOneBy(array());
        }
        $entities = $respository->findBy(array(), array('name' => 'ASC'));

        return array(
            'docs' => $entities,
            'current' => $doc,
        );
    }

}
