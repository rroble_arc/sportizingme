<?php

namespace Arcanys\SportizingmeBundle\Controller;

use Arcanys\SportizingmeBundle\Entity\Message\Feed;
use Arcanys\SportizingmeBundle\Model\AthleteFilter;
use Arcanys\SportizingmeBundle\Model\Enums;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException as AccessDeniedException2;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class MessageCRUDController extends CRUDController
{

    /**
     * return the Response object associated to the create action
     *
     * @throws AccessDeniedException2
     * @return Response
     */
    public function createAction()
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }
        
        $object = $this->admin->getNewInstance();
        $object->setFilters(new AthleteFilter());
        
        $this->admin->setSubject($object);
        
        $request = $this->getRequest();
        $requestKey = $this->admin->getUniqid();
        $data = $request->request->get($requestKey);
        $fake = !empty($data['filters']['submit']);
        $data['filters']['submit'] = null;
        $request->request->set($requestKey, $data);
        if ($fake) {
            $this->admin->fakeSubmission();
        }

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->getRestMethod() == 'POST') {
            $form->bind($this->get('request'));
            
            /* @var $filter AthleteFilter */
            $filter = $object->getFilters();
            $athletes = $filter->getAthletes();
            if (count($athletes) == 0) {
                $em = $this->getDoctrine()->getManager();
                $qb = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')->createQueryBuilder('a')
                        ->leftJoin('a.address', 'aa')
                        ->leftJoin('a.trainingLocation', 't')
                        ->leftJoin('t.address', 'ta');
                if ($filter->getGym()) {
                    $qb->where($qb->expr()->eq('t.id', $filter->getGym()->getId()));
                } else if ($filter->getCity()) {
                    $name = $qb->expr()->literal($filter->getCity()->getName());
                    $qb->where($qb->expr()->eq('aa.city', $name))
                       ->orWhere($qb->expr()->eq('ta.city', $name));
                } else if ($filter->getRegion()) {
                    $name = $qb->expr()->literal($filter->getRegion()->getName());
                    $qb->where($qb->expr()->eq('aa.state', $name))
                       ->orWhere($qb->expr()->eq('ta.state', $name));
                } else if ($filter->getCountry()) {
                    $name = $qb->expr()->literal($filter->getCountry()->getName());
                    $qb->where($qb->expr()->eq('aa.country', $name))
                       ->orWhere($qb->expr()->eq('ta.country', $name));
                }
                $athletes = $qb->getQuery()->getResult();
                $filter->setAthletes($athletes);
            }
            if ($filter->getGym()) {
                $object->setGym($filter->getGym());
                if ($object instanceof Feed) {
                    $object->setTitle(Enums::TYPE_GYM);
                }
            }

            $isFormValid = $fake ? true : $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if (!$fake && $isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                if (count($athletes) == 0) {
                    $isFormValid = false;
                    $error = new FormError('No athletes found');
                    $form->get('filters')->get('athletes')->addError($error);
                } else {
                    foreach ($athletes as $athlete) {
                        $object = clone $object;
                        $object->setAthlete($athlete);
                        $this->admin->create($object);
                    }

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                                    'result' => 'ok',
                                    'objectId' => $this->admin->getNormalizedIdentifier($object)
                        ));
                    }

                    $this->addFlash('sonata_flash_success', 'flash_create_success');
                    $this->addFlash('sonata_flash_success', 'Successfully sent to '.count($athletes).' athlete/s.');
                    
                    // redirect to edit mode
                    return $this->redirectTo($object);
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', 'flash_create_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
                    'action' => 'create',
                    'form' => $view,
                    'object' => $object,
        ));
    }

}
