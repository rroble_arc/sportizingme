<?php

namespace Arcanys\SportizingmeBundle\Controller;

use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\Cache;
use Doctrine\Common\Cache\PhpFileCache;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException as AccessDeniedException2;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class EmailTemplateCRUDController extends CRUDController
{
    
    
    /**
     * @var Cache
     */
    private $cache;
    
    /**
     * @var float
     */
    private $cacheLifetime;

    public function __construct()
    {
        if (function_exists('apc_fetch')) {
            $this->cache = new ApcCache();
        }
        $this->cacheLifetime = /* 1 minute */ 1 * 60*60; // to seconds
    }
    
    public function getCache()
    {
        if (!$this->cache) {
            $cacheDir = $this->container->getParameter('kernel.root_dir').'/cache/'.
                $this->container->getParameter('kernel.environment').'/mailchimpcache';
            if (!file_exists($cacheDir)) {
                mkdir($cacheDir, 0777, true);
            }
            $this->cache = new PhpFileCache($cacheDir);
        }
        return $this->cache;
    }

    /**
     * return the Response object associated to the create action
     *
     * @throws AccessDeniedException2
     * @return Response
     */
    public function createAction()
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }
        
        $object = $this->admin->getNewInstance();
        $this->admin->setSubject($object);
        
        $fake = $this->isFakeSubmit();

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);
        
        if ($this->getRestMethod() == 'POST') {
            $form->bind($this->get('request'));
            
            $isFormValid = $fake ? true : $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if (!$fake && $isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $this->admin->create($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                $this->addFlash('sonata_flash_success','flash_create_success');
                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', 'flash_create_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
                    'action' => 'create',
                    'form' => $view,
                    'object' => $object,
        ));
    }
    
    private function isFakeSubmit()
    {
        $request = $this->getRequest();
        $requestKey = $this->admin->getUniqid();
        $data = $request->request->get($requestKey);
        $fake = !empty($data['submit']);
        $data['submit'] = null;
        if ($fake) {
            $this->admin->fakeSubmission();
            if (($cid = $data['campaign'])) {
                $content = $this->getCampaignContent($cid);
                $data['htmlContent'] = $content['html'];
                $data['plainTextContent'] = $content['text'];
            }
        }
        $request->request->set($requestKey, $data);
        return $fake;
    }

    public function editAction($id = null)
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);
        
        $fake = $this->isFakeSubmit();

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->getRestMethod() == 'POST') {
            $form->bind($this->get('request'));
            
            $isFormValid = $fake ? true : $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if (!$fake && $isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $this->admin->update($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result'    => 'ok',
                        'objectId'  => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                $this->addFlash('sonata_flash_success', 'flash_edit_success');

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form'   => $view,
            'object' => $object,
        ));
    }
    
    private function getCampaignContent($cid)
    {
        /* @var $mailchimp MailChimp */
        $mailchimp = $this->get('hype_mailchimp');
        list($templateType,$id,) = explode('-', $cid);
        if ($templateType == 'campaign') {
            $campaignContent = $this->getCache()->fetch($cachekey = 'mailchimp_campaign_'.$id);
            if (!$campaignContent) {
                $campaignContent = $mailchimp->getCampaign()->setCi($id)->content();
                $this->getCache()->save($cachekey, $campaignContent, $this->cacheLifetime);
            }
            return $campaignContent;
        } else if ($templateType == 'template') {
            list(,,$type) = explode('-', $cid);
            $templateContent = $this->getCache()->fetch($cachekey = 'mailchimp_template_'.$type.'_'.$id);
            if (!$templateContent) {
                $temp = $mailchimp->getTemplate()->setTemplateId($id)->info($type);
                $templateContent = array(
                    'html' => $temp['source'],
                    'text' => strip_tags(implode("\n", $temp['default_content'])),
                );
                $this->getCache()->save($cachekey, $templateContent, $this->cacheLifetime);
            }
            return $templateContent;
        }
    }

}
