<?php

namespace Arcanys\SportizingmeBundle\Controller;

use Arcanys\SecurityBundle\Model\Helper;
use Arcanys\SportizingmeApiBundle\Model\ResetPasswordParams;
use Arcanys\SportizingmeBundle\Entity\User\Admin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return array(
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        );
    }

    /**
     * @Route("/login_check", name="login_check")
     * @Template()
     */
    public function loginCheckAction()
    {
        return array();
    }

    /**
     * @Route("/logout", name="logout")
     * @Template()
     */
    public function logoutAction()
    {
        return array();
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     * @Template()
     */
    public function resetPasswordAction($token)
    {
        $class = 'Arcanys\SportizingmeBundle\Entity\User\Athlete';
        $factory = $this->get('security.encoder_factory');
        $encoded = Helper::encodePassword($factory, $token, $class, $class);
        $em = $this->getDoctrine()->getManager();
        $athlete = $em->getRepository($class)->findOneBy(array('passwordToken' => $encoded));
        if ($athlete) {
            // update new password
            $password = Helper::randomString();
            $athlete->setPasswordToken('expired');
            $athlete->setPassword($password);
            $em->persist($athlete);
            $em->flush();

            // send mail
            $params = new ResetPasswordParams($password);
            $this->get('arcanys_sportizingme.mailer')
                    ->setMessageTemplate('Reset password')
                    ->send($athlete->getEmail(), $params->getParams());
        } else {
            return array('error' => true);
        }
        return array();
    }
    
    /**
     * @Route("/login/target", name="login_target")
     */
    public function loginTargetAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        if (is_object($user) && $user instanceof Admin) {
            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        }
        // TODO: redirect to referer?
        return $this->redirect($this->generateUrl('homepage'));
    }

}
