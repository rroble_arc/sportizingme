<?php

namespace Arcanys\SportizingmeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImageController extends Controller
{

    /**
     * @Route("/image/{id}", name="link_image")
     */
    public function imageAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('ArcanysSportizingmeBundle:Image')
                ->find($id);
        if (!$image) {
            throw $this->createNotFoundException(sprintf('Image <%s> not found.', $id));
        }
        if ($image->getEncodedData()) {
            $response = new Response();
            $response->headers->set('Content-type', $image->getType());
            $response->setContent(base64_decode($image->getEncodedData()));
            return $response;
        }
        return new RedirectResponse($image->getLink());
    }

}
