<?php

namespace Arcanys\SportizingmeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/admin/notification")
 */
class AdminNotificationController extends Controller
{
    
    /**
     * @Route("/new", name="admin_notification_new")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('ArcanysSportizingmeBundle:Message\Notification')
                ->createQueryBuilder('n');
        $since = $qb->expr()->literal($request->get('since'));
        $qb->where($qb->expr()->gt('n.dateCreated', $since));
        $notifications = $qb->getQuery()->getResult();
        $result = array();
        foreach ($notifications as $value) {
            $link = $this->generateUrl('admin_arcanys_sportizingme_message_notification_show', 
                    array('id' => $value->getId()), true);
            $result[$link] = $value.'';
        }
        return new JsonResponse(array(
            'count' => count($result),
            'result' => $result,
            'since' => date('Y-m-d H:i:s'),
        ));
    }
    
}
