<?php

namespace Arcanys\SportizingmeBundle\Manager;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Symfony\Bridge\Doctrine\ManagerRegistry;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AthleteManager
{
    protected $em;
    
    public function __construct(ManagerRegistry $registry)
    {
        $this->em = $registry->getManager();
    }

    public function getMe(Athlete $athlete) 
    {
        $default = $this->getProgress();
        $sponsor = $athlete->getCurrentSponsor();
        $result = array(
            'my_sports' => array(
                'this_week'  => $default,
                'this_month' => $default,
                'all_time'   => $default,
            ),
            'current_deals' => array(
                'this_week'  => $default,
                'this_month' => $default,
            ),
            'athlete' => $athlete->toArray(),
            'sponsor' => $sponsor ? $sponsor->toArray() : null,
            'gym' => $athlete->getTrainingLocation() ? $athlete->getTrainingLocation()->toArray() : null,
        );
        
        // pointers
        $dealsThisWeek =& $result['current_deals']['this_week'];
        $dealsThisMonth =& $result['current_deals']['this_month'];
        
        // date range
        $week = $this->getWeekDateRange();
        $month = $this->getMonthDateRange();
        
        $repository = $this->em->getRepository('ArcanysSportizingmeBundle:Workout');
        $offer = $athlete->getCurrentOffer();
        // current deal
        if ($offer) {
            // weekly
            if ($offer->getTrainingsPerWeek() > 0) {
                $weeklyWorkoutSessions = $repository->getSessions($athlete, $offer, $week->start, $week->end);
                $dealsThisWeek = $this->getProgress(count($weeklyWorkoutSessions), $offer->getTrainingsPerWeek());
            }
            // montly
            $trainingsPerMonth = max($this->getNumberOfWeeks($month), $offer->getNumberOfWeeks()) * $offer->getTrainingsPerWeek();
            if ($trainingsPerMonth > 0) {
                $monthlyWorkoutSessions = $repository->getSessions($athlete, $offer, $month->start, $month->end);
                $dealsThisMonth = $this->getProgress(count($monthlyWorkoutSessions), $trainingsPerMonth);
            }
        }
        
        // FIXME: what if there is current offer?
        // in other words, this_week and this_month should be
        // the same on my_sports and current_deals
        
        // my sports
        $sessionsThisWeek = $repository->getSessions($athlete, null, $week->start, $week->end);
        $result['my_sports']['this_week'] = $this->getProgress(count($sessionsThisWeek), 7);
        
        $sessionsThisMonth = $repository->getSessions($athlete, null, $month->start, $month->end);
        $result['my_sports']['this_month'] = $this->getProgress(count($sessionsThisMonth), date('t'));
        
        // FIXME: how do we get all time total ?
        $sessionsAllTime = $repository->getSessions($athlete);
        $temp = array();
        foreach ((array)$sessionsAllTime as $workout) {
            if ($workout->getStart()) {
                $ts = $workout->getStart()->getTimestamp();
                $temp[date('n', $ts)] = date('t', $ts);
            }
        }
        $allTimeTotal = array_sum($temp);
        $result['my_sports']['all_time'] = $this->getProgress(count($sessionsAllTime), $allTimeTotal);
        
        return $result;
    }
    
    protected function getWeekDateRange()
    {
        $ts = time();
        $startOfWeek = mktime(0, 0, 0, date('n', $ts), date('j', $ts) - date('N', $ts) + 1);
        $endOfWeek = strtotime('+6days +23hours +59minutes +59seconds', $startOfWeek);
        return (object) array('start' => $startOfWeek, 'end' => $endOfWeek);
    }
    
    protected function getMonthDateRange()
    {
        $ts = time();
        $startOfMonth = mktime(0, 0, 0, date('n', $ts), 1);
        $endOfMonth = mktime(23, 59, 59, date('n', $ts), date('t', $ts));
        return (object) array('start' => $startOfMonth, 'end' => $endOfMonth);
    }
    
    protected function getNumberOfWeeks($range)
    {
        $w1 = date('W', $range->start);
        $w2 = date('W', $range->end);
        $w3 = $w2 < $w1 ? 52 : $w2;
        $diff = $w3 - $w1 + 1;
        return $diff;
    }
    
    protected function getProgress($current = 0, $total = 0)
    {
        return array(
            'current' => (float) $current,
            'total' => (float) $total,
            'progress' => $total > 0 ? round($current / $total, 2) : 0,
        );
    }

}
