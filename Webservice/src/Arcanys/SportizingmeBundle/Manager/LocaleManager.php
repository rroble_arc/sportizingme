<?php

namespace Arcanys\SportizingmeBundle\Manager;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class LocaleManager
{

    const LOCALE = '_locale';

    protected $locale;
    protected $languages;
    protected $router;
    protected $ignorePaths;

    public function __construct($locale, $languages, Router $router, array $ignorePaths = array())
    {
        $this->locale = $locale;
        $this->languages = explode('|', $languages);
        $this->router = $router;
        $this->ignorePaths = $ignorePaths;
    }

    /**
     * Set the default browser language.
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $pathinfo = $request->getPathInfo();
        foreach ($this->ignorePaths as $pattern) {
            if (strstr($pathinfo, $pattern) != false) {
                return;
            }
        }
        
        $session = $request->getSession();
        $locale = $request->attributes->get(static::LOCALE);
        if (!$locale) {
            if ($session->get(static::LOCALE)) { // from session
                $locale = $session->get(static::LOCALE);
            } else if ($request->getLanguages()) { // get first accepted language from browser
                $languages = $request->getLanguages();
                $locale = $languages[0];
            } else if ($request->getLocale()) { // get from browser
                $locale = $request->getLocale();
            } else {
                $locale = $this->locale; // get from config
            }
            $locale = strtolower(substr($locale, 0, 2));
            if (!array_search($locale, $this->languages)) { // after all, we want only language we defined
                $locale = array_pop($this->languages);
            }
            $request->attributes->set(static::LOCALE, $locale);
        }
        $session->set(static::LOCALE, $locale);

        // if 404: try to redirect to localized url
        $route = $request->attributes->get('_route');
        if (!$route) {
            $uris = $this->getUriCombination($pathinfo, $locale);
            foreach ($uris as $uri) {
                try {
                    $result = $this->router->match($uri);
                    $redirect = $this->router->generate($result['_route'], array(static::LOCALE => $locale));
                    $response = new RedirectResponse($redirect);
                    $event->setResponse($response);
                } catch(ResourceNotFoundException $e) {
                    // silent, we don't want to know next
                }
            }
        }
    }
    
    private function getUriCombination($pathinfo, $locale)
    {
        // clean uri
        $pathinfo = substr($pathinfo, 1).'';
        while (strpos($pathinfo, '//') !== false) {
            $pathinfo = str_replace('//', '/', $pathinfo);
        }
        if (substr($pathinfo, -1) == '/') {
            $pathinfo = substr($pathinfo, 0, -1);
        }
        
        $uris = array();
        $left = explode('/', $pathinfo);
        $right = array();
        while (count($left) > 0) {
            $current = array_pop($left);
            array_unshift($right, $current);
            $l = implode('/', $left);
            $r = implode('/', $right);
            $u = ($l?'/'.$l:$l) . '/'. $locale . '/'. $r;
            $uris[] = $u;
        }
        if ($pathinfo) {
            array_unshift($uris, '/'.$pathinfo.'/'.$locale);
        }
        return $uris;
    }


    public function getLanguages()
    {
        return (array) $this->languages;
    }
        
}
