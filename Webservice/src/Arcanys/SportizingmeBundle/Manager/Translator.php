<?php

namespace Arcanys\SportizingmeBundle\Manager;

use Arcanys\SportizingmeBundle\Entity\Email\Template;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Translation\Translator as BaseTranslator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;
use Symfony\Component\Translation\MessageSelector;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class Translator extends BaseTranslator  implements LoaderInterface
{
    
    protected $container;
    protected $em;
    protected $languages;

    public function __construct(ContainerInterface $container, MessageSelector $selector, $loaderIds = array(), array $options = array(), ManagerRegistry $registry = null, $languages = null)
    {
        parent::__construct($container, $selector, $loaderIds, $options);
        
        $this->em = $registry->getManager();
        $this->languages = explode('|', $languages);
    }
    
    public function load($resource, $locale, $domain = 'messages')
    {
        $catalogue = new MessageCatalogue($locale);
        $translations = $this->em->getRepository('ArcanysSportizingmeBundle:Email\Template')
                ->findAll();
        /* @var $template Template */
        foreach ($translations as $template) {
            $name = $template->getName();
            $catalogue->set($name.'.subject', $template->getSubject(), $domain);
            $catalogue->set($name.'.body', $template->getHtmlContent(), $domain);
            $catalogue->set($name.'.text', $template->getPlainTextContent(), $domain);
        }
        return $catalogue;
    }

    /**
     * {@inheritdoc}
     */
    protected function loadCatalogue($locale)
    {
        foreach ($this->languages as $lang) {
            $this->addResource('arc', null, $lang, 'mails');
        }
        $this->options['cache_dir'] = null;
        return parent::loadCatalogue($locale);
    }

}
