<?php

namespace Arcanys\SportizingmeBundle\Manager;

use Arcanys\SportizingmeBundle\Entity\Image;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImageManager
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    protected $web;
    
    /**
     * @var string
     */
    protected $uploads;
    
    /**
     * @var string
     */
    protected $dir;

    public function __construct(ContainerInterface $container, $web, $uploads)
    {
        $this->container = $container;
        $this->web = realpath($web);
        $this->uploads = $uploads;
    }
    
    protected function init()
    {
        $security = $this->container->get('security.context');
        $token = $security->getToken();
        $user = $token->getUser();
        $name = method_exists($user, 'getId') ? $user->getId() : preg_replace('/[^a-zA-Z0-9]/', '', $token->getUsername());
        $dir = implode(DIRECTORY_SEPARATOR, array($this->web, $this->uploads, $name));
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $this->dir = realpath($dir);
    }

    /**
     * @param mixed $file
     * @return mixed array or Image
     */
    public function upload(File $file)
    {
        $this->init();
        $destination = substr(md5($file->getSize()), 0, 7).'-'.$file->getClientOriginalName();
        $file = $file->move(realpath($this->dir), $destination);
        // TODO: resize
        return $file;
    }

    public function getLink(File $file)
    {
        $path = str_replace($this->web, '', $file->getRealPath());
        $request = $this->container->get('request');
        $url = $request->getUriForPath(str_replace('\\', '/', $path));
        return str_replace(
                array('//localhost', '/app_dev.php', '/app.php'),
                array('//127.0.0.1', '', ''), 
                $url);
    }
    
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Image) {
            if ($entity->isEncoded()) {
                $router = $this->container->get('router');
                $url = $router->generate('link_image', array('id' => $entity->getId()), true);
                $entity->setLink($url);
            }
        }
    }

}
