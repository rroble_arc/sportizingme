<?php

namespace Arcanys\SportizingmeBundle\Manager;

use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Translation\Translator as SymfonyTranslator;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class Mailer
{

    protected $mailer;
    protected $translator;
    protected $templating;
    protected $from;
    protected $subject;
    protected $body;
    protected $text;
    protected $domain;

    public function __construct(Swift_Mailer $mailer, SymfonyTranslator $translator, $templating, $from, $domain = 'mails')
    {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->templating = $templating;
        $this->from = $from;
        $this->domain = $domain;
    }

    public function setMessageTemplate($template)
    {
        $this->setSubject($template . '.subject');
        $this->setBody($template . '.body');
        $this->setText($template . '.text');
        return $this;
    }

    public function send($to, array $params = array())
    {
        $subject = $this->translator->trans($this->subject, $params, $this->domain);
        $body = $this->templating->render('ArcanysSportizingmeBundle:Mail:standard.html.twig', array(
            'content' => $this->translator->trans($this->body, $params, $this->domain),
        ));
        $text = $this->templating->render('ArcanysSportizingmeBundle:Mail:standard.html.twig', array(
            'content' => $this->translator->trans($this->text, $params, $this->domain),
        ));
        if ($this->text === trim($text)) {
            $text = strip_tags($body);
        }
        $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->from)
                ->setTo($to)
                ->setBody($text)
                ->addPart($body, 'text/html');
        $this->mailer->send($message);
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setFrom($from)
    {
        $this->from = $from;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }
    
}
