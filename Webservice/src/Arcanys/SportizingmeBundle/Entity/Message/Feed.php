<?php

namespace Arcanys\SportizingmeBundle\Entity\Message;

use Arcanys\SportizingmeBundle\Entity\Image;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\Enums;
use Doctrine\ORM\Mapping as ORM;

/**
 * Feed
 *
 * @ORM\Table(name="feed")
 * @ORM\Entity()
 */
class Feed extends Message
{

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="picture", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Image
     */
    protected $picture;

    /**
     * @ORM\OneToMany(targetEntity="FeedLike", mappedBy="feed")
     * @var \Doctrine\ORM\PersistentCollection
     * */
    protected $likes;

    public function __construct($type = Enums::TYPE_SPORTIZING)
    {
        $this->title = $type;
    }

    /**
     * Set picture
     *
     * @param Image $picture
     * @return Feed
     */
    public function setPicture(Image $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return Image 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    public function toArray()
    {
        $result = array(
            'id' => $this->id,
            'message' => $this->message,
            'picture' => $this->picture ? $this->picture->getLink() : null,
            'type' => $this->title,
            'date' => $this->dateCreated->format('Y-m-d H:i:s'),
            'likes' => $this->countLikes(),
            'athlete' => $this->athlete? $this->athlete->toArray(): null,
            'gym' => $this->gym? $this->gym->toArray(): null,
        );
        if ($this->title == 'checkout' && $this->athlete) {
            $sponsor = $this->athlete->getCurrentSponsor();
            $result['sponsor'] = $sponsor? $sponsor->toArray(): null;
        }
        return $result;
    }

    public function getLikes()
    {
        return $this->likes;
    }

    public function setLikes($likes)
    {
        $this->likes = $likes;
    }
    
    public function countLikes()
    {
        $likes = array_filter($this->likes->getValues(), function($like) {
            return $like->isLiked();
        });
        return count($likes);
    }

    public function hasLiked(Athlete $athlete)
    {
        $likes = array_filter($this->likes->getValues(), function($like) use($athlete) {
            return $athlete->equals($like->getAthlete()) && $like->isLiked();
        });
        return count($likes) > 0;
    }

}
