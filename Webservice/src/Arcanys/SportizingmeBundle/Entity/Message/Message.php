<?php

namespace Arcanys\SportizingmeBundle\Entity\Message;

use Arcanys\SportizingmeBundle\Entity\AbstractTimestamptable;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\AthleteFilter;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Arcanys\SportizingmeBundle\Entity\Gym;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *      "message" = "Message", "offer" = "Offer", "feed" = "Feed"
 * })
 */
class Message extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $title;

    /**
     * @ORM\Column(name="message", type="text")
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $message;

    /**
     * @ORM\Column(name="is_read", type="boolean", nullable=true)
     * 
     * @var boolean
     */
    protected $read;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\User\Athlete", cascade={"persist"})
     * @ORM\JoinColumn(name="athlete", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Athlete
     */
    protected $athlete;
    
    /**
     * @var AthleteFilter
     */
    protected $filters;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Gym", cascade={"persist"})
     * @ORM\JoinColumn(name="gym_id", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Gym
     */
    protected $gym;

    public function __construct()
    {
        $this->read = false;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Message
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return boolean
     */
    public function isRead()
    {
        return $this->read;
    }

    public function setRead($read)
    {
        $this->read = $read;
        
        return $this;
    }
    
    /**
     * @return Athlete
     */
    public function getAthlete()
    {
        return $this->athlete;
    }

    public function setAthlete(Athlete $athlete)
    {
        $this->athlete = $athlete;
        
        return $this;
    }

    public function toArray()
    {
        $result = array(
            'id' => $this->id,
            'title' => $this->title,
            'message' => $this->message,
            'date' => $this->dateCreated->format('l, F j, Y'),
            'read' => (bool) $this->read,
            'type' => 'message',
        );
        if ($this->gym) {
            $result['type'] = 'gym';
            $result['gym'] = $this->gym->toArray();
        }
        return $result;
    }
    
    public function __toString()
    {
        return $this->title.'';
    }

    /**
     * @return AthleteFilter
     */
    public function getFilters()
    {
        return $this->filters;
    }

    public function setFilters(AthleteFilter $filter = null)
    {
        $this->filters = $filter;
        
        return $this;
    }

    /**
     * @return Gym
     */
    public function getGym()
    {
        return $this->gym;
    }

    public function setGym(Gym $gym = null)
    {
        $this->gym = $gym;
        
        return $this;
    }
    
}
