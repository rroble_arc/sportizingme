<?php

namespace Arcanys\SportizingmeBundle\Entity\Message;

use Arcanys\SportizingmeBundle\Entity\Image;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Entity\User\Sponsor;
use Arcanys\SportizingmeBundle\Model\Enums;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Offer
 *
 * @ORM\Table(name="offer")
 * @ORM\Entity(repositoryClass="Arcanys\SportizingmeBundle\Entity\Repository\OfferRepository")
 * @Assert\Callback(methods={"validateDates"})
 */
class Offer extends Message
{

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\User\Sponsor")
     * @ORM\JoinColumn(name="sponsor_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     * 
     * @var Sponsor
     */
    protected $sponsor;

    /**
     * @ORM\Column(name="status", type="string", length=255)
     * @Assert\Choice(callback = {"Arcanys\SportizingmeBundle\Model\Enums", "getStatusList"})
     * 
     * @var string
     */
    protected $status;

    /**
     * @ORM\Column(name="start_date", type="date")
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @ORM\Column(name="end_date", type="date")
     * 
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @ORM\Column(name="trainings_per_week", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     * 
     * @var integer
     */
    protected $trainingsPerWeek;

    /**
     * @ORM\Column(name="how_many_weeks", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     * 
     * @var integer
     */
    protected $numberOfWeeks;

    /**
     * @ORM\Column(name="hours_per_session", type="float")
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\GreaterThan(0)
     * @Assert\LessThan(24)
     * 
     * @var float
     */
    protected $hoursPerSession;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="ads", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $ads;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="front", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $front;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="back", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $back;

    /**
     * @ORM\Column(name="payment", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $payment;

    /**
     * @ORM\Column(name="sports", type="simple_array", nullable=true)
     * @Assert\All({
     *      @Assert\NotBlank(),
     *      @Assert\Choice(callback = {"Arcanys\SportizingmeBundle\Model\Enums", "getSports"})
     * })
     * @var array
     */
    protected $sports;

    /**
     * @ORM\Column(name="base_rate", type="float")
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\GreaterThan(0)
     * 
     * @var float
     */
    protected $rate;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\User\Athlete", inversedBy="offers", cascade={"persist"})
     * @ORM\JoinColumn(name="athlete", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Athlete
     */
    protected $athlete;

    /**
     * @ORM\OneToMany(targetEntity="Arcanys\SportizingmeBundle\Entity\Workout", mappedBy="offer")
     * @var ArrayCollection
     * */
    protected $workouts;

    public function __construct()
    {
        $this->status = Enums::STATUS_PENDING;
        $this->rate = Enums::DEAL_BASE_RATE; // base rate in $
        $this->title = 'Sponsorship offer';
    }

    /**
     * Set sponsor
     *
     * @param Sponsor $sponsor
     * @return Offer
     */
    public function setSponsor($sponsor)
    {
        $this->sponsor = $sponsor;

        return $this;
    }

    /**
     * Get sponsor
     *
     * @return Sponsor 
     */
    public function getSponsor()
    {
        return $this->sponsor;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    public function getTrainingsPerWeek()
    {
        return $this->trainingsPerWeek;
    }

    public function setTrainingsPerWeek($trainingsPerWeek)
    {
        $this->trainingsPerWeek = $trainingsPerWeek;
    }

    public function getNumberOfWeeks()
    {
        return $this->numberOfWeeks;
    }

    public function setNumberOfWeeks($numberOfWeeks)
    {
        $this->numberOfWeeks = $numberOfWeeks;
    }

    public function getHoursPerSession()
    {
        return $this->hoursPerSession;
    }

    public function setHoursPerSession($hoursPerSession)
    {
        $this->hoursPerSession = $hoursPerSession;
    }

    /**
     * @return Image
     */
    public function getAds()
    {
        return $this->ads;
    }

    public function setAds(Image $ads = null)
    {
        $this->ads = $ads;
    }

    public function getTotalSessions()
    {
        return $this->trainingsPerWeek * $this->numberOfWeeks;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    public function getSports()
    {
        return $this->sports;
    }

    public function setSports(array $sports = null)
    {
        $this->sports = $sports;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'title' => $this->title,
            'message' => $this->message,
            'hours_per_session' => $this->hoursPerSession,
            'trainings_per_week' => $this->trainingsPerWeek,
            'number_of_weeks' => $this->numberOfWeeks,
            'date' => $this->dateCreated ? $this->dateCreated->format('l, F j, Y') : null,
            'start_date' => $this->startDate ? $this->startDate->format('l, F j, Y') : null,
            'end_date' => $this->endDate ? $this->endDate->format('l, F j, Y') : null,
            'rate' => $this->rate,
            'front' => $this->front ? $this->front->getLink() : null,
            'back' => $this->back ? $this->back->getLink() : null,
            'ads' => $this->ads ? $this->ads->getLink() : null,
            'sponsor' => $this->sponsor ? $this->sponsor->toArray() : null,
            'read' => (bool) $this->read,
            'type' => 'offer',
        );
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function setRate($rate)
    {
        $this->rate = $rate;
    }
    
    public function isDone()
    {
        if ($this->status == Enums::STATUS_DONE) {
            return true;
        }
        $today = new \DateTime();
        $today->setTimestamp(mktime(0, 0, 0));
        if ($today > $this->endDate) {
            return true;
        }
        return false;
    }
    
    public function isPending()
    {
        return $this->status == Enums::STATUS_PENDING;
    }
    
    public function isAccepted()
    {
        return $this->status == Enums::STATUS_ACCEPTED;
    }
    
    public function isDeclined()
    {
        return $this->status == Enums::STATUS_DECLINED;
    }
    
    public function getFront()
    {
        return $this->front;
    }

    public function setFront(Image $front)
    {
        $this->front = $front;
    }

    public function getBack()
    {
        return $this->back;
    }

    public function setBack(Image $back)
    {
        $this->back = $back;
    }
    
    public function getProgress()
    {
        $total = $this->getTotalSessions();
        if (!$total) return 0;
        $actual = count($this->workouts);
        $progress = number_format(100*$actual / $total, 2);
        return str_replace('.00', '', $progress);
    }
    
    /**
     * @return ArrayCollection
     */
    public function getWorkouts()
    {
        return $this->workouts;
    }

    public function equals(Offer $other)
    {
        if ($this->id) {
            return $this->id === $other->getId();
        }
        return spl_object_hash($this) == spl_object_hash($other);
    }
    
    public function validateDates(ExecutionContextInterface $context) 
    {
        if ($this->startDate > $this->endDate) {
            $context->addViolationAt('startDate', 'Start date must be before end date');
            $context->addViolationAt('endDate', 'End date must be after start date');
        }
    }
    
}
