<?php

namespace Arcanys\SportizingmeBundle\Entity\Message;

use Arcanys\SportizingmeBundle\Entity\AbstractTimestamptable;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Likes
 *
 * @ORM\Table(name="feed_like")
 * @ORM\Entity()
 */
class FeedLike extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Feed")
     * @ORM\JoinColumn(name="feed_id", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Feed
     */
    protected $feed;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\User\Athlete")
     * @ORM\JoinColumn(name="athlete_id", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Athlete
     */
    protected $athlete;
    
    /**
     * @ORM\Column(name="liked", type="boolean")
     * 
     * @var boolean
     */
    protected $liked;

    public function __construct(Feed $feed, Athlete $athlete)
    {
        $this->feed = $feed;
        $this->athlete = $athlete;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set feed
     *
     * @param Feed $feed
     * @return FeedLike
     */
    public function setFeed(Feed $feed)
    {
        $this->feed = $feed;

        return $this;
    }

    /**
     * Get feed
     *
     * @return Feed 
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * Set athlete
     *
     * @param Athlete $athlete
     * @return FeedLike
     */
    public function setAthlete(Athlete $athlete)
    {
        $this->athlete = $athlete;

        return $this;
    }

    /**
     * Get athlete
     *
     * @return Athlete 
     */
    public function getAthlete()
    {
        return $this->athlete;
    }

    public function isLiked()
    {
        return $this->liked;
    }

    public function setLiked($liked)
    {
        $this->liked = $liked;
    }

}
