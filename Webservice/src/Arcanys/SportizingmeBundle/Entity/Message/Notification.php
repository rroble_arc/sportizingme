<?php

namespace Arcanys\SportizingmeBundle\Entity\Message;

use Arcanys\SportizingmeBundle\Entity\AbstractTimestamptable;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity
 */
class Notification extends AbstractTimestamptable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="content", type="string", length=255)
     * 
     * @var string
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\User\Athlete")
     * @ORM\JoinColumn(name="athlete_id", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Athlete
     */
    protected $athlete;

    /**
     * @ORM\ManyToOne(targetEntity="Message")
     * @ORM\JoinColumn(name="message_id", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Message
     */
    protected $message;
    
    public function __construct(Message $message, Athlete $athlete = null, $content = null)
    {
        $this->setAthlete($athlete? $athlete: $message->getAthlete());
        $this->setMessage($message);
        $this->setContent($content);
    }
    
    public function forAcceptedOffer($content = 'Athlete has accepted a sponsorship offer')
    {
        return $this->setContent($content);
    }
    
    public function forDeclinedOffer($content = 'Athlete has declined a sponsorship offer')
    {
        return $this->setContent($content);
    }
    
    public function forReadMessage($content = 'Athlete has read a message')
    {
        return $this->setContent($content);
    }
    
    public function forLikedFeed($content = 'Athlete has liked  a feed')
    {
        return $this->setContent($content);
    }
    
    public function forUnlikedFeed($content = 'Athlete has unliked  a feed')
    {
        return $this->setContent($content);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Notification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set athlete
     *
     * @param Athlete $athlete
     * @return Notification
     */
    public function setAthlete(Athlete $athlete)
    {
        $this->athlete = $athlete;

        return $this;
    }

    /**
     * Get athlete
     *
     * @return Athlete 
     */
    public function getAthlete()
    {
        return $this->athlete;
    }

    /**
     * Set message
     *
     * @param Message $message
     * @return Notification
     */
    public function setMessage(Message $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return Message 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param Message $message
     * @return Notification
     */
    public static function create(Message $message)
    {
        $notification = new static($message);
        return $notification;
    }
    
    public function __toString()
    {
        $str = str_replace('Athlete', $this->athlete->getName(), $this->content);
        return $str.'';
    }
    
}
