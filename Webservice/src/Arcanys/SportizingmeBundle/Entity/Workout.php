<?php

namespace Arcanys\SportizingmeBundle\Entity;

use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Workout
 *
 * @ORM\Table(name="workout")
 * @ORM\Entity(repositoryClass="Arcanys\SportizingmeBundle\Entity\Repository\WorkoutRepository")
 */
class Workout extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\User\Athlete", inversedBy="workouts")
     * @ORM\JoinColumn(name="athlete_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     * 
     * @var Athlete
     */
    protected $athlete;

    /**
     * @ORM\Column(name="start", type="datetime")
     * 
     * @var \DateTime
     */
    protected $start;

    /**
     * @ORM\Column(name="end", type="datetime", nullable=true)
     * 
     * @var \DateTime
     */
    protected $end;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Message\Offer", inversedBy="workouts")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Offer
     */
    protected $offer;

    /**
     * @ORM\Column(name="earning", type="float", nullable=true)
     * 
     * @var float
     */
    protected $earning;

    /**
     * @ORM\ManyToOne(targetEntity="Gym", cascade={"persist"})
     * @ORM\JoinColumn(name="gym_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     * 
     * @var Gym
     */
    protected $gym;

    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinColumn(name="picture", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $picture;

    /**
     * @ORM\Column(name="caption", type="string", length=128, nullable=true)
     * @Assert\NotBlank(groups={"Checkin2"})
     * 
     * @var string
     */
    protected $caption;

    public function __construct()
    {
        $this->start = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set athlete
     *
     * @param Athlete $athlete
     * @return Workout
     */
    public function setAthlete(Athlete $athlete)
    {
        $this->athlete = $athlete;

        return $this;
    }

    /**
     * Get athlete
     *
     * @return Athlete 
     */
    public function getAthlete()
    {
        return $this->athlete;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Workout
     */
    public function setStart(\DateTime $start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Workout
     */
    public function setEnd(\DateTime $end = null)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    public function getOffer()
    {
        return $this->offer;
    }

    public function setOffer(Offer $offer)
    {
        $this->offer = $offer;
    }

    public function getEarning()
    {
        return $this->earning;
    }

    public function setEarning($earning)
    {
        $this->earning = $earning;
    }

    public function isOpen()
    {
        return $this->end == null;
    }

    /**
     * @return Gym
     */
    public function getGym()
    {
        return $this->gym;
    }

    public function setGym(Gym $gym)
    {
        $this->gym = $gym;
    }
    
    /**
     * @return Image
     */
    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture(Image $picture = null)
    {
        $this->picture = $picture;
    }

    public function getCaption()
    {
        return $this->caption;
    }

    public function setCaption($caption)
    {
        $this->caption = $caption;
    }
    
    public function getHours()
    {
        if ($this->start && $this->end) {
            $diff = $this->start->diff($this->end);
            $h = $diff->format('%h');
            $m = $diff->format('%i');
            $s = $diff->format('%s');
            $hours = $h + (($h+$s/60)/60);
            return $hours;
        }
        return 0;
    }
    
    public function __toString()
    {
        return $this->caption.'';
    }
    
    public function toArray()
    {
        return array(
            'id' => $this->id,
            'caption' => $this->caption,
            'picture' => $this->picture ? $this->picture->getLink() : null,
            'date' => $this->start->format('d/m/Y'),
            'time' => $this->start->format('H:i:s'),
        );
    }
    
}
