<?php

namespace Arcanys\SportizingmeBundle\Entity\Email;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Template
 *
 * @ORM\Table(name="email_template", uniqueConstraints={@ORM\UniqueConstraint(name="unique_idx", columns={"name"})})
 * @ORM\Entity()
 * @UniqueEntity(fields={"name"})
 */
class Template
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="subject", type="text", nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $subject;

    /**
     * @ORM\Column(name="html_content", type="text", nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $htmlContent;

    /**
     * @ORM\Column(name="plain_text_content", type="text", nullable=true)
     * 
     * @var string
     */
    protected $plainTextContent;

    /**
     * @ORM\Column(name="mailchimp_campaign", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $campaign;

    /**
     * @var string
     */
    protected $submit;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        
        return $this;
    }

    public function getHtmlContent()
    {
        return $this->htmlContent;
    }

    public function setHtmlContent($htmlContent)
    {
        $this->htmlContent = $htmlContent;
        
        return $this;
    }

    public function getPlainTextContent()
    {
        if ($this->plainTextContent) {
            return $this->plainTextContent;
        }
        return strip_tags($this->htmlContent);
    }

    public function setPlainTextContent($plainTextContent)
    {
        $this->plainTextContent = $plainTextContent;
        
        return $this;
    }

    public function __toString()
    {
        return $this->name . '';
    }

    public function getCampaign()
    {
        return $this->campaign;
    }

    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
        
        return $this;
    }

    public function getSubmit()
    {
        return $this->submit;
    }

    public function setSubmit($submit)
    {
        $this->submit = $submit;
        
        return $this;
    }

}
