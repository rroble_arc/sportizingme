<?php

namespace Arcanys\SportizingmeBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GymRepository extends EntityRepository
{

    public function filter($isApproved)
    {
        $qb = $this->createQueryBuilder('g');
        if ($isApproved !== null) {
            $qb->where($qb->expr()->eq('g.approved', $qb->expr()->literal((bool) $isApproved)));
        }
        return $qb;
    }
    
    public function findActiveGyms(array $include = array())
    {
        $qb = $this->createQueryBuilder('g');
//        $qb->where($qb->expr()->eq('g.approved', true));
        if ($include) {
            $qb->orWhere($qb->expr()->in('g.id', $include));
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

}
