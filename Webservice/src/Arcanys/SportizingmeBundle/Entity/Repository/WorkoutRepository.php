<?php

namespace Arcanys\SportizingmeBundle\Entity\Repository;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Doctrine\ORM\EntityRepository;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class WorkoutRepository extends EntityRepository
{
    
    public function getSessions(Athlete $athlete, Offer $offer = null, $start = null, $end = null)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('w')->from('ArcanysSportizingmeBundle:Workout', 'w')
                ->where($qb->expr()->eq('w.athlete', $athlete->getId()))
                ->andWhere($qb->expr()->isNotNull('w.end'));
        if ($offer) {
            $qb->andWhere($qb->expr()->eq('w.offer', $offer->getId()));
        }
        if ($start && $end) {
            $startDate = $qb->expr()->literal(date('Y-m-d H:i:s', $start));
            $endDate = $qb->expr()->literal(date('Y-m-d H:i:s', $end));
            $qb->andWhere($qb->expr()->gte('w.start', $startDate))
                ->andWhere($qb->expr()->lte('w.end', $endDate));
        }
        $workouts = $qb->getQuery()->getResult();
        return $workouts;
    }
    
    public function getVisitedGymsByAthlete(Athlete $athlete)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('w.id, g.id as gym, COUNT(w.gym) as visits')
                ->from('ArcanysSportizingmeBundle:Workout', 'w')
                ->leftJoin('w.gym', 'g')
                ->where($qb->expr()->eq('w.athlete', $athlete->getId()))
                ->groupBy('w.gym');
        $visits = $qb->getQuery()->getResult();
        return $visits;
    }
    
    public function getFriendsVisitedGyms(array $friendIds, array $gymIds)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('w')
                ->from('ArcanysSportizingmeBundle:Workout', 'w')
                ->where($qb->expr()->in('w.athlete', $friendIds))
                ->andWhere($qb->expr()->in('w.gym', $gymIds));
        $result = $qb->getQuery()->getResult();
        $visits = array();
        foreach ($result as $value) {
            $visits[$value->getGym()->getId()][] = $value->getAthlete();
        }
        return $visits;
    }
    
    /**
     * @param string $since (1hr, 1day, 2hrs, etc)
     */
    public function findOpenSessions($since)
    {
        $t = date('Y-m-d H:i:s', strtotime($since));
        $qb = $this->_em->createQueryBuilder();
        $qb->select('w')
                ->from('ArcanysSportizingmeBundle:Workout', 'w')
                ->where($qb->expr()->isNull('w.end'))
                ->andWhere($qb->expr()->lt('w.start', $qb->expr()->literal($t)));
        $result = $qb->getQuery()->getResult();
        return $result;
    }
    
}
