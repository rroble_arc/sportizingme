<?php

namespace Arcanys\SportizingmeBundle\Entity\Repository;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Arcanys\SportizingmeBundle\Entity\User\Sponsor;
use Arcanys\SportizingmeBundle\Model\Enums;
use Doctrine\ORM\EntityRepository;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class OfferRepository extends EntityRepository
{

    public function getAvailableOffers(Athlete $athlete)
    {
        $qb = $this->_em->createQueryBuilder();
        $status = $qb->expr()->literal(Enums::STATUS_PENDING);
        $qb->select('o')->from('ArcanysSportizingmeBundle:Message\Offer', 'o')
                ->where($qb->expr()->eq('o.athlete', $athlete->getId()))
                ->andWhere($qb->expr()->eq('o.status', $status));
        return $qb->getQuery()->getResult();
    }
    
    public function findLastAcceptedOffer(Athlete $athlete)
    {
        $qb = $this->_em->createQueryBuilder();
        $accepted = $qb->expr()->literal(Enums::STATUS_ACCEPTED);
        $qb->select('o')->from('ArcanysSportizingmeBundle:Message\Offer', 'o')
                ->where($qb->expr()->eq('o.athlete', $athlete->getId()))
                ->andWhere($qb->expr()->eq('o.status', $accepted))
                ->orderBy('o.startDate', 'DESC')
                ->setMaxResults(1);
        try {
            $result = $qb->getQuery()->getSingleResult();
            return $result;
        } catch(\Exception $ex) {
            // nothing, returns null
        }
    }

    public function getSponsoredAthletes(Sponsor $sponsor)
    {
        $qb = $this->_em->createQueryBuilder();
        $declined = $qb->expr()->literal(Enums::STATUS_DECLINED);
        $qb->select('o')->from('ArcanysSportizingmeBundle:Message\Offer', 'o');
        $qb->where($qb->expr()->eq('o.sponsor', $sponsor->getId()));
        $qb->andWhere($qb->expr()->neq('o.status', $qb->expr()->literal($declined)));

        $result = $qb->getQuery()->getResult();
        return $result;
    }
    
    public function isCurrentlySponsoring(Offer $offer)
    {
        $qb = $this->_em->createQueryBuilder();
        $accepted = $qb->expr()->literal(Enums::STATUS_ACCEPTED);
        $pending = $qb->expr()->literal(Enums::STATUS_PENDING);
        $qb->select('o')->from('ArcanysSportizingmeBundle:Message\Offer', 'o')
                ->where($qb->expr()->eq('o.sponsor', $offer->getSponsor()->getId()))
                ->andWhere($qb->expr()->eq('o.athlete', $offer->getAthlete()->getId()))
                ->andWhere($qb->expr()->in('o.status', array($pending, $accepted)));
        $result = $qb->getQuery()->getResult();
        return count($result) > 0;
    }

}
