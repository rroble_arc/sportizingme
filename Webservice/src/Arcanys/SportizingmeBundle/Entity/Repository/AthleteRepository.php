<?php

namespace Arcanys\SportizingmeBundle\Entity\Repository;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Entity\Message\Message;
use Doctrine\ORM\EntityRepository;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AthleteRepository extends EntityRepository
{

    public function findAthletes(array $sports = array())
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('a')->from('ArcanysSportizingmeBundle:User\Athlete', 'a');
        if ($sports) {
            foreach ($sports as $sport) {
                $val = $qb->expr()->literal("%$sport%");
                $qb->orWhere($qb->expr()->like('a.sports', $val));
            }
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function findAllAthletes($limit, $offset, Athlete $except = null, $name = null)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('a')->from('ArcanysSportizingmeBundle:User\Athlete', 'a');
        $qb->setMaxResults($limit);
        $qb->setFirstResult($offset);
        $qb->orderBy('a.firstname');
        if ($name) {
            $nameLiteral = $qb->expr()->literal("%$name%");
            $qb->andWhere($qb->expr()->like('a.firstname', $nameLiteral));
        }
        if ($except) {
            $qb->andWhere($qb->expr()->neq('a.id', $except->getId()));
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }
    
    public function findFriendsInGym($gymId, array $friendIds)
    {
        if (!$friendIds) {
            return array(); // means no friends
        }
        $qb = $this->_em->createQueryBuilder();
        $date = $qb->expr()->literal(date('Y-m-d').'%');
        $qb->select('a')
                ->from('ArcanysSportizingmeBundle:User\Athlete', 'a')
                ->leftJoin('a.workouts', 'w')
                ->where($qb->expr()->eq('w.gym', $gymId))
                ->andWhere($qb->expr()->like('w.start', $date))
                ->andWhere($qb->expr()->isNull('w.end'))
                ->andWhere($qb->expr()->in('w.athlete', $friendIds))
                ->orderBy('w.start', 'DESC');
        $qb->setFirstResult(0);
        $qb->setMaxResults(3);
        $result = $qb->getQuery()->getResult();
        return $result;
    }
    
    public function findLastAthletesInGym($gymId, array $exceptIds)
    {
        $qb = $this->_em->createQueryBuilder();
        $date = $qb->expr()->literal(date('Y-m-d').'%');
        $qb->select('a')
                ->from('ArcanysSportizingmeBundle:User\Athlete', 'a')
                ->leftJoin('a.workouts', 'w')
                ->where($qb->expr()->eq('w.gym', $gymId))
                ->andWhere($qb->expr()->like('w.start', $date))
                ->andWhere($qb->expr()->isNull('w.end'))
                ->andWhere($qb->expr()->notIn('w.athlete', $exceptIds))
                ->orderBy('w.start', 'DESC');
        $qb->setFirstResult(0);
        $qb->setMaxResults(3);
        $result = $qb->getQuery()->getResult();
        return $result;
    }
    
    public function matchMessageBox(MessageBox $filters)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->leftJoin('a.trainingLocation', 'g');
        $qb->leftJoin('a.postalAddress', 'l');
        if ($filters->getGym()) {
            $qb->andWhere($qb->expr()->eq('g.id', $filters->getGym()->getId()));
        }
        if ($filters->getCountry()) {
            $country = $qb->expr()->literal($filters->getCountry()->getCode());
            $qb->andWhere($qb->expr()->eq('l.country', $country));
        }
        if ($filters->getCity()) {
            $city = $qb->expr()->literal($filters->getCity()->getName());
            $qb->andWhere($qb->expr()->eq('l.city', $city));
        }
        return $qb->getQuery()->getResult();
    }
    
}
