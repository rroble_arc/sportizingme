<?php

namespace Arcanys\SportizingmeBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class CompanyRepository extends EntityRepository
{

    public function search($str = null)
    {
        $qb = $this->createQueryBuilder('c');
        if ($str) {
            $literal = $qb->expr()->literal("%{$str}%");
            $qb->where($qb->expr()->like('c.name', $literal));
        }
        return $qb->getQuery()->getResult();
    }

}
