<?php

namespace Arcanys\SportizingmeBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SchoolRepository extends EntityRepository
{

    public function search($str = null)
    {
        $qb = $this->createQueryBuilder('s');
        if ($str) {
            $literal = $qb->expr()->literal("%{$str}%");
            $qb->where($qb->expr()->like('s.name', $literal));
        }
        return $qb->getQuery()->getResult();
    }

}
