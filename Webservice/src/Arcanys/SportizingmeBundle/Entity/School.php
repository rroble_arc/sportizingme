<?php

namespace Arcanys\SportizingmeBundle\Entity;

use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * School
 *
 * @ORM\Table(name="school")
 * @ORM\Entity(repositoryClass="Arcanys\SportizingmeBundle\Entity\Repository\SchoolRepository")
 */
class School extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return School
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function toArray()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
        );
    }
    
    public function __toString()
    {
        return $this->name.'';
    }

}
