<?php

namespace Arcanys\SportizingmeBundle\Entity;

use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Arcanys\SportizingmeBundle\Validator\ImageLink;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity()
 * @Assert\Callback(methods={"validate"}, groups={"Default", "Registration","RegistrationViaFacebook"})
 */
class Image extends AbstractTimestamptable implements Timestamptable
{

    const TYPE_EXTERNAL = 'external/link';

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="link", type="string", length=500, nullable=false)
     * 
     * @var string
     */
    protected $link;

    /**
     * @ORM\Column(name="type", type="string", length=32, nullable=true)
     * 
     * @var string
     */
    protected $type;

    /**
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $file;
    private $encodedData;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Image
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Image
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    public function validate(ExecutionContextInterface $context)
    {
        if (is_object($this->file)) {
            $context->validateValue($this->file, new Assert\Image(), 'file');
        }

        $context->validateValue($this->link, new Assert\NotBlank(), 'link');
        $context->validateValue($this->link, new Assert\Url(), 'link');
        $context->validateValue($this->link, new ImageLink(), 'link');
    }

    public function __toString()
    {
        return $this->link . '';
    }

    public function isEncoded()
    {
        if ($this->file === null) {
            if (strpos($this->link, 'http') !== 0) {
                $this->encodedData = $this->link;
                return true;
            }
        }
        return false;
    }

    public function getEncodedData()
    {
        return $this->encodedData;
    }

}
