<?php

namespace Arcanys\SportizingmeBundle\Entity;

use Arcanys\SportizingmeBundle\Model\LocationInterface;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity()
 */
class Address extends AbstractTimestamptable implements Timestamptable, LocationInterface
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="number", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $number;

    /**
     * @ORM\Column(name="street", type="string", length=128, nullable=true)
     * 
     * @var string
     */
    protected $street;

    /**
     * @ORM\Column(name="postal_code", type="string", length=32, nullable=true)
     * 
     * @var string
     */
    protected $zipcode;

    /**
     * @ORM\Column(name="city", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $city;

    /**
     * @ORM\Column(name="state", type="string", length=32, nullable=true)
     * 
     * @var string
     */
    protected $state;

    /**
     * @ORM\Column(name="country", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $country;

    /**
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     * 
     * @var float
     */
    protected $longitude;

    /**
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     * 
     * @var float
     */
    protected $latitude;

    /**
     * @ORM\Column(name="maprect", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $maprect;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Address
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return Address
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string 
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Address
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Address
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Address
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
    
    public function getMaprect()
    {
        return $this->maprect;
    }

    public function setMaprect($maprect)
    {
        $this->maprect = $maprect;
    }

    public function getDistance(LocationInterface $location)
    {
        $lat1 = $this->getLatitude();
        $lon1 = $this->getLongitude();
        $lat2 = $location->getLatitude();
        $lon2 = $location->getLongitude();
        
        $theta = $lon1 - $lon2;
        $dista = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $distb = acos($dista);
        $dist = rad2deg($distb);
        $miles = $dist * 60 * 1.1515;
        $km = $miles * 1.609344;
        $m = $km * 1000;
        return $m;
    }
    
    public function __toString()
    {
        return $this->street.'';
    }
    
    public function toArray()
    {
        return array(
            'id' => $this->id,
            'street' => $this->street,
            'number' => $this->number,
            'city' => $this->city,
            'state' => $this->state,
            'country' => $this->country,
            'zipcode' => $this->zipcode,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'maprect' => $this->maprect,
        );
    }
    
}
