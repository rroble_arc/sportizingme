<?php

namespace Arcanys\SportizingmeBundle\Entity\Company;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Sponsor Company
 *
 * @ORM\Table(name="sponsor_company")
 * @ORM\Entity()
 */
class Sponsor extends Company
{

    /**
     * @ORM\Column(name="industry", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $industry;

    /**
     * @ORM\Column(name="annual_revenue", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $annualRevenue;

    /**
     * @ORM\Column(name="presentation", type="text", nullable=true)
     * 
     * @var string
     */
    protected $presentation;

    /**
     * @ORM\Column(name="contact_person_name", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $contactPersonName;

    /**
     * @ORM\Column(name="contact_person_title", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $contactPersonTitle;

    /**
     * @ORM\Column(name="contact_person_email", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $contactPersonEmail;

    /**
     * @ORM\Column(name="facebook_page", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $facebookPage;

    /**
     * @ORM\Column(name="linkedin_page", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $linkedInPage;

    /**
     * @ORM\Column(name="number_of_employess", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $numberOfEmployees;

    /**
     * Set industry
     *
     * @param string $industry
     * @return Sponsor
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * Get industry
     *
     * @return string 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set annualRevenue
     *
     * @param string $annualRevenue
     * @return Sponsor
     */
    public function setAnnualRevenue($annualRevenue)
    {
        $this->annualRevenue = $annualRevenue;

        return $this;
    }

    /**
     * Get annualRevenue
     *
     * @return string 
     */
    public function getAnnualRevenue()
    {
        return $this->annualRevenue;
    }

    /**
     * Set presentation
     *
     * @param string $presentation
     * @return Sponsor
     */
    public function setPresentation($presentation)
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * Get presentation
     *
     * @return string 
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Set contactPersonName
     *
     * @param string $contactPersonName
     * @return Sponsor
     */
    public function setContactPersonName($contactPersonName)
    {
        $this->contactPersonName = $contactPersonName;

        return $this;
    }

    /**
     * Get contactPersonName
     *
     * @return string 
     */
    public function getContactPersonName()
    {
        return $this->contactPersonName;
    }

    /**
     * Set contactPersonTitle
     *
     * @param string $contactPersonTitle
     * @return Sponsor
     */
    public function setContactPersonTitle($contactPersonTitle)
    {
        $this->contactPersonTitle = $contactPersonTitle;

        return $this;
    }

    /**
     * Get contactPersonTitle
     *
     * @return string 
     */
    public function getContactPersonTitle()
    {
        return $this->contactPersonTitle;
    }

    /**
     * Set contactPersonEmail
     *
     * @param string $contactPersonEmail
     * @return Sponsor
     */
    public function setContactPersonEmail($contactPersonEmail)
    {
        $this->contactPersonEmail = $contactPersonEmail;

        return $this;
    }

    /**
     * Get contactPersonEmail
     *
     * @return string 
     */
    public function getContactPersonEmail()
    {
        return $this->contactPersonEmail;
    }

    /**
     * Set facebookPage
     *
     * @param string $facebookPage
     * @return Sponsor
     */
    public function setFacebookPage($facebookPage)
    {
        $this->facebookPage = $facebookPage;

        return $this;
    }

    /**
     * Get facebookPage
     *
     * @return string 
     */
    public function getFacebookPage()
    {
        return $this->facebookPage;
    }

    /**
     * Set linkedInPage
     *
     * @param string $linkedInPage
     * @return Sponsor
     */
    public function setLinkedInPage($linkedInPage)
    {
        $this->linkedInPage = $linkedInPage;

        return $this;
    }

    /**
     * Get linkedInPage
     *
     * @return string 
     */
    public function getLinkedInPage()
    {
        return $this->linkedInPage;
    }

    public function getNumberOfEmployees()
    {
        return $this->numberOfEmployees;
    }

    public function setNumberOfEmployees($numberOfEmployees)
    {
        $this->numberOfEmployees = $numberOfEmployees;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
            'industry' => $this->industry,
            'annual_revenue' => $this->annualRevenue,
            'presentation' => $this->presentation,
            'contact_person_name' => $this->contactPersonName,
            'contact_person_title' => $this->contactPersonTitle,
            'contact_person_email' => $this->contactPersonEmail,
            'facebook_page' => $this->facebookPage,
            'linkedin_page' => $this->linkedInPage,
            'number_of_employees' => $this->numberOfEmployees,
        );
    }
    
}
