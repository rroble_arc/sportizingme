<?php

namespace Arcanys\SportizingmeBundle\Entity\Company;

use Arcanys\SportizingmeBundle\Entity\AbstractTimestamptable;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="Arcanys\SportizingmeBundle\Entity\Repository\CompanyRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"company" = "Company", "sponsor" = "Sponsor"})
 */
class Company extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name.'';
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
        );
    }
    
    public function toArrayList()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
        );
    }
    
}
