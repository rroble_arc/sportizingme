<?php

namespace Arcanys\SportizingmeBundle\Entity;

use Arcanys\SportizingmeBundle\Model\Helper;
use Arcanys\SportizingmeBundle\Model\LocationInterface;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Gym
 *
 * @ORM\Table(name="gym")
 * @ORM\Entity(repositoryClass="Arcanys\SportizingmeBundle\Entity\Repository\GymRepository")
 */
class Gym extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=128)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Address", cascade={"persist"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Address
     */
    protected $address;

    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $image;

    /**
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     * 
     * @var boolean
     */
    protected $approved = false;

    /**
     * @ORM\Column(name="hashtags", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $hashtags;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Gym
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param Address $address
     * @return Gym
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address 
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function toArray(LocationInterface $location = null)
    {
        $result = array(
            'id' => $this->id,
            'name' => $this->name,
            'hashtag' => $this->getHashtags(true),
            'hashtag_facebook' => $this->getHashtags(true, '#'),
            'hashtag_twitter' => $this->getHashtags(true, '@'),
            'approved' => $this->approved,
            'image' => $this->image ? $this->image->getLink() : null,
            'address' => $this->address ? $this->address->toArray() : null,
        );
        if ($location && $this->address) {
            $result['distance'] = $this->address->getDistance($location);
        }
        return $result;
    }

    public function getDistance(LocationInterface $location)
    {
        if ($this->address) {
            return $this->address->getDistance($location);
        }
    }

    public function __toString()
    {
        return $this->name.'';
    }

    /**
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }

    public function setImage(Image $image = null)
    {
        $this->image = $image;
    }

    public function isApproved()
    {
        return $this->approved;
    }

    public function setApproved($approved)
    {
        $this->approved = $approved;
    }
    
    public function getHashtags($process = false, $prefix = null)
    {
        return Helper::getHashtags($this->hashtags, $process, $prefix);
    }

    public function setHashtags($hashtags)
    {
        $this->hashtags = $hashtags;
    }
    
    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->address? $this->address->getLongitude(): null;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->address? $this->address->getLatitude(): null;
    }
    
    public function getZoom()
    {
        if ($this->getLatitude() && $this->getLongitude()) {
            return 12;
        }
        return 4;
    }
    
}
