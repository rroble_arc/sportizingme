<?php

namespace Arcanys\SportizingmeBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin
 *
 * @ORM\Table(name="gym_user")
 * @ORM\Entity()
 */
class Gym extends User
{

    /**
     * @ORM\Column(name="name", type="string", length=64)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;

    public function __construct()
    {
        $this->addRole(static::ROLE_GYM);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function __toString()
    {
        return $this->name.'';
    }

}
