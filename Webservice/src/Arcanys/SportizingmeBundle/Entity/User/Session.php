<?php

namespace Arcanys\SportizingmeBundle\Entity\User;

use Arcanys\SecurityBundle\Model\UserInterface;
use Arcanys\SecurityBundle\Model\UserSessionInterface;
use Arcanys\SportizingmeBundle\Entity\AbstractTimestamptable;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="user_session")
 * @ORM\Entity()
 */
class Session extends AbstractTimestamptable implements Timestamptable, UserSessionInterface
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="sessions", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * 
     * @var User
     */
    protected $user;

    /**
     * @ORM\Column(name="token", type="string", length=255)
     * 
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $plainToken;

    public function __construct()
    {
        $this->plainToken = hash('sha1', microtime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Session
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set lastAccessDate
     *
     * @param \DateTime $lastAccessDate
     * @return Session
     */
    public function setLastAccessDate(\DateTime $lastAccessDate)
    {
        $this->dateUpdated = $lastAccessDate;

        return $this;
    }

    /**
     * Get lastAccessDate
     *
     * @return \DateTime 
     */
    public function getLastAccessDate()
    {
        if ($this->dateUpdated) {
            return $this->dateUpdated;
        }
        return $this->dateCreated;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Session
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getPlainToken()
    {
        return $this->plainToken;
    }

    /**
     * @param string $plainToken
     * @return Session
     */
    public function setPlainToken($plainToken)
    {
        $this->plainToken = $plainToken;

        return $this;
    }

}
