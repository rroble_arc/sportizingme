<?php

namespace Arcanys\SportizingmeBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin
 *
 * @ORM\Table(name="user_admin")
 * @ORM\Entity()
 */
class Admin extends User
{

    /**
     * @ORM\Column(name="name", type="string", length=64)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;

    public function __construct()
    {
        $this->addRole(static::ROLE_ADMIN);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function __toString()
    {
        return $this->name.'';
    }

}
