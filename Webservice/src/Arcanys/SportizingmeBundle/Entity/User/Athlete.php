<?php

namespace Arcanys\SportizingmeBundle\Entity\User;

use Arcanys\SportizingmeBundle\Entity\Address;
use Arcanys\SportizingmeBundle\Entity\Company\Company;
use Arcanys\SportizingmeBundle\Entity\Gym;
use Arcanys\SportizingmeBundle\Entity\Image;
use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Arcanys\SportizingmeBundle\Entity\School;
use Arcanys\SportizingmeBundle\Entity\Workout;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Athlete
 *
 * @ORM\Table(name="athlete")
 * @ORM\Entity(repositoryClass="Arcanys\SportizingmeBundle\Entity\Repository\AthleteRepository")
 * @Assert\Callback(methods={"validateWeight", "validateHeight"})
 * @UniqueEntity(fields={"facebookId"}, groups={"Default","RegistrationViaFacebook","UpdateAthleteProfile"})
 */
class Athlete extends User
{

    /**
     * @ORM\Column(name="first_name", type="string", length=64)
     * @Assert\NotBlank(groups={"Default","Registration","RegistrationViaFacebook"})
     * 
     * @var string
     */
    protected $firstname;

    /**
     * @ORM\Column(name="last_name", type="string", length=64)
     * @Assert\NotBlank(groups={"Default","Registration","RegistrationViaFacebook"})
     * 
     * @var string
     */
    protected $lastname;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="profile_picture", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $profilePicture;

    /**
     * @ORM\Column(name="date_of_birth", type="date", nullable=true)
     * 
     * @var \DateTime
     */
    protected $dateOfBirth;

    /**
     * @ORM\Column(name="short_presentation", type="text", nullable=true)
     * 
     * @var string
     */
    protected $shortPresentation;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumn(name="address", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Address
     */
    protected $address;

    /**
     * @ORM\Column(name="sports", type="simple_array", nullable=true)
     * @Assert\All({
     *      @Assert\Choice(callback = {"Arcanys\SportizingmeBundle\Model\Enums", "getSports"})
     * })
     * 
     * @var array
     */
    protected $sports;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Gym", cascade={"persist"})
     * @ORM\JoinColumn(name="training_location", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Gym
     */
    protected $trainingLocation;

    /**
     * @ORM\Column(name="training_frequency", type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(1)
     * @Assert\LessThanOrEqual(7)
     * 
     * @var integer
     */
    protected $trainingFrequency;

    /**
     * @ORM\Column(name="weight", type="float", nullable=true)
     * 
     * @var float
     */
    protected $weight;

    /**
     * @ORM\Column(name="weight_unit", type="string", length=16, nullable=true)
     * @Assert\Choice(callback = {"Arcanys\SportizingmeBundle\Model\Enums", "getWeightUnits"})
     * 
     * @var string
     */
    protected $weightUnit;

    /**
     * @ORM\Column(name="height", type="float", nullable=true)
     * 
     * @var float
     */
    protected $height;

    /**
     * @ORM\Column(name="height_unit", type="string", length=16, nullable=true)
     * @Assert\Choice(callback = {"Arcanys\SportizingmeBundle\Model\Enums", "getHeightUnits"})
     * 
     * @var string
     */
    protected $heightUnit;

    /**
     * @ORM\Column(name="training_shirt_size", type="string", length=5, nullable=true)
     * @Assert\Choice(callback = {"Arcanys\SportizingmeBundle\Model\Enums", "getShirtSizes"})
     * 
     * @var string
     */
    protected $trainingShirtSize;

    /**
     * @ORM\Column(name="personal_income", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $personalIncome;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\School", cascade={"persist"})
     * @ORM\JoinColumn(name="school_university", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var School
     */
    protected $school;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Company\Company", cascade={"persist"})
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Company
     */
    protected $company;

    /**
     * @ORM\Column(name="max_education_level", type="string", length=64, nullable=true)
     * 
     * @var string
     */
    protected $maxEducationLevel;

    /**
     * @ORM\Column(name="paypal", type="string", length=128, nullable=true)
     * 
     * @var string
     */
    protected $paypal;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Message\Offer", cascade={"persist"})
     * @ORM\JoinColumn(name="current_offer", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Offer
     */
    protected $currentOffer;

    /**
     * @ORM\Column(name="cash", type="float", nullable=true)
     * 
     * @var float
     */
    protected $cash;

    /**
     * @ORM\Column(name="facebook_link", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $facebookLink;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"RegistrationViaFacebook"})
     * 
     * @var string
     */
    protected $facebookId;

    /**
     * @ORM\Column(name="facebook_email", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $facebookEmail;

    /**
     * @ORM\Column(name="facebook_friends", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $facebookFriends;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="facebook_photo", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $facebookPhoto;

    /**
     * @ORM\Column(name="twitter_link", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $twitterLink;

    /**
     * @ORM\Column(name="twitter_username", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $twitterUsername;

    /**
     * @ORM\Column(name="twitter_followers", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $twitterFollowers;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="twitter_photo", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $twitterPhoto;

    /**
     * @ORM\ManyToMany(targetEntity="Athlete", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="friend",
     *      joinColumns={@ORM\JoinColumn(name="athlete_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="friend_athlete_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\ORM\PersistentCollection
     * */
    protected $friends;

    /**
     * @ORM\OneToMany(targetEntity="Arcanys\SportizingmeBundle\Entity\Workout", mappedBy="athlete")
     * @var \Doctrine\ORM\PersistentCollection
     * */
    protected $workouts;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Workout", cascade={"persist"})
     * @ORM\JoinColumn(name="last_workout", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var Workout
     */
    protected $lastWorkout;

    /**
     * @ORM\Column(name="gender", type="string", length=16, nullable=true)
     * @Assert\Choice(callback = {"Arcanys\SportizingmeBundle\Model\Enums", "getGenders"})
     * 
     * @var string
     */
    protected $gender;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="cover_photo", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $coverPhoto;

    /**
     * @ORM\Column(name="device_identifier", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $deviceId;

    /**
     * @ORM\Column(name="tokenInformationExpirationDateKey", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $tokenInformationExpirationDateKey;

    /**
     * @ORM\Column(name="tokenInformationLoginTypeLoginKey", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $tokenInformationLoginTypeLoginKey;

    /**
     * @ORM\Column(name="tokenInformationPermissionsKey", type="simple_array", nullable=true)
     * 
     * @var array
     */
    protected $tokenInformationPermissionsKey;

    /**
     * @ORM\Column(name="tokenInformationRefreshDateKey", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $tokenInformationRefreshDateKey;

    /**
     * @ORM\Column(name="tokenInformationTokenKey", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $tokenInformationTokenKey;

    /**
     * @ORM\Column(name="twitterAccessToken", type="text", nullable=true)
     * 
     * @var string
     */
    protected $twitterAccessToken;

    /**
     * @ORM\OneToMany(targetEntity="Arcanys\SportizingmeBundle\Entity\Message\Offer", mappedBy="athlete")
     * @var \Doctrine\ORM\PersistentCollection
     * */
    protected $offers;

    public function __construct()
    {
        $this->addRole(static::ROLE_ATHLETE);
        $this->sports = array();
        $this->trainingFrequency = 1;
        $this->cash = 0;

        parent::__construct();
    }

    public function __toString()
    {
        return $this->getName() . '';
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Athlete
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Athlete
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     * @return Athlete
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime 
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set shortPresentation
     *
     * @param string $shortPresentation
     * @return Athlete
     */
    public function setShortPresentation($shortPresentation)
    {
        $this->shortPresentation = $shortPresentation;

        return $this;
    }

    /**
     * Get shortPresentation
     *
     * @return string 
     */
    public function getShortPresentation()
    {
        return $this->shortPresentation;
    }

    /**
     * Set sports
     *
     * @param array $sports
     * @return Athlete
     */
    public function setSports($sports)
    {
        $this->sports = $sports;

        return $this;
    }

    /**
     * Get sports
     *
     * @return array 
     */
    public function getSports()
    {
        return $this->sports;
    }

    /**
     * Set trainingFrequency
     *
     * @param integer $trainingFrequency
     * @return Athlete
     */
    public function setTrainingFrequency($trainingFrequency)
    {
        $this->trainingFrequency = $trainingFrequency;

        return $this;
    }

    /**
     * Get trainingFrequency
     *
     * @return integer 
     */
    public function getTrainingFrequency()
    {
        return $this->trainingFrequency;
    }

    /**
     * Set twitterUsername
     *
     * @param string $twitterUsername
     * @return Athlete
     */
    public function setTwitterUsername($twitterUsername)
    {
        $this->twitterUsername = $twitterUsername;

        return $this;
    }

    /**
     * Get twitterUsername
     *
     * @return string 
     */
    public function getTwitterUsername()
    {
        return $this->twitterUsername;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return Athlete
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weightUnit
     *
     * @param string $weightUnit
     * @return Athlete
     */
    public function setWeightUnit($weightUnit)
    {
        $this->weightUnit = $weightUnit;

        return $this;
    }

    /**
     * Get weightUnit
     *
     * @return string 
     */
    public function getWeightUnit()
    {
        return $this->weightUnit;
    }

    /**
     * Set height
     *
     * @param float $height
     * @return Athlete
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set heightUnit
     *
     * @param string $heightUnit
     * @return Athlete
     */
    public function setHeightUnit($heightUnit)
    {
        $this->heightUnit = $heightUnit;

        return $this;
    }

    /**
     * Get heightUnit
     *
     * @return string 
     */
    public function getHeightUnit()
    {
        return $this->heightUnit;
    }

    /**
     * Set trainingShirtSize
     *
     * @param string $trainingShirtSize
     * @return Athlete
     */
    public function setTrainingShirtSize($trainingShirtSize)
    {
        $this->trainingShirtSize = $trainingShirtSize;

        return $this;
    }

    /**
     * Get trainingShirtSize
     *
     * @return string 
     */
    public function getTrainingShirtSize()
    {
        return $this->trainingShirtSize;
    }

    /**
     * Set personalIncome
     *
     * @param string $personalIncome
     * @return Athlete
     */
    public function setPersonalIncome($personalIncome)
    {
        $this->personalIncome = $personalIncome;

        return $this;
    }

    /**
     * Get personalIncome
     *
     * @return string 
     */
    public function getPersonalIncome()
    {
        return $this->personalIncome;
    }

    /**
     * Set maxEducationLevel
     *
     * @param string $maxEducationLevel
     * @return Athlete
     */
    public function setMaxEducationLevel($maxEducationLevel)
    {
        $this->maxEducationLevel = $maxEducationLevel;

        return $this;
    }

    /**
     * Get maxEducationLevel
     *
     * @return string 
     */
    public function getMaxEducationLevel()
    {
        return $this->maxEducationLevel;
    }

    /**
     * Set paypal
     *
     * @param string $paypal
     * @return Athlete
     */
    public function setPaypal($paypal)
    {
        $this->paypal = $paypal;

        return $this;
    }

    /**
     * Get paypal
     *
     * @return string 
     */
    public function getPaypal()
    {
        return $this->paypal;
    }

    /**
     * Set profilePicture
     *
     * @param Image $profilePicture
     * @return Athlete
     */
    public function setProfilePicture(Image $profilePicture = null)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * Get profilePicture
     *
     * @return Image 
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * Set address
     *
     * @param Address $address
     * @return Athlete
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set trainingLocation
     *
     * @param Gym $trainingLocation
     * @return Athlete
     */
    public function setTrainingLocation(Gym $trainingLocation = null)
    {
        $this->trainingLocation = $trainingLocation;

        return $this;
    }

    /**
     * Get trainingLocation
     *
     * @return Gym 
     */
    public function getTrainingLocation()
    {
        return $this->trainingLocation;
    }

    /**
     * Set school
     *
     * @param School $school
     * @return Athlete
     */
    public function setSchool(School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return School 
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set company
     *
     * @param Company $company
     * @return Athlete
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return Athlete
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    public function validateWeight(ExecutionContextInterface $context)
    {
        if ($this->weight !== null && $this->weight > 0 && !$this->weightUnit) {
            $context->addViolationAt('weightUnit', 'Please specify weight unit.');
        }
    }

    public function validateHeight(ExecutionContextInterface $context)
    {
        if ($this->height !== null && $this->height > 0 && !$this->heightUnit) {
            $context->addViolationAt('heightUnit', 'Please specify height unit.');
        }
    }

    /**
     * @return Sponsor
     */
    public function getCurrentSponsor()
    {
        $offer = $this->getCurrentOffer();
        if ($offer) {
            return $offer->getSponsor();
        }
    }

    /**
     * Get currentOffer
     * 
     * @return Offer
     */
    public function getCurrentOffer()
    {
        return $this->currentOffer;
    }

    /**
     * Set currentOffer
     * 
     * @param Offer $currentOffer
     */
    public function setCurrentOffer(Offer $currentOffer = null)
    {
        $this->currentOffer = $currentOffer;
    }

    public function getCash()
    {
        return $this->cash;
    }

    public function setCash($cash)
    {
        $this->cash = $cash;
    }

    public function addEarning($earning)
    {
        $this->cash += floatval($earning);
    }

    public function getFacebookFriends()
    {
        return $this->facebookFriends;
    }

    public function setFacebookFriends($facebookFriends)
    {
        $this->facebookFriends = $facebookFriends;
    }

    public function getFacebookEmail()
    {
        return $this->facebookEmail;
    }

    public function setFacebookEmail($facebookEmail)
    {
        $this->facebookEmail = $facebookEmail;
    }

    public function getTwitterFollowers()
    {
        return $this->twitterFollowers;
    }

    public function setTwitterFollowers($twitterFollowers)
    {
        $this->twitterFollowers = $twitterFollowers;
    }

    /**
     * @return Image
     */
    public function getFacebookPhoto()
    {
        return $this->facebookPhoto;
    }

    public function setFacebookPhoto(Image $facebookPhoto = null)
    {
        $this->facebookPhoto = $facebookPhoto;
    }

    /**
     * @return Image
     */
    public function getTwitterPhoto()
    {
        return $this->twitterPhoto;
    }

    public function setTwitterPhoto(Image $twitterPhoto = null)
    {
        $this->twitterPhoto = $twitterPhoto;
    }

    public function getFacebookLink()
    {
        return $this->facebookLink;
    }

    public function setFacebookLink($facebookLink)
    {
        $this->facebookLink = $facebookLink;
    }

    public function getTwitterLink()
    {
        return $this->twitterLink;
    }

    public function setTwitterLink($twitterLink)
    {
        $this->twitterLink = $twitterLink;
    }

//    <<<<<<<<<<<<<<<<<<<<<<<<<<<< REMOVE
    private function getRandomCoverPhotos()
    {
        $photos = array(
            'http://www.fbcoolcovers.com/wp-content/uploads/2012/05/rugby-football-cover-pictures-500x185.jpg',
            'http://coverphotobook.com/facebook-cover-photos-timeline/download/hobbies/Whats-Your-Game-Sports-Facebook-Cover.jpg',
            'http://www.fbcoolcovers.com/wp-content/uploads/2012/05/sports-facebook-cover-500x185.jpg',
            'http://www.big5sportinggoods.com/content/category-header-graphics/fitness/fitness_3.jpg',
            'http://beta.active.com/Assets/Fitness/460x345/MMA-Workout.jpg',
            'http://img.howcast.com/topic_thumbnails/856/shutterstock_62210722_xxxlarge.jpg',
            'http://www.vancouversun.com/cms/binary/8466005.jpg',
            'http://blogs.mirror.co.uk/wrestling-wwe-ufc-mma/css/jimimanuwa2.jpg',
            'http://www.lifefitness.com/static-assets/image/blog/Posts/Science_Behind_the_Fight.jpg',
            'http://aaronlindberg.com/wp-content/uploads/2012/08/fitness_0014.jpg',
            'http://blog.hella-life.com/wp-content/uploads/2013/08/7188327283_8dd604c69f_o.jpg',
            'http://a.espncdn.com/photo/2013/0412/mma_g_tate1x_576.jpg',
            'http://a.espncdn.com/photo/2010/1013/mma_e_bisping-hendo01_576.jpg',
            'http://media.supercheapauto.com.au/rebel/images/menu-items/7f313e1e-eb61-49db-9d33-2149de4070ec.jpg',
            'http://o2fitnessclubs.com/wp-content/uploads/2013/02/url1.jpg',
            'http://img.grouponcdn.com/deal/s8rZHqAVmQpJrYDizcgF/Zn-440x267.jpg',
        );
        shuffle($photos);
        return $photos[0];
    }

//    <<<<<<<<<<<<<<<<<<<<<<<<<<<< REMOVE

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array_merge(parent::toArray(), array(
            'gender' => $this->gender,
            'first_name' => $this->firstname,
            'last_name' => $this->lastname,
            'sports' => $this->sports,
            'weight' => $this->weight,
            'weight_unit' => $this->weightUnit,
            'height' => $this->height,
            'height_unit' => $this->heightUnit,
            'training_frequency' => $this->trainingFrequency,
            'date_of_birth' => $this->formatDate($this->dateOfBirth),
            'short_presentation' => $this->shortPresentation,
            'profile_picture' => $this->getImageLink($this->profilePicture),
            'cover_photo' => $this->getImageLink($this->coverPhoto),
            'cover_photo' => $this->getRandomCoverPhotos(), // FIXME: remove

            'facebook_id' => $this->facebookId,
            'facebook_link' => $this->facebookLink,
            'facebook_email' => $this->facebookEmail,
            'facebook_friends' => $this->facebookFriends,
            'facebook_photo' => $this->getImageLink($this->facebookPhoto),
            'twitter_link' => $this->twitterLink,
            'twitter_username' => $this->twitterUsername,
            'twitter_followers' => $this->twitterFollowers,
            'twitter_photo' => $this->getImageLink($this->twitterPhoto),
            'paypal' => $this->paypal,
            'personal_income' => $this->personalIncome,
            'max_education_level' => $this->maxEducationLevel,
            'training_shirt_size' => $this->trainingShirtSize,
            'address' => $this->address ? $this->address->toArray() : null,
            'training_location' => $this->trainingLocation ? $this->trainingLocation->toArray() : null,
            'school_university' => $this->school ? $this->school->toArray() : null,
            'company' => $this->company ? $this->company->toArray() : null,
            'workout' => $this->getLastWorkout() ? $this->getLastWorkout()->toArray() : null,
            // tokens
            'tokenInformationExpirationDateKey' => $this->tokenInformationExpirationDateKey,
            'tokenInformationLoginTypeLoginKey' => $this->tokenInformationLoginTypeLoginKey,
            'tokenInformationPermissionsKey' => $this->tokenInformationPermissionsKey,
            'tokenInformationRefreshDateKey' => $this->tokenInformationRefreshDateKey,
            'tokenInformationTokenKey' => $this->tokenInformationTokenKey,
            'twitterAccessToken' => $this->twitterAccessToken,
            'deviceId' => $this->deviceId,
            
            'cash' => floatval($this->cash),
        ));
        return $array;
    }

    public function toArrayWithSponsor()
    {
        $array = $this->toArray();
        $array['sponsor'] = $this->getCurrentSponsor() ? $this->getCurrentSponsor()->toArray() : null;
        return $array;
    }

    /**
     * @param \DateTime $datetime
     */
    public function formatDate($datetime)
    {
        if ($datetime) {
            return $datetime->format('d/m/Y');
        }
    }

    /**
     * @param Image $image
     */
    public function getImageLink($image)
    {
        if ($image) {
            return $image->getLink();
        }
    }

    public function getFriends()
    {
        return $this->friends;
    }

    public function setFriends($friends)
    {
        $this->friends = $friends;
    }

    public function isFriend(Athlete $athlete)
    {
        $friends = $this->getFriends();
        foreach ($friends as $friend) {
            if ($friend->getId() == $athlete->getId()) {
                return true;
            }
        }
        return false;
    }

    public function addFriend(Athlete $athlete)
    {
        $this->friends[] = $athlete;
    }

    public function removeFriend(Athlete $athlete)
    {
        $this->friends->removeElement($athlete);
    }

    public function getWorkouts()
    {
        return $this->workouts;
    }

    public function setWorkouts($workouts)
    {
        $this->workouts = $workouts;
    }

    /**
     * @return Workout
     */
    public function getLastWorkout()
    {
        return $this->lastWorkout;
    }

    public function setLastWorkout(Workout $lastWorkout = null)
    {
        $this->lastWorkout = $lastWorkout;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return Image
     */
    public function getCoverPhoto()
    {
        return $this->coverPhoto;
    }

    public function setCoverPhoto(Image $coverPhoto = null)
    {
        $this->coverPhoto = $coverPhoto;
    }

    public function equals(Athlete $other)
    {
        if ($this->id) {
            return $this->id === $other->getId();
        }
        return spl_object_hash($this) == spl_object_hash($other);
    }

    public function getDeviceId()
    {
        return $this->deviceId;
    }

    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    }

    public function getName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getFriendIds()
    {
        $result = array();
        if (!$this->friends)
            return $result;
        foreach ($this->friends->getValues() as $friend) {
            $result[] = $friend->getId();
        }
        return $result;
    }

    public function getHeightStr()
    {
        if ($this->height) {
            return $this->height . ' ' . $this->heightUnit;
        }
    }

    public function getWeightStr()
    {
        if ($this->weight) {
            return $this->weight . ' ' . $this->weightUnit;
        }
    }

    public function getSportsStr()
    {
        return implode(', ', $this->sports);
    }

    public function getTokenInformationExpirationDateKey()
    {
        return $this->tokenInformationExpirationDateKey;
    }

    public function setTokenInformationExpirationDateKey($tokenInformationExpirationDateKey)
    {
        $this->tokenInformationExpirationDateKey = $tokenInformationExpirationDateKey;
    }

    public function getTokenInformationLoginTypeLoginKey()
    {
        return $this->tokenInformationLoginTypeLoginKey;
    }

    public function setTokenInformationLoginTypeLoginKey($tokenInformationLoginTypeLoginKey)
    {
        $this->tokenInformationLoginTypeLoginKey = $tokenInformationLoginTypeLoginKey;
    }

    public function getTokenInformationPermissionsKey()
    {
        return (array) $this->tokenInformationPermissionsKey;
    }

    public function setTokenInformationPermissionsKey($tokenInformationPermissionsKey)
    {
        if (is_string($tokenInformationPermissionsKey)) {
            $tokenInformationPermissionsKey = explode(',', $tokenInformationPermissionsKey);
        }
        $this->tokenInformationPermissionsKey = $tokenInformationPermissionsKey;
    }

    public function getTokenInformationRefreshDateKey()
    {
        return $this->tokenInformationRefreshDateKey;
    }

    public function setTokenInformationRefreshDateKey($tokenInformationRefreshDateKey)
    {
        $this->tokenInformationRefreshDateKey = $tokenInformationRefreshDateKey;
    }

    public function getTokenInformationTokenKey()
    {
        return $this->tokenInformationTokenKey;
    }

    public function setTokenInformationTokenKey($tokenInformationTokenKey)
    {
        $this->tokenInformationTokenKey = $tokenInformationTokenKey;
    }

    public function getTwitterAccessToken()
    {
        return $this->twitterAccessToken;
    }

    public function setTwitterAccessToken($twitterAccessToken)
    {
        $this->twitterAccessToken = $twitterAccessToken;
    }

    /**
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getAcceptedOffers()
    {
        /* @var $offer \Arcanys\SportizingmeBundle\Entity\Message\Offer */
        $offers = array_filter($this->offers->getValues(), function($offer) {
            return $offer->isAccepted() || $offer->isDone();
        });
        // TODO: sort
        return $offers;
    }

    public function getWorkoutsWithoutSponsor()
    {
        /* @var $workout \Arcanys\SportizingmeBundle\Entity\Workout */
        $workouts = array_filter($this->workouts->getValues(), function($workout) {
            return $workout->getOffer() == null;
        });
        return $workouts;
    }
    
    public function getSelfProgress()
    {
        $result = array();
        $workouts = $this->getWorkoutsWithoutSponsor();
        
        $weekRange = $this->getWeekDateRange();
        $weekly = array_filter($workouts, function($workout) use($weekRange) {
            return $workout->getStart() >= $weekRange->start || 
                    $workout->getStart() <= $weekRange->end;
        });
        $result[] = (object) array(
            'name' => 'This week',
            'current' => ($wc=count($weekly)),
            'total' => ($wt=7),
            'progress' => $this->getProgress($wc, $wt),
        );
        
        $monthRange = $this->getMonthDateRange();
        $monthly = array_filter($workouts, function($workout) use($monthRange) {
            return $workout->getStart() >= $monthRange->start || 
                    $workout->getStart() <= $monthRange->end;
        });
        $result[] = (object) array(
            'name' => 'This month',
            'current' => ($mc=count($monthly)),
            'total' => ($mt=date('t')),
            'progress' => $this->getProgress($mc, $mt),
        );
        
        $result[] = (object) array(
            'name' => 'All time',
            'total' => ($tc=count($workouts)),
            'current' => $tc,
            'progress' => 100,
        );
        return $result;
    }
    
    protected function getWeekDateRange()
    {
        $ts = time();
        $startOfWeek = mktime(0, 0, 0, date('n', $ts), date('j', $ts) - date('N', $ts) + 1);
        $endOfWeek = strtotime('+6days +23hours +59minutes +59seconds', $startOfWeek);
        $start = new \DateTime();
        $start->setTimestamp($startOfWeek);
        $end = new \DateTime();
        $end->setTimestamp($endOfWeek);
        return (object) array(
            'start' => $start, 
            'end' => $end
        );
    }
    
    protected function getMonthDateRange()
    {
        $ts = time();
        $startOfMonth = mktime(0, 0, 0, date('n', $ts), 1);
        $endOfMonth = mktime(23, 59, 59, date('n', $ts), date('t', $ts));
        $start = new \DateTime();
        $start->setTimestamp($startOfMonth);
        $end = new \DateTime();
        $end->setTimestamp($endOfMonth);
        return (object) array(
            'start' => $start, 
            'end' => $end
        );
    }
    
    public function getProgress($actual, $total)
    {
        if (!$total) return 0;
        $progress = number_format(100*$actual / $total, 2);
        return str_replace('.00', '', $progress);
    }
    
    public function getNextOffer()
    {
        if ($this->currentOffer && !$this->currentOffer->isDone()) {
            return $this->currentOffer;
        }
        $offers = array_filter($this->offers->getValues(), function(Offer $offer) {
            return $offer->isAccepted();
        });
        if (!$offers) return null;
        uasort($offers, function(Offer $a, $b) {
            if ($a->getStartDate() == $b->getStartDate()) {
                return 0;
            }
            return ($a->getStartDate() < $b->getStartDate()) ? -1 : 1;
        });
        $offer = current($offers);
        $this->setCurrentOffer($offer);
        return $offer;
    }
    
}
