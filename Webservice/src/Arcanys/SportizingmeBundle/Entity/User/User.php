<?php

namespace Arcanys\SportizingmeBundle\Entity\User;

use Arcanys\SecurityBundle\Model\UserInterface;
use Arcanys\SecurityBundle\Model\UserSessionInterface;
use Arcanys\SportizingmeBundle\Entity\AbstractTimestamptable;
use Arcanys\SportizingmeBundle\Model\Timestamptable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="unique_idx", columns={"email"})})
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *  "athlete" = "Athlete", "sponsor" = "Sponsor", "admin" = "Admin", "gym" = "Gym"
 * })
 * @UniqueEntity(fields={"email"}, groups={"Default","Registration","RegistrationViaFacebook"})
 */
abstract class User extends AbstractTimestamptable implements Timestamptable, UserInterface
{

    const ROLE_DEFAULT = 'ROLE_USER',
            ROLE_ATHLETE = 'ROLE_ATHLETE',
            ROLE_SPONSOR = 'ROLE_SPONSOR',
            ROLE_GYM = 'ROLE_GYM',
            ROLE_ADMIN = 'ROLE_ADMIN',
            ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="email", type="string", length=128)
     * @Assert\NotBlank(groups={"Default","Registration","RegistrationViaFacebook"})
     * @Assert\Email(groups={"Default","Registration","RegistrationViaFacebook"})
     * 
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\NotBlank(groups={"Registration"})
     * 
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     * 
     * @var string
     */
    protected $salt;

    /**
     * @ORM\Column(name="roles", type="simple_array")
     * 
     * @var array
     */
    protected $roles;

    /**
     * @ORM\OneToMany(targetEntity="Session", mappedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="token_id", referencedColumnName="id", onDelete="SET NULL")
     * 
     * @var \Doctrine\ORM\PersistentCollection
     */
    protected $sessions;

    /**
     * @ORM\Column(name="password_token", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $passwordToken;

    public function __construct($createSession = true)
    {
        if ($createSession) {
            $this->addSession(new Session());
        }
    }
    
    public function addSession(UserSessionInterface $token)
    {
        $token->setUser($this);
        $this->sessions[] = $token;
    }
    
    public function getSessions()
    {
        return $this->sessions;
    }

    public function setSessions($sessions)
    {
        $this->sessions = $sessions;
    }
    
    /**
     * @return Session
     */
    public function getToken()
    {
        if (is_array($this->sessions)) {
            return current($this->sessions);
        }
        return $this->sessions->first();
    }
    
    public function getSession()
    {
        return $this->getToken();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password, $checkSalt = true)
    {
        if ($checkSalt && $this->password !== $password) {
            $this->getSalt(true); // generate new salt
        }
        
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt($new = false)
    {
        if (!$this->salt || $new) {
            $this->salt = hash('sha1', rand());
        }
        return $this->salt;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return string 
     */
    public function getRoles()
    {
        $this->addRole(static::ROLE_DEFAULT);
        return array_unique($this->roles);
    }
    
    public function addRole($role)
    {
        $this->roles[] = $role;
        
        return $this;
    }

    public function eraseCredentials()
    {
        
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'email' => $this->email,
        );
    }
    
    public function getPasswordToken()
    {
        return $this->passwordToken;
    }

    public function setPasswordToken($passwordToken)
    {
        $this->passwordToken = $passwordToken;
    }

}
