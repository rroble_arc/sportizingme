<?php

namespace Arcanys\SportizingmeBundle\Entity\User;

use Arcanys\SportizingmeBundle\Entity\Address;
use Arcanys\SportizingmeBundle\Entity\Company\Sponsor as Company;
use Arcanys\SportizingmeBundle\Entity\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Arcanys\SportizingmeBundle\Model\Helper;

/**
 * Sponsor
 *
 * @ORM\Table(name="sponsor")
 * @ORM\Entity()
 */
class Sponsor extends User
{

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Company\Sponsor", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Company
     */
    protected $company;

    /**
     * @ORM\Column(name="paypal", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $paypal;
    
    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumn(name="postal_address", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Address
     */
    protected $address;

    /**
     * @ORM\ManyToOne(targetEntity="Arcanys\SportizingmeBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="logo", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Valid()
     * 
     * @var Image
     */
    protected $logo;

    /**
     * @ORM\Column(name="hashtags", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $hashtags;

    public function __construct()
    {
        $this->addRole(static::ROLE_SPONSOR);
        $this->company = new Company();

        parent::__construct();
    }

    /**
     * Set company
     *
     * @param Company $company
     * @return Sponsor
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    public function toArray()
    {
        $array = array(
            'email' => $this->email,
            'paypal' => $this->paypal,
            'address' => $this->address ? $this->address->toArray() : null,
            'logo' => $this->getImageLink($this->logo),
            'hashtag' => $this->getHashtags(true),
            'hashtag_facebook' => $this->getHashtags(true, '#'),
            'hashtag_twitter' => $this->getHashtags(true, '@'),
        );
        if ($this->company) {
            $array = array_merge($array, $this->company->toArray());
        }
        $array['id'] = $this->id;
        return $array;
    }
    
    public function __toString()
    {
        return $this->getName().'';
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        if ($this->company) {
            return $this->company->getName();
        }
    }
    
    public function getPaypal()
    {
        return $this->paypal;
    }

    public function setPaypal($paypal)
    {
        $this->paypal = $paypal;
    }

    /**
     * Set address
     *
     * @param Address $address
     * @return Sponsor
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address 
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * @param Image $image
     */
    public function getImageLink($image)
    {
        if ($image) {
            return $image->getLink();
        }
    }

    /**
     * Set logo
     *
     * @param Image $logo
     * @return Sponsor
     */
    public function setLogo(Image $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return Image 
     */
    public function getLogo()
    {
        return $this->logo;
    }
    
    public function getHashtags($process = false, $prefix = null)
    {
        return Helper::getHashtags($this->hashtags, $process, $prefix);
    }

    public function setHashtags($hashtags)
    {
        $this->hashtags = $hashtags;
    }

}
