<?php

namespace Arcanys\SportizingmeBundle\EventListener;

use Arcanys\SecurityBundle\Model\UserInterface;
use Arcanys\SecurityBundle\Model\UserSessionInterface;
use Arcanys\SportizingmeApiBundle\Model\AthleteRegistrationParams;
use Arcanys\SportizingmeApiBundle\Model\SponsorshipOfferParams;
use Arcanys\SportizingmeBundle\Entity\Message\Feed;
use Arcanys\SportizingmeBundle\Entity\Message\Message;
use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class EntityListener
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof UserInterface) {
            $password = $this->encodePassword($entity, $entity->getPassword(), $entity->getSalt());
            $entity->setPassword($password, false);
        } elseif ($entity instanceof UserSessionInterface) {
            $token = $this->encodePassword($entity, $entity->getPlainToken(), get_class($entity));
            $entity->setToken($token);
        } elseif ($entity instanceof Message && !$entity instanceof Feed) {
            // send push notification
            if (($deviceId = $entity->getAthlete()->getDeviceId())) {
                $message = new iOSMessage();
                $message->setAPSBadge(1+$this->countUnread($entity->getAthlete()));
                $message->setMessage($entity->getTitle());
                $message->setDeviceIdentifier($deviceId);
                $this->get('rms_push_notifications')->send($message);
            }
            
            // send sponsorship email
            if ($entity instanceof Offer) {
                $this->sendSponsorshipEmail($entity);
            }
        }
    }
    
    private function countUnread(Athlete $athlete)
    {
        $em = $this->container->get('doctrine');
        $result = $em->getRepository('ArcanysSportizingmeBundle:Message\Message')
                ->findBy(array(
                    'athlete' => $athlete->getId(),
                ));
        $unread = 0;
        foreach ($result as $msg) {
            if ($msg instanceof Feed) {
                continue;
            }
            if ($msg->isRead()) {
                continue;
            }
            $unread ++;
        }
        return $unread;
    }

    private function sendSponsorshipEmail(Offer $offer)
    {
        try {
            $athlete = $offer->getAthlete();
            $params = new SponsorshipOfferParams($offer);
            $this->get('arcanys_sportizingme.mailer')
                 ->setMessageTemplate('Sponsorship offer')
                 ->send($athlete->getEmail(), $params->getParams());
        } catch(\Exception $ex) {
            $this->get('logger')->err($ex);
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof UserInterface) {
            if ($args->hasChangedField('password')) {
                if ($entity->getPassword()) {
                    $password = $this->encodePassword($entity, $entity->getPassword(), $entity->getSalt());
                    $args->setNewValue('password', $password);
                } else {
                    $args->setNewValue('password', $args->getOldValue('password'));
                    $args->setNewValue('salt', $args->getOldValue('salt'));
                }
            }
        }
    }

    private function encodePassword($entity, $password, $salt)
    {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);
        return $encoder->encodePassword($password, $salt);
    }
    
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Athlete) {
            if ($entity instanceof Athlete) {
                $this->sendWelcomeMessage($entity);
            }
        }
    }

    private function sendWelcomeMessage(Athlete $athlete)
    {
        $translator = $this->get('translator');
        $title = $translator->trans('athlete.welcome_message.title', array(), 'Sportizingme');
        $content = $translator->trans('athlete.welcome_message.body', array(), 'Sportizingme');
        $message = new Message();
        $message->setTitle($title)
                ->setMessage($content)
                ->setAthlete($athlete);
        
        $feed = new Feed();
        $feed->setAthlete($athlete)
                ->setMessage($content);
        
        $em = $this->get('doctrine')->getManager();
        $em->persist($message);
        $em->persist($feed);
        $em->flush();
                
        // send mail
        $this->sendConfirmationEmail($athlete);
    }


    private function sendConfirmationEmail(Athlete $athlete)
    {
        try {
            $params = new AthleteRegistrationParams($athlete);
            $this->get('arcanys_sportizingme.mailer')
                 ->setMessageTemplate('Athlete registration')
                 ->send($athlete->getEmail(), $params->getParams());
        } catch(\Exception $ex) {
            $this->get('logger')->err($ex);
        }
    }
    
    private function get($id)
    {
        return $this->container->get($id);
    }

}
