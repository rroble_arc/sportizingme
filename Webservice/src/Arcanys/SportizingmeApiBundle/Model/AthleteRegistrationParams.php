<?php

namespace Arcanys\SportizingmeApiBundle\Model;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AthleteRegistrationParams extends AbstractReplacementParams
{

    protected $athlete;

    public function __construct(Athlete $athlete)
    {
        $this->athlete = $athlete;
    }

    public function getObject()
    {
        return $this->athlete;
    }

    public function getFields()
    {
        return array('name', 'firstname', 'lastname', 'email');
    }

}
