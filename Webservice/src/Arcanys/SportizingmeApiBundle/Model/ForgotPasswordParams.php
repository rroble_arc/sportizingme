<?php

namespace Arcanys\SportizingmeApiBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ForgotPasswordParams extends AbstractReplacementParams
{

    protected $link;

    public function __construct($link)
    {
        $this->link = $link;
    }

    public function getFields()
    {
        return array('link');
    }

    public function getObject()
    {
        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

}
