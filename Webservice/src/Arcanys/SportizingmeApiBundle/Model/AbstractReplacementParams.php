<?php

namespace Arcanys\SportizingmeApiBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
abstract class AbstractReplacementParams
{

    protected function getParam($name, $prefix = '@')
    {
        $getter = 'get' . ucfirst($name);
        if (method_exists($this, $getter)) {
            return call_user_method($getter, $this);
        }
        if (($object = $this->getObject()) && method_exists($object, $getter)) {
            return call_user_method($getter, $object);
        }
        return $prefix . $name;
    }
    
    public abstract function getObject();
    
    public abstract function getFields();
    
    public function getParams($prefix = '@')
    {
        $params = array();
        foreach ((array) $this->getFields() as $param) {
            $params[$prefix . $param] = $this->getParam($param, $prefix);
        }
        return $params;
    }

}
