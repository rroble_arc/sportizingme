<?php

namespace Arcanys\SportizingmeApiBundle\Model;

use Arcanys\SportizingmeBundle\Entity\Message\Offer;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SponsorshipOfferParams extends AthleteRegistrationParams
{

    protected $offer;

    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
        parent::__construct($offer->getAthlete());
    }

    public function getFields()
    {
        return array_merge(parent::getFields(), array(
                    'sponsor', 'from', 'to', 'trainings', 'weeks', 'hours', 'payment', 'photo'));
    }

    public function getSponsor()
    {
        return $this->offer->getSponsor()->getName();
    }

    public function getFrom()
    {
        return $this->offer->getStartDate()->format('F j, Y');
    }

    public function getTo()
    {
        return $this->offer->getEndDate()->format('F j, Y');
    }

    public function getTrainings()
    {
        return $this->offer->getTrainingsPerWeek();
    }

    public function getWeeks()
    {
        return $this->offer->getNumberOfWeeks();
    }

    public function getHours()
    {
        return $this->offer->getHoursPerSession();
    }

    public function getPayment()
    {
        return $this->offer->getRate();
    }

    public function getPhoto()
    {
        return $this->offer->getAds()->getLink();
    }

}
