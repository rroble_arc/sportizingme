<?php

namespace Arcanys\SportizingmeApiBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ResetPasswordParams extends AbstractReplacementParams
{

    protected $password;

    public function __construct($password)
    {
        $this->password = $password;
    }

    public function getFields()
    {
        return array('password');
    }

    public function getObject()
    {
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

}
