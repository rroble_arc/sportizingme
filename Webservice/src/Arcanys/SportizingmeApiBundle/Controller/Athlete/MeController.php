<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/athlete")
 * @method \Arcanys\SportizingmeBundle\Entity\User\Athlete getUser()
 */
class MeController extends Controller
{

    /**
     * @Route("/me", name="athlete_me")
     */
    public function meAction()
    {
        $result = $this->get('arcanys_sportizingme.athlete_manager')
                ->getMe($this->getUser());
        return new JsonResponse($result);
    }

}
