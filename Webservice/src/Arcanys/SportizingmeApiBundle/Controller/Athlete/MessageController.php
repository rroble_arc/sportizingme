<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Arcanys\SportizingmeBundle\Entity\Message\Notification;
use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/athlete")
 * @method Athlete getUser()
 */
class MessageController extends Controller
{
    /**
     * @Route("/message/read/{id}", name="athlete_read_message")
     */
    public function readAction($id)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $message = $em->getRepository('ArcanysSportizingmeBundle:Message\Message')->find($id);
            if (!$message) {
                throw new \Exception('Message does not exists.');
            }
            $message->setRead(true);
            $em->persist($message);
            
            // create notification
            $notification = Notification::create($message)->forReadMessage();
            $em->persist($notification);
            
            $em->flush();
            
            $result = $message->toArray();
            return ApiResponse::success($result);
            
        } catch(\Exception $ex) {
            return ApiResponse::error($ex->getMessage(), array('id' => $id));
        }
    }
    
    /**
     * @Route("/messages", name="athlete_messages")
     */
    public function messagesAction()
    {
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('ArcanysSportizingmeBundle:Message\Message')->createQueryBuilder('m');
        $qb->where($qb->expr()->eq('m.athlete', $this->getUser()->getId()));
        $orx = $qb->expr()->orX();
        $orx->add('m INSTANCE OF Arcanys\SportizingmeBundle\Entity\Message\Message');
        $orx->add('m INSTANCE OF Arcanys\SportizingmeBundle\Entity\Message\Offer');
        $qb->andWhere($orx);
        $qb->orderBy('m.id', 'DESC');
        
        $messages = $qb->getQuery()->getResult();
        foreach ($messages as $message) {
            if ($message instanceof Offer && !$message->isPending()) continue;
            $result[] = $message->toArray();
        }
                
        return new JsonResponse($result);
    }
    
}
