<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/athlete")
 * @method \Arcanys\SportizingmeBundle\Entity\User\Athlete getUser()
 */
class SearchAthleteController extends Controller
{

    /**
     * @Route("/list/{limit}/{offset}", name="athlete_list", defaults={"limit":25, "offset":0})
     */
    public function listAction($limit, $offset)
    {
        $name = $this->getRequest()->get('name');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $athletes = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')
                ->findAllAthletes($limit, $offset, $user, $name);
        $result = array();
        /* @var $athlete Athlete */
        foreach ($athletes as $athlete) {
            $array = $athlete->toArray();
            $array['friend'] = $user->isFriend($athlete) ? '1' : '0';
            $result[] = $array;
        }
        return new JsonResponse($result);
    }
    
    /**
     * @Route("/search/facebook/{limit}/{offset}", name="athlete_list_facebook", defaults={"limit":25, "offset":0})
     * @Method("POST")
     */
    public function searchFromFacebookAction($limit, $offset)
    {
        $result = array();
        $ids = (array) $this->getRequest()->get('id');
        if ($ids) {
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $athletes = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')
                    ->findBy(array('facebookId' => $ids), null, $limit, $offset);
            /* @var $athlete \Arcanys\SportizingmeBundle\Entity\User\Athlete */
            foreach ($athletes as $athlete) {
                if ($athlete->equals($user)) continue;
                $array = $athlete->toArray();
                $array['friend'] = $user->isFriend($athlete) ? '1' : '0';
                $result[] = $array;
            }
        }
        return new JsonResponse($result);
    }
    
    /**
     * @Route("/search/contacts/{limit}/{offset}", name="athlete_list_contacts", defaults={"limit":25, "offset":0})
     * @Method("POST")
     */
    public function searchFromContactsAction($limit, $offset)
    {
        $result = array();
        $emails = (array) $this->getRequest()->get('email');
        if ($emails) {
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $athletes = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')
                    ->findBy(array('email' => $emails), null, $limit, $offset);
            /* @var $athlete \Arcanys\SportizingmeBundle\Entity\User\Athlete */
            foreach ($athletes as $athlete) {
                if ($athlete->equals($user)) continue;
                $array = $athlete->toArray();
                $array['friend'] = $user->isFriend($athlete) ? '1' : '0';
                $result[] = $array;
            }
        }
        return new JsonResponse($result);
    }
    
}
