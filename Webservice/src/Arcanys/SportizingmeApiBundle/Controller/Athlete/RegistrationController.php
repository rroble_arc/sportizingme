<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Arcanys\SportizingmeApiBundle\Form\AthleteType;
use Arcanys\SportizingmeApiBundle\Security\Token\WebserviceToken;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Arcanys\SportizingmeBundle\Model\FormUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class RegistrationController extends Controller
{
    
    /**
     * @Route("/athlete/signup", name="athlete_signup")
     * @Method("POST")
     */
    public function signupAction($viaFacebook = false)
    {
        $form = $this->createForm(new AthleteType(), new Athlete(), array(
            'validation_groups' => $viaFacebook ? 'RegistrationViaFacebook' : 'Registration',
        ));
        $form->submit($this->getRequest());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {
                $athlete = $form->getData();
                if ($viaFacebook) {
                    $athlete->setFacebookPhoto($athlete->getProfilePicture());
                    if (!$athlete->getFacebookEmail()) {
                        $athlete->setFacebookEmail($athlete->getEmail());
                    }
                }
                
                $em->persist($athlete);
                $em->flush();
                
                $token = new WebserviceToken($athlete->getToken()->getPlainToken(), $athlete->getRoles());
                $token->setUser($athlete);
                return ApiResponse::success('New athlete has been registered', $token->toArray());
            } catch(\Exception $ex) {
                $this->get('logger')->err($ex);
                return ApiResponse::error($ex->getMessage());
            }
        }
        return FormUtil::with($form, $this->getRequest(), $this->get('logger'))
            ->getFormErrors();
    }

    /**
     * @Route("/athlete/signup/facebook", name="athlete_signup_facebook")
     * @Method("POST")
     */
    public function signupViaFacebookAction()
    {
        return $this->signupAction(true);
    }
    
}
