<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Arcanys\SportizingmeApiBundle\Form\Checkin1Type;
use Arcanys\SportizingmeApiBundle\Form\Checkin2Type;
use Arcanys\SportizingmeBundle\Entity\Message\Feed;
use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Arcanys\SportizingmeBundle\Entity\Workout;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Arcanys\SportizingmeBundle\Model\Enums;
use Arcanys\SportizingmeBundle\Model\FormUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/athlete/workout")
 * @method \Arcanys\SportizingmeBundle\Entity\User\Athlete getUser()
 */
class WorkoutController extends Controller
{

    /**
     * @Route("/checkin", name="athlete_checkin")
     * @Method("POST")
     */
    public function checkinAction()
    {
        $athlete = $this->getUser();
        $workout = new Workout();
        $workout->setAthlete($athlete);
        
        $form = $this->createForm(new Checkin1Type(), $workout);
        $form->submit($this->getRequest());
        if ($form->isValid()) {
            // Record start of workout
            try {
                $athlete->setTrainingLocation($workout->getGym());
                $athlete->setLastWorkout($workout);
    
                $currentOffer = $athlete->getNextOffer();
                if ($currentOffer) {
                    $workout->setOffer($currentOffer);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($athlete);
                $em->persist($workout);
                $em->flush();

                $result = array(
                    'athlete' => $athlete->toArray(),
                    'sponsor' => $athlete->getCurrentSponsor() ? $athlete->getCurrentSponsor()->toArray() : null,
                    'training_location' => $athlete->getTrainingLocation() ? $athlete->getTrainingLocation()->toArray() : null,
                );
                return ApiResponse::success($result);
                
            } catch(\Exception $ex) {
                return ApiResponse::error($ex->getMessage());
            }
        }
        
        return FormUtil::with($form, $this->getRequest(), $this->get('logger'))
                ->getFormErrors();
    }

    /**
     * @Route("/checkin2", name="athlete_checkin2")
     * @Method("POST")
     */
    public function checkin2Action()
    {
        $athlete = $this->getUser();
        $lastWorkout = $athlete->getLastWorkout();
        try {
            if (!$lastWorkout || !$lastWorkout->isOpen()) {
                throw new \Exception('You are not checked in.');
            }

            $form = $this->createForm(new Checkin2Type(), $lastWorkout, array(
                'validation_groups' => 'Checkin2',
            ));
            $form->submit($this->getRequest(), false);
            if ($form->isValid()) {

                // create feed
                $this->onSaveWorkout($lastWorkout, Enums::TYPE_CHECKIN);

                $em = $this->getDoctrine()->getManager();
                $em->persist($lastWorkout);
                $em->flush();

                $result = array(
                    'athlete' => $athlete->toArray(),
                    'sponsor' => $athlete->getCurrentSponsor() ? $athlete->getCurrentSponsor()->toArray() : null,
                    'training_location' => $athlete->getTrainingLocation() ? $athlete->getTrainingLocation()->toArray() : null,
                );
                return ApiResponse::success($result);
            }
        } catch(\Exception $ex) {
            $this->get('logger')->err($ex->getMessage(), array($ex));
            return ApiResponse::error($ex->getMessage());
        }
        
        return FormUtil::with($form, $this->getRequest(), $this->get('logger'))
                ->getFormErrors();
    }

    /**
     * @Route("/checkout", name="athlete_checkout")
     */
    public function checkoutAction()
    {
        try {
            $athlete = $this->getUser();
            $lastWorkout = $athlete->getLastWorkout();
            if ($lastWorkout && $lastWorkout->isOpen()) {
                $em = $this->getDoctrine()->getManager();
                $lastWorkout->setEnd(new \DateTime());

                $earning = $this->calculateEarning($lastWorkout, $athlete->getCurrentOffer());
                $lastWorkout->setEarning($earning);
                
                if (($currentOffer = $athlete->getCurrentOffer())) {
                    $athlete->addEarning($earning);
                    
                    if ($currentOffer->getProgress() >= 100) {
                        $currentOffer->setStatus(Enums::STATUS_DONE);
                        $em->persist($currentOffer);
                    }
                }
                
                // create feed
                $this->onSaveWorkout($lastWorkout, Enums::TYPE_CHECKOUT);

                $em->persist($lastWorkout);
                $em->persist($athlete);
                $em->flush();
                
                return ApiResponse::success(array(
                    'earning' => $earning,
                    'sponsor' => $athlete->getCurrentSponsor() ? $athlete->getCurrentSponsor()->getName() : null,
                ));
            }
        } catch (\Exception $ex) {
            return ApiResponse::error($ex->getMessage());
        }
        return ApiResponse::error('You have not started workout yet.');
    }
    
    private function onSaveWorkout(Workout $workout, $type)
    {
        $feed = new Feed($type);
        $feed->setAthlete($workout->getAthlete());
        $feed->setPicture($workout->getPicture());
        $feed->setMessage($workout->getCaption().'');
        $feed->setGym($workout->getGym());

        $em = $this->getDoctrine()->getManager();
        $em->persist($feed);
    }

    private function calculateEarning(Workout $workout, Offer $offer = null)
    {
        $rate = $offer ? $offer->getRate() : Enums::DEAL_BASE_RATE;
        $hours = $workout->getHours();
        return number_format($rate * $hours, 2);
    }

}
