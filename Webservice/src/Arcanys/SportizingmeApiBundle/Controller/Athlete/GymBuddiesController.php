<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/athlete")
 * @method \Arcanys\SportizingmeBundle\Entity\User\Athlete getUser()
 */
class GymBuddiesController extends Controller
{
    
    /**
     * @Route("/buddies/{gymId}", name="athlete_buddies")
     */
    public function buddiesAction($gymId)
    {
        $result = array(
            'friends' => array(),
            'last_people' => array(),
        );
        $athlete = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $friends = $athlete->getFriends();
        $friendIds = array();
        foreach ($friends as $value) {
            $friendIds[] = $value->getId();
        }
        
        if ($friendIds) {
            // friends
            $friendsInGym = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')
                    ->findFriendsInGym($gymId, $friendIds);
            foreach ($friendsInGym as $friend) {
                $result['friends'][] = $friend->toArray();
            }
        }
        
        // exclude current athlete
        $friendIds[] = $athlete->getId();
        
        // last people (not friends)
        $people = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')
                ->findLastAthletesInGym($gymId, $friendIds);
        foreach ($people as $person) {
            $result['last_people'][] = $person->toArray();
        }
        
        return new JsonResponse($result);
    }

}
