<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Arcanys\SportizingmeBundle\Entity\Message\Notification;
use Arcanys\SportizingmeBundle\Entity\Message\Offer;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Arcanys\SportizingmeBundle\Model\Enums;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/athlete/offer")
 * @method Athlete getUser()
 */
class OfferController extends Controller
{

    /**
     * @Route("/decline", name="decline_offer")
     * @Method("POST")
     */
    public function declineAction()
    {
        try {
            /* @var $offer Offer */
            $offer = $this->getOffer($this->getRequest());
            $offer->setStatus(Enums::STATUS_DECLINED);
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            
            // create notification
            $notification = Notification::create($offer)->forDeclinedOffer();
            $em->persist($notification);
            
            $em->flush();

            return ApiResponse::success('Offer was declined.');
        } catch(\Exception $ex) {
            return ApiResponse::error($ex->getMessage());
        }
    }

    /**
     * @Route("/accept", name="accept_offer")
     * @Method("POST")
     */
    public function acceptAction()
    {
        try {
            /* @var $offer Offer */
            $offer = $this->getOffer($this->getRequest());
            $offer->setStatus(Enums::STATUS_ACCEPTED);
            
            // TODO: validate overlapping date with other offer?
            
            $em = $this->getDoctrine()->getManager();
            
            $athlete = $this->getUser();
            if (!$athlete->getCurrentOffer() || $athlete->getCurrentOffer()->isDone()) {
                $athlete->setCurrentOffer($offer);
                $em->persist($athlete);
            }
            
            // create notification
            $notification = Notification::create($offer)->forAcceptedOffer();
            $em->persist($notification);
            
            $em->persist($offer);
            $em->flush();

            return ApiResponse::success('Offer was accepted.');
        } catch(\Exception $ex) {
            return ApiResponse::error($ex->getMessage());
        }
    }
    
    /**
     * @return Offer
     */
    private function getOffer(Request $request)
    {
        $offerId = $request->get('offerId');
        if ($offerId) {
            $em = $this->getDoctrine()->getManager();
            $offer = $em->getRepository('ArcanysSportizingmeBundle:Message\Offer')
                    ->findOneBy(array(
                        'id' => $offerId, 
                        'status' => Enums::STATUS_PENDING,
                        'athlete' => $this->getUser()->getId()));
            if ($offer) {
                return $offer;
            }
        }
        throw new \Exception('Offer does not exists.');
    }
    
    /**
     * @Route("/{id}", name="offer_details")
     */
    public function offerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $offer = $em->getRepository('ArcanysSportizingmeBundle:Message\Offer')
                ->find($id);
        if ($offer) {
            return ApiResponse::success($offer->toArray());
        } else {
            return ApiResponse::error('Offer does not exists.');
        }
    }
    
}
