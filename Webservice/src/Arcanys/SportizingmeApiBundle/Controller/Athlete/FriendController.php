<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/athlete/friend")
 * @method \Arcanys\SportizingmeBundle\Entity\User\Athlete getUser()
 */
class FriendController extends Controller
{

    /**
     * @Route("/list/{limit}/{offset}", name="athlete_friends", defaults={"limit":25, "offset":0})
     */
    public function listAction($limit, $offset)
    {
        $user = $this->getUser();
        $friends = $user->getFriends()->slice($offset, $limit);
        $result = array();
        /* @var $athlete Athlete */
        foreach ($friends as $athlete) {
            $array = $athlete->toArray();
            $array['friend'] = $user->isFriend($athlete) ? '1' : '0';
            $result[] = $array;
        }
        return new JsonResponse($result);
    }

    /**
     * @Route("/toggle", name="friend_toggle")
     * @Method("POST")
     */
    public function toggleAction(Request $request)
    {
        $request = $this->getRequest();
        $id = $request->get('athleteId');
        $isFriend = (bool) $request->get('friend');
        
        try {
            if ($id == $this->getUser()->getId()) {
                throw new \Exception('You cannot be friend with yourself.');
            }

            $em = $this->getDoctrine()->getManager();
            $athlete = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')->find($id);
            if (!$athlete) {
                throw new \Exception('Athlete does not exists.');
            }

            if ($isFriend) {
                return $this->removeFriend($athlete);
            } else {
                return $this->addFriend($athlete);
            }
        } catch(\Exception $ex) {
            return ApiResponse::error($ex->getMessage(), array('id' => $id));
        }
    }
    
    private function addFriend(Athlete $athlete)
    {
        $user = $this->getUser();
        
        if ($user->isFriend($athlete)) {
            throw new \Exception('Already friend.');
        }
        $user->addFriend($athlete);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return ApiResponse::success('Friend added.', array(
            'id' => $athlete->getId(),
            'friend' => '1',
        ));
    }
    
    private function removeFriend(Athlete $athlete)
    {
        $user = $this->getUser();
        if (!$user->isFriend($athlete)) {
            throw new \Exception('Not friend.');
        }
        $user->removeFriend($athlete);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return ApiResponse::success('Friend removed.', array(
            'id' => $athlete->getId(),
            'friend' => '0',
        ));
    }

}
