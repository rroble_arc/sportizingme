<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Arcanys\SportizingmeApiBundle\Form\AthleteType;
use Arcanys\SportizingmeApiBundle\Model\ChangePasswordParams;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Arcanys\SportizingmeBundle\Model\FormUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Util\StringUtils;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @method \Arcanys\SportizingmeBundle\Entity\User\Athlete getUser()
 * @Route("/athlete")
 */
class ProfileController extends Controller
{
    
    /**
     * @Route("/profile/{id}", name="athlete_profile", defaults={"id":null})
     * @Method("GET")
     */
    public function profileAction($id)
    {
        $user = $this->getUser();
        if (!$id) {
            return ApiResponse::success($user->toArray());
        } else {
            $em = $this->getDoctrine()->getManager();
            $athlete = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')
                    ->find($id);
            if (!$athlete) {
                return ApiResponse::error('Athlete does not exists.');
            } else {
                $array = $athlete->toArray();
                $array['friend'] = $user->isFriend($athlete);
                return ApiResponse::success($array);
            }
        }
    }
    
    /**
     * @Route("/profile", name="athlete_profile_update")
     * @Method("POST")
     */
    public function updateProfileAction()
    {
        $athlete = $this->getUser();
        $form = $this->createForm(new AthleteType(), $athlete, array(
            'validation_groups' => 'UpdateAthleteProfile',
        ));

        $form->submit($this->getRequest(), false);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($athlete);
                $em->flush();
                return ApiResponse::success('Your profile has been updated', $athlete->toArray());
            } catch(\Exception $ex) {
                $this->get('logger')->err($ex);
                return ApiResponse::error($ex->getMessage(), array($athlete));
            }
        }
        return FormUtil::with($form, $this->getRequest(), $this->get('logger'))
            ->getFormErrors();
    }
    
    
    /**
     * @Route("/delete", name="athlete_profile_delete")
     * @Method("POST")
     */
    public function deleteAccountAction()
    {
        try {
            $id = $this->getRequest()->get('id');
            $user = $this->getUser();
            if (!$user || $user->getId() != $id) {
                throw new \Exception('Action aborted.');
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            return ApiResponse::success('Your account has been deleted.');
        } catch(\Exception $ex) {
            return ApiResponse::error($ex->getMessage());
        }
        return ApiResponse::success();
    }
    
    
    /**
     * @Route("/change-password", name="athlete_change_password")
     * @Method("POST")
     */
    public function changePasswordAction()
    {
        $request = $this->getRequest();
        $oldPassword = (string) $request->get('old');
        $newPassword = (string) $request->get('new');
        
        $athlete = $this->getUser();
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($athlete);
        if (!$encoder->isPasswordValid($athlete->getPassword(), $oldPassword, $athlete->getSalt())) {
            return ApiResponse::error('Old password is invalid.');
        }
        if (!$newPassword) {
            return ApiResponse::error('New password is invalid.');
        }
        
        if (StringUtils::equals($newPassword, $oldPassword)) {
            return ApiResponse::success('Your password is not changed.');
        }
        
        $athlete->setPassword($newPassword);
        $em = $this->getDoctrine()->getManager();
        $em->persist($athlete);
        $em->flush();
         
        // send email
        $params = new ChangePasswordParams($newPassword);
        $this->get('arcanys_sportizingme.mailer')
             ->setMessageTemplate('Change password')
             ->send($athlete->getEmail(), $params->getParams());
        
        return ApiResponse::success('Your password is successfully changed.');
    }
    
}
