<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Athlete;

use Arcanys\SportizingmeBundle\Entity\Message\FeedLike;
use Arcanys\SportizingmeBundle\Entity\Message\Notification;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @method Athlete getUser()
 * @Route("/athlete/feed")
 */
class FeedController extends Controller
{
    
    /**
     * @Route("/like", name="athlete_like_feed")
     */
    public function likeAction()
    {
        try {
            $id = $this->getRequest()->get('feedId');
            $em = $this->getDoctrine()->getManager();
            $feed = $em->getRepository('ArcanysSportizingmeBundle:Message\Feed')->find($id);
            if (!$feed) {
                throw new \Exception('Feed does not exists.');
            }
            $athlete = $this->getUser();
            $like = $em->getRepository('ArcanysSportizingmeBundle:Message\FeedLike')->findOneBy(array(
                'feed' => $feed->getId(),
                'athlete' => $athlete->getId(),
            ));
            if (!$like) {
                $like = new FeedLike($feed, $athlete);
            }
            $like->setLiked(!$like->isLiked());            
            $em->persist($like);
            
            // create notification
            $notification = Notification::create($feed);
            if ($like->isLiked()) {
                $notification->forLikedFeed();
            } else {
                $notification->forUnlikedFeed();
            }
            $em->persist($notification);
            
            $em->flush();
            
            $result = $feed->toArray();
            $result['liked'] = $like->isLiked();
            return ApiResponse::success($result);
            
        } catch(\Exception $ex) {
            return ApiResponse::error($ex->getMessage(), array('id' => $id));
        }
    }

    /**
     * @Route("/{limit}/{offset}", name="athlete_social_feed", defaults={"limit":20, "offset":0})
     */
    public function feedAction($limit, $offset)
    {
        $max = intval($limit) < 1 ? 20 : $limit;
        $first = intval($offset) < 0 ? 0 : $offset;
        $athlete = $this->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('ArcanysSportizingmeBundle:Message\Feed')->createQueryBuilder('f');
        $qb->leftJoin('f.likes', 'l')->setFirstResult($first)->setMaxResults($max)
                ->orderBy('f.dateCreated', 'DESC');
        
        $qb->where($qb->expr()->eq('f.athlete', $athlete->getId()));
        
        if (($friendIds = $athlete->getFriendIds())) {
            $andX = $qb->expr()->andX();
            $andX->add($qb->expr()->in('f.athlete', $friendIds));
            $andX->add($qb->expr()->like('f.title', $qb->expr()->literal('check%')));
            $qb->orWhere($andX);
        }
        
        $feeds = $qb->getQuery()->getResult();
        $result = array();
        foreach ($feeds as $feed) {
            $r = $feed->toArray();
            $r['liked'] = $feed->hasLiked($athlete);
            // self?
            $result[] = $r;
        }
        return new JsonResponse($result);
    }
    
}
