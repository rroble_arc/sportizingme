<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Crud;

use Arcanys\SportizingmeBundle\Entity\School;
use Arcanys\SportizingmeApiBundle\Form\SchoolType;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Arcanys\SportizingmeBundle\Model\FormUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/school")
 */
class SchoolController extends Controller
{

    /**
     * @Route("/add", name="school_add")
     * @Method("POST")
     */
    public function addAction()
    {
        $form = $this->createForm(new SchoolType(), new School());
        $form->submit($this->getRequest());
        if ($form->isValid()) {
            try {
                $school = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($school);
                $em->flush();
                return ApiResponse::success($school->toArray());
            } catch (\Exception $ex) {
                return ApiResponse::error($ex->getMessage());
            }
        }
        
        return FormUtil::with($form, $this->getRequest(), $this->get('logger'))
                ->getFormErrors();
    }

    /**
     * @Route("/list", name="school_list")
     */
    public function listAction()
    {
        $search = $this->getRequest()->get('search');
        $em = $this->getDoctrine()->getManager();
        $schools = $em->getRepository('ArcanysSportizingmeBundle:School')
                ->search($search);
        $result = array();
        /* @var $school School */
        foreach ($schools as $school) {
            $result[] = $school->toArray();
        }
        return new JsonResponse($result);
    }

}
