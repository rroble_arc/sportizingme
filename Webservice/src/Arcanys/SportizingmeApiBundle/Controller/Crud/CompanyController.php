<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Crud;

use Arcanys\SportizingmeBundle\Entity\Company\Company;
use Arcanys\SportizingmeApiBundle\Form\CompanyType;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Arcanys\SportizingmeBundle\Model\FormUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/company")
 */
class CompanyController extends Controller
{

    /**
     * @Route("/add", name="company_add")
     * @Method("POST")
     */
    public function addAction()
    {
        $form = $this->createForm(new CompanyType(), new Company());
        $form->submit($this->getRequest());
        if ($form->isValid()) {
            try {
                $company = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($company);
                $em->flush();
                return ApiResponse::success($company->toArray());
            } catch (\Exception $ex) {
                return ApiResponse::error($ex->getMessage());
            }
        }

        return FormUtil::with($form, $this->getRequest(), $this->get('logger'))
                ->getFormErrors();
    }

    /**
     * @Route("/list", name="company_list")
     */
    public function listAction()
    {
        $search = $this->getRequest()->get('search');
        $em = $this->getDoctrine()->getManager();
        $companies = $em->getRepository('ArcanysSportizingmeBundle:Company\Company')
                ->search($search);
        $result = array();
        /* @var $company Company */
        foreach ($companies as $company) {
            $result[] = $company->toArrayList();
        }
        return new JsonResponse($result);
    }

}
