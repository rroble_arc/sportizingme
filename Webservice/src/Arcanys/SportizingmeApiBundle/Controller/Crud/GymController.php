<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Crud;

use Arcanys\SportizingmeApiBundle\Form\GymType;
use Arcanys\SportizingmeBundle\Entity\Address;
use Arcanys\SportizingmeBundle\Entity\Gym;
use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Arcanys\SportizingmeBundle\Model\FormUtil;
use Arcanys\SportizingmeBundle\Model\GymListSorter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/gym")
 */
class GymController extends Controller
{

    /**
     * @Route("/add", name="gym_add")
     * @Method("POST")
     */
    public function addAction()
    {
        $form = $this->createForm(new GymType(), new Gym());
        $form->submit($this->getRequest());
        if ($form->isValid()) {
            try {
                $gym = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($gym);
                $em->flush();
                return ApiResponse::success($gym->toArray());
            } catch (\Exception $ex) {
                return ApiResponse::error($ex->getMessage());
            }
        }
        
        return FormUtil::with($form, $this->getRequest(), $this->get('logger'))
            ->getFormErrors();
    }

    /**
     * @Route("/list", name="gym_list")
     */
    public function listAction()
    {
        $request = $this->getRequest();
        $longitude = $request->get('longitude');
        $latitude = $request->get('latitude');
        
        $distanceCalc = new Address();
        $distanceCalc->setLongitude($longitude);
        $distanceCalc->setLatitude($latitude);
        
        $include = (array) $request->get('new');
        
        $em = $this->getDoctrine()->getManager();
        $gyms = $em->getRepository('ArcanysSportizingmeBundle:Gym')
                ->findActiveGyms($include);

        // sort by distance
        $sorter = new GymListSorter($longitude, $latitude);
        uasort($gyms, array($sorter, 'sort'));
        
        $result = array();
        /* @var $gym Gym */
        foreach ($gyms as $gym) {
            $result[$gym->getId()] = $gym->toArray($distanceCalc);
        }
        
        $athlete = $this->getUser();
        if ($athlete && $athlete instanceof Athlete) {
            $visited = $em->getRepository('ArcanysSportizingmeBundle:Workout')
                    ->getVisitedGymsByAthlete($athlete);
            // 1. get most visited gym
            // 2. get all visited gyms and check if not visited there
            $topGymId = null;
            if ($visited) {
                uasort($visited, function($a, $b) {
                    if ($a['visits'] == $b['visits']) {
                        return 0;
                    }
                    return ($a['visits'] > $b['visits']) ? -1 : 1;
                });
                $topGymId = $visited[0]['gym'];
                $result[$topGymId]['top'] = true;
                $visitedGymIds = array();
                foreach ($visited as $value) {
                    $visitedGymIds[$value['gym']] = $value;
                }
                $notVisited = array_diff(array_keys($result), array_keys($visitedGymIds));
                foreach ($notVisited as $id) {
                    $result[$id]['visited'] = false;
                }
            }
            // 3. get random friend visited to this gym
            $friends = $athlete->getFriends();
            if (count($friends) > 0) {
                $friendIds = array();
                foreach ($friends as $friend) {
                    $friendIds[] = $friend->getId();
                }
                $friendVisits = $em->getRepository('ArcanysSportizingmeBundle:Workout')
                        ->getFriendsVisitedGyms($friendIds, array_keys($result));
                foreach ((array)$friendVisits as $gymId => $visit) {
                    shuffle($visit);
                    $result[$gymId]['friend'] = $visit[0]->getName();
                }
            }
            
            // fill everything
            $result = array_map(function ($row) {
                if (!array_key_exists('top', $row)) $row['top'] = false;
                if (!array_key_exists('visited', $row)) $row['visited'] = false;
                if (!array_key_exists('friend', $row)) $row['friend'] = null;
                return $row;
            }, $result);
                
            if ($topGymId) {
                // move top gym to the top spot
                $topGym = $result[$topGymId];
                unset($result[$topGymId]);
                array_unshift($result, $topGym);
            }
        }
        
        return new JsonResponse(array_values($result));
    }

}
