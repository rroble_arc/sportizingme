<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Listing;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\NoResultException;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class Worlddb
{

    protected $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function getCities($country, $state = null, $offset = 0, $limit = 100)
    {
        $respository = $this->em->getRepository('ArcanysSportizingmeBundle:Worlddb\City');
        /** @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $respository->createQueryBuilder('c');
        $qb->distinct('c.name')
                ->where($qb->expr()->isNotNull('c.name'))
                ->andWhere($qb->expr()->eq('c.country', $qb->expr()->literal($this->getCountry($country))));
        if ($state) {
            $qb->andWhere($qb->expr()->eq('c.region', $qb->expr()->literal($this->getState($state))));
        }
        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit < 1 ? 100 : $limit);

        $list = $qb->getQuery()->getResult();
        return $list;
    }

    public function getStates($country)
    {
        $respository = $this->em->getRepository('ArcanysSportizingmeBundle:Worlddb\Region');
        /** @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $respository->createQueryBuilder('r');
        $literal = $qb->expr()->literal($this->getCountry($country));
        $qb->distinct('r.name')
                ->where($qb->expr()->isNotNull('r.name'))
                ->andWhere($qb->expr()->eq('r.country', $literal));
        $list = $qb->getQuery()->getResult();
        return $list;
    }
    
    private function getState($name)
    {
        $respository = $this->em->getRepository('ArcanysSportizingmeBundle:Worlddb\Region');
        $qb = $respository->createQueryBuilder('r');
        $literal = $qb->expr()->literal($name);
        $orx = $qb->expr()->orX();
        $orx->add($qb->expr()->eq('r.code', $literal));
        $orx->add($qb->expr()->eq('r.name', $literal));
        $qb->where($orx);
        try {
            $country = $qb->getQuery()->getSingleResult();
            return $country->getCode();
        } catch(NoResultException $ex) {
            return $name;
        }
    }
    
    private function getCountry($name)
    {
        $respository = $this->em->getRepository('ArcanysSportizingmeBundle:Worlddb\Country');
        $qb = $respository->createQueryBuilder('c');
        $literal = $qb->expr()->literal($name);
        $orx = $qb->expr()->orX();
        $orx->add($qb->expr()->eq('c.code', $literal));
        $orx->add($qb->expr()->eq('c.name', $literal));
        $qb->where($orx);
        try {
            $country = $qb->getQuery()->getSingleResult();
            return $country->getCode();
        } catch(NoResultException $ex) {
            return $name;
        }
    }

}
