<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Listing;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Model\GymListSorter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SettingsListController extends Controller
{
    
    /**
     * @Route("/settings/list", name="settings_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $include = (array) $this->getRequest()->get('new_gym');
        
        // gym list
        $gyms = $em->getRepository('ArcanysSportizingmeBundle:Gym')
                ->findActiveGyms($include);
        // sort by distance
        $sorter = new GymListSorter(0, 0);
        uasort($gyms, array($sorter, 'sort'));
        
        $result = array(
            'gym' => array(),
            'school' => array(),
            'company' => array(),
        );
        foreach ($gyms as $gym) {
            $result['gym'][] = $gym->toArray();
        }
        
        $search = (array) $this->getRequest()->get('search');
        
        // school list
        $schools = $em->getRepository('ArcanysSportizingmeBundle:School')
                ->search(array_key_exists('school', $search) ? $search['school'] : null);
        foreach ($schools as $school) {
            $result['school'][] = $school->toArray();
        }
        
        // company list
        $companies = $em->getRepository('ArcanysSportizingmeBundle:Company\Company')
                ->search(array_key_exists('company', $search) ? $search['company'] : null);
        foreach ($companies as $company) {
            $result['company'][] = $company->toArrayList();
        }
        
        // also preload states and cities
        $user = $this->getUser();
        if ($user && $user instanceof Athlete) {
            $result['state'] = array();
            $result['city'] = array();
            if (null !== $address = $user->getAddress()) {
                $db = new Worlddb($em);
                $statesList = $db->getStates($address->getCountry());
                foreach ($statesList as $region) {
                    $result['state'][] = array(
                        'code' => $region->getCode(),
                        'name' => $region->getName(),
                    );
                }
                $citiesList = $db->getCities($address->getCountry(), $address->getState());
                foreach ($citiesList as $city) {
                    $result['city'][] = $city->getName();
                }
            }
        }
        
        return new JsonResponse($result);
    }
    
}
