<?php

namespace Arcanys\SportizingmeApiBundle\Controller\Listing;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AddressController extends Controller
{
    
    /**
     * @Route("/city/list/{offset}/{limit}", name="city_list", defaults={"offset":0, "limit":100})
     */
    public function cityListAction($offset, $limit)
    {
        $request = $this->getRequest();
        $country = $request->query->get('country');
        $state = $request->query->get('state');
        $db = new Worlddb($this->getDoctrine()->getManager());
        $list = $db->getCities($country, $state, $offset, $limit);
        $result = array();
        foreach ($list as $city) {
            $result[] = $city->getName();
        }
        return new JsonResponse($result);
    }
    
    /**
     * @Route("/state/list", name="state_list")
     */
    public function stateListAction()
    {
        $request = $this->getRequest();
        $country = $request->query->get('country');
        $db = new Worlddb($this->getDoctrine()->getManager());
        $list = $db->getStates($country);
        $result = array();
        foreach ($list as $region) {
            $result[] = array(
                'code' => $region->getCode(),
                'name' => $region->getName(),
            );
        }
        return new JsonResponse($result);
    }
    
}
