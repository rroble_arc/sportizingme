<?php

namespace Arcanys\SportizingmeApiBundle\Controller;

use Arcanys\SportizingmeApiBundle\Model\ForgotPasswordParams;
use Arcanys\SportizingmeApiBundle\Security\Token\WebserviceToken;
use Arcanys\SportizingmeBundle\Model\ApiResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SecurityController extends Controller
{

    /**
     * @Route("/login/{sportizingme_trigger_login}", name="user_login", defaults={"sportizingme_trigger_login":""})
     * @Method("POST")
     */
    public function loginAction()
    {
        $token = $this->get('security.context')->getToken();
        if ($token instanceof WebserviceToken) {
            return ApiResponse::success('User successfully logged in', $token->toArray());
        }
        $message = $this->getRequest()->attributes
                ->get('_LAST_LOGIN_MESSAGE', 'Unauthorized');
        return ApiResponse::error($message);
    }
    
    /**
     * @Route("/signout", name="user_logout")
     */
    public function signoutAction()
    {
        $token = $this->get('security.context')->getToken();
        if (is_object($token) && $token instanceof WebserviceToken) {
            try {
                $em = $this->getDoctrine()->getManager();
                $session = $token->getSession();
                $user = $session->getUser();
                $user->setDeviceId(null);
                $em->persist($user);
                $em->remove($session);
                $em->flush();
            } catch(\Exception $ex) {
                return ApiResponse::error($ex->getMesage());
            }
        } else {
            return ApiResponse::error('Not logged in.');
        }
        return ApiResponse::success('You have been logged out successfully.');
    }
    
    /**
     * @Route("/forgot-password", name="forgot_password")
     * @Method("POST")
     */
    public function forgotPasswordAction()
    {
        $email = $this->getRequest()->request->get('email');
        $em = $this->getDoctrine()->getManager();
        $athlete = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')
                ->findOneBy(array('email' => $email));
        if ($athlete) {
            $plain = hash('sha1', time().rand());
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($athlete);
            $token = $encoder->encodePassword($plain, get_class($athlete));
            
            // update user
            $athlete->setPasswordToken($token);
            $em->persist($athlete);
            $em->flush();
            
            // send email
            $resetUrl = $this->generateUrl('reset_password', array('token' => $plain), true);
            $params = new ForgotPasswordParams($resetUrl);
            $this->get('arcanys_sportizingme.mailer')
                 ->setMessageTemplate('Forgot password')
                 ->send($athlete->getEmail(), $params->getParams());
        
            return ApiResponse::success('Reset password link has been sent to your email.');
        } else {
            return ApiResponse::error('Email does not exists.');
        }
    }
    
}
