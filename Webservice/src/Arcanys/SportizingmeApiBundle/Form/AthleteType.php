<?php

namespace Arcanys\SportizingmeApiBundle\Form;

use Arcanys\SportizingmeApiBundle\Form\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AthleteType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                // step 1
                ->add('firstname')
                ->add('lastname')
                ->add('email')
                ->add('password')
        
                // step 1-facebook
                ->add('dateOfBirth', 'date', array(
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                ))
                ->add('facebookId')
                ->add('facebookEmail')
                ->add('facebookFriends')
                ->add('profilePicture', 'image_type', array('required' => false))
                // step 1-facebook (copy)
                ->add('facebookLink') // FIXME: not used?
                ->add('facebookPhoto', 'image_type', array('required' => false))
                
                // step 2
                ->add('shortPresentation')
                ->add('sports')
                ->add('twitterLink') // FIXME: is it used ?
                ->add('twitterUsername')
                ->add('twitterFollowers')
                ->add('twitterPhoto', 'image_type', array('required' => false))
                
                // step 3
                ->add('trainingLocation') // Gym
                ->add('trainingFrequency')
                ->add('school') // School
                ->add('maxEducationLevel')
                ->add('company') // Company
                ->add('personalIncome')
                
                // step 4
                // - personal info
                // state, city, zip, country
                ->add('address', new AddressType(), array('required' => false))
                ->add('paypal')
                // - health info
                ->add('weight')
                ->add('weightUnit')
                ->add('height')
                ->add('heightUnit')
                ->add('gender')
                ->add('trainingShirtSize')
                
                // fb/twitter tokens
                ->add('tokenInformationExpirationDateKey')
                ->add('tokenInformationLoginTypeLoginKey')
                ->add('tokenInformationPermissionsKey')
                ->add('tokenInformationRefreshDateKey')
                ->add('tokenInformationTokenKey')
                ->add('twitterAccessToken')
                
                ->add('deviceId')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Arcanys\SportizingmeBundle\Entity\User\Athlete',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return '';
    }

}
