<?php

namespace Arcanys\SportizingmeApiBundle\Form;

use Arcanys\SportizingmeBundle\Entity\Image;
use Arcanys\SportizingmeBundle\Manager\ImageManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ImageType extends AbstractType
{

    /**
     * @var ImageManager
     */
    protected $imageManager;

    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('link', null, array(
                    'required' => false,
                    'data_class' => null,
                    'attr' => array(
                        'title' => 'File upload overrides link',
                        'class' => 'span9',
                    ),
                ))
                ->add('file', 'file', array(
                    'required' => false,
                    'data_class' => null,
                ))
                ->add('type', 'hidden')
        ;
        $im = $this->imageManager;
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use($im) {
                    $data = $event->getData();
                    if (is_string($data)) {
                        $data = array(
                            'link' => $data,
                            'type' => Image::TYPE_EXTERNAL,
                            'file' => '',
                        );
                    } else if (is_object($data)) {
                        $data = array(
                            'file' => $data,
                            'type' => $data->getMimeType(),
                            'link' => $im->getLink($data),
                        );
                    }
                    
                    if (is_object($data['file'])) {
                        $file = $im->upload($data['file']);
                        $data = array(
                            'file' => $file,
                            'type' => $file->getMimeType(),
                            'link' => $im->getLink($file),
                        );
                    }

                    $event->setData($data);
        });
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Arcanys\SportizingmeBundle\Entity\Image',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'image_type';
    }

}
