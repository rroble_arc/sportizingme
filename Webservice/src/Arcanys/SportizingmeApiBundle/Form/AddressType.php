<?php

namespace Arcanys\SportizingmeApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('street') // address
                ->add('number')
                ->add('zipcode')
                ->add('city')
                ->add('state')
                ->add('country')
                ->add('maprect') // ?
                ->add('longitude')
                ->add('latitude')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Arcanys\SportizingmeBundle\Entity\Address',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return '';
    }

}
