<?php

namespace Arcanys\SportizingmeApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Randolph
 */
class GymType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name')
                ->add('image', 'image_type', array('required' => false))
                ->add('hashtags')
                ->add('address', new AddressType(), array('required' => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Arcanys\SportizingmeBundle\Entity\Gym',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return '';
    }

}
