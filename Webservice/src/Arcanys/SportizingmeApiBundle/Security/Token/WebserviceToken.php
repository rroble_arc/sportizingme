<?php

namespace Arcanys\SportizingmeApiBundle\Security\Token;

use Arcanys\SecurityBundle\Model\UserInterface;
use Arcanys\SecurityBundle\Model\UserSessionInterface;
use Arcanys\SecurityBundle\Webservice\Token\WebserviceTokenInterface;
use Arcanys\SportizingmeBundle\Entity\User\Session;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class WebserviceToken extends AbstractToken implements TokenInterface, WebserviceTokenInterface
{

    /**
     * @var string
     */
    protected $token;
    
    /**
     * @var Session
     */
    protected $session;

    public function __construct($token, array $roles = array())
    {
        $this->token = $token;
        parent::__construct($roles);
        parent::setAuthenticated(count($roles) > 0);
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * {@inheritDoc}
     */
    public function getCredentials()
    {
        
    }
    
    /**
     * @return array
     */
    public function toArray()
    {
        $result = array('token' => $this->token);
        $user = $this->getUser();
        if (is_object($user) && $user instanceof UserInterface) {
            $result = array_merge($user->toArray(), $result);
        }
        return $result;
    }
    
    /**
     * @return UserSessionInterface
     */
    public function getSession()
    {
        return $this->session;
    }

    public function setSession(UserSessionInterface $session = null)
    {
        $this->session = $session;
    }

    public function isValid()
    {
        return true;
    }

}
