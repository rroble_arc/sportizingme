<?php

namespace Arcanys\SportizingmeApiBundle\Security\Token;

use Arcanys\SecurityBundle\Webservice\Token\BasicLoginTokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class FacebookLoginToken extends AbstractToken implements TokenInterface, BasicLoginTokenInterface
{

    protected $id;
    protected $email;

    public function __construct(Request $request)
    {
        $id = $request->request->get('facebookId');
        $email = $request->request->get('facebookEmail');
        
        $this->setId($id);
        $this->setEmail($email);
    }

    /**
     * @return string
     */
    public function getCredentials()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function isValid()
    {
        return true;
    }

}
