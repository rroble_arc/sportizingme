<?php

namespace Arcanys\SportizingmeApiBundle\Security\Token;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface TokenInterface
{

    /**
     * @return boolean
     */
    public function isValid();
}
