<?php

namespace Arcanys\SportizingmeApiBundle\Security\Token;

use Arcanys\SecurityBundle\Webservice\Token\BasicLoginTokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class BasicLoginToken extends AbstractToken implements TokenInterface, BasicLoginTokenInterface
{

    protected $credentials;
    protected $type;

    public function __construct(Request $request, $type)
    {
        $user = $request->request->get('email');
        $credentials = $request->request->get('password');
        
        $this->setUser($user ?: '');
        $this->setCredentials($credentials);
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * @param string $credentials
     */
    public function setCredentials($credentials)
    {
        $this->credentials = $credentials;
    }

    public function getClass()
    {
        $classes = array(
            'athlete' => 'ArcanysSportizingmeBundle:User\Athlete',
            'sponsor' => 'ArcanysSportizingmeBundle:User\Sponsor',
        );
        if (array_key_exists($this->type, $classes)) {
            return $classes[$this->type];
        }
    }

    public function isValid()
    {
        return $this->getClass() != null;
    }
    
}
