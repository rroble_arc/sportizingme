<?php

namespace Arcanys\SportizingmeApiBundle\Security\Authentication;

use Arcanys\SecurityBundle\Model\UserSessionInterface;
use Arcanys\SecurityBundle\Webservice\Authentication\Authenticator\AuthenticatorInterface;
use Arcanys\SecurityBundle\Webservice\Authentication\Authenticator\DefaultAuthenticator;
use Arcanys\SecurityBundle\Webservice\Token\BasicLoginTokenInterface;
use Arcanys\SecurityBundle\Webservice\Token\WebserviceTokenInterface;
use Arcanys\SportizingmeApiBundle\Security\Token\BasicLoginToken;
use Arcanys\SportizingmeApiBundle\Security\Token\FacebookLoginToken;
use Arcanys\SportizingmeApiBundle\Security\Token\TokenInterface as SportizingTokenInterface;
use Arcanys\SportizingmeApiBundle\Security\Token\WebserviceToken;
use Arcanys\SportizingmeBundle\Entity\User\Session;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class Authenticator extends DefaultAuthenticator implements AuthenticatorInterface
{
    
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;
    
    /**
     * @var string
     */
    protected $basicLoginTrigger;

    public function __construct(EncoderFactoryInterface $encoderFactory, Request $request, $usernameKey, $passwordKey, $tokenKey, $lifetime, ManagerRegistry $doctrine, $basicLoginTrigger)
    {
        parent::__construct($encoderFactory, $request, $usernameKey, $passwordKey, $tokenKey, $lifetime);
        $this->doctrine = $doctrine;
        $this->basicLoginTrigger = $basicLoginTrigger;
    }
    
    public function createWebserviceToken()
    {
        return new WebserviceToken($this->getToken());
    }
    
    public function createBasicToken()
    {
        $type = $this->getBasicLoginTrigger();
        if ('facebook' == $type) {
            return new FacebookLoginToken($this->request);
        } else {
            return new BasicLoginToken($this->request, $type);
        }
    }
    
    protected function getBasicLoginTrigger()
    {
        return $this->request->get($this->basicLoginTrigger);
    }

    public function isBasicAuthenticationNeeded()
    {
        return null !== $this->getBasicLoginTrigger();
    }

    /**
     * {@inheritDoc}
     */
    public function authenticate(TokenInterface $token)
    {
        if ($token instanceof FacebookLoginToken) {
            return $this->authenticateFacebook($token);
        }
        return parent::authenticate($token);
    }

    public function authenticateBasic(BasicLoginTokenInterface $token)
    {
        $user = null;
        if (null != $class = $token->getClass()) {
            $em = $this->doctrine->getManager();
            $user = $em->getRepository($class)->findOneBy(array('email' => $token->getUsername()));
        }
        if (!$user) {
            throw new AuthenticationException("The user does not exists.");
        }

        // from DaoAuthenticationProvider
        $currentUser = $token->getUser();
        if ($currentUser instanceof UserInterface) {
            if ($currentUser->getPassword() !== $user->getPassword()) {
                throw new BadCredentialsException('The credentials were changed from another session.');
            }
        } else {
            $presentedPassword = $token->getCredentials();
            if ("" === $presentedPassword) {
                throw new BadCredentialsException('The presented password cannot be empty.');
            }
            $userEncoder = $this->encoderFactory->getEncoder($user);
            if (!$userEncoder->isPasswordValid($user->getPassword(), $presentedPassword, $user->getSalt())) {
                throw new BadCredentialsException('The presented password is invalid.');
            }
        }
        
        $sessionToken = new Session();
        $sessionToken->setUser($user);
        $user->addSession($sessionToken);
        
        $this->onLogin($user);

        // flush
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();

        return $this->createAuthenticatedWebserviceToken($user, $sessionToken);
    }
    
    protected function onLogin(UserInterface $user)
    {
        $user->setDeviceId($this->request->get('deviceId'));
    }

    protected function authenticateFacebook(FacebookLoginToken $token)
    {
        $em = $this->doctrine->getManager();
        $qb = $em->getRepository('ArcanysSportizingmeBundle:User\Athlete')->createQueryBuilder('a');
        $qb->where($qb->expr()->eq('a.email', $qb->expr()->literal($token->getEmail())));
        if ($token->getId()) {
            $qb->andWhere($qb->expr()->eq('a.facebookId', $qb->expr()->literal($token->getId())));
        }
        try {
            $user = $qb->getQuery()->getSingleResult();
        } catch(NoResultException $ex) {
            throw new AuthenticationException("The facebook user does not exists.");
        }

        $sessionToken = new Session();
        $sessionToken->setUser($user);
        $user->addSession($sessionToken);
        $user->setFacebookEmail($token->getEmail());
        
        $this->onLogin($user);

        // flush
        $em->persist($user);
        $em->flush();

        return $this->createAuthenticatedWebserviceToken($user, $sessionToken);
    }

    public function authenticateToken(WebserviceTokenInterface $token)
    {
        $session = new Session();
        $tokenEncoder = $this->encoderFactory->getEncoder($session);
        $encodedToken = $tokenEncoder->encodePassword($token->getToken(), get_class($session));
        $em = $this->doctrine->getManager();
        $sessionToken = $em->getRepository('ArcanysSportizingmeBundle:User\Session')
                ->findOneBy(array('token' => $encodedToken));
        if (!$sessionToken) {
            throw new UsernameNotFoundException('Token "' . $token->getToken() . '" not found.');
        }

        // check expiration
        $now = new \DateTime();
        if ($this->lifetime) {
            $lifetime = new \DateInterval('PT' . $this->lifetime . 'S');
            $date = $sessionToken->getLastAccessDate()->add($lifetime);
            if ($date < $now) {
                throw new BadCredentialsException('The token has expired.');
            }
        }
        $sessionToken->setDateUpdated($now);
        $em->persist($sessionToken);
        $em->flush();
        return $this->createAuthenticatedWebserviceToken($sessionToken->getUser(), $sessionToken);

    }

    /**
     * @param TokenInterface $token
     * @return boolean
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof SportizingTokenInterface
                && $token->isValid();
    }

    /**
     * @param UserInterface $user
     * @param UserSessionInterface $session
     * @return WebserviceToken
     */
    protected function createAuthenticatedWebserviceToken(UserInterface $user, UserSessionInterface $session)
    {
        $authToken = new WebserviceToken($session->getPlainToken(), $user->getRoles());
        $authToken->setUser($user);
        $authToken->setSession($session);
        return $authToken;
    }

}
