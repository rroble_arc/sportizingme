<?php

namespace Arcanys\SportizingmeTestBundle\DataFixtures\ORM;

use Arcanys\SportizingmeBundle\Entity\Address;
use Arcanys\SportizingmeBundle\Entity\Gym;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class LoadGymData implements FixtureInterface
{
    // @codeCoverageIgnoreStart

    public function load(ObjectManager $em)
    {
        $gym1 = new Gym();
        $gym1->setName('Gym.1');
        $address = new Address();
        $address->setStreetName('Gym.1.Address');
        $gym1->setAddress($address);
        $em->persist($gym1);
        $em->flush();
        
//        $importer = new GymImporter($em);
//        $importer->import(__DIR__.'/../../Resources/gym_list');
    }

    // @codeCoverageIgnoreEnd
    
}
