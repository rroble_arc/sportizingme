<?php

namespace Arcanys\SportizingmeTestBundle\DataFixtures\ORM;

use Arcanys\SportizingmeBundle\Entity\School;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class LoadSchoolData implements FixtureInterface
{
    
    // @codeCoverageIgnoreStart

    public function load(ObjectManager $em)
    {
        $school1 = new School();
        $school1->setName('School.1');
        $em->persist($school1);
        $em->flush();
    }
    
    // @codeCoverageIgnoreEnd

}
