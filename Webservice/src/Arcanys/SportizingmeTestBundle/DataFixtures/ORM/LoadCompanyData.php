<?php

namespace Arcanys\SportizingmeTestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class LoadCompanyData implements FixtureInterface
{

    // @codeCoverageIgnoreStart
    
    public function load(ObjectManager $em)
    {
    }
    
    // @codeCoverageIgnoreEnd

}
