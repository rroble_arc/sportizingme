<?php

namespace Arcanys\SportizingmeTestBundle\DataFixtures\ORM;

use Arcanys\SportizingmeBundle\Entity\User\Athlete;
use Arcanys\SportizingmeBundle\Entity\User\Sponsor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class LoadUserData implements FixtureInterface
{
    // @codeCoverageIgnoreStart
    
    public function load(ObjectManager $em)
    {
        // create a test athlete
        $athlete0 = $this->createAthlete('test.athlete@mail.com');
        $em->persist($athlete0);

        // create a test athlete with facebook
        $athlete1 = $this->createAthlete('facebook.athlete@mail.com', 'athlete_fb_token');
        $athlete1->setFacebookId('12345');
        $em->persist($athlete1);
        
        $sponsor0 = $this->createSponsor('test.sponsor@gmail.com');
        $em->persist($sponsor0);

        $em->flush();
    }

    private function createAthlete($email, $token = 'athlete_token')
    {
        $athlete = new Athlete();
        $athlete->setEmail($email);
        $athlete->setPassword('testing');
        $athlete->setFirstname('Test');
        $athlete->setLastname('Athlete');
        foreach ((array)$athlete->getSessions() as $session) {
            $session->setPlainToken($token);
        };
        return $athlete;
    }

    private function createSponsor($email, $token = 'sponsor_token')
    {
        $sponsor = new Sponsor();
        $sponsor->setEmail($email);
        $sponsor->setPassword('testing');
        $sponsor->getCompany()->setName('Company.1');
        $sponsor->setIndustry('Company.1.Industry');
        foreach ((array)$sponsor->getSessions() as $session) {
            $session->setPlainToken($token);
        };
        return $sponsor;
    }

    // @codeCoverageIgnoreEnd
    
}
