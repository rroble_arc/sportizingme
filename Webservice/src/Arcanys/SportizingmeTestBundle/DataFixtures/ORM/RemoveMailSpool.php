<?php

namespace Arcanys\SportizingmeTestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class RemoveMailSpool implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        list($dir,) = explode('src', __DIR__);
        if (false != $spool = realpath($dir.'/app/cache/test/spool/default')) {
            $files = array_diff(scandir($spool), array('.','..')); 
            foreach ($files as $file) {
                @unlink($spool.'/'.$file);
            }
        }
    }

}
