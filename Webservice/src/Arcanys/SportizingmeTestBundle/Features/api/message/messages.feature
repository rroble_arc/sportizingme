Feature: Messages

Scenario: Messages
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email          | token |
        | FIRSTNAME | LASTNAME | test@email.com | messagetoken |
    When I go to "api/athlete/messages?token=messagetoken"
    Then the response status code should be 200
