Feature: Read message

Scenario: Read message
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email          | token |
        | FIRSTNAME | LASTNAME | test@email.com | messagetoken |
    When I go to "api/athlete/messages/read/999?token=messagetoken"
    Then the response should be error
