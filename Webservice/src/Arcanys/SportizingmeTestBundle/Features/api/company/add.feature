Feature: Add company

Scenario: Add company
    When I go to "api/company/add" with post data:
    """
    name => COMPANY
    """
    Then the response should be success
    And the response data should contain key "id"
    And the response data should contain key "name"

Scenario: Add company validation
    When I go to "api/company/add" as post
    Then the response should be error
    And the response data for key "fields" should contain "name"
