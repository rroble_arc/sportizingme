Feature: Add gym

Scenario: Add gym
    When I go to "api/gym/add" with post data:
    """
    city => Nuiqsut
    country => United States
    latitude => 10.31896
    longitude => 123.8905
    name => GYM
    state => Alaska
    """
    Then the response should be success


Scenario: Add gym validation
    When I go to "api/gym/add" with post data:
    """
    city => Nuiqsut
    country => United States
    latitude => 10.31896
    longitude => 123.8905
    name => GYM
    state => Alaska
    """
    Then the response should be success
