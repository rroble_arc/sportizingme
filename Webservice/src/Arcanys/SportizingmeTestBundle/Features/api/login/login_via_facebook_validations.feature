Feature: Athlete login (via facebook) validations

Scenario: No post data
    When I go to "api/login/facebook" as post
    Then the response should be error
    And the response message should contain "Unauthorized"

Scenario: Invalid login
    When I go to "api/login/facebook" with post data:
    """
    facebookEmail => non.existing@email.com
    """
    Then the response should be error
    And the response message should contain "Unauthorized"

@dev
Scenario: Wrong login
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email                      | facebookId |
        | John | Doe | juandelacruz@testing.local | testing    |
    When I go to "api/login/facebook" with post data:
    """
    facebookEmail => juandelacruz@testing.local
    facebookId => wrong-facebook-id
    """
    Then the response should be error
    And the response message should contain "Unauthorized"
