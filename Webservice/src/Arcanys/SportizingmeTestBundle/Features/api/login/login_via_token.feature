Feature: Athlete login (via token)

@dev
Scenario:
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email          | token | password | 
        | Jones | James | test@gmail.com | testtoken | testing |
    When I go to "api/athlete/profile?token=testtoken"
    Then the response should be success

Scenario: Validation
    When I go to "api/athlete/profile?token=invalid-token"
    Then the response should be error
