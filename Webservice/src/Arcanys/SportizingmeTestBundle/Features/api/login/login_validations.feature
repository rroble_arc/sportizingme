Feature: Athlete login validations

Scenario: No post data
    When I go to "api/login" as post
    Then the response should be error
    And the response message should contain "Unauthorized"

Scenario: Invalid login endpoint
    When I go to "api/login/test" as post
    Then the response should be error
    And the response message should contain "Unauthorized"

Scenario: Invalid login
    When I go to "api/login/athlete" with post data:
    """
    email => non.existing@email.com
    password => testing
    """
    Then the response should be error
    And the response message should contain "Unauthorized"

@dev
Scenario: Wrong login
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email                      | password |
        | John | Doe | juandelacruz@testing.local | testing |
    When I go to "api/login/athlete" with post data:
    """
    email => juandelacruz@testing.local
    password => wrong-password
    """
    Then the response should be error
    And the response message should contain "Unauthorized"
