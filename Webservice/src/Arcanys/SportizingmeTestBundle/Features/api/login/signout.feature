Feature: Athlete signout

@dev
Scenario:
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email          | token |
        | Jones | James | test@gmail.com | testing |
    When I go to "api/signout?token=testing"
    Then the response should be success

Scenario: Validation
    When I go to "api/signout?token=invalid-token"
    Then the response should be error
