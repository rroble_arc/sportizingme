Feature: Athlete login via facebook

@dev
Scenario:
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email                      | facebookId |
        | John | Doe | juandelacruz@testing.local | 123456987  |
    When I go to "api/login/facebook" with post data:
    """
    facebookEmail => juandelacruz@testing.local
    facebookId => 123456987
    """
    Then the response should be success
    And the response data should contain key "token"
    And the response data should contain key "id"
    And the response data should contain key "facebook_id"
