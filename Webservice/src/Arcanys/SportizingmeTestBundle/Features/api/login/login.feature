Feature: Athlete login

@dev
Scenario:
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email                      | password |
        | John | Doe | juandelacruz@testing.local | testing |
    When I go to "api/login/athlete" with post data:
    """
    email => juandelacruz@testing.local
    password => testing
    """
    Then the response should be success
    And the response data should contain key "token"
    And the response data should contain key "id"
