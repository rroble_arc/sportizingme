Feature: Settings

Scenario:
    When I go to "api/settings/list"
    Then the response status code should be 200
    And the response should contain "School.1"
    And the response should contain "Gym.1"
    And the response should contain "Company.1"