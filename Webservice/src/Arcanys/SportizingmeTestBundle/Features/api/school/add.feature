Feature: Add school

Scenario: Add school
    When I go to "api/school/add" with post data:
    """
    name => SCHOOL
    """
    Then the response should be success

Scenario: Add school validation
    When I go to "api/school/add" as post
    Then the response should be error
