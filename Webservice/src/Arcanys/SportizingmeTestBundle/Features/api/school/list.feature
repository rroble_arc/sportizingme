Feature: List school

Scenario: List school
    When I go to "api/school/list"
    Then the response status code should be 200
    And the response should contain "School.1"
