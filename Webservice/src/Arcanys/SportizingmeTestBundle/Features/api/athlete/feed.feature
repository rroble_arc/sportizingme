Feature: Athlete feed

Scenario: Athlete feed
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email          | token |
        | FIRSTNAME | LASTNAME | test@email.com | feedtoken |
    When I go to "api/athlete/feed?token=feedtoken"
    Then the response status code should be 200
