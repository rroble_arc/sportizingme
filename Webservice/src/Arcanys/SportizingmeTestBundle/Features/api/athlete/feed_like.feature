Feature: Like feed

Scenario: Like feed
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email          | token |
        | FIRSTNAME | LASTNAME | test@email.com | feedtoken |
    When I go to "api/athlete/feed/like/99?token=feedtoken"
    Then the response should be error
