Feature: Athlete registration (token)

Scenario:
    When I go to "api/athlete/signup" with post data:
    """
    firstname => Juan
    lastname =>  de la Cruz
    email => juandelacruz@testing.local
    password => testing
    """
    Then the response should be success
    And the response data should contain key "token"
    And the response data should contain key "id"
    
    When I go to "api/athlete/profile" using previous token
    Then the response should be success
    
    When I go to "api/athlete/profile" as post using previous token
    Then the response should be success
    
    When I go to "api/athlete/profile" using previous token with post data:
    """
    city => Cebu
    """
    Then the response should be success
