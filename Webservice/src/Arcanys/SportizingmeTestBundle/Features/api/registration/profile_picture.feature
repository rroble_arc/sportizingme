Feature: Athlete registration profile picture (file upload)

@dev
Scenario:
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname  | email          | token |
        | Jones | Doe | test@gmail.com | testing |
    When I go to "api/athlete/profile?token=testing" with post data:
    """
    profilePicture => @pictures\profile-photo.jpg
    """
    Then the response should be success
    And the response data should contain key "profile_picture"
