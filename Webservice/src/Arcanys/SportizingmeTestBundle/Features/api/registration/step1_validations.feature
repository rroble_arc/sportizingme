Feature: Athlete registration validations

Scenario: No post data
    When I go to "api/athlete/signup" as post
    Then the response should be error
    And the response message should contain "Invalid form fields"
    And the response data should contain key "fields->email"
    And the response data should contain key "fields->firstname"
    And the response data should contain key "fields->lastname"
    And the response data should contain key "fields->password"

@dev
Scenario: Existing email
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email                      |
        | John | Lenon | juandelacruz@testing.local |
    When I go to "api/athlete/signup" with post data:
    """
    email => juandelacruz@testing.local
    """
    Then the response should be error
    And the response data for key "fields->email" should contain "already used"

Scenario Outline: Invalid emails
    When I go to "api/athlete/signup" with post data:
    """
    email => <email>
    """
    Then the response should be error
    Examples:
        | email             |
        |  12               |
        |  xx               |
        |  xx@gmail.com.    |
        |  xx@gmail.com.1   |
        |  xx@gmail..1      |
        |  xx.gmail.1       |
        |  test @gmail.com  |
