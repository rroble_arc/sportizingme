Feature: Athlete registration (via facebook) validations

Scenario: No post data
    When I go to "api/athlete/signup/facebook" as post
    Then the response should be error
    And the response message should contain "Invalid form fields"
    And the response data should contain key "fields->email"
    And the response data should contain key "fields->firstname"
    And the response data should contain key "fields->lastname"
    And the response data should contain key "fields->facebookId"

@dev
Scenario: Existing email
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email                      |
        | John | Lenon | juandelacruz@testing.local |
    When I go to "api/athlete/signup/facebook" with post data:
    """
    email => juandelacruz@testing.local
    """
    Then the response should be error
    And the response data for key "fields->email" should contain "already used"
