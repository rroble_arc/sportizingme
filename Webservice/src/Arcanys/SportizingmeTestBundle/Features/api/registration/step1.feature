Feature: Athlete registration

Scenario: Step 1
    When I go to "api/athlete/signup" with post data:
    """
    firstname => Juan
    lastname =>  de la Cruz
    email => juandelacruz@testing.local
    password => testing
    """
    Then the response should be success
    And the response data should contain key "token"
    And the response data should contain key "id"
    And mail "Registration confirmation" is sent
