Feature: Athlete registration profile

@dev
Scenario: Step 2
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email          | token |
        | Jones | Doe | test@gmail.com | testing |
    Given the following "ArcanysSportizingmeBundle:School" exist:
        | name |
        | School 2 |
    Given the following "ArcanysSportizingmeBundle:Company\Company" exist:
        | name |
        | Company 2 |
    Given the following "ArcanysSportizingmeBundle:Gym" exist:
        | name |
        | Gym 2 |
    When I go to "api/athlete/profile?token=testing" with post data:
    """
    # step 2
    shortPresentation => I am a runner at the same time I do cycling and martial arts
    sports[0] => RUN
    sports[1] => CYC
    sports[] => MMA
    twitterUsername => my_twitter
    twitterFollowers => 987
    twitterPhoto => http://si0.twimg.com/profile_images/378800000281850475/75205d8d4a6d234efabc76b7a73f2ba3_normal.jpeg

    # step 3 
    trainingLocation => 2
    trainingFrequency => 3
    school => 2
    maxEducationLevel => high school
    company => 2
    personalIncome => >150k

    # step 4
    # - personal info
    # postalAddress
    address => my liko2x st
    city => cebu
    state => cebu
    zip => 6000
    country => Philippines
    paypal => my_paypal@gmail.com
    # - health info
    weight => 130
    weightUnit => lb
    height => 150
    heightUnit => cm
    gender => M
    trainingShirtSize => L
    """
    Then the response should be success
    And the response data should contain key "email"
    And the response data should contain key "training_location"
    And the response data should contain key "school_university"
    And the response data should contain key "company"
