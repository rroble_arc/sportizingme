Feature: Athlete registration

Scenario: Step 1
    When I go to "api/athlete/signup/facebook" with post data:
    """
    firstname => Juan
    lastname =>  de la Cruz
    email => juandelacruz@testing.local
    facebookId => 123987654
    profilePicture => http://www.google.com/images/icons/ui/doodle_plus/doodle_plus_google_logo_on_grey.gif
    facebookFriends => 124
    dateOfBirth => 01/01/2001
    """
    Then the response should be success
    And the response data should contain key "token"
    And the response data should contain key "id"
    And the response data should contain key "facebook_email"
    And the response data should contain key "facebook_photo"
    And the response data should contain key "facebook_friends"
    And the response data should contain key "date_of_birth"
    And mail "Registration confirmation" is sent
