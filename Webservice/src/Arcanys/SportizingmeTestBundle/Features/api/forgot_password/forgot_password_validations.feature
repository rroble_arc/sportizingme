Feature: Forgot password (validations)

Scenario:
    When I go to "api/forgot-password" with post data:
    """
    email => juandelacruz@testing.local
    """
    Then the response should be error
    And the response message should contain "Email does not exists."
