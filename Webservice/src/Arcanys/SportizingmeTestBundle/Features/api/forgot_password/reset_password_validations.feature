Feature: Reset password (validations)

Scenario:
    When I go to "en/reset-password/token"
    Then the response should contain "This link has either expired or is invalid."
