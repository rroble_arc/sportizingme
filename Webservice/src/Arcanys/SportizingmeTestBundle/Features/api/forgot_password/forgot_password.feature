Feature: Forgot password

@dev
Scenario:
    Given the following "ArcanysSportizingmeBundle:User\Athlete" exist:
        | firstname | lastname | email                      | password |
        | John | Doe | juandelacruz@testing.local | testing |
    When I go to "api/forgot-password" with post data:
    """
    email => juandelacruz@testing.local
    """
    Then the response should be success
    And the response message should contain "Reset password link has been sent to your email."
    And mail "Forgot password" is sent
