<?php

namespace Arcanys\SportizingmeTestBundle\Features\Context;

use Arcanys\SecurityBundle\Model\UserInterface;
use Arcanys\SportizingmeBundle\Entity\User\Session;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Driver\Goutte\Client as GoutteClient;
use Behat\Mink\Driver\GoutteDriver;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\Symfony2Extension\Driver\KernelDriver;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

// @codeCoverageIgnoreStart
require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

/**
 * Feature context.
 */
class FeatureContext extends MinkContext implements KernelAwareInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;
    
    private $parameters;
    
    private $token;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
    
    /**
     * @return Client
     */
    private function getClient()
    {
        $driver = $this->getSession()->getDriver();
        if (!$driver instanceof KernelDriver && !$driver instanceof GoutteDriver) {
            throw new PendingException('Only KernelDriver & GoutteDriver are supported');
        }
        
        return $driver->getClient();
    }
    
    private function getUrlWithToken($url)
    {
        $array = $this->getJsonResponse();
        $data = $array['data'];
        if (array_key_exists('token', $data)) {
            $token = $this->token = $data['token'];
        } else {
            $token = $this->token;
        }
        return $url .'?token='. $token;
    }
    
    private function getMailFiles()
    {
        list($dir,) = explode('src', __DIR__);
        $spool = realpath($dir.'/app/cache/test/spool/default');
        $finder = new Finder();
        $finder->files()->in($spool);
        return $finder;
    }

    // -------------------------------------------------------------------------
    // Internals
    
    private function getResponse()
    {
        return $this->getSession()->getPage()->getContent();
    }
    
    private function getJsonResponse()
    {
        $array = json_decode($this->getResponse(), true);
        assertTrue(is_array($array), 'is_array(response json)?');
        assertArrayHasKey('error', $array);
        assertArrayHasKey('message', $array);
        assertArrayHasKey('data', $array);
        return $array;
    }
    
    private function getKeys($string)
    {
        return explode('->', $string);
    }

    // -------------------------------------------------------------------------
    // Asserts

    /**
     * @When /^I send a POST request to "([^"]*)"$/
     * @When /^I go to "([^"]*)" as post$/
     */
    public function iSendAPostRequestTo($url)
    {
        $uri = $this->locatePath($url);
        $this->getClient()->request('POST', $uri);
    }
    
     /**
     * @When /^I send a POST request to "([^"]*)" with body:$/
      * @When /^I go to "([^"]*)" with post data:$/
     */
    public function iSendAPostRequestToWithBody($url, PyStringNode $string)
    {
        $data = array();
        foreach ($string->getLines() as $line) {
            if (trim($line) == '') continue; // skip empty line
            if ($line[0] == '#' || $line[0] == '//') continue; // skip comment
            $row = explode('=>', $line);
            if (isset($row[0])) {
                $key = trim($row[0]);
                $value = isset($row[1]) ? trim($row[1]) : '';
                preg_match('#(.+)\[(.*)\]#', $key, $matches); // look for array
                if ($matches) {
                    $data[$matches[1]][$matches[2]] = $value;
                } else {
                    $data[$key] = $value;
                }
            }
        }
        
        $client = $this->getClient();
        $external = $client instanceof GoutteClient;
        
        // randomize email for goutte only
        if ($external && array_key_exists('email', $data)) {
            $parts = explode('@', $data['email']);
            if (isset($parts[1])) {
                $newEmail = $parts[0].'-'.date('YmdHis').'@'.$parts[1];
                $data['email'] = $newEmail;
            }
        }
        
        // get files
        $filesbase = realpath(__DIR__.'/../_files');
        list($dir,) = explode('src', __DIR__);
        $temp = realpath($dir.'/app/cache/test');
        $files = array();
        foreach ($data as $key => $value) {
            if ($value && $value[0] == '@') {
                $filename = realpath($filesbase . '/'. substr($value, 1));
                if (!$filename) continue;
                // make a copy to avoid deletion
                $filename2 = $temp .'/'. 'copy-'.basename($filename);
                copy($filename, $filename2);
                $files[$key] = new UploadedFile($filename2, basename($filename2));
                unset($data[$key]);
            }
        }
        
        $uri = $this->locatePath($url);
        $content = $external ? $data : http_build_query($data);
        $client->request('POST', $uri, $data, $files, array()/*server*/, $content/*content*/ );
    }
    
    /**
     * @Then /^the response should be success$/
     */
    public function theResponseShouldBeSuccess()
    {
        $array = $this->getJsonResponse();
        assertFalse($array['error'], '(response error == false)?');
    }

    /**
     * @Then /^the response should be error$/
     */
    public function theResponseShouldBeError()
    {
        $array = $this->getJsonResponse();
        assertTrue($array['error'], '(response error == true)?');
    }

    /**
     * @Then /^the response message should contain "([^"]*)"$/
     */
    public function theResponseMessageShouldContain($text)
    {
        $array = $this->getJsonResponse();
        assertContains($text, $array['message'], "(message has '$text')?");
    }

    /**
     * @Then /^the response data should contain key "([^"]*)"$/
     */
    public function theResponseDataShouldContainKey($key)
    {
        $array = $this->getJsonResponse();
        $data = $array['data'];
        foreach ($this->getKeys($key) as $subkey) {
            assertArrayHasKey($subkey, $data, "(data($key) has key '$subkey')?");
            assertNotNull($data[$subkey], "(data($key)[$subkey] is not null)?");
            $data = $data[$subkey];
        }
    }

    /**
     * @Then /^the response data for key "([^"]*)" should contain "([^"]*)"$/
     */
    public function theResponseDataForKeyShouldContain($key, $text)
    {
        $array = $this->getJsonResponse();
        $data = $array['data'];
        foreach ($this->getKeys($key) as $subkey) {
            assertArrayHasKey($subkey, $data, "(data($key) has key '$subkey')?");
            $data = $data[$subkey];
        }
        $str = is_string($data) ? $data : var_export($data, true);
        assertContains($text, $str, "(data[$subkey] has '$text')?");
    }

    /**
     * @Given /^the following "([^"]*)" exist:$/
     */
    public function theFollowingExist($class, TableNode $table)
    {
        /** @var $doctrine \Doctrine\Bundle\DoctrineBundle\Registry */
        $doctrine = $this->kernel->getContainer()->get('doctrine');
        /** @var $em \Doctrine\ORM\EntityManager */
        $em = $doctrine->getManager();
        $metadata = $em->getClassMetadata($class);
        $class = $metadata->getName();
        if (!class_exists($class)) {
            throw new PendingException($class.' does not exists.');
        }
        $hash = $table->getHash();
        foreach ($hash as $row) {
            $entity = new $class();
            foreach ($row as $name => $value) {
                if ($entity instanceof UserInterface && $name == 'token') {
                    $token = new Session();
                    $token->setPlainToken($value);
                    $entity->addSession($token);
                    continue;
                }
                $method = 'set'.ucfirst($name);
                if (method_exists($entity, $method)) {
                    call_user_func(array($entity, $method), $value);
                }
            }
            $em->persist($entity);
        }
        
        $em->flush();
    }    
    
    /**
     * @When /^I go to "([^"]*)" using previous token$/
     */
    public function iGoToUsingPreviousToken($url)
    {
        $this->visit($this->getUrlWithToken($url));
    }
        
    /**
     * @When /^I go to "([^"]*)" as post using previous token$/
     */
    public function iGoToAsPostUsingPreviousToken($url)
    {
        $this->iSendAPostRequestTo($this->getUrlWithToken($url));
    }
    
    /**
     * @When /^I go to "([^"]*)" using previous token with post data:$/
     */
    public function iGoToUsingPreviousTokenWithPostData($url, PyStringNode $string)
    {
        $this->iSendAPostRequestToWithBody($this->getUrlWithToken($url), $string);
    }
        
    /**
     * @Given /^mail "([^"]*)" is sent$/
     */
    public function mailIsSent($subject)
    {
        $finder = $this->getMailFiles();
        foreach ($finder as $file) {
            $message = unserialize(file_get_contents($file));
            assertInstanceOf('\Swift_Message', $message);
            assertEquals($subject, $message->getSubject());
        }
    }
        
    /**
     * @When /^I go to reset password link$/
     */
    public function iGoToResetPasswordLink()
    {
        $link = null;
        $finder = $this->getMailFiles();
        foreach ($finder as $file) {
            $message = unserialize(file_get_contents($file));
            assertInstanceOf('\Swift_Message', $message);
            preg_match('#http(.*)#', $message->getBody(), $matches);
            if ($matches) {
                $link = $matches[0];
                break;
            }
        }
        assertNotNull($link, 'Reset password link is not null?');
        $this->visit($link);
    }

}
    
// @codeCoverageIgnoreEnd
