<?php

namespace Arcanys\SportizingmeTestBundle\Tests;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class BehatTest extends BehatTestCase
{
    
    protected function getFeatures()
    {
        return '@ArcanysSportizingmeTestBundle';
    }

    public function testThatBehatScenariosMeetAcceptanceCriteria()
    {
        $result = $this->runBehat();
        $this->assertEquals(0, $result);
    }

}
