<?php

namespace Arcanys\SportizingmeTestBundle\Tests;

use Behat\Behat\Console\BehatApplication;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
abstract class BehatTestCase extends WebTestCase
{

    protected $input;
    protected $output;
    protected $app;

    public function __construct()
    {
        list($folder, ) = explode('src', __DIR__);
        $config = realpath($folder . 'behat.yml');
        $features = $this->getFeatures();
        if (!$features) {
            throw new \Exception('Please specify features or bundle. eg. @BlogBundle/api');
        }
        $format = $this->getFormat();
        $this->input = new ArrayInput(array(
                    '--format' => $format ? $format : 'failed',
                    '--config' => $config,
                    '--no-paths' => '',
                    'features' => $features,
                ));
        $this->output = new ConsoleOutput();
        $this->app = new BehatApplication('DEV');
        $this->app->setAutoExit(false);
    }

    protected function getFormat()
    {
        return 'failed';
    }

    protected abstract function getFeatures();

    protected function runBehat()
    {
        try {
            $result = $this->app->run($this->input, $this->output);
            return $result;
        } catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }

}
