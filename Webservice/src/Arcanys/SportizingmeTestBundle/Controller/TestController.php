<?php

namespace Arcanys\SportizingmeTestBundle\Controller;

use RMS\PushNotificationsBundle\Message\iOSMessage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class TestController extends Controller
{
    
    /**
     * @Route("/ios", name="test_ios")
     */
    public function testIOSAction()
    {
        $message = new iOSMessage();
        $message->setMessage('Oh my! A push notification!');
        $message->setDeviceIdentifier('test012fasdf482asdfd63f6d7bc6d4293aedd5fb448fe505eb4asdfef8595a7');

        $this->container->get('rms_push_notifications')->send($message);
        
        return new Response('Hello world!');
    }
    
}
