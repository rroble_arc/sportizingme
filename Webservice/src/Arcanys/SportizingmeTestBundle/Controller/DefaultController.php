<?php

namespace Arcanys\SportizingmeTestBundle\Controller;

use Arcanys\SecurityBundle\Webservice\Token\WebserviceToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class DefaultController extends Controller
{

    /**
     * @Route("/login", name="arcTestLoginAction")
     */
    public function indexAction()
    {
        $token = $this->get('security.context')->getToken();
        $result = array(
            'error' => false,
            'message' => 'test',
            'data' => array(
                'testing' => 'okay', 
                'token' => $token instanceof WebserviceToken ? $token->getToken() : null,
                'user' => $token->getUsername()
            ),
        );
        return new JsonResponse($result);
    }

}
