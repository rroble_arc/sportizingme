//
//  SMEWorkOutTimer.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SMETimeManager.h"

typedef enum {
    WPGym,
    WPRunning,
    WPCycling,
    WPMMA,
    WPSoccer
}Workoutplace;

@interface SMEWorkOutTimer : UIViewController<SMETimeManagerDelegate,UIAlertViewDelegate>

@property (nonatomic) Workoutplace workoutplace;
@property (weak, nonatomic) IBOutlet UILabel *hoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *minsLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondsLabel;
@property (weak, nonatomic) IBOutlet UIButton *timerBtn;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

- (IBAction)timerAction:(id)sender;

@end
