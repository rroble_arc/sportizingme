//
//  SMEAthleteSettings.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEAthleteSettings : UIViewController
- (IBAction)englishAction:(id)sender;
- (IBAction)frenchAction:(id)sender;

@end
