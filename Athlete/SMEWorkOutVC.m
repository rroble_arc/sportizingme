//
//  SMEWorkOutVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEWorkOutVC.h"

#import "IIViewDeckController.h"

#import "SMEWorkOutTimer.h"

#import "SMELanguage.h"

@interface SMEWorkOutVC ()

@end

@implementation SMEWorkOutVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self setupNavigationBar];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setLanguage];
}
#pragma mark Methods
-(void)setLanguage{
     SMELanguage *lang = [SMELanguage sharedManager];
    
     [self.gymBtn setTitle:[lang athleteWorkOutBtnGym] forState:UIControlStateNormal];
    [self.runBtn setTitle:[lang athleteWorkOutBtnRunning] forState:UIControlStateNormal];
    [self.cycBtn setTitle:[lang athleteWorkOutBtnCycling] forState:UIControlStateNormal];
    [self.mmaBtn setTitle:[lang athleteWorkOutBtnMMA] forState:UIControlStateNormal];
    [self.socBtn setTitle:[lang athleteWorkOutBtnSoccer] forState:UIControlStateNormal];
}
-(void)setupNavigationBar{
     
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
     self.navigationItem.leftBarButtonItem = btnBar;
  
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)workoutBtnAction:(id)sender {
    UIButton *btn = (UIButton*)sender;
    SMEWorkOutTimer *wot = [[SMEWorkOutTimer alloc]initWithNibName:@"SMEWorkOutTimer" bundle:nil];
    
    if(btn.tag == 1){
        wot.workoutplace = WPGym;
    }
    else if(btn.tag == 4){
        wot.workoutplace = WPMMA;
    }
    else if(btn.tag == 5){
        wot.workoutplace = WPSoccer;
    }

    [self.navigationController pushViewController:wot animated:YES];

}

@end
