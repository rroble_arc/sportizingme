//
//  SMEMeVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEMeVC : UIViewController

- (IBAction)twitterAction:(id)sender;
- (IBAction)facebookAction:(id)sender;
- (IBAction)locationAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *innerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *innerImageView2;
@property (strong, nonatomic) IBOutlet UIImageView *specialProgress;
@property (weak, nonatomic) IBOutlet UIImageView *outprog2;
@property (strong, nonatomic) IBOutlet UILabel *lblPercent;
@property (weak, nonatomic) IBOutlet UIImageView *outprog;
@property (strong, nonatomic) IBOutlet UIImageView *specialProgress2;
@property (strong, nonatomic) IBOutlet UILabel *lblPercent2;
@property (weak, nonatomic) IBOutlet UILabel *lblWeekResult;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthResult;
@property (weak, nonatomic) IBOutlet UILabel *lblSponsor;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrSponsor;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrDeal;
@property (weak, nonatomic) IBOutlet UILabel *lblWeek;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblOut1;
@property (weak, nonatomic) IBOutlet UILabel *lblOut2;
@property (weak, nonatomic) IBOutlet UILabel *lblCash;


//!
@property (weak, nonatomic) IBOutlet UILabel *lblCashResult;

@property (weak, nonatomic) IBOutlet UILabel *lblSponsorName;
@property (weak, nonatomic) IBOutlet UIImageView *imageMe;

@property (weak, nonatomic) IBOutlet UIImageView *imgGYM;
@property (weak, nonatomic) IBOutlet UIImageView *imgRUN;
@property (weak, nonatomic) IBOutlet UIImageView *imgCYC;
@property (weak, nonatomic) IBOutlet UIImageView *imgMMA;
@property (weak, nonatomic) IBOutlet UIImageView *imgSOC;





//!
@end
