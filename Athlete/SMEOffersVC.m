//
//  SMEOffersVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "SMEUserManager.h"
#import "SMEOffersVC.h"
#import "IIViewDeckController.h"
#import "SMEAppDelegate.h"

#import "SMEOffersDeclineVC.h"
#import "SMEOffersAcceptVC.h"
#import "SMELanguage.h"
#import "SVProgressHUD.h"

@interface SMEOffersVC ()

@end

@implementation SMEOffersVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLanguage];
    [self setupNavigationBar];
    self.theDealArray = [[NSMutableArray alloc]initWithObjects:@"Sports:",@"Frequency:",@"Location:",@"Duration:", nil];
    [self setOffer];
    
}
#pragma mark Methods
-(void)setOffer{
    SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.offersArray count]>0){
    self.durlbl2.text = [NSString stringWithFormat:@"%d",[[[appDelegate.offersArray objectAtIndex:0] objectForKey:@"duration"] intValue]];
    self.freqlbl2.text = [NSString stringWithFormat:@"%d",[[[appDelegate.offersArray objectAtIndex:0] objectForKey:@"frequency"] intValue]];
    self.loclbl2.text = [NSString stringWithFormat:@"%@",[[appDelegate.offersArray objectAtIndex:0] objectForKey:@"location"]];
       // [self.imgOffer setImageWithURL:[NSURL URLWithString:@""]];
    //self.durlbl2.text = [[appDelegate.offersArray objectAtIndex:0] ];
    
    NSMutableString *sportString =  [NSMutableString string];
    for(int i = 0; i<[[[appDelegate.offersArray objectAtIndex:0] objectForKey:@"sports"] count];i++)
    {
        [sportString appendString:[NSString stringWithFormat:@" %@",[[[appDelegate.offersArray objectAtIndex:0] objectForKey:@"sports"] objectAtIndex:i]]];
    }
    self.sportslbl2.text = sportString;
    }
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    
    [self.lblDeal setText:[lang athleteOffersLblTheDeal]];
    [self.sportslbl1 setText:[lang athleteMySponsorLblSports]];
    [self.freqlbl1 setText:[lang athleteMySponsorLblFrequency]];
    [self.loclbl1 setText:[lang athleteMySponsorLblLocation]];
    [self.durlbl1 setText:[lang athleteOffersLblDuration]];
    [self.acceptBtn setTitle:[lang athleteOffersLblAccept] forState:UIControlStateNormal];
    [self.declineBtn setTitle:[lang athleteOffersLblDecline] forState:UIControlStateNormal];
    
    
}

-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return self.cellSports;
    }
    else if(indexPath.row == 1){
        return self.cellFrequency;
    }else if (indexPath.row ==2){
        return self.cellLocation;
    }
    else if(indexPath.row == 3){
        return self.cellDuration;
    }
    return nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)acceptAction:(id)sender {
    
    SMEOffersAcceptVC *vc = [[SMEOffersAcceptVC alloc]initWithNibName:@"SMEOffersAcceptVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)declineAction:(id)sender {
    [SVProgressHUD show];
     SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString * urlString;
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:[[appDelegate.offersArray objectAtIndex:0] objectForKey:@"id"],@"offer_id", nil];
    
    urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/athlete/offer/decline?token=%@",[[SMEUserManager sharedManager]token]];
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        [SVProgressHUD dismiss];
        if(error){
            NSLog(@"error %@",result);
        }else{
            NSLog(@"result %@",result);
            if([result objectForKey:@"success"]!=nil){
            SMEOffersDeclineVC *vc = [[SMEOffersDeclineVC alloc]initWithNibName:@"SMEOffersDeclineVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
            }else{
                NSLog(@"Failed to decline offer");
            }
        }
        
    } progressCallback:nil];
    

}
@end
