//
//  SMEOffersConfirmVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/9/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEOffersConfirmVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblCong;
@property (weak, nonatomic) IBOutlet UILabel *lblSpons;
@property (weak, nonatomic) IBOutlet UILabel *lblRec;

@end
