//
//  SMEWorkOutTimer.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEWorkOutTimer.h"
#import "SMEAppDelegate.h"
#import "SMETimeManager.h"
#import "ARCTwitterManager.h"
#import "SMEWorkOutFinishVC.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"

@interface SMEWorkOutTimer ()

@end

@implementation SMEWorkOutTimer

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     SMELanguage *lang = [SMELanguage sharedManager];
    [self.timerBtn setTitle:[lang athleteWorkOutTimerBtnStart] forState:UIControlStateNormal];
   
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [self registerTimerNotification];

   [self.lbl1 setFont:[UIFont fontWithName:@"Let's go Digital" size:60]];
    [self.lbl2 setFont:[UIFont fontWithName:@"Let's go Digital" size:60]];
    [self.hoursLabel setFont:[UIFont fontWithName:@"Let's go Digital" size:60]];
    [self.secondsLabel setFont:[UIFont fontWithName:@"Let's go Digital" size:60]];
    [self.minsLabel setFont:[UIFont fontWithName:@"Let's go Digital" size:60]];
    [self setupNav];
}
-(void)setupNav{
 
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark CLocation

#pragma mark Timer
-(BOOL)checkTime{
    SMETimeManager *tm = [SMETimeManager sharedManager];
    
    if(tm.seconds >= 10){
        return YES;
    }
    
    return NO;
}
-(void)didChangeTime{
    SMELanguage *lang = [SMELanguage sharedManager];
    SMETimeManager *tm = [SMETimeManager sharedManager];
    if(tm.seconds == 0){
        [self.timerBtn setTitle:[lang athleteWorkOutTimerBtnStart] forState:UIControlStateNormal];
    }else{
        [self.timerBtn setTitle:[lang athleteWorkOutTimerBtnStop] forState:UIControlStateNormal];
    }

    NSString *secondsString = [NSString stringWithFormat:@"%ld",tm.seconds % 60];
    NSString *minuteString = [NSString stringWithFormat:@"%ld",(tm.seconds / 60) % 60];
    NSString *hourString = [NSString stringWithFormat:@"%ld",tm.seconds/3600];
    
    if([secondsString length]==1){
        secondsString = [NSString stringWithFormat:@"0%@",secondsString];
    }
    if([minuteString length]==1){
        minuteString = [NSString stringWithFormat:@"0%@",minuteString];
    }
    if([hourString length]==1){
        hourString = [NSString stringWithFormat:@"0%@",hourString];
    }
    
    self.hoursLabel.text =hourString;
    self.minsLabel.text = minuteString;
    self.secondsLabel.text = secondsString;

    
}
-(void)resetTime{
    [self.imgView setImage:[UIImage imageNamed:@"workoutcheck"]];
      SMETimeManager *tm = [SMETimeManager sharedManager];
    NSString *secondsString = [NSString stringWithFormat:@"%ld",tm.seconds % 60];
    NSString *minuteString = [NSString stringWithFormat:@"%ld",(tm.seconds / 60) % 60];
    NSString *hourString = [NSString stringWithFormat:@"%ld",tm.seconds/3600];
    
    if([secondsString length]==1){
        secondsString = [NSString stringWithFormat:@"0%@",secondsString];
    }
    if([minuteString length]==1){
        minuteString = [NSString stringWithFormat:@"0%@",minuteString];
    }
    if([hourString length]==1){
        hourString = [NSString stringWithFormat:@"0%@",hourString];
    }
    
    self.hoursLabel.text =hourString;
    self.minsLabel.text = minuteString;
    self.secondsLabel.text = secondsString;
    

    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    [ud setObject:[NSNumber numberWithLong:0] forKey:@"seconds"];
    [ud synchronize];
}
#pragma mark Actions
- (IBAction)timerAction:(id)sender {
    SMELanguage *lang = [SMELanguage sharedManager];
    SMETimeManager *tm = [SMETimeManager sharedManager];
     SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(![tm hasStarted]){
        [self.imgView setImage:[UIImage imageNamed:@"workoutx"]];
       
        [self.timerBtn setTitle: [lang athleteWorkOutTimerBtnStart] forState:UIControlStateNormal];
        [tm startTimer];
        [appDelegate.locationManager startUpdatingLocation];
       
    }else{
        
        if([self checkTime]){
            [tm stopTimer];
            [self resetTime];
            [self share];
            
        }else{
            [[[UIAlertView alloc]initWithTitle:[[SMELanguage sharedManager] athleteWorkOutAlert30] message:[[SMELanguage sharedManager] athleteWorkOutAlertSure] delegate:self cancelButtonTitle:[[SMELanguage sharedManager] athleteWorkOutAlertNo] otherButtonTitles:[[SMELanguage sharedManager] athleteWorkOutAlertYes], nil]show];
        }

    }
}
#pragma mark Methods
-(void)share{
    
    SMEWorkOutFinishVC *vc = [[SMEWorkOutFinishVC alloc]initWithNibName:@"SMEWorkOutFinishVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)fetch{
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"athlete_id", nil];
    NSString * urlString = @"";
    
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        if(error){
            
        }else{
            
        }
    } progressCallback:nil];
}
#pragma mark ARCTwitterManagerDelegate

-(void)ARCTwitterManagerSuccessfullyTweeted:(ARCTwitterManager*)manager{
    NSLog(@"Tweeted");
}
-(void)ARCTwitterManagerFailedToTweet:(ARCTwitterManager*)manager{
    NSLog(@"Tweet Fail");
}
#pragma mark AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
   SMELanguage *lang = [SMELanguage sharedManager];
     SMETimeManager *tm = [SMETimeManager sharedManager];
    if(buttonIndex == 1){
        [self.timerBtn setTitle:[lang athleteWorkOutTimerBtnStart] forState:UIControlStateNormal];
        [tm stopTimer];
        [self resetTime];
    }
    
}
-(void)registerTimerNotification{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didChangeTime) name:SMETimerManagerNotification object:nil];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
