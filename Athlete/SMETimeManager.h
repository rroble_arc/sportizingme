//
//  SMETimeManager.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString* const SMETimerManagerNotification;

@class SMETimeManager;
@protocol SMETimeManagerDelegate <NSObject>
@optional
-(void)didChangeTime:(SMETimeManager*)vc withSeconds:(long)seconds;

@end
@interface SMETimeManager : NSObject
@property (weak, nonatomic) id <SMETimeManagerDelegate> delegate;
//TIMER
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, readonly) long seconds;
@property (nonatomic) BOOL hasStarted;
//!
+ (SMETimeManager *)sharedManager;
-(void)startTimer;
-(void)stopTimer;
@end
