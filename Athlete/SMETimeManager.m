//
//  SMETimeManager.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMETimeManager.h"
NSString* const SMETimerManagerNotification = @"SMETimerManagerNotification";

static SMETimeManager *instance;

@implementation SMETimeManager


+ (SMETimeManager *)sharedManager{
    if (instance == nil) {
        instance = [[SMETimeManager alloc] init];
       
    }
    
    return instance;
}
//Timer Methods
-(void)startTimer{
    //self.delegate = delegate;
    self.hasStarted = YES;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDate *dateAwhile = [ud objectForKey:@"date"];
    
  
    NSNumber *seconds = [ud objectForKey:@"seconds"];

    NSTimeInterval lastDiff = [dateAwhile timeIntervalSinceNow];
    NSTimeInterval todaysDiff = [[NSDate date] timeIntervalSinceNow];
    NSTimeInterval dateDiff = todaysDiff - lastDiff;
    
    NSLog(@"SECONDS DRI %ld",[seconds longValue]);
    
    if([seconds longValue]>0) _seconds = [seconds longValue] + dateDiff;
    else _seconds = 0;
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                         target:self
                                                       selector:@selector(timerStarting)
                                                       userInfo:nil
                                                        repeats:YES];
    
}
-(void)timerStarting{
    
   
    _seconds++;
    [self sendNotification];
    //[[self delegate]didChangeTime:self withSeconds:_seconds];
    
    
}
-(void)stopTimer{
    self.hasStarted = NO;
    _seconds = 0;
     [self sendNotification];
    [self.timer invalidate];
}

-(void)sendNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:SMETimerManagerNotification object:nil];
    
  
}


@end
