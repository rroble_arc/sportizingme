//
//  SMEMySponsorVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEMySponsorVC.h"
#import "IIViewDeckController.h"
#import <QuartzCore/QuartzCore.h>
#import "SMELanguage.h"
#import "ARCFetcher.h"

static const CGFloat gx = 15;
static const CGFloat gy = 384;
static const CGFloat gw = 290;
static const CGFloat gh = 0.3;


@interface SMEMySponsorVC ()

@end

@implementation SMEMySponsorVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLanguage];
    [self setupNavigationBar];
    yey = .01;
    
    self.mydealArray = [[NSMutableArray alloc]initWithObjects:@"Sports:",@"Frequency:",@"Locaiton:", nil];

    [self addProgressBarInFrame:CGRectMake(self.outprog.frame.origin.x+2, self.outprog.frame.origin.y+4, self.outprog.frame.size.width, self.outprog.frame.size.height-5) withProgress:yey];

}
#pragma mark Methods
-(void)fetch{
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"athlete_id", nil];
    NSString * urlString = @"";
    
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        if(error){
            
        }else{
            
        }
    } progressCallback:nil];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.lblMySponsor setText:[lang athleteMySponsorLblSponsor]];
     [self.lblMyDeal setText:[lang athleteMySponsorLblDeal]];
     [self.lblsports1 setText:[lang athleteMySponsorLblSports]];
    [self.lblfrequency1 setText:[lang athleteMySponsorLblFrequency]];
    [self.lbllocation1 setText:[lang athleteMySponsorLblLocation]];
    [self.lblOut setText:[lang athleteMySponsorLblOut]];
    [self.lblWhere setText:[lang athleteMySponsorLblWhere]];
    
    
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)addProgressBarInFrame:(CGRect)frame withProgress:(CGFloat)progress
{
    
    float percent;
    percent = progress * 100;
    
    if(yey>0.13){
    [self.innerImageView setFrame:CGRectMake(gx, gy, gw*progress-34, self.innerImageView.frame.size.height)];
    
   
    [self.specialProgress setFrame:CGRectMake(self.innerImageView.frame.origin.x+self.innerImageView.frame.size.width-1, self.innerImageView.frame.origin.y,35, 18)];
        [self.lblPercent setText:[NSString stringWithFormat:@"%.0f%%",percent]];
        
        [self.specialProgress addSubview:self.lblPercent];
        
    [self.view addSubview:self.specialProgress];
    }else{
        [self.innerImageView setFrame:CGRectMake(gx, gy, gw*.13-34, self.innerImageView.frame.size.height)];
        
        
        [self.specialProgress setFrame:CGRectMake(self.innerImageView.frame.origin.x+self.innerImageView.frame.size.width-1, self.innerImageView.frame.origin.y,36, 18)];
        [self.lblPercent setText:[NSString stringWithFormat:@"%.0f%%",percent]];
        
        [self.specialProgress addSubview:self.lblPercent];
        [self.view addSubview:self.specialProgress];
    }
    [self.lblMainPercent setText:[NSString stringWithFormat:@"%.0f",percent]];

   
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)return self.cellSports;
    else if(indexPath.row == 1) return self.cellFrequency;
    else if(indexPath.row == 2) return self.cellLocation;
    return nil;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 1){
         [self.specialProgress removeFromSuperview];
        yey += 0.01;
        NSLog(@"%f",yey);
        [self addProgressBarInFrame:CGRectMake(self.outprog.frame.origin.x+2, self.outprog.frame.origin.y+4, self.outprog.frame.size.width, self.outprog.frame.size.height-5) withProgress:yey];
       
    }else{
        yey += 0.01;
         [self.specialProgress removeFromSuperview];
        [self addProgressBarInFrame:CGRectMake(self.outprog.frame.origin.x+2, self.outprog.frame.origin.y+4, self.outprog.frame.size.width, self.outprog.frame.size.height-5) withProgress:yey];
       
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
