//
//  SMEOffersAcceptVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/9/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    S,
    M,
    L,
    XL
}shirtSize;
@interface SMEOffersAcceptVC : UIViewController <UITableViewDataSource,UITableViewDelegate>{
    shirtSize shirtsize;
}
- (IBAction)confirmAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblAddress;
@property (weak, nonatomic) IBOutlet UITableView *tblSizes;
//
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSmall;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMedium;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellLarge;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellXLarge;
@property (weak, nonatomic) IBOutlet UILabel *lblSmall2;
@property (weak, nonatomic) IBOutlet UILabel *lblMed2;
@property (weak, nonatomic) IBOutlet UILabel *lblLarge2;
@property (weak, nonatomic) IBOutlet UILabel *lblXLarge2;

//
@property (weak, nonatomic) IBOutlet UILabel *lblSmall1;
@property (weak, nonatomic) IBOutlet UILabel *lblMed1;
@property (weak, nonatomic) IBOutlet UILabel *lblLarge1;
@property (weak, nonatomic) IBOutlet UILabel *lblXLarge1;


//!
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAdd1;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAdd2;
//
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UILabel *lblReconfirm;
@property (weak, nonatomic) IBOutlet UILabel *lblYourSize;

//!

//
@property (weak, nonatomic) IBOutlet UITextField *txtUserAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtNewAddress;

//!
@end
