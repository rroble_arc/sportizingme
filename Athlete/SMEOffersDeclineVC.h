//
//  SMEOffersDeclineVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/9/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEOffersDeclineVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblSorry;
@property (weak, nonatomic) IBOutlet UILabel *lblNext;

@end
