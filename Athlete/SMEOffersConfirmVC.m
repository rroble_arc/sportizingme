//
//  SMEOffersConfirmVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/9/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEOffersConfirmVC.h"
#import "IIViewDeckController.h"
#import "SMELanguage.h"

@interface SMEOffersConfirmVC ()

@end

@implementation SMEOffersConfirmVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setLanguage];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.lblCong setText:[lang athleteOffersCongratulations]];
    [self.lblSpons setText:[lang athleteOffersSponsored]];
    [self.lblRec setText:[lang athleteOffersReceive]];
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
