//
//  SMEMySponsorVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEMySponsorVC : UIViewController
{
    float yey;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *specialProgress;
@property (nonatomic, strong) NSMutableArray *mydealArray;
@property (weak, nonatomic) IBOutlet UIImageView *outprog;
@property (weak, nonatomic) IBOutlet UIImageView *innerImageView;


//!
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSports;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellFrequency;

@property (strong, nonatomic) IBOutlet UITableViewCell *cellLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblsports1;
@property (weak, nonatomic) IBOutlet UILabel *lblsports2;
@property (weak, nonatomic) IBOutlet UILabel *lblfrequency1;
@property (weak, nonatomic) IBOutlet UILabel *lblfrequency2;

@property (weak, nonatomic) IBOutlet UILabel *lbllocation1;
@property (weak, nonatomic) IBOutlet UILabel *lbllocation2;


@property (weak, nonatomic) IBOutlet UILabel *lblMySponsor;
@property (weak, nonatomic) IBOutlet UILabel *lblMyDeal;

@property (weak, nonatomic) IBOutlet UILabel *lblWhere;
@property (weak, nonatomic) IBOutlet UILabel *lblOut;
@property (strong, nonatomic) IBOutlet UILabel *lblPercent;
@property (weak, nonatomic) IBOutlet UILabel *lblMainPercent;

//!
@end
