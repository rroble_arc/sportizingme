//
//  SMEOffersDeclineVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/9/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEOffersDeclineVC.h"
#import "SMELanguage.h"
#import "IIViewDeckController.h"

@interface SMEOffersDeclineVC ()

@end

@implementation SMEOffersDeclineVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLanguage];
    [self setupNavigationBar];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    
    [self.lblSorry setText:[lang athleteOffersSorry]];
    [self.lblNext setText:[lang athleteOffersNextTime]];
    
    
}
-(void)setupNavigationBar{
     [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
