//
//  SMEWorkOutFinishVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/10/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEWorkOutFinishVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblHope;
@property (weak, nonatomic) IBOutlet UILabel *lblEarned;

@end
