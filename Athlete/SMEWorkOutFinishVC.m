//
//  SMEWorkOutFinishVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/10/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEWorkOutFinishVC.h"
#import "IIViewDeckController.h"
#import "SMELanguage.h"

@interface SMEWorkOutFinishVC ()

@end

@implementation SMEWorkOutFinishVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNavigationBar];
    [self setLanguage];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    
    [self.lblHope setText:[lang athleteWorkoutLblHope]];
    [self.lblEarned setText:[lang athleteWorkOutLblEarned]];
}
-(void)setupNavigationBar{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
