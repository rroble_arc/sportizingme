//
//  SMEMeVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "SMEUserManager.h"
#import "SMEAppDelegate.h"
#import "SMEMeVC.h"
#import "IIViewDeckController.h"
#import "ARCTwitterManager.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"
#import "SVProgressHUD.h"


static const CGFloat gx = 15;
static const CGFloat gy = 282;
static const CGFloat gw = 290;
static const CGFloat gh = 0.3;

static const CGFloat gx2 = 17;
static const CGFloat gy2 = 337;
static const CGFloat gw2 = 290;
static const CGFloat gh2 = 0.3;

@interface SMEMeVC ()<ARCTwitterManagerDelegate>

@end

@implementation SMEMeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLanguage];
    [self setupNavigationBar];
    
   
   
}
-(void)viewWillAppear:(BOOL)animated{
     [self fetch];
}

#pragma mark Methods
-(void)fetch{
    [SVProgressHUD show];

    NSString *urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/athlete/me?token=%@",[[SMEUserManager sharedManager] token]];
    
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:nil userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        [SVProgressHUD dismiss];
        if(error){
             NSLog(@"error %@",result);
        }else{
            NSLog(@"error2 %@",result);
            if([result objectForKey:@"error"]==nil){
                [self setMe:result];
            }else{
                
            }
        }
    } progressCallback:nil];
}
-(void)setMe:(id)result{
    [self.imageMe setImageWithURL:[NSURL URLWithString:[result objectForKey:@"link_image"]]];
    NSArray *sports = [result objectForKey:@"sports"];
    for(int i=0;i<[sports count];i++){
        if([[sports objectAtIndex:i]isEqualToString:@"GYM"]) [self.imgGYM setAlpha:1]; else [self.imgGYM setAlpha:.3];
        
        if([[sports objectAtIndex:i]isEqualToString:@"RUN"]) [self.imgRUN setAlpha:1]; else [self.imgRUN setAlpha:.3];
        
        if([[sports objectAtIndex:i]isEqualToString:@"CYC"]) [self.imgCYC setAlpha:1]; else [self.imgCYC setAlpha:.3];
        
         if([[sports objectAtIndex:i]isEqualToString:@"MMA"]) [self.imgMMA setAlpha:1]; else [self.imgMMA setAlpha:.3];
        
         if([[sports objectAtIndex:i]isEqualToString:@"SOC"]) [self.imgSOC setAlpha:1]; else [self.imgSOC setAlpha:.3];
    }
    
    self.lblCashResult.text = [NSString stringWithFormat:@"$ %2f",[[result objectForKey:@"cash"] floatValue]];
    self.lblSponsorName.text = [result objectForKey:@"sponsor_name"];
     
    float weekResult = [[result objectForKey:@"this_week"] floatValue]*100;
    float monthResult = [[result objectForKey:@"this_month"] floatValue]*100;
  
    self.lblWeekResult.text = [NSString stringWithFormat:@"%f",weekResult];
    self.lblMonthResult.text = [NSString stringWithFormat:@"%f",monthResult];
    
    [self addProgressBarInFrame:CGRectMake(self.outprog.frame.origin.x+2, self.outprog.frame.origin.y+4, self.outprog.frame.size.width, self.outprog.frame.size.height-5) withProgress:weekResult/100];
    
    [self addProgressBarInFrame2:CGRectMake(self.outprog.frame.origin.x+2, self.outprog.frame.origin.y+4, self.outprog.frame.size.width, self.outprog.frame.size.height-5) withProgress:monthResult/100];
    
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    if(lang.type == SMELanguageType_french)
        [self.lblCash setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    else [self.lblCash setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    [self.lblWeek setText:[lang athleteMeWeek]];
    [self.lblMonth setText:[lang athleteMeMonth]];
    [self.lblSponsor setText:[lang athleteMeSports]];
    [self.lblCurrSponsor setText:[lang athleteMeCurrSponsor]];
    [self.lblCurrDeal setText:[lang athleteMeCurrDeal]];
    [self.lblOut1 setText:[lang athleteMeOut]];
    [self.lblOut2 setText:[lang athleteMeOut]];
    [self.lblCash setText:[lang athleteMeCash]];
}
-(void)addProgressBarInFrame:(CGRect)frame withProgress:(CGFloat)progress
{
    
    float percent;
    percent = progress * 100;
    
   // if(yey>0.13){
        [self.innerImageView setFrame:CGRectMake(gx, gy, gw*progress-34, self.innerImageView.frame.size.height)];
        
        
        [self.specialProgress setFrame:CGRectMake(self.innerImageView.frame.origin.x+self.innerImageView.frame.size.width-1, self.innerImageView.frame.origin.y,35, 18)];
        [self.lblPercent setText:[NSString stringWithFormat:@"%.0f%%",percent]];
    [self.lblWeekResult setText:[NSString stringWithFormat:@"%.0f",percent]];
        [self.specialProgress addSubview:self.lblPercent];
        
        [self.view addSubview:self.specialProgress];

    
    
}
-(void)addProgressBarInFrame2:(CGRect)frame withProgress:(CGFloat)progress
{
    
    float percent;
    percent = progress * 100;
    
    [self.innerImageView2 setFrame:CGRectMake(gx2, gy2, gw2*progress-34, self.innerImageView2.frame.size.height)];
    
    
    [self.specialProgress2 setFrame:CGRectMake(self.innerImageView2.frame.origin.x+self.innerImageView2.frame.size.width-1, self.innerImageView2.frame.origin.y,35, 18)];
    [self.lblPercent2 setText:[NSString stringWithFormat:@"%.0f%%",percent]];
      [self.lblMonthResult setText:[NSString stringWithFormat:@"%.0f",percent]];
    [self.specialProgress2 addSubview:self.lblPercent2];
    
    [self.view addSubview:self.specialProgress2];
    
    
    
}
-(void)setupNavigationBar{
      [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG.png"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)setupTwitter{
    [[ARCTwitterManager sharedManager] authenticateWithApiKey:@"xchwwHPy8YXB10Y2E7Mifw" secret:@"CarfeWRC9Om7GsV2Q6bMTXN6AcQuT0mg7q5k1vxs0I" delegate:self];
    [[ARCTwitterManager sharedManager]showLoginWindow:self];
   
}
#pragma mark 
-(void)ARCTwitterManagerDidLoginSuccessfully:(ARCTwitterManager*)manager{
    [[ARCTwitterManager sharedManager]postTweet:@"YEAH"];
}
-(void)ARCTwitterManagerDidFailToLogin:(ARCTwitterManager*)manager{
    NSLog(@"Login Fail");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)twitterAction:(id)sender {
    [self setupTwitter];
}

- (IBAction)facebookAction:(id)sender {
}

- (IBAction)locationAction:(id)sender {
   SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.locationManager stopUpdatingLocation];
   
}
@end
