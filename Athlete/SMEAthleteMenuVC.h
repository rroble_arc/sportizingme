//
//  SMEAthleteMenuVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/6/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEAthleteMenuVC : UIViewController

@property (strong, nonatomic) NSMutableArray *offersArray;
@property (weak, nonatomic) IBOutlet UIImageView *imgBadge;
@property (weak, nonatomic) IBOutlet UILabel *lblBadge;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSponsor;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMe;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellOffers;
@property (strong, nonatomic) IBOutlet UITableViewCell *celWorkout;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSettings;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAccount;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSignout;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellBlank;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellBlank2;
//
@property (weak, nonatomic) IBOutlet UILabel *lblSponsor;
@property (weak, nonatomic) IBOutlet UILabel *lblMe;
@property (weak, nonatomic) IBOutlet UILabel *lblOffers;
@property (weak, nonatomic) IBOutlet UILabel *lblAccount;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkout;
@property (weak, nonatomic) IBOutlet UILabel *lblSettings;
@property (weak, nonatomic) IBOutlet UILabel *lblSignOut;

//!








@end
