//
//  SMEAthleteMenuVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/6/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEUserManager.h"
#import "ARCFacebookManager.h"
#import "ARCFetcher.h"

#import "SMEAthleteMenuVC.h"

#import "IIViewDeckController.h"

#import "SMEAppDelegate.h"
#import "SMEIntroVC.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"

//Views

#import "SMEMySponsorVC.h"
#import "SMEMeVC.h"
#import "SMEOffersVC.h"
#import "SMEWorkOutVC.h"
#import "SMEMyAccountVC.h"
#import "SMEAthleteSettings.h"

//!

@interface SMEAthleteMenuVC ()

@property (strong, nonatomic) NSMutableArray *tableArray;

@end

@implementation SMEAthleteMenuVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"WHEN MN KA OI");
    
    self.tableArray = [[NSMutableArray alloc]initWithObjects:@"My Sponsor",@"Me",@"Offers",@"Work Out!",@"My Account",@"Settings",@"Sign Out", nil];
    
   
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateLanguage];
    [self.tableView reloadData];
   

    [self fetch];
    
    
}
-(void)fetch{
    
    NSString * urlString;
   
    urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/athlete/offer/list?token=%@",[[SMEUserManager sharedManager]token]];
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:nil userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        if(error){
            NSLog(@"error %@",result);
        }else{
            NSLog(@"result %@",result);
            if([result isKindOfClass:[NSArray class]]){
              SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.offersArray = [NSMutableArray arrayWithArray:result];
            
                
                if([appDelegate.offersArray count]!=0){
                    [self.lblBadge setText:[NSString stringWithFormat:@"%d",[appDelegate.offersArray count]]];
                    
                    [self.imgBadge setHidden:NO];
                    [self.lblBadge setHidden:NO];
                }else{
                    [self.imgBadge setHidden:YES];
                    [self.lblBadge setHidden:YES];
                }
                
            }else{
                
            }
        }
        
    } progressCallback:nil];
}
-(void)updateLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];

    [self.lblSponsor setText:[lang athleteMenuCellMySponsor]];
    [self.lblMe setText:[lang athleteMenuCellMe]];
    [self.lblOffers setText:[lang athleteMenuCellOffers]];
    [self.lblWorkout setText:[lang athleteMenuCellWorkOut]];
    [self.lblAccount setText:[lang athleteMenuCellMyAccount]];
    [self.lblSettings setText:[lang athleteMenuCellSettings]];
    [self.lblSignOut setText:[lang athleteMenuCellSignOut]];
  
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{


    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{


    return [self.tableArray count]+2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    
    if(row == 0){
        return self.cellSponsor;
    }else if(row == 1){
        return self.cellMe;
    }else if(row ==2){
        return self.cellOffers;
    }else if(row == 3){
        return self.celWorkout;
    }else if(row ==4){
        return self.cellAccount;
    }else if(row ==5){
        return self.cellSettings;
    }else if(row ==6){
        return self.cellSignout;
    }else if(row == 7){
        return self.cellBlank;
    }else if(row == 8){
        return self.cellBlank2;
    }
    return nil;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 3 || indexPath.row == 8){
        return 176;
    }
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        SMEMySponsorVC *vc = [[SMEMySponsorVC alloc]initWithNibName:@"SMEMySponsorVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
        
    }
    else if(indexPath.row == 1){
        SMEMeVC *vc = [[SMEMeVC alloc]initWithNibName:@"SMEMeVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
        
    }
    else if(indexPath.row == 2){
         SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
        if([appDelegate.offersArray count] != 0){
        SMEOffersVC *vc = [[SMEOffersVC alloc]initWithNibName:@"SMEOffersVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
        }
    }
    else if(indexPath.row == 3){
        SMEWorkOutVC *vc = [[SMEWorkOutVC alloc]initWithNibName:@"SMEWorkOutVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
    }
    else if(indexPath.row == 4){
        SMEMyAccountVC *vc = [[SMEMyAccountVC alloc]initWithNibName:@"SMEMyAccountVC" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
    }
    else if(indexPath.row == 5){
        SMEAthleteSettings *vc = [[SMEAthleteSettings alloc]initWithNibName:@"SMEAthleteSettings" bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
        [self.viewDeckController setCenterController:nc];
        [self.viewDeckController closeLeftViewBouncing:nil];
    }else if(indexPath.row == 6){
        
        
        NSString *urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/signout?token=%@",[[SMEUserManager sharedManager] token]];
        ARCFetcher *fetch = [[ARCFetcher alloc]init];
        
        [fetch startFetchWithURLString:urlString postParams:nil userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
            if(error){
                NSLog(@"resultxx %@",result);
            }else{
               
                if([result objectForKey:@"error"]==nil){
                    
                    [[SMEUserManager sharedManager]logout];
                    ARCFacebookManager *fbm = [ARCFacebookManager sharedManager];
                    [fbm logoutFromFacebook];
                    
                    SMEIntroVC *vc = [[SMEIntroVC alloc]initWithNibName:@"SMEIntroVC" bundle:nil];
                    UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:vc];
                    
                    SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
                    appDelegate.window.rootViewController = nc;
                    [appDelegate.window makeKeyAndVisible];
                }else{
                    NSLog(@"Unable to Logout");
                    
                }
                
                
            }
        } progressCallback:nil];
        
        
    }
    
}

@end
