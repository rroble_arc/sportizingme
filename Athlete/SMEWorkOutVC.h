//
//  SMEWorkOutVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEWorkOutVC : UIViewController
- (IBAction)workoutBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *gymBtn;
@property (weak, nonatomic) IBOutlet UIButton *runBtn;
@property (weak, nonatomic) IBOutlet UIButton *cycBtn;
@property (weak, nonatomic) IBOutlet UIButton *mmaBtn;
@property (weak, nonatomic) IBOutlet UIButton *socBtn;

@end
