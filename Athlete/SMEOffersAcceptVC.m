//
//  SMEOffersAcceptVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/9/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "SMEUserManager.h"
#import "SMEAppDelegate.h"
#import "SMEOffersAcceptVC.h"
#import "IIViewDeckController.h"
#import "SMEOffersConfirmVC.h"
#import "SMELanguage.h"
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface SMEOffersAcceptVC ()
{
    CGFloat animatedDistance;
}
@end

@implementation SMEOffersAcceptVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setLanguage];
    [self setOffersAccept];
}
-(void)setOffersAccept{
    self.txtUserAddress.text = [[SMEUserManager sharedManager] location];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    if(lang.type == SMELanguageType_french){
        [self.lblReconfirm setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
        
    }else{
        [self.lblReconfirm setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    }
    
    [self.btnConfirm setTitle:[lang athleteOffersConfirm] forState:UIControlStateNormal];
    [self.lblReconfirm setText:[lang athleteOffersReconfirm]];
    [self.lblYourSize setText:[lang athleteOffersYourSize]];
    
    [self.lblSmall1 setText:[lang athleteOffersSmall]];
    [self.lblMed1 setText:[lang athleteOffersMedium]];
    [self.lblLarge1 setText:[lang athleteOffersLarge]];
    [self.lblXLarge1 setText:[lang athleteOffersXLarge]];
    [self.lblSmall2 setText:[lang athleteOffersSmall]];
    [self.lblMed2 setText:[lang athleteOffersMedium]];
    [self.lblLarge2 setText:[lang athleteOffersLarge]];
    [self.lblXLarge2 setText:[lang athleteOffersXLarge]];
}
-(void)setupNavigationBar{
       [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 29, 30)];
    [btn setImage:[UIImage imageNamed:@"deckMenuButton"] forState:UIControlStateNormal];
    [btn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if(tableView == self.tblAddress){
            return 2;
    }else if(tableView == self.tblSizes){
        return 4;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tblAddress){
        if(indexPath.row == 0)
        return self.cellAdd1;
        
        else if(indexPath.row == 1)
        return self.cellAdd2;
   
    }
        
    else if(tableView == self.tblSizes){
        if(indexPath.row == 0)      return self.cellSmall;
        else if(indexPath.row == 1) return self.cellMedium;
        else if (indexPath.row ==2) return self.cellLarge;
        else if(indexPath.row == 3) return self.cellXLarge;
    }
    
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.tblSizes){
        if(indexPath.row == 0)    {
            shirtsize = S;
            [self.lblSmall2 setHidden:NO];
            [self.lblMed2 setHidden:YES];
            [self.lblLarge2 setHidden:YES];
            [self.lblXLarge2 setHidden:YES];
            
            [self.lblSmall1 setHidden:YES];
            [self.lblMed1 setHidden:NO];
            [self.lblLarge1 setHidden:NO];
            [self.lblXLarge1 setHidden:NO];
        }else if(indexPath.row == 1){
            shirtsize = M;
            [self.lblSmall2 setHidden:YES];
            [self.lblMed2 setHidden:NO];
            [self.lblLarge2 setHidden:YES];
            [self.lblXLarge2 setHidden:YES];
            
            [self.lblSmall1 setHidden:NO];
            [self.lblMed1 setHidden:YES];
            [self.lblLarge1 setHidden:NO];
            [self.lblXLarge1 setHidden:NO];
        }else if(indexPath.row == 2){
            shirtsize = L;
            [self.lblSmall2 setHidden:YES];
            [self.lblMed2 setHidden:YES];
            [self.lblLarge2 setHidden:NO];
            [self.lblXLarge2 setHidden:YES];
            
            [self.lblSmall1 setHidden:NO];
            [self.lblMed1 setHidden:NO];
            [self.lblLarge1 setHidden:YES];
            [self.lblXLarge1 setHidden:NO];
        }else if(indexPath.row == 3){
            shirtsize = XL;
            [self.lblSmall2 setHidden:YES];
            [self.lblMed2 setHidden:YES];
            [self.lblLarge2 setHidden:YES];
            [self.lblXLarge2 setHidden:NO];
            
            [self.lblSmall1 setHidden:NO];
            [self.lblMed1 setHidden:NO];
            [self.lblLarge1 setHidden:NO];
            [self.lblXLarge1 setHidden:YES];
        }
    
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirmAction:(id)sender {
    SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString * urlString;
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:[[appDelegate.offersArray objectAtIndex:0] objectForKey:@"id"],@"offer_id", nil];
    
    urlString = [NSString stringWithFormat:@"http://192.168.254.20/sportizingme/api/athlete/offer/accept?token=%@",[[SMEUserManager sharedManager]token]];
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        if(error){
            NSLog(@"error %@",result);
        }else{
            NSLog(@"result %@",result);
            if([result objectForKey:@"success"]!=nil){
              
            }else{
                NSLog(@"Failed to decline offer");
            }
        }
        
    } progressCallback:nil];
   // SMEOffersConfirmVC *vc = [[SMEOffersConfirmVC alloc]initWithNibName:@"SMEOffersConfirmVC" bundle:nil];
   // [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == self.txtNewAddress){
    if([textField.text isEqualToString:@"Add new one"]){
        textField.text = @"";
    }
    }

    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if(textField == self.txtNewAddress){
        if([textField.text isEqualToString:@""]){
            textField.text = @"Add new one";
        }
    }}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
 
        [textField resignFirstResponder];
    
    
    return YES;
}
@end
