//
//  SMEOffersVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEOffersVC : UIViewController <UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *theDealArray;
@property (weak, nonatomic) IBOutlet UIImageView *imgOffer;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *declineBtn;
- (IBAction)acceptAction:(id)sender;
- (IBAction)declineAction:(id)sender;
//
@property (strong, nonatomic) IBOutlet UITableViewCell *cellSports;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellFrequency;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellLocation;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDuration;
//Label1
@property (weak, nonatomic) IBOutlet UILabel *sportslbl1;
@property (weak, nonatomic) IBOutlet UILabel *freqlbl1;
@property (weak, nonatomic) IBOutlet UILabel *loclbl1;
@property (weak, nonatomic) IBOutlet UILabel *durlbl1;

//
//Label2
@property (weak, nonatomic) IBOutlet UILabel *sportslbl2;
@property (weak, nonatomic) IBOutlet UILabel *freqlbl2;
@property (weak, nonatomic) IBOutlet UILabel *loclbl2;
@property (weak, nonatomic) IBOutlet UILabel *durlbl2;

@property (weak, nonatomic) IBOutlet UILabel *lblDeal;
//
@end
