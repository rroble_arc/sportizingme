//
//  SMELanguage.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SMELanguageType_english,
    SMELanguageType_french
}SMELanguageType;

@interface SMELanguage : NSObject

@property (nonatomic) SMELanguageType type;
+ (SMELanguage*)sharedManager;
//SMEIntroVC
-(NSString*)introBtnAthlete;
-(NSString*)introBtnSponsor;
-(NSString*)introBtnHowItWorks;
-(NSString*)back;

//SMEAthleteIntroVC
-(NSString*)athleteIntroBtnLogin;
-(NSString*)athleteIntroBtnSignup;
-(NSString*)athleteTakeMeBackBtnSignup;
-(NSString*)athleteIntroLblAthlete;
-(NSString*)athleteIntroLblOr;

//SMEAthleteLoginVC
-(NSString*)athleteLoginBtnLogin;
-(NSString*)athleteLoginBtnLoginFacebook;
-(NSString*)athleteLoginBtnForgetPassword;
-(NSString*)athleteLoginTxtEmail;
-(NSString*)athleteLoginTxtPassword;
-(NSString*)athleteLoginLblOr;

//SMEAthleteSignUpVC
-(NSString*)athleteSignupBtnNext;
-(NSString*)athleteSignupBtnSignUpFacebook;
-(NSString*)athleteSignupTxtName;
-(NSString*)athleteSignupTxtEmail;
-(NSString*)athleteSignupTxtPassword;
-(NSString*)athleteSignupTxtPassword2;

//SMEAthleteSignUpNextVC
-(NSString*)athleteSignupBtnDone;
-(NSString*)athleteSignupTxtAge;
-(NSString*)athleteSignupTxtLocation;
-(NSString*)athleteSignupTxtSport;
-(NSString*)athleteSignupTxtHeight;
-(NSString*)athleteSignupTxtWeight;
-(NSString*)athleteSignupTxtPaypal;
-(NSString*)athleteSignupLblTellus;

//SMESponsorIntroVC
-(NSString*)sponsorIntroBtnLogin;
-(NSString*)sponsorIntroBtnSignup;
-(NSString*)sponsorIntroBtnAthlete;
-(NSString*)sponsorIntroBtnOr;

//SMESponsorLoginVC
-(NSString*)sponsorLoginBtnLogin;
-(NSString*)sponsorLoginBtnForgetPassword;
-(NSString*)sponsorLoginTxtEmail;
-(NSString*)sponsorLoginTxtPassword;

//SMESponsorSignUpVC
-(NSString*)sponsorSignupBtnNext;
-(NSString*)sponsorSignupTxtCompanyName;
-(NSString*)sponsorSignupTxtEmail;
-(NSString*)sponsorSignupTxtPassword;
-(NSString*)sponsorSignupTxtPassword2;

//SMESponsorSignupNextVC
-(NSString*)sponsorSignupNextBtnDone;
-(NSString*)sponsorSignupNextTxtLocation;
-(NSString*)sponsorSignupNextTxtIndustry;
-(NSString*)sponsorSignupNextTxtEmployees;
-(NSString*)sponsorSignupNextTxtRevenue;
-(NSString*)sponsorSignupNextTxtPaypal;
-(NSString*)sponsorSignupNextLblTellUs;
-(NSString*)sponsorSignupNextLblLetsFind;
-(NSString*)sponsorSignupNextLblLocation;
-(NSString*)sponsorSignupNextLblIndustry;
-(NSString*)sponsorSignupNextLblEmployees;
-(NSString*)sponsorSignupNextLblRevenue;
-(NSString*)sponsorSignupNextLblPaypal;

//SMEAthleteMenuVC
-(NSString*)athleteMenuCellMySponsor;
-(NSString*)athleteMenuCellMe;
-(NSString*)athleteMenuCellOffers;
-(NSString*)athleteMenuCellWorkOut;
-(NSString*)athleteMenuCellMyAccount;
-(NSString*)athleteMenuCellSettings;
-(NSString*)athleteMenuCellSignOut;

//MESponsorMenUVC
-(NSString*)sponsorMenuCellMyAthletes;
-(NSString*)sponsorMenuCellMyCompany;
-(NSString*)sponsorMenuCellDashboard;
-(NSString*)sponsorMenuCellFindAthletes;
-(NSString*)sponsorMenuCellMyAccount;
-(NSString*)sponsorMenuCellSettings;
-(NSString*)sponsorMenuCellSignOut;

//SMEAthleteWorkOutVC
-(NSString*)athleteWorkOutAlert30;
-(NSString*)athleteWorkOutAlertSure;
-(NSString*)athleteWorkOutAlertYes;
-(NSString*)athleteWorkOutAlertNo;
-(NSString*)athleteWorkOutBtnGym;
-(NSString*)athleteWorkOutBtnRunning;
-(NSString*)athleteWorkOutBtnCycling;
-(NSString*)athleteWorkOutBtnMMA;
-(NSString*)athleteWorkOutBtnSoccer;
-(NSString*)athleteWorkoutLblHope;
-(NSString*)athleteWorkOutLblEarned;

//SMEAthleteWorkOutTimerVC
-(NSString*)athleteWorkOutTimerBtnStart;
-(NSString*)athleteWorkOutTimerBtnStop;

//SMEFindAthleteVC
-(NSString*)sponsorFindAthleteBtnFind;
-(NSString*)sponsorFindAthleteBtnAdvanced;
-(NSString*)sponsorFindAthleteTxtLocation;
-(NSString*)sponsorFindAthleteTxtSports;

//SMEMapAthleteVC
-(NSString*)sponsorMapAthleteBtnList;
-(NSString*)sponsorMapAthleteBtnMap;


//SMEAthleteDetailsVC
-(NSString*)sponsorAthleteDetailsBtnSportize;
-(NSString*)sponsorAthleteDetailsLblWhere;
-(NSString*)sponsorAthleteDetailsLblSports;

//SMEAthleteSportizeMeVC
-(NSString*)sponsorSportizeMeBtnReview;
-(NSString*)sponsorSportizeMeBtnDownload;
-(NSString*)sponsorSportizeMeLblAD;
-(NSString*)sponsorSendOfferTitle;
-(NSString*)sponsorSportizeMeNumber;
-(NSString*)sponsorSportizeMeSports;

//SMEAthleteReviewVC
-(NSString*)sponsorReviewBtnSend;
-(NSString*)sponsorReviewAgree;
-(NSString*)sponsorReviewTimes;
-(NSString*)sponsorReviewFor;
-(NSString*)sponsorReviewHour;
-(NSString*)sponsorReviewGym;
-(NSString*)sponsorReviewWeek;
-(NSString*)sponsorReviewWear;
-(NSString*)sponsorReviewPay;

//SMESportizeMeSent
-(NSString*)sponsorSentOffer;
-(NSString*)sponsorSentReview;
-(NSString*)sponsorSentAccept;
-(NSString*)sponsorSentPack;
-(NSString*)sponsorSentTitle;

//SMEMyAthletesVC
-(NSString*)sponsorMyAthletesBtnCurrent;
-(NSString*)sponsorMyAthletesBtnPast;
-(NSString*)sponsorMyAthletesBtnSaved;

//MySponsor
-(NSString*)athleteMySponsorLblSponsor;
-(NSString*)athleteMySponsorLblDeal;
-(NSString*)athleteMySponsorLblSports;
-(NSString*)athleteMySponsorLblFrequency;
-(NSString*)athleteMySponsorLblLocation;
-(NSString*)athleteMySponsorLblWhere;
-(NSString*)athleteMySponsorLblOut;

//Offers
-(NSString*)athleteOffersLblTheDeal;
-(NSString*)athleteOffersLblDuration;
-(NSString*)athleteOffersLblAccept;
-(NSString*)athleteOffersLblDecline;
-(NSString*)athleteOffersSorry;
-(NSString*)athleteOffersNextTime;
-(NSString*)athleteOffersReconfirm;
-(NSString*)athleteOffersAdd;
-(NSString*)athleteOffersYourSize;
-(NSString*)athleteOffersSmall;
-(NSString*)athleteOffersMedium;
-(NSString*)athleteOffersLarge;
-(NSString*)athleteOffersXLarge;
-(NSString*)athleteOffersConfirm;
-(NSString*)athleteOffersCongratulations;
-(NSString*)athleteOffersSponsored;
-(NSString*)athleteOffersReceive;

//Me
-(NSString*)athleteMeSports;
-(NSString*)athleteMeCurrSponsor;
-(NSString*)athleteMeCurrDeal;
-(NSString*)athleteMeWeek;
-(NSString*)athleteMeMonth;
-(NSString*)athleteMeOut;
-(NSString*)athleteMeCash;

//SMEMyCompany
-(NSString*)sponsorCompanyPresentation;
-(NSString*)sponsorCompanyAd;

@end
