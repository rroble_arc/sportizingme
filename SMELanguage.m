//
//  SMELanguage.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMELanguage.h"

static SMELanguage *instance;

@implementation SMELanguage

+ (SMELanguage*)sharedManager{
    if(instance == nil){
        instance = [[SMELanguage alloc]init];
        instance.type = SMELanguageType_english;
        //instance.type = SMELanguageType_french;
    }
    return instance;
}
-(NSString*)back{
    if(self.type == SMELanguageType_english)
        return @"  Back";
    else return @" Retour";
}

#pragma mark SMEIntroVC
-(NSString*)introBtnAthlete{
    if(self.type == SMELanguageType_english)
            return @"I am an Athlete";
    return @"Je suis un athlète";
    
}
-(NSString*)introBtnSponsor{
    if(self.type == SMELanguageType_english)
        return @"I am a Sponsor";
    return @"Je suis un commanditaire";
    
}
-(NSString*)introBtnHowItWorks{
    if(self.type == SMELanguageType_english)
        return @"How it Works ?";
    return @"Comment ça marche?";
    
}
#pragma mark SMEAthleteIntroVC
-(NSString*)athleteIntroBtnLogin{
    if(self.type == SMELanguageType_english)
        return @"Log In";
    return @"Connexion ";
    
}
-(NSString*)athleteIntroBtnSignup{
    if(self.type == SMELanguageType_english)
        return @"Sign Up";
    return @"Inscrivez-vous";
    
}
-(NSString*)athleteTakeMeBackBtnSignup{
    if(self.type == SMELanguageType_english)
        return @"Take me Back";
    return @"Ramenez-moi";
    
}
-(NSString*)athleteIntroLblAthlete{
    if(self.type == SMELanguageType_english)
        return @"I am an Athlete!";
    return @"Je suis un athlète!";
    
}
-(NSString*)athleteIntroLblOr{
    if(self.type == SMELanguageType_english)
        return @"- or -";
    return @"- ou -";
    
}
#pragma mark SMEAthleteLoginVC
-(NSString*)athleteLoginBtnLogin{
    if(self.type == SMELanguageType_english)
        return @"Log In";
    return @"Connexion";
    
}
-(NSString*)athleteLoginBtnLoginFacebook{
    if(self.type == SMELanguageType_english)
        return @"Log In with Facebook";
    return @"Connectez-vous avec Facebook";
    
}
-(NSString*)athleteLoginBtnForgetPassword{
    if(self.type == SMELanguageType_english)
        return @"Forgot my Password";
    return @"J'ai oublié mon mot de passe";
    
}
-(NSString*)athleteLoginTxtEmail{
    if(self.type == SMELanguageType_english)
        return @"Email";
    return @"Email";
}
-(NSString*)athleteLoginTxtPassword{
    if(self.type == SMELanguageType_english)
        return @"Password";
    return @"mot de passe";
}
-(NSString*)athleteLoginLblOr{
    if(self.type == SMELanguageType_english)
        return @"- or -";
    return @"- ou -";
}

#pragma mark SMEAthleteSignUpVC
-(NSString*)athleteSignupBtnNext{
    if(self.type == SMELanguageType_english)
        return @"Next";
    return @"Suivant";
    
}
-(NSString*)athleteSignupBtnSignUpFacebook{
    if(self.type == SMELanguageType_english)
        return @"Sign Up with Facebook    ";
    return @"Inscrivez-vous avec Facebook";
    
}
-(NSString*)athleteSignupTxtName{
    if(self.type == SMELanguageType_english)
        return @"Name";
    return @"Nom";
    
}
-(NSString*)athleteSignupTxtEmail{
    if(self.type == SMELanguageType_english)
        return @"Email";
    return @"Email";
}
-(NSString*)athleteSignupTxtPassword{
    if(self.type == SMELanguageType_english)
        return @"Password";
    return @"mot de passe";
}
-(NSString*)athleteSignupTxtPassword2{
    if(self.type == SMELanguageType_english)
        return @"Confirm Password";
    return @"confirmer le mot de passe";
}

#pragma mark SMEAthleteSignUpNextVC
-(NSString*)athleteSignupBtnDone{
    if(self.type == SMELanguageType_english)
        return @"Done";
    return @"Fait";
    
}
-(NSString*)athleteSignupTxtAge{
    if(self.type == SMELanguageType_english)
        return @"Age";
    return @"Age";
    
}
-(NSString*)athleteSignupTxtLocation{
    if(self.type == SMELanguageType_english)
        return @"Training Location";
    return @"Lieu de la formation";
}
-(NSString*)athleteSignupTxtSport{
    if(self.type == SMELanguageType_english)
        return @"Sport";
    return @"Sport";
}
-(NSString*)athleteSignupTxtHeight{
    if(self.type == SMELanguageType_english)
        return @"Height";
    return @"Hauteur";
}
-(NSString*)athleteSignupTxtWeight{
    if(self.type == SMELanguageType_english)
        return @"Weight";
    return @"Poids";
}
-(NSString*)athleteSignupTxtPaypal{
    if(self.type == SMELanguageType_english)
        return @"Paypal";
    return @"Paypal";
}
-(NSString*)athleteSignupLblTellus{
    if(self.type == SMELanguageType_english)
        return @"Tell us a bit about your short meditation";
    return @"Parlez-nous un peu de votre courte méditation";
}
#pragma mark SMESponsorIntroVC
-(NSString*)sponsorIntroBtnLogin{
    if(self.type == SMELanguageType_english)
        return @"Log In";
    return @"Connexion";
    
}
-(NSString*)sponsorIntroBtnSignup{
    if(self.type == SMELanguageType_english)
        return @"Sign Up";
    return @"Inscrivez-vous";
    
}
-(NSString*)sponsorIntroBtnAthlete{
    if(self.type == SMELanguageType_english)
        return @"I am a Sponsor!";
    return @"Je suis un commanditaire";
    
}
-(NSString*)sponsorIntroBtnOr{
    if(self.type == SMELanguageType_english)
        return @"- or -";
    return @"- ou -";
    
}
#pragma mark SMESponsorLoginVC
-(NSString*)sponsorLoginBtnLogin{
    if(self.type == SMELanguageType_english)
        return @"Log In";
    return @"Connexion";
    
}

-(NSString*)sponsorLoginBtnForgetPassword{
    if(self.type == SMELanguageType_english)
        return @"Forgot my Password";
    return @"J'ai oublié mon mot de passe";
    
}
-(NSString*)sponsorLoginTxtEmail{
    if(self.type == SMELanguageType_english)
        return @"Email";
    return @"Email";
}
-(NSString*)sponsorLoginTxtPassword{
    if(self.type == SMELanguageType_english)
        return @"Password";
    return @"mot de passe";
}

#pragma mark SMESponsorSignupVC
-(NSString*)sponsorSignupBtnNext{
    if(self.type == SMELanguageType_english)
        return @"Next";
    return @"Suivant";
    
}
-(NSString*)sponsorSignupTxtCompanyName{
    if(self.type == SMELanguageType_english)
        return @"Company Name";
    return @"Nom de l'entreprise";
    
}
-(NSString*)sponsorSignupTxtEmail{
    if(self.type == SMELanguageType_english)
        return @"Email";
    return @"Email";
}
-(NSString*)sponsorSignupTxtPassword{
    if(self.type == SMELanguageType_english)
        return @"Password";
    return @"mot de passe";
}
-(NSString*)sponsorSignupTxtPassword2{
    if(self.type == SMELanguageType_english)
        return @"Confirm Password";
    return @"Confirmez mot de passe";
}

#pragma mark SMESponsorSignupNextVC
-(NSString*)sponsorSignupNextBtnDone{
    if(self.type == SMELanguageType_english)
        return @"Done";
    return @"Fait";
    
}
-(NSString*)sponsorSignupNextTxtLocation{
    if(self.type == SMELanguageType_english)
        return @"Location";
    return @"Emplacement";
    
}
-(NSString*)sponsorSignupNextTxtIndustry{
    if(self.type == SMELanguageType_english)
        return @"Industry";
    return @"Industrie";
}
-(NSString*)sponsorSignupNextTxtEmployees{
    if(self.type == SMELanguageType_english)
        return @"No. of employees";
    return @"Nombre d'employés";
}
-(NSString*)sponsorSignupNextTxtRevenue{
    if(self.type == SMELanguageType_english)
        return @"Revenue (optional)";
    return @"Revenu (en option)";
}
-(NSString*)sponsorSignupNextTxtPaypal{
    if(self.type == SMELanguageType_english)
        return @"Paypal (optional)";
    return @"Paypal (en option)";
}
-(NSString*)sponsorSignupNextLblTellUs{
    if(self.type == SMELanguageType_english)
        return @"Tell us a bit now";
    return @"Parlez-nous un peu maintenant";
}
-(NSString*)sponsorSignupNextLblLetsFind{
    if(self.type == SMELanguageType_english)
        return @"Let's find Athletes";
    return @"Voyons athlètes";
}
-(NSString*)sponsorSignupNextLblLocation{
    if(self.type == SMELanguageType_english)
        return @"Location";
    return @"Emplacement";
}
-(NSString*)sponsorSignupNextLblIndustry{
    if(self.type == SMELanguageType_english)
        return @"Industry";
    return @"Industrie";
}
-(NSString*)sponsorSignupNextLblEmployees{
    if(self.type == SMELanguageType_english)
        return @"No. of employees";
    return @"Nombre d'employés";
}
-(NSString*)sponsorSignupNextLblRevenue{
    if(self.type == SMELanguageType_english)
        return @"Revenue";
    return @"Revenu";
}
-(NSString*)sponsorSignupNextLblPaypal{
    if(self.type == SMELanguageType_english)
        return @"Paypal";
    return @"Paypal";
}
#pragma mark SMEAthleteMenuVC
-(NSString*)athleteMenuCellMySponsor{
    if(self.type == SMELanguageType_english)
        return @"My Sponsor";
    return @"Mon Parrain";
    
}
-(NSString*)athleteMenuCellMe{
    if(self.type == SMELanguageType_english)
        return @"Me";
    return @"Me";
    
}
-(NSString*)athleteMenuCellOffers{
    if(self.type == SMELanguageType_english)
        return @"Offers";
    return @"Offres";
    
}
-(NSString*)athleteMenuCellWorkOut{
    if(self.type == SMELanguageType_english)
        return @"Work Out!";
    return @"Work Out!";
    
}
-(NSString*)athleteMenuCellMyAccount{
    if(self.type == SMELanguageType_english)
        return @"My Account";
    return @"Mon Compte";
    
}
-(NSString*)athleteMenuCellSettings{
    if(self.type == SMELanguageType_english)
        return @"Settings";
    return @"Réglages";
    
}
-(NSString*)athleteMenuCellSignOut{
    if(self.type == SMELanguageType_english)
        return @"Sign Out";
    return @"Déconnexion    ";
    
}

#pragma mark SMESponsorMenUVC
-(NSString*)sponsorMenuCellMyAthletes{
    if(self.type == SMELanguageType_english)
        return @"My Athletes";
    return @"Mes Athlètes";
    
}
-(NSString*)sponsorMenuCellMyCompany{
    if(self.type == SMELanguageType_english)
        return @"My Company";
    return @"Mon Entreprise";
    
}
-(NSString*)sponsorMenuCellDashboard{
    if(self.type == SMELanguageType_english)
        return @"Dashboard";
    return @"Tableau de bord";
    
}
-(NSString*)sponsorMenuCellFindAthletes{
    if(self.type == SMELanguageType_english)
        return @"Find Athletes!";
    return @"Trouvez athlètes!";
    
}
-(NSString*)sponsorMenuCellMyAccount{
    if(self.type == SMELanguageType_english)
        return @"My Account";
    return @"Mon compte";
    
}
-(NSString*)sponsorMenuCellSettings{
    if(self.type == SMELanguageType_english)
        return @"Settings";
    return @"Réglages";
    
}
-(NSString*)sponsorMenuCellSignOut{
    if(self.type == SMELanguageType_english)
        return @"Sign Out";
    return @"Déconnexion";
    
}

#pragma mark SMEAthleteWorkOutVC
-(NSString*)athleteWorkOutAlert30{
    if(self.type == SMELanguageType_english)
        return @"You haven't pass 30 minutes of your practise";
    return @"Vous n'avez pas passer 30 minutes de votre pratique";
}
-(NSString*)athleteWorkOutAlertSure{
    if(self.type == SMELanguageType_english)
        return @"Are you sure you want to stop?";
    return @"Êtes-vous sûr de vouloir arrêter";
}
-(NSString*)athleteWorkOutAlertYes{
    if(self.type == SMELanguageType_english)
        return @"Yes";
    return @"Oui";
}
-(NSString*)athleteWorkOutAlertNo{
    if(self.type == SMELanguageType_english)
        return @"No";
    return @"Non";
}
-(NSString*)athleteWorkOutBtnGym{
    if(self.type == SMELanguageType_english)
        return @"Gym Workout";
    return @"Gym Workout";
    
}
-(NSString*)athleteWorkOutBtnRunning{
    if(self.type == SMELanguageType_english)
        return @"Running Outside";
    return @"Courir à l'extérieur";
    
}
-(NSString*)athleteWorkOutBtnCycling{
    if(self.type == SMELanguageType_english)
        return @"Cycling Outside";
    return @"Cyclisme extérieur";
    
}
-(NSString*)athleteWorkOutBtnMMA{
    if(self.type == SMELanguageType_english)
        return @"MMA Practise";
    return @"MMA Practise";
    
}
-(NSString*)athleteWorkOutBtnSoccer{
    if(self.type == SMELanguageType_english)
        return @"Soccer Practise";
    return @"Football Pratique";
    
}
-(NSString*)athleteWorkoutLblHope{
   if(self.type == SMELanguageType_english)
        return @"Hope you had a good training session";
    return @"J'espère que vous avez eu une bonne séance d'entraînement";
}
-(NSString*)athleteWorkOutLblEarned{
    if(self.type == SMELanguageType_english)
        return @"you earned";
    return @"vous avez gagné";
}

#pragma mark SMEAthleteWorkOutTimerVC
-(NSString*)athleteWorkOutTimerBtnStart{
    if(self.type == SMELanguageType_english)
        return @"Start Training";
    return @"Début de la formation";
    
}
-(NSString*)athleteWorkOutTimerBtnStop{
    if(self.type == SMELanguageType_english)
        return @"Stop Training";
    return @"Arrêtez de formation";
    
}

#pragma mark SMEFindAthleteVC
-(NSString*)sponsorFindAthleteBtnFind{
    if(self.type == SMELanguageType_english)
        return @"Find";
    return @"Trouvez";
    
}
-(NSString*)sponsorFindAthleteBtnAdvanced{
    if(self.type == SMELanguageType_english)
        return @"Advanced Options";
    return @"Options Avancées";
    
}
-(NSString*)sponsorFindAthleteTxtLocation{
    if(self.type == SMELanguageType_english)
        return @"Location";
    return @"Emplacement";
    
}
-(NSString*)sponsorFindAthleteTxtSports{
    if(self.type == SMELanguageType_english)
        return @"Sports";
    return @"Sports";
    
}

#pragma mark SMEMapAthleteVC
-(NSString*)sponsorMapAthleteBtnList{
    if(self.type == SMELanguageType_english)
        return @"List";
    return @"Liste";
    
}
-(NSString*)sponsorMapAthleteBtnMap{
    if(self.type == SMELanguageType_english)
        return @"Map";
    return @"Carte";

}

#pragma mark SMEAthleteDetailsVC
-(NSString*)sponsorAthleteDetailsBtnSportize{
    if(self.type == SMELanguageType_english)
        return @"Sportize Me";
    return @"Sportize moi";
    
}
-(NSString*)sponsorAthleteDetailsLblWhere{
    if(self.type == SMELanguageType_english)
        return @"Where I train";
    return @"Lorsque je m'entraîne";
}
-(NSString*)sponsorAthleteDetailsLblSports{
    if(self.type == SMELanguageType_english)
        return @"My Sports";
    return @"Mes sports";
}

#pragma mark SMEAthleteSportizeMeVC
-(NSString*)sponsorSportizeMeBtnReview{
    if(self.type == SMELanguageType_english)
        return @"Review";
    return @"Revoir";
    
}
-(NSString*)sponsorSportizeMeBtnDownload{
    if(self.type == SMELanguageType_english)
        return @"Download";
    return @"Télécharger";
}
-(NSString*)sponsorSportizeMeLblAD{
    if(self.type == SMELanguageType_english)
        return @"AD to display";
    return @"AD à afficher";
}
-(NSString*)sponsorSendOfferTitle{
    if(self.type == SMELanguageType_english)
        return @"Send Offer";
    return @"Envoyez l'offre";
}
-(NSString*)sponsorSportizeMeNumber{
    if(self.type == SMELanguageType_english)
        return @"No. of Trainings per Week";
    return @"Nombre de formations par semaine";
}
-(NSString*)sponsorSportizeMeSports{
    if(self.type == SMELanguageType_english)
        return @"Sports";
    return @"Sports";
}

#pragma mark SMEAthleteSportizeReviewVC
-(NSString*)sponsorReviewBtnSend{
    if(self.type == SMELanguageType_english)
        return @"Send";
    return @"Envoyer";
    
}
-(NSString*)sponsorReviewAgree{
    if(self.type == SMELanguageType_english)
        return @"should agree to train";
    return @"devraient s'entendre pour former";
    
}
-(NSString*)sponsorReviewTimes{
    if(self.type == SMELanguageType_english)
        return @"times a week";
    return @"fois par semaine";
    
}
-(NSString*)sponsorReviewFor{
    if(self.type == SMELanguageType_english)
        return @"for";
    return @"pour";
    
}
-(NSString*)sponsorReviewHour{
    if(self.type == SMELanguageType_english)
        return @"hour at";
    return @"heure à";
    
}
-(NSString*)sponsorReviewGym{
    if(self.type == SMELanguageType_english)
        return @"gym";
    return @"gym";
    
}
-(NSString*)sponsorReviewWeek{
    if(self.type == SMELanguageType_english)
        return @"weeks.";
    return @"semaines.";
    
}
-(NSString*)sponsorReviewWear{
    if(self.type == SMELanguageType_english)
        return @"He will wear and post the ad:";
    return @"Il portera et poster la publicité:";
    
}
-(NSString*)sponsorReviewPay{
    if(self.type == SMELanguageType_english)
        return @"I commit to pay";
    return @"Je m'engage à payer";
    
}
#pragma mark SMESportizeMeSent
-(NSString*)sponsorSentOffer{
    if(self.type == SMELanguageType_english)
        return @"Your offer has been sent to";
    return @"Votre offre a été envoyée à";
    
}
-(NSString*)sponsorSentReview{
    if(self.type == SMELanguageType_english)
        return @"for review";
    return @"pour examen";
    
}
-(NSString*)sponsorSentAccept{
    if(self.type == SMELanguageType_english)
        return @"If athlete accept your offer we will charge you";
    return @"Si athlète accepter votre offre, nous vous facturerons";
    
}
-(NSString*)sponsorSentPack{
    if(self.type == SMELanguageType_english)
        return @"and you will receive your sponsor pack.";
    return @"et vous recevrez votre pack sponsor.";
    
}
-(NSString*)sponsorSentTitle{
    if(self.type == SMELanguageType_english)
        return @"Confirmation of Payment";
    return @"Confirmation de Paiement";
    
}

#pragma mark SMEMyAthletesVC
-(NSString*)sponsorMyAthletesBtnCurrent{
    if(self.type == SMELanguageType_english)
        return @"Current";
    return @"Courant";
    
}
-(NSString*)sponsorMyAthletesBtnPast{
    if(self.type == SMELanguageType_english)
        return @"Past";
    return @"Passé";
    
}
-(NSString*)sponsorMyAthletesBtnSaved{
    if(self.type == SMELanguageType_english)
        return @"Saved";
    return @"Sauvé";
    
}
#pragma mark MySponsor
-(NSString*)athleteMySponsorLblSponsor{
    if(self.type == SMELanguageType_english)
        return @"My Sponsor";
    return @"Mon parrain";
}
-(NSString*)athleteMySponsorLblDeal{
    if(self.type == SMELanguageType_english)
        return @"My Deal";
    return @"Mon Deal";
}
-(NSString*)athleteMySponsorLblSports{
    if(self.type == SMELanguageType_english)
        return @"Sports :";
    return @"Sports :";
}
-(NSString*)athleteMySponsorLblFrequency{
    if(self.type == SMELanguageType_english)
        return @"Frequency :";
    return @"Fréquence :";
}
-(NSString*)athleteMySponsorLblLocation{
    if(self.type == SMELanguageType_english)
        return @"Location :";
    return @"Emplacement :";
}
-(NSString*)athleteMySponsorLblWhere{
    if(self.type == SMELanguageType_english)
        return @"Where am I now";
    return @"Où suis-je maintenant";
}
-(NSString*)athleteMySponsorLblOut{
    if(self.type == SMELanguageType_english)
        return @"out of";
    return @"sur";
}
#pragma mark Offers
-(NSString*)athleteOffersLblTheDeal{
    if(self.type == SMELanguageType_english)
        return @"The Deal";
    return @"The Deal";
}
-(NSString*)athleteOffersLblDuration{
    if(self.type == SMELanguageType_english)
        return @"Duration :";
    return @"Durée :";
}
-(NSString*)athleteOffersLblAccept{
    if(self.type == SMELanguageType_english)
        return @"Accept";
    return @"Accepter";
}
-(NSString*)athleteOffersLblDecline{
    if(self.type == SMELanguageType_english)
        return @"Decline";
    return @"Refuser";
}
-(NSString*)athleteOffersSorry{
    if(self.type == SMELanguageType_english)
        return @"We are sorry, it's not the right deal for you";
    return @"Nous sommes désolé, ce n'est pas la bonne affaire pour vous";
}
-(NSString*)athleteOffersNextTime{
    if(self.type == SMELanguageType_english)
        return @"next time!";
    return @"la prochaine fois!";
}
-(NSString*)athleteOffersReconfirm{
    if(self.type == SMELanguageType_english)
        return @"Please reconfirm your address to send your training approval";
    return @"S'il vous plaît confirmer votre adresse pour envoyer votre approbation de formation";
}
-(NSString*)athleteOffersAdd{
    if(self.type == SMELanguageType_english)
        return @"Add new one";
    return @"Ajouter un nouveau";
}
-(NSString*)athleteOffersYourSize{
    if(self.type == SMELanguageType_english)
        return @"and your size";
    return @"et votre taille";
}
-(NSString*)athleteOffersSmall{
    if(self.type == SMELanguageType_english)
        return @"Small";
    return @"Faible";
}
-(NSString*)athleteOffersMedium{
    if(self.type == SMELanguageType_english)
        return @"Medium";
    return @"MediumF";
}
-(NSString*)athleteOffersLarge{
    if(self.type == SMELanguageType_english)
        return @"Grand";
    return @"Grand";
}
-(NSString*)athleteOffersXLarge{
    if(self.type == SMELanguageType_english)
        return @"X - Large";
    return @"X - Grand";
}
-(NSString*)athleteOffersConfirm{
    if(self.type == SMELanguageType_english)
        return @"Confirm";
    return @"Confirmez";
}
-(NSString*)athleteOffersCongratulations{
    if(self.type == SMELanguageType_english)
        return @"Congratulations! you are";
    return @"Félicitations! vous êtes";
}
-(NSString*)athleteOffersSponsored{
    if(self.type == SMELanguageType_english)
        return @"sponsored by:";
    return @"sponsorisé par:";
}
-(NSString*)athleteOffersReceive{
    if(self.type == SMELanguageType_english)
        return @"you will receive your free training approval soon.";
    return @"vous recevrez votre autorisation d'une formation gratuite bientôt.";
}

#pragma mark AthleteMeVC
-(NSString*)athleteMeSports{
    if(self.type == SMELanguageType_english)
        return @"My Sports";
    return @"Mes Sports";
}
-(NSString*)athleteMeCurrSponsor{
    if(self.type == SMELanguageType_english)
        return @"My Current Sponsor";
    return @"Mon parrain actuel";
}
-(NSString*)athleteMeCurrDeal{
    if(self.type == SMELanguageType_english)
        return @"My Current Deal";
    return @"Mon Deal du jour";
}
-(NSString*)athleteMeWeek{
    if(self.type == SMELanguageType_english)
        return @"This Week";
    return @"Cette Semaine";
}
-(NSString*)athleteMeMonth{
    if(self.type == SMELanguageType_english)
        return @"This Month";
    return @"Ce Mois";
}
-(NSString*)athleteMeOut{
    if(self.type == SMELanguageType_english)
        return @"out of";
    return @"sur";
}
-(NSString*)athleteMeCash{
    if(self.type == SMELanguageType_english)
        return @"Cash:";
    return @"Argent:";
}

#pragma mark SMEMyCompany
-(NSString*)sponsorCompanyPresentation{
    if(self.type == SMELanguageType_english)
        return @"Presentation";
    return @"Présentation";
}
-(NSString*)sponsorCompanyAd{
    if(self.type == SMELanguageType_english)
        return @"My Ads";
    return @"Mes Aannonces";
}
@end
