//
//  ARCTwitterManager.h
//  ARCTwitterManager v2
//
//  Created by Shlby Puerto on 5/3/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
//------------------------          Integrating           ------------------------

//------1. Copy FHSTwitterEngine folder from this project to your project.
//------2. Copy ARCTwitterManager.h and .m to your project.
//------3. Import the SystemConfiguration.framework
//------4. Fin, you may now use ARCTwitterManager

//------------------------          How To Use            ------------------------

//------1. Authenticate first your twitter.(Needs twitter key and secret)
//------ [[ARCTwitterManager sharedManager] authenticateWithApiKey:@"xxxxxxxxxxxxxxxxxxxxxx" secret:@"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" delegate:self];
//------2. To Login Twitter.
//------ [[ARCTwitterManager sharedManager]showLoginWindow:self];
//------ Were self is a viewController.
//------ Apply the delegates needed
//------ -(void)ARCTwitterManagerDidLoginSuccessfully:(ARCTwitterManager*)manager;
//------ -(void)ARCTwitterManagerDidFailToLogin:(ARCTwitterManager*)manager;

//------3. Share a photo with message.
//------ [[ARCTwitterManager sharedManager]postTweet:xxx withImage:xxx];
//------ Apply the delegates needed
//------ -(void)ARCTwitterManagerSuccessfullyTweeted:(ARCTwitterManager*)manager;
//------ -(void)ARCTwitterManagerFailedToTweet:(ARCTwitterManager*)manager;

//------4. Get followers
//------ [[ARCTwitterManager sharedManager]listFollowers];
//------ Apple the delegate
//------ -(void)ARCTwitterManager:(ARCTwitterManager*)manager listOfFollowers:(id)list;






#import <Foundation/Foundation.h>
@class ARCTwitterManager;
@protocol ARCTwitterManagerDelegate <NSObject>
@optional
-(void)ARCTwitterManagerDidLoginSuccessfully:(ARCTwitterManager*)manager;
-(void)ARCTwitterManagerDidFailToLogin:(ARCTwitterManager*)manager;

-(void)ARCTwitterManager:(ARCTwitterManager*)manager listOfFollowers:(id)list;
-(void)ARCTwitterManager:(ARCTwitterManager*)manager listOfFollowing:(id)list;

-(void)ARCTwitterManagerSuccessfullyTweeted:(ARCTwitterManager*)manager;
-(void)ARCTwitterManagerFailedToTweet:(ARCTwitterManager*)manager;

@end

@interface ARCTwitterManager : NSObject

+ (ARCTwitterManager *)sharedManager;
-(void) authenticateWithApiKey:(NSString*)key secret:(NSString*)secret delegate:(id<ARCTwitterManagerDelegate>)delegate;

- (void)showLoginWindow:(UIViewController*)vc;

- (void)listFollowers;
- (void)listFollowing;

- (void)postTweet:(NSString*)tweet;
- (void)postTweet:(NSString*)tweet withImage:(UIImage*)image;
@end
