//
//  ARCTwitterManager.m
//  ARCTwitterManager v2
//
//  Created by Shlby Puerto on 5/3/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "ARCTwitterManager.h"
#import "FHSTwitterEngine.h"

static ARCTwitterManager *instance;
@interface ARCTwitterManager ()

@property (nonatomic, strong) FHSTwitterEngine *engine;
@property (weak, nonatomic) id <ARCTwitterManagerDelegate> delegate;

@end
@implementation ARCTwitterManager
+ (ARCTwitterManager *)sharedManager{
    if (instance == nil) {
        instance = [[ARCTwitterManager alloc] init];
    }
    return instance;
}
-(void) authenticateWithApiKey:(NSString*)key secret:(NSString*)secret delegate:(id<ARCTwitterManagerDelegate>)delegate{
    self.delegate = delegate;
    
    self.engine = [[FHSTwitterEngine alloc]initWithConsumerKey:key andSecret:secret];
}
- (void)showLoginWindow:(UIViewController*)vc{
    [self.engine showOAuthLoginControllerFromViewController:vc withCompletion:^(BOOL success) {
        
        if (success) {
            [[self delegate]ARCTwitterManagerDidLoginSuccessfully:self];
        } else {
            [[self delegate]ARCTwitterManagerDidFailToLogin:self];
        }
        
    }];
}
- (void)postTweet:(NSString*)tweet {
    
  
    
    dispatch_async(GCDBackgroundThread, ^{
        @autoreleasepool {
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            NSError *returnCode = [self.engine postTweet:tweet];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            NSString *title = nil;
            NSString *message = nil;
            
            if (returnCode) {
                title = [NSString stringWithFormat:@"Error %d",returnCode.code];
                message = returnCode.domain;
            } else {
                title = @"Tweet Posted";
                message = tweet;
            }
            
            dispatch_sync(GCDMainThread, ^{
                @autoreleasepool {
                    UIAlertView *av = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [av show];
                }
            });
        }
    });
}
- (void)postTweet:(NSString*)tweet withImage:(UIImage*)image{

        NSData *imageData = UIImagePNGRepresentation(image);
    
    dispatch_async(GCDBackgroundThread, ^{
        @autoreleasepool {
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            NSError *returnCode = [self.engine postTweet:tweet withImageData:imageData];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            NSString *title = nil;
            NSString *message = nil;
            
            if (returnCode) {
                title = [NSString stringWithFormat:@"Error %d",returnCode.code];
                message = returnCode.domain;
            } else {
                title = @"Tweet Posted";
                message = tweet;
            }
            
            dispatch_sync(GCDMainThread, ^{
                @autoreleasepool {
                    if(returnCode){
                        [[self delegate]ARCTwitterManagerFailedToTweet:self];
                    }else{
                        [[self delegate]ARCTwitterManagerSuccessfullyTweeted:self];
                    }
                }
            });
        }
    });
}


- (void)listFollowers {
   
    dispatch_async(GCDBackgroundThread, ^{
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            NSLog(@"followers:\n%@",[self.engine getFollowers]);
            [[self delegate]ARCTwitterManager:self listOfFollowers:[self.engine getFollowers]];
            
            dispatch_sync(GCDMainThread, ^{
                @autoreleasepool {
                    /*UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Complete!" message:@"Your list of followers has been fetched" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [av show];*/
                    
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                }
            });
            
        }
    });
}
- (void)listFollowing {
    
    dispatch_async(GCDBackgroundThread, ^{
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            NSLog(@"followers:\n%@",[self.engine getFriendsIDs]);
            [[self delegate]ARCTwitterManager:self listOfFollowing:[self.engine getFriends]];
            
            dispatch_sync(GCDMainThread, ^{
                @autoreleasepool {
                    /*UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Complete!" message:@"Your list of followers has been fetched" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [av show];*/
                    
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                }
            });
            
        }
    });
}

- (void)storeAccessToken:(NSString *)accessToken {
    [[NSUserDefaults standardUserDefaults]setObject:accessToken forKey:@"SavedAccessHTTPBody"];
}

- (NSString *)loadAccessToken {
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"SavedAccessHTTPBody"];
}
@end
