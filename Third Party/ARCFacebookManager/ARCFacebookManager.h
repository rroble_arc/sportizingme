//
//  ARCFacebookManager.h
//
//  Created by Edward Aries Abais on 4/11/13.
//  Copyright (c) 2013 Edward Aries Abais. All rights reserved.
//


// Supports FacebookSDK 3.5
// This is a wrapper class to simplify using the FacebookSDK

// Adding to the project
// Follow the steps from https://developers.facebook.com/docs/getting-started/facebook-sdk-for-ios/
// Once done just #import "ARCFacebookManager.h" and your ready to go



// Sample

// // (get the shared instance)
// ARCFacebookManager *fbm = [ARCFacebookManager sharedManager];
 
// ================================= Authenticate User =================================
//// (Delegate Method)
//[fbm loginToFacebookWithDelegate:self
//          FBSessionLoginBehavior:FBSessionLoginBehaviorForcingWebView];
// 
//- (void)ARCFacebookManagerAuthenticationSucceeded:(ARCFacebookManager *)manager{
// NSLog(@"ang user: %@",manager.fb_graphuser);
//}
//- (void)ARCFacebookManager:(ARCFacebookManager *)manager authenticationFailedWithError:(NSError *)error{
// NSLog(@"failed to login");
//}
// 
//// (Blocks Method)
//[fbm authenticateWithFBSessionLoginBehavior:FBSessionLoginBehaviorForcingWebView
//                          completionHandler:^(id result, NSError *error) {
//                             if (error) { // failure
//                              NSLog(@"authenticate fail");
//                             }
//                             else{ // success
//                              NSLog(@"authenticate success");
//                              NSLog(@"ang user: %@",fbm.fb_graphuser);
//                             }
//                          }];
// 
// ================================= Authenticate User End =================================


// ================================= Feed Dialog =================================
// NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
//                           @"a very simple description of share" ,@"description",
//                           @"name of share"                      ,@"name",
//                           @"http://google.com"                  ,@"link",
//                           nil];
// 
// // (User Already Authenticated)
// [fbm presentFeedDialogModallyWithParameters:params
//                                     handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//                                        if (error) {
//                                          NSLog(@"web dialog error: %@",error.localizedDescription);
//                                        }
//                                        if (result == FBWebDialogResultDialogCompleted) {
//                                          NSLog(@"FBWebDialogResultDialogCompleted");
//                                        }
//                                        else if (result == FBWebDialogResultDialogNotCompleted){
//                                          NSLog(@"FBWebDialogResultDialogNotCompleted");
//                                        }
//                                        NSLog(@"URL: %@",resultURL);
//                                     }];
// 
// // (User Not Yet Authenticted)
// [fbm authenticateAndPresentFeedDialogModallyWithParameters:params
//                                                    handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//                                                     if (error) {
//                                                      NSLog(@"web dialog error: %@",error.localizedDescription);
//                                                     }
//                                                     if (result == FBWebDialogResultDialogCompleted) {
//                                                      NSLog(@"FBWebDialogResultDialogCompleted");
//                                                     }
//                                                     else if (result == FBWebDialogResultDialogNotCompleted){
//                                                      NSLog(@"FBWebDialogResultDialogNotCompleted");
//                                                     }
//                                                      NSLog(@"URL: %@",resultURL);
//                                                    }];
// 
// ================================= Feed Dialog End =================================


// ================================= Post photo =================================
//  ARCFacebookManager *fbm = [ARCFacebookManager sharedManager];
//  UIImage *image = [UIImage imageNamed:@"FacebookSDKResources.bundle/FBProfilePictureView/images/fb_blank_profile_portrait.png"];
//  NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
//                          image                       ,@"source",
//                          @"A simple picture uploading on FB" ,@"message",
//                          nil];
//  
//  [fbm uploadPictureWithParameters:params
//                 completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                   if (error) {
//                     NSLog(@"FBRequestConnection error: %@",error.localizedDescription);
//                   }
//                   else{
//                     NSLog(@"FBRequestConnection result: %@",result);
//                   }
//                   [sender setEnabled:YES];
//                 }];
// ================================= Post photo end =================================



#import <Foundation/Foundation.h>
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>
#import <FacebookSDK/FacebookSDK.h>

// completion handler
typedef void (^ARCFacebookManagerCompletionHandler)(id result,
                                                    NSError *error);


// This key is used to get the token from the NSUserDefaults (not sure)
extern NSString *const ARCFacebookTokenCachingStrategyKey;

// Notification
// this is called when ever the session is changed
extern NSString *const ARCFacebookManagerSessionChangedNotification;

@class ARCFacebookManager;
@protocol ARCFacebookManagerAuthenticationDelegate <NSObject>
@optional
- (void)ARCFacebookManagerAuthenticationSucceeded:(ARCFacebookManager *)manager;
- (void)ARCFacebookManager:(ARCFacebookManager *)manager authenticationFailedWithError:(NSError*)error;
@end

// this is good for one session
@interface ARCFacebookManager : NSObject

@property (strong, readonly)FBSession *fb_session;
@property (strong, readonly)NSDictionary <FBGraphUser> *fb_graphuser;


// shared instance
+ (ARCFacebookManager *)sharedManager;

// Authenticate Facebook
// - if previous session was not logged out, opens the session directly
// - else opens a view to input credentials
// common behavior
// FBSessionLoginBehaviorForcingWebView - shows a login dialog inside the app
// FBSessionLoginBehaviorWithFallbackToWebView - fast app switch/ goes to safari
- (void)authenticateWithDelegate:(id <ARCFacebookManagerAuthenticationDelegate>)delegate
             FBSessionLoginBehavior:(FBSessionLoginBehavior)behavior;


- (void)authenticateWithFBSessionLoginBehavior:(FBSessionLoginBehavior)behavior
                             completionHandler:(ARCFacebookManagerCompletionHandler)handler;

// Logout to Facebook
- (void) logoutFromFacebook;

// Feed Dialog
@property (nonatomic, readonly)BOOL isFeedDialogVisible;
// a simple wrapper method for FBWebDialogs
// user must be authenticate before using this method
// weird behavior will occur if the user is not authenticated
- (void) presentFeedDialogModallyWithParameters:(NSDictionary *)parameters
                                        handler:(FBWebDialogHandler)handler;

// authenticates user if not yet authenticated and shows the feed dialog
// else show the dialog directly
- (void)authenticateAndPresentFeedDialogModallyWithParameters:(NSDictionary *)parameters
                                                      handler:(FBWebDialogHandler)handler;

// Photo Sharing
- (void)uploadPictureWithParameters:(NSDictionary*)parameters completionHandler:(FBRequestHandler)handler;


@end
