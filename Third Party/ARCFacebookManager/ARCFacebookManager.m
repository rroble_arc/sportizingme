//
//  ARCFacebookManager.m
//
//  Created by Edward Aries Abais on 4/11/13.
//  Copyright (c) 2013 Edward Aries Abais. All rights reserved.
//

#import "ARCFacebookManager.h"


NSString *const ARCFacebookTokenCachingStrategyKey = @"ARCFacebookTokenCachingStrategyKey";

// Notification
// this is called when ever the session is changed
NSString *const ARCFacebookManagerSessionChangedNotification = @"ARCFacebookManagerLoginChangedNotification";

// Shared Instance
static ARCFacebookManager *instance;

@interface ARCFacebookManager ()

@end


@implementation ARCFacebookManager

#pragma mark - init and shared instance
// init
- (id)init{
  self = [super init];
  if (self) {
    // initialization
    _fb_session = [self createSession];
  }
  return self;
}

// shared instance
+ (ARCFacebookManager *)sharedManager{
  if (instance == nil) {
    instance = [[ARCFacebookManager alloc] init];
  }
  return instance;
}

#pragma mark - Authenticate Facebook
// Login to facebook
- (void)authenticateWithDelegate:(id <ARCFacebookManagerAuthenticationDelegate>)delegate FBSessionLoginBehavior:(FBSessionLoginBehavior)behavior{
  // to avoid continued login check the session if its open
//  if (self.fb_session.isOpen) { // user already logged in
//    NSLog(@"user already logged in");
//  }
//  else{
    // logs in the user
    _fb_session = [self createSession];
    
    __weak ARCFacebookManager *weakSelf = self;
    [weakSelf.fb_session openWithBehavior:behavior
                        completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                          if (error) {
                            NSLog(@"ARCFacebookManager openWithBehavior:completionHandler: %@",error.localizedDescription);
                            [weakSelf logoutFromFacebook];
                            if ([delegate respondsToSelector:@selector(ARCFacebookManager:authenticationFailedWithError:)]) {
                              [delegate ARCFacebookManager:self authenticationFailedWithError:error];
                            }
                          }
                          else{
                            // login success
                            if (session.isOpen) {
                              FBRequest *me = [[FBRequest alloc] initWithSession:session
                                                                       graphPath:@"me"];
                              [me startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                if (error) {
                                  NSLog(@"ARCFacebookManager startWithCompletionHandler: %@",error.localizedDescription);
                                  [weakSelf logoutFromFacebook];
                                  if ([delegate respondsToSelector:@selector(ARCFacebookManager:authenticationFailedWithError:)]) {
                                    [delegate ARCFacebookManager:self authenticationFailedWithError:error];
                                  }
                                }
                                else{
                                  // request success
                                  _fb_graphuser = (NSDictionary <FBGraphUser> *)result;
                                  [self sendNotification];
                                  if ([delegate respondsToSelector:@selector(ARCFacebookManagerAuthenticationSucceeded:)]) {
                                    [delegate ARCFacebookManagerAuthenticationSucceeded:self];
                                  }
                                }
                              }];
                            }
                          }
                        }];
  //}
}

- (void)authenticateWithFBSessionLoginBehavior:(FBSessionLoginBehavior)behavior
                             completionHandler:(ARCFacebookManagerCompletionHandler)handler
{
//    // to avoid continued login check the session if its open
//    if (self.fb_session.isOpen) { // user already logged in
//      NSLog(@"ARCFacebookManager authenticateWithFBSessionLoginBehavior:completionHandler: already authenticated");
//    }
//    else{
      // logs in the user
      _fb_session = [self createSession];
      
      __weak ARCFacebookManager *weakSelf = self;
      [weakSelf.fb_session openWithBehavior:behavior
                          completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                            if (error) {
                              NSLog(@"ARCFacebookManager openWithBehavior:completionHandler: %@",error.localizedDescription);
                              [weakSelf logoutFromFacebook];
                              
                              // call the handler
                              handler(nil,error);
                            }
                            else{
                              // login success
                              if (session.isOpen) {
                                FBRequest *me = [[FBRequest alloc] initWithSession:session
                                                                         graphPath:@"me"];
                                [me startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                  if (error) {
                                    NSLog(@"ARCFacebookManager startWithCompletionHandler: %@",error.localizedDescription);
                                    [weakSelf logoutFromFacebook];
                                  }
                                  else{
                                    // request success
                                    _fb_graphuser = (NSDictionary <FBGraphUser> *)result;
                                    [self sendNotification];
                                    
                                  }
                                  
                                  // call the handler
                                  handler(result,error);
                                }];
                              }
                            }
                          }];
    //}
}


#pragma mark - Logout to Facebook
// Logout to Facebook
- (void) logoutFromFacebook{
  //[[FBSession activeSession] closeAndClearTokenInformation];
  //[FBSession setActiveSession:nil];
  
  [self.fb_session closeAndClearTokenInformation];
  _fb_graphuser = nil;  
  
  [self sendNotification];
}

#pragma mark -
#pragma mark Session
- (FBSession*)createSession{
  FBSession *session = [[FBSession alloc] initWithAppID:nil
                                            permissions:[NSArray arrayWithObjects:@"email",@"user_birthday",@"user_photos",@"publish_actions", nil]
                                        urlSchemeSuffix:nil
                                     tokenCacheStrategy:[self createTokenCachingStrategy]];
  return session;
}

#pragma mark Token Caching Strategy
- (FBSessionTokenCachingStrategy*)createTokenCachingStrategy{
  FBSessionTokenCachingStrategy *tokenCachingStrategy = [[FBSessionTokenCachingStrategy alloc]
                                                         initWithUserDefaultTokenInformationKeyName:ARCFacebookTokenCachingStrategyKey];
  return tokenCachingStrategy;
}

#pragma mark - Notification
- (void)sendNotification{
  //update the activeSession
  if (! [self.fb_session isEqual:[FBSession activeSession]]) {
    NSLog(@"setting the new active session");
    // they are not equal set the active session
    [FBSession setActiveSession:self.fb_session];
  }
  
  
  [[NSNotificationCenter defaultCenter] postNotificationName:ARCFacebookManagerSessionChangedNotification
                                                      object:nil];
}

#pragma mark - Feed Dialog
// Feed Dialog
// a simple wrapper method for FBWebDialogs
// user must be authenticate before using this method
// weird behavior will occur if the user is not authenticated
- (void) presentFeedDialogModallyWithParameters:(NSDictionary *)parameters
                                        handler:(FBWebDialogHandler)handler
{
  if (self.isFeedDialogVisible == NO) {
    _isFeedDialogVisible = YES;
    [FBWebDialogs presentFeedDialogModallyWithSession:self.fb_session
                                           parameters:parameters
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                // set back the isFeedDialogVisible
                                                _isFeedDialogVisible = NO;
                                                
                                                handler(result,resultURL,error);
                                              }];
  }
  else{
    NSLog(@"A feed dialog is still visible");
  }
}


// authenticates user if not yet authenticated and shows the feed dialog
// else show the dialog directly
- (void)authenticateAndPresentFeedDialogModallyWithParameters:(NSDictionary *)parameters
                                                      handler:(FBWebDialogHandler)handler
{
  if (self.isFeedDialogVisible == NO) {
    _isFeedDialogVisible = YES;
    if (self.fb_session.isOpen) {
      _isFeedDialogVisible = NO; // to ensure to show the feed dialog
      [self presentFeedDialogModallyWithParameters:parameters
                                           handler:handler];
    }
    else{
      // user session is not yet open
      [self authenticateWithFBSessionLoginBehavior:FBSessionLoginBehaviorForcingWebView
                                 completionHandler:^(id result, NSError *error) {
                                   _isFeedDialogVisible = NO; // set back the isFeedDialogVisible
                                   
                                   if (error) {
                                     // error on authenticating
                                     handler(FBWebDialogResultDialogNotCompleted,nil,error);
                                   }
                                   else{
                                     // successfull in authentication
                                     // show the feed dialog
                                     [self presentFeedDialogModallyWithParameters:parameters
                                                                          handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                                            _isFeedDialogVisible = NO; // set back the isFeedDialogVisible
                                                                            handler(result,resultURL,error);
                                                                          }];
                                   }
                                 }];
    }
  }
  else{
    NSLog(@"A feed dialog is still visible");
  }
}

#pragma mark - Photo sharing
// Photo Sharing
- (void)uploadPictureWithParameters:(NSDictionary*)parameters completionHandler:(FBRequestHandler)handler
{
  [FBRequestConnection startWithGraphPath:@"me/photos"
                               parameters:parameters
                               HTTPMethod:@"POST"
                        completionHandler:handler];
}

@end
