//
//  ARCFetcher.h
//
//  Created by Edward Aries Abais on 4/25/13.
//  Copyright (c) 2013 Edward Aries Abais. All rights reserved.
//
// Supports the GTMHTTPFetcher downloaded on (25 Apr 2013)
// This class is a simple wrapper class of GTMHTTPFetcher.
// This class will take a URL String and once finished downloading it will
// automatically parse the data depending on the options provided.
// The class can be used directly without creating a property,
// e.g its self contained and it releases automatically when finished using.

// Make sure the type of data your fetching matches to the options below:
// Options:
// ARCFetcherType_Data                => NSData object result (raw data from http response)
// ARCFetcherType_JSON                => NSDictionary or NSArray result (The data is in JSON format)
// ARCFetcherFetchType_XMLDictionary  => NSDictionary result (The data is in XML format)


// Adding to the project
// 1) Copy the files com/gtmhttpfetcher and add it to the project
// 2) Add the SystemConfiguration.framework
// 3) Just #import "ARCFetcher.h" and your ready to go.



// Sample Usage

// ---------------------------- Block style ----------------------------
// Once created you can start fecthing directly.
/*
  NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                         @"my object"      ,@"key_1",
                         @"my object too"  ,@"key_2",
                         nil];
 
 
  ARCFetcher *fetcher = [[ARCFetcher alloc] init];
 
  [fetcher startFetchWithURLString:@"https://github.com/facebook/facebook-ios-sdk/archive/master.zip"
                        postParams:params
                    userProperties:nil
                         fetchType:ARCFetcherFetchType_Data
                 completionHandler:^(id result, 
                                    NSError *error,
                                    NSMutableDictionary *userProperties, 
                                    ARCFetcherType fetchType) 
                 {
                   if (error) {
                     NSLog(@"parsing/network error");
                   }
                   else{
                     NSLog(@"successfully fetched and parsed the |result|");
                   }
                 }
                  progressCallback:^(NSUInteger receivedDataLength,
                                     long long expectedContentLength,
                                     NSMutableDictionary *userProperties,
                                     ARCFetcherType fetchType)
                 {
                   double perc = (double)receivedDataLength / expectedContentLength;
                   NSLog(@"percent: %.2f %%",perc*100.0);
                 }];
// ---------------------------- Block style End ----------------------------
 
// ---------------------------- Delegation style ----------------------------
  NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"my object"      ,@"key_1",
                          @"my object too"  ,@"key_2",
                          nil];
  
  ARCFetcher *fetcher = [[ARCFetcher alloc] init];
  [fetcher startFetchWithURLString:@"http://www.i-italy.org/webservices/json/getMagazineFrontPage"
                          delegate:self
                        postParams:params
                    userProperties:nil
                         fetchType:ARCFetcherFetchType_JSON];
  
  
 #pragma mark - ARCFetcher Delegate
  - (void)ARCFetcher:(ARCFetcher *)fetcher
 didFinishWithResult:(id)result
      userProperties:(NSMutableDictionary*)userProperties
           fetchType:(ARCFetcherType)fetchType
  {
    NSLog(@"parsing/network error");
  }
  
  - (void)ARCFetcher:(ARCFetcher *)fetcher
    didFailWithError:(NSError*)error
      userProperties:(NSMutableDictionary *)userProperties
           fetchType:(ARCFetcherType)fetchType
  {
    NSLog(@"successfully fetched and parsed the |result|");
  }
  
 // progress callback (OPTIONAL)
  - (void)ARCFetcher:(ARCFetcher *)fetcher
  receivedDataLength:(NSUInteger)receivedDataLength
expectedContentLength:(long long)expectedContentLength
      userProperties:(NSMutableDictionary*)userProperties
           fetchType:(ARCFetcherType)fetchType
  {
    double perc = (double)receivedDataLength / expectedContentLength;
    NSLog(@"percent: %.2f %%",perc*100.0);
  }
 
// ---------------------------- Delegation style End ----------------------------


// ---------------------------- Stop the fetcher ----------------------------
// After initializing the fetcher you can start the fetch directly
// and the object will automatically deallocated when finished fetching.
// But sometimes you want to interrupt the fetcher, and also its a good 
// practice to cancel the fetcher when not needed.
// To interrupt the fetching you need to acquire a weak or strong reference to fetcher
// Sample:

  ARCFetcher *fetcher = [[ARCFetcher alloc] init];

  // |myFetcher| can be a weak or strong property
  self.myFetcher = fetcher;

  // to top the fetcher just call stopFetching
  [self.myFetcher stopFetching];
 
 
  // you can start again another fetch
  // or you can start another fetch and the previous fetch will be cancled for you.

// ---------------------------- Stop the fetcher End ----------------------------
*/
#import <Foundation/Foundation.h>
#import "GTMHTTPFetcher.h"
#import "XMLReader.h"

typedef enum {
  ARCFetcherFetchType_Data = 0,
  ARCFetcherFetchType_JSON,
  ARCFetcherFetchType_XMLDictionary,
}ARCFetcherType;

// the completion handler block
typedef void (^ARCFetcherCompletionHandler) (id result,
                                            NSError *error,
                                            NSMutableDictionary *userProperties,
                                            ARCFetcherType fetchType);

// the progress handler
typedef void (^ARCFetcherProgressCallback)  (NSUInteger receivedDataLength,
                                            long long expectedContentLength,
                                            NSMutableDictionary *userProperties,
                                            ARCFetcherType fetchType);


@class ARCFetcher;
@protocol ARCFetcherDelegate <NSObject>
@required
- (void)ARCFetcher:(ARCFetcher *)fetcher
didFinishWithResult:(id)result
    userProperties:(NSMutableDictionary*)userProperties
         fetchType:(ARCFetcherType)fetchType;

- (void)ARCFetcher:(ARCFetcher *)fetcher
  didFailWithError:(NSError*)error
    userProperties:(NSMutableDictionary *)userProperties
         fetchType:(ARCFetcherType)fetchType;

@optional
// progress callback
- (void)ARCFetcher:(ARCFetcher *)fetcher
receivedDataLength:(NSUInteger)receivedDataLength
expectedContentLength:(long long)expectedContentLength
    userProperties:(NSMutableDictionary*)userProperties
         fetchType:(ARCFetcherType)fetchType;

@end

@interface ARCFetcher : NSObject
@property (nonatomic, weak, readonly) GTMHTTPFetcher *gtmFetcher;
// block style method
- (void)startFetchWithURLString:(NSString*)URLString
                     postParams:(NSDictionary*)postParams
                 userProperties:(NSMutableDictionary*)userProperties
                      fetchType:(ARCFetcherType)fetchType
              completionHandler:(ARCFetcherCompletionHandler)completionHandler
               progressCallback:(ARCFetcherProgressCallback)progressCallback;

// delegation style
- (void)startFetchWithURLString:(NSString *)URLString
                       delegate:(id<ARCFetcherDelegate>)delegate
                     postParams:(NSDictionary *)postParams
                 userProperties:(NSMutableDictionary *)userProperties
                      fetchType:(ARCFetcherType)fetchType;


// stop fetching
- (void)stopFetching;

@end
