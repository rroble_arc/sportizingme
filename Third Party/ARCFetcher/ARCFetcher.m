//
//  ARCFetcher.m
//
//  Created by Edward Aries Abais on 4/25/13.
//  Copyright (c) 2013 Edward Aries Abais. All rights reserved.
//

#import "ARCFetcher.h"

@implementation ARCFetcher

#pragma mark - start fetch block
- (void)startFetchWithURLString:(NSString*)URLString
                     postParams:(NSDictionary*)postParams
                 userProperties:(NSMutableDictionary*)userProperties
                      fetchType:(ARCFetcherType)fetchType
              completionHandler:(ARCFetcherCompletionHandler)completionHandler
               progressCallback:(ARCFetcherProgressCallback)progressCallback
{
  [self stopFetching];
  
  
  NSLog(@"ARCFetcher fetch: %@",URLString);
  
  
  // create a fetcher
  _gtmFetcher = [GTMHTTPFetcher fetcherWithURLString:URLString];
  
  // add the user info
  _gtmFetcher.properties = userProperties;
  
  // if there are post data
  // include
  if (postParams) {
    // getting the post data
    NSArray *keys = [postParams allKeys];
  //  NSLog(@"params: %@",keys);
    NSMutableString *postString = [NSMutableString stringWithCapacity:keys.count];
    for (NSString *key in keys) {
      [postString appendFormat:@"%@=%@&"
       ,key
       ,[postParams objectForKey:key]];
    }
    // delete the last "&"
    [postString deleteCharactersInRange:NSMakeRange([postString length]-1, 1)];
    // set post data
      // NSLog(@"postString: %@",postString);
    [_gtmFetcher setPostData:[postString dataUsingEncoding:NSUTF8StringEncoding]];
  }
  
  // add received data callback if necessary
  if (progressCallback) {
    [_gtmFetcher setReceivedDataBlock:^(NSData *dataReceivedSoFar) {
      if (progressCallback) {
        progressCallback(dataReceivedSoFar.length,
                         _gtmFetcher.response.expectedContentLength,
                         _gtmFetcher.properties,
                         fetchType);
      }
    }];
  }
  
  // execute the fetch
  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
  [_gtmFetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (error) {
      // fetch failed
      NSLog(@"%@",[error localizedDescription]);
      completionHandler(nil,
                        error,
                        _gtmFetcher.properties,
                        fetchType);
    }
    else{
      // fetch succeed
      // DATA
      if (fetchType == ARCFetcherFetchType_Data)
      {
        completionHandler(retrievedData,
                          error,
                          _gtmFetcher.properties,
                          fetchType);
      }
      // JSON
      else if (fetchType == ARCFetcherFetchType_JSON)
      {
        NSError *jsonParseError = nil;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:retrievedData
                                                        options:NSJSONReadingAllowFragments
                                                          error:&jsonParseError];
        if (jsonObject != nil && jsonParseError == nil)
        {
          // json deserialize successfull
        }
        else
        {
          //NSLog(@"An error happened while deserializing the JSON data.");
          NSLog(@"%@",jsonParseError.localizedDescription);
        }
        
        completionHandler(jsonObject,
                          jsonParseError,
                          _gtmFetcher.properties,
                          fetchType);
      }
      // XML
      else if (fetchType == ARCFetcherFetchType_XMLDictionary)
      {
        NSError *xmlParseError = nil;
        NSDictionary *xmlDict = [XMLReader dictionaryForXMLData:retrievedData error:&xmlParseError];
        if (xmlParseError) {
          NSLog(@"%@",xmlParseError.localizedDescription);
          xmlDict = nil;
        }
        else
        {
          // XML succeeded
        }
        
        completionHandler(xmlDict,
                          xmlParseError,
                          _gtmFetcher.properties,
                          fetchType);
      }
    }
    
  }];
  
}


#pragma mark - start fetch delegate
// delegation style
- (void)startFetchWithURLString:(NSString *)URLString
                       delegate:(id<ARCFetcherDelegate>)delegate
                     postParams:(NSDictionary *)postParams
                 userProperties:(NSMutableDictionary *)userProperties
                      fetchType:(ARCFetcherType)fetchType
{
  [self startFetchWithURLString:URLString
                     postParams:postParams
                 userProperties:userProperties
                      fetchType:fetchType
              completionHandler:^(id result, NSError *error,
                                  NSMutableDictionary *userProperties,
                                  ARCFetcherType fetchType)
              {
                [delegate ARCFetcher:self
                 didFinishWithResult:result
                      userProperties:userProperties
                           fetchType:fetchType];
                
              }
               progressCallback:
              ([delegate respondsToSelector:@selector(ARCFetcher:receivedDataLength:expectedContentLength:userProperties:fetchType:)])
                // if the delegate responds to the callback
                // do the block
                ?^(NSUInteger receivedDataLength,
                 long long expectedContentLength,
                 NSMutableDictionary *userProperties,
                   ARCFetcherType fetchType)
                {
                  [delegate ARCFetcher:self
                    receivedDataLength:receivedDataLength
                 expectedContentLength:expectedContentLength
                        userProperties:userProperties
                             fetchType:fetchType];
                }
                // else just pass nil to the block
                :nil];
}


#pragma mark - stop fetch
// stop fetch
- (void)stopFetching
{
  [self.gtmFetcher stopFetching];
}

#pragma mark - dealloc
- (void)dealloc{
  NSLog(@"ARCFetcher deallocated");
}

@end
