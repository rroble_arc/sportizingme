//
//  NSString+EmailAddress.h
//  ARCStringUtil
//
//  Created by Shlby Puerto on 5/2/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ARCStringUtil)
-(BOOL)isEmail;

- (NSString *)getYoutubeID;
- (NSString*)removeWhiteSpaces;

@end
