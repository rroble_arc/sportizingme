//
//  NSString+EmailAddress.m
//  ARCStringUtil
//
//  Created by Shlby Puerto on 5/2/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "NSString+ARCStringUtil.h"

@implementation NSString (EmailAddress)
-(BOOL)isEmail{

    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}
- (NSString *) getYoutubeID{
    NSString *youtubeString = self;
    NSError *error = NULL;
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:@"(?:youtube\\.com\\/embed\\/|(?:[a-z]{2,3}\\.)?youtube\\.com\\/watch(?:\\?|#\\!)!feature=|v=)([\\w-]{11})"
     
                                              options:NSRegularExpressionCaseInsensitive
                                                error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:youtubeString
                                                    options:0
                                                      range:NSMakeRange(0, [youtubeString length])];
    
    NSRange videoIDRange = [match rangeAtIndex:1];
    
    NSString *substringForFirstMatch = [youtubeString substringWithRange:videoIDRange];
    
    if([substringForFirstMatch length]==0){
        NSRegularExpression *regex =
        [NSRegularExpression regularExpressionWithPattern:@"(?:youtu\\.be\\/|(?:[a-z]{2,3}\\.)?youtube\\.com\\/watch(?:\\?|#\\!)!feature=|v=)([\\w-]{11})"
         
                                                  options:NSRegularExpressionCaseInsensitive
                                                    error:&error];
        NSTextCheckingResult *match = [regex firstMatchInString:youtubeString
                                                        options:0
                                                          range:NSMakeRange(0, [youtubeString length])];
        
        NSRange videoIDRange = [match rangeAtIndex:1];
        
        substringForFirstMatch = [youtubeString substringWithRange:videoIDRange];
        if([substringForFirstMatch length]==0){
            
        }else{
            return substringForFirstMatch;
        }
    }
    else{
        return substringForFirstMatch;
    }
    
    return nil;
}
-(NSString*)removeWhiteSpaces{
    return [self stringByReplacingOccurrencesOfString:@" " withString:@""];
}
@end
