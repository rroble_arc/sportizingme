//
//  SMEIntroVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/6/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEIntroVC.h"
#import "SMEAppDelegate.h"
#import "SMEAthleteIntroVC.h"
#import "SMESponsorIntroVC.h"
#import "SMELanguage.h"
#import "SMEHowItWorksVC.h"

//Temporary
#import "SMESponsorMenuVC.h"
#import "SMEFindAthleteVC.h"


@interface SMEIntroVC ()

@end

@implementation SMEIntroVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"] forBarMetrics:UIBarMetricsDefault];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self updateLanguage];
}
-(void)updateLanguage{
    [self.btnAthlete setTitle:[[SMELanguage sharedManager] introBtnAthlete] forState:UIControlStateNormal];
    [self.btnSponsor setTitle:[[SMELanguage sharedManager] introBtnSponsor] forState:UIControlStateNormal];
    [self.btnWorks setTitle:[[SMELanguage sharedManager] introBtnHowItWorks] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)athleteAction:(id)sender {
//    SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [appDelegate createDeck];
//    appDelegate.window.rootViewController = appDelegate.deckAthlete;
//    [appDelegate.window makeKeyAndVisible];
  SMEAthleteIntroVC *vc = [[SMEAthleteIntroVC alloc] initWithNibName:@"SMEAthleteIntroVC"
                                                              bundle:nil];
  
  [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)sponsorAction:(id)sender {
    SMESponsorIntroVC *vc = [[SMESponsorIntroVC alloc] initWithNibName:@"SMESponsorIntroVC"
                                                                bundle:nil];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    /*SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate createDeckSponsor];
    appDelegate.window.rootViewController = appDelegate.deckSponsor;
    [appDelegate.window makeKeyAndVisible];*/
}


- (IBAction)howItWorksAction:(id)sender {
    SMEHowItWorksVC *vc = [[SMEHowItWorksVC alloc]initWithNibName:@"SMEHowItWorksVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
