//
//  SMEAthleteSignUpNextVC.m
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "SMEAppDelegate.h"
#import "SMEAthleteSignUpNextVC.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"
#import "SMEUserManager.h"
#import "Reachability.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface SMEAthleteSignUpNextVC ()
{
  CGFloat animatedDistance;
    BOOL GYM;
    BOOL RUN;
    BOOL CYC;
    BOOL MMA;
    BOOL SOC;
}
@end

@implementation SMEAthleteSignUpNextVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"FB : %@ %d",self.facebookIDString,self.FBsignup);
    [self setupNavigationBar];
    [self setLanguage];
}
-(void)setLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.lblAge setText:[lang athleteSignupTxtAge]];
    [self.lblTL setText:[lang athleteSignupTxtLocation]];
    [self.lblSports setText:[lang athleteSignupTxtSport]];
    [self.lblHeight setText:[lang athleteSignupTxtHeight]];
    [self.lblWeight setText:[lang athleteSignupTxtWeight]];
    [self.lblPaypal setText:[lang athleteSignupTxtPaypal]];
    [self.doneBtn setTitle:[lang athleteSignupBtnDone] forState:UIControlStateNormal];
    [self.lblTellus setText:[lang athleteSignupLblTellus]];
    if(lang.type == SMELanguageType_french){
        [self.lblTellus setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    }else{
        [self.lblTellus setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    }
    
    
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];

    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  // this resign any keyboard
  [self.view endEditing:YES];
}

#pragma mark - view life cycle
- (void)viewDidAppear:(BOOL)animated{
  [super viewDidAppear:animated];
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
  CGRect textFieldRect =
  [self.view.window convertRect:textField.bounds fromView:textField];
  CGRect viewRect =
  [self.view.window convertRect:self.view.bounds fromView:self.view];
  
  CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
  CGFloat numerator =
  midline - viewRect.origin.y
  - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
  CGFloat denominator =
  (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
  * viewRect.size.height;
  CGFloat heightFraction = numerator / denominator;
  
  if (heightFraction < 0.0)
  {
    heightFraction = 0.0;
  }
  else if (heightFraction > 1.0)
  {
    heightFraction = 1.0;
  }
  
  UIInterfaceOrientation orientation =
  [[UIApplication sharedApplication] statusBarOrientation];
  if (orientation == UIInterfaceOrientationPortrait ||
      orientation == UIInterfaceOrientationPortraitUpsideDown)
  {
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
  }
  else
  {
    animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
  }
  
  CGRect viewFrame = self.view.frame;
  viewFrame.origin.y -= animatedDistance;
  
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationBeginsFromCurrentState:YES];
  [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
  
  [self.view setFrame:viewFrame];
  
  [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
  CGRect viewFrame = self.view.frame;
  viewFrame.origin.y += animatedDistance;
  
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationBeginsFromCurrentState:YES];
  [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
  
  [self.view setFrame:viewFrame];
  
  [UIView commitAnimations];
    
    if ([textField isEqual:self.ageTextField]) {
        self.ageString = self.ageTextField.text;
      
    }
    else if ([textField isEqual:self.trainingLocationTextField]){
        self.locationString = self.trainingLocationTextField.text;
       
    }
    else if ([textField isEqual:self.heightTextField]){
        self.heightString = self.heightTextField.text;
       
    }
    else if ([textField isEqual:self.weightTextField]){
        self.weightString = self.weightTextField.text;
      
    }
    else if ([textField isEqual:self.paypalTextField]){
        self.paypalString = self.paypalTextField.text;
       
    }

    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
  //[textField resignFirstResponder];
  
  if ([textField isEqual:self.ageTextField]) {
      self.ageString = self.ageTextField.text;
    [self.trainingLocationTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.trainingLocationTextField]){
      self.locationString = self.trainingLocationTextField.text;
    [self.heightTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.heightTextField]){
      self.heightString = self.heightTextField.text;
    [self.weightTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.weightTextField]){
      self.weightString = self.weightTextField.text;
    [self.paypalTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.paypalTextField]){
      self.paypalString = self.paypalTextField.text;
    [textField resignFirstResponder];
  }
  
  return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{

    if([self.textView.text isEqualToString:@""]){
        textView.text = @"Input text book for athletes";
        self.descriptionString = @"";
    }else{
        self.descriptionString = textView.text;
    }
    [textView resignFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@"Input text book for athletes"]){
        textView.text = @"";
    }
    return YES;
}

#pragma mark - Button Action
- (IBAction)doneAction:(id)sender {
    
   [self.view endEditing:YES];
   
    if([[Reachability reachabilityForInternetConnection] isReachable]){
       
    //SignUp
    self.sportsArray = [NSMutableArray array];

    if(GYM) [self.sportsArray addObject:@"GYM"];
    if(RUN) [self.sportsArray addObject:@"RUN"];
    if(CYC) [self.sportsArray addObject:@"CYC"];
    if(MMA) [self.sportsArray addObject:@"MMA"];
    if(SOC) [self.sportsArray addObject:@"SOC"];

    if([self checkTextFields]){
        NSMutableDictionary *postDict;
        NSString * urlString;
        if(!self.FBsignup){
        
    postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.nameString,@"name",self.ageString,@"age",self.emailString,@"email",self.passwordString,@"password",self.locationString,@"trainingLocation",self.heightString,@"height",self.weightString,@"weight",self.paypalString,@"paypal",self.descriptionString,@"shortDescription", nil];
        
        for(int i = 0;i <[self.sportsArray count];i++){
            NSString *key = [NSString stringWithFormat:@"sports[%d]",i];
            [postDict setValue:[self.sportsArray objectAtIndex:i] forKey:key];
        }
        
    urlString = @"http://192.168.254.20/sportizingme/api/athlete/signup";
            
        }else{
            postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.nameString,@"name",self.ageString,@"age",self.emailString,@"email",self.passwordString,@"password",self.locationString,@"trainingLocation",self.heightString,@"height",self.weightString,@"weight",self.paypalString,@"paypal",self.descriptionString,@"shortDescription",self.facebookIDString,@"facebook_id", nil];
            
            for(int i = 0;i <[self.sportsArray count];i++){
                NSString *key = [NSString stringWithFormat:@"sports[%d]",i];
                [postDict setValue:[self.sportsArray objectAtIndex:i] forKey:key];
            }
            
            urlString = @"http://192.168.254.20/sportizingme/api/athlete/signup/facebook";
        }
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        if(error){
            NSLog(@"result %@",result);
        }else{

            if([result objectForKey:@"error"]==nil){
                SMEUserManager *userMgr = [SMEUserManager sharedManager];
                
                [userMgr setToken:[result objectForKey:@"token"]];
                userMgr.userID = [result objectForKey:@"id"];
                userMgr.name = [result objectForKey:@"name"];
                userMgr.email = [result objectForKey:@"email"];
                userMgr.age = [result objectForKey:@"age"];
                userMgr.location = [result objectForKey:@"training_location"];
                userMgr.sport = [result objectForKey:@"sport"];
                userMgr.height = [result objectForKey:@"height"];
                userMgr.weight = [result objectForKey:@"weight"];
                userMgr.paypal = [result objectForKey:@"paypal"];
                
                [self loginAndCreateDeck];
            }else{
                NSLog(@"XX%@",result);
                NSLog(@"XX%@",[result objectForKey:@"fields"]);
                if([[result objectForKey:@"fields"] objectForKey:@"email"]!=nil){
                NSString *msg = @"Email is being used";
                
                [[[UIAlertView alloc]initWithTitle:[result objectForKey:@"error"] message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show ];
                }
                
            }


        }
    } progressCallback:nil];
    
    }
    //!
    }
    else{
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }

}

- (IBAction)sportsAction:(id)sender {
    if(sender == self.btnGym){
        if(GYM)GYM=NO;else GYM=YES;
    }else if(sender == self.btnRun){
        if(RUN)RUN=NO;else RUN=YES;
    }else if(sender == self.btnCyc){
        if(CYC)CYC=NO;else CYC=YES;
    }else if(sender == self.btnMMA){
        if(MMA)MMA=NO;else MMA=YES;
    }else if(sender == self.btnSoc){
        if(SOC)SOC=NO;else SOC=YES;
    }
}

#pragma mark - login and create deck
- (void)loginAndCreateDeck{
    SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate createDeck];
    appDelegate.window.rootViewController = appDelegate.deckAthlete;
    [appDelegate.window makeKeyAndVisible];
}
-(BOOL)checkTextFields{
    

    
    if(self.ageString.length <= 0)      {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input age field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(self.locationString.length <= 0) {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input training location field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(self.sportsArray.count == 0)     {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please add a sport" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(self.heightString.length <= 0 )  {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input height field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(self.weightString.length <= 0)   {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input weight field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(self.descriptionString.length <= 0) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input short meditation field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
        return NO;
    }
    //if(self.sportsArray.count <= 0)     {NSLog(@"NO SPORTS"); return NO;}
    //if(self.paypalString.length <= 0 )  {NSLog(@"NO PAYPAL"); return NO;}

    
    
    return YES;
}
@end
