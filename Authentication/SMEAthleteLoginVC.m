//
//  SMEAthleteLoginVC.m
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "Reachability.h"
#import "ARCFetcher.h"
#import "SMEAthleteLoginVC.h"
#import "SMEAppDelegate.h"
#import "ARCFacebookManager.h"
#import "SMELanguage.h"

#import "SMEUserManager.h"

#import "ARCFetcher.h"
#import "SVProgressHUD.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface SMEAthleteLoginVC () <ARCFetcherDelegate>
{
  CGFloat animatedDistance;
}

@end

@implementation SMEAthleteLoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateLanguage];
    [self setupNavigationBar];
}
-(void)setupNavigationBar{
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"mainBackground"]]];
   
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  // this resign any keyboard
  [self.view endEditing:YES];
}

#pragma mark - Updates the language
-(void)updateLanguage{
  SMELanguage *lang = [SMELanguage sharedManager];
   
    if(lang.type == SMELanguageType_french){
        [self.loginWithFacebookBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    }
   
    [self.loginWithFacebookBtn setTitle:[lang athleteLoginBtnLoginFacebook] forState:UIControlStateNormal];
    [self.loginBtn setTitle:[lang athleteLoginBtnLogin] forState:UIControlStateNormal];
    [self.emailTextField setPlaceholder:[lang athleteLoginTxtEmail]];
    [self.passwordTextField setPlaceholder:[lang athleteLoginTxtPassword]];
    [self.forgetBtn setTitle:[lang athleteLoginBtnForgetPassword] forState:UIControlStateNormal];
    [self.lblOr setText:[lang athleteLoginLblOr]];
    
    
}
#pragma mark - login and create deck
- (void)loginAndCreateDeck{
  SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
  [appDelegate createDeck];
  appDelegate.window.rootViewController = appDelegate.deckAthlete;
  [appDelegate.window makeKeyAndVisible];
}


#pragma mark - Button Action
- (IBAction)loginWithFacebookAction:(UIButton*)sender {
   
    if([[Reachability reachabilityForInternetConnection] isReachable]){
   
  //[self loginAndCreateDeck];
  [sender setEnabled:NO];
    [SVProgressHUD show];
    
      
  ARCFacebookManager *fbm = [ARCFacebookManager sharedManager];
  [fbm authenticateWithFBSessionLoginBehavior:FBSessionLoginBehaviorForcingWebView
                            completionHandler:^(id result, NSError *error) {
                              if (error) {
                                NSLog(@"failed to authenticate to facebook");
                              }
                              else{
                                 
                                NSLog(@"result: %@", result);
                                  NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:[result objectForKey:@"email"],@"email",nil];
                                  NSString * urlString = @"http://192.168.254.20/sportizingme/api/login/facebook";

                                  
                                  
                                  ARCFetcher *fetch = [[ARCFetcher alloc]init];
                                  
                                  [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
                                      
                                      [SVProgressHUD dismiss];
                                      if(error){
                                          NSLog(@"result %@",result);
                                          NSLog(@"error %@",error.localizedDescription);
                                      }else{
                                          
                                          NSLog(@"result %@",result);
                                          if([result objectForKey:@"error"]!=nil){
                                              [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Facebook account not registered to sport. Please register first." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show ];
                                          }else{
                                              
                                               SMEUserManager *userMgr = [SMEUserManager sharedManager];
                                               
                                               [userMgr setToken:[result objectForKey:@"token"]];
                                               userMgr.userID = [result objectForKey:@"id"];
                                               userMgr.name = [result objectForKey:@"name"];
                                               userMgr.email = [result objectForKey:@"email"];
                                               userMgr.age = [result objectForKey:@"age"];
                                               userMgr.location = [result objectForKey:@"training_location"];
                                               userMgr.sport = [result objectForKey:@"sport"];
                                               userMgr.height = [result objectForKey:@"height"];
                                               userMgr.weight = [result objectForKey:@"weight"];
                                               userMgr.paypal = [result objectForKey:@"paypal"];
                                               
                                               [self loginAndCreateDeck];
                                               
                                               
                                          }
                                          
                                      }
                                  } progressCallback:nil];
                                   
                              }
                              [sender setEnabled:YES];
                            }];
    }else{
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}
- (IBAction)loginAction:(id)sender {
    
    if([[Reachability reachabilityForInternetConnection] isReachable]){
        
    [SVProgressHUD show];

    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTextField.text,@"email",self.passwordTextField.text,@"password", nil];
    NSString * urlString = @"http://192.168.254.20/sportizingme/api/login/athlete";
    
   
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        [SVProgressHUD dismiss];
            if(error){
                NSLog(@"result %@",result);
                NSLog(@"error %@",error.localizedDescription);
            }else{
                
                NSLog(@"result %@",result);
                if([result objectForKey:@"error"]==nil){
                SMEUserManager *userMgr = [SMEUserManager sharedManager];
        
                [userMgr setToken:[result objectForKey:@"token"]];
                userMgr.userID = [result objectForKey:@"id"];
                userMgr.name = [result objectForKey:@"name"];
                userMgr.email = [result objectForKey:@"email"];
                userMgr.age = [result objectForKey:@"age"];
                userMgr.location = [result objectForKey:@"training_location"];
                userMgr.sport = [result objectForKey:@"sport"];
                userMgr.height = [result objectForKey:@"height"];
                userMgr.weight = [result objectForKey:@"weight"];
                userMgr.paypal = [result objectForKey:@"paypal"];
                
                [self loginAndCreateDeck];
                }
                else{
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:[result objectForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                }
            }
    } progressCallback:nil];
        
    
    }else{
         [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
 //[self loginAndCreateDeck];
}
- (IBAction)forgotMyPasswordAction:(id)sender {
  NSLog(@"forgot my password action");
  
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
  CGRect textFieldRect =
  [self.view.window convertRect:textField.bounds fromView:textField];
  CGRect viewRect =
  [self.view.window convertRect:self.view.bounds fromView:self.view];
  
  CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
  CGFloat numerator =
  midline - viewRect.origin.y
  - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
  CGFloat denominator =
  (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
  * viewRect.size.height;
  CGFloat heightFraction = numerator / denominator;
  
  if (heightFraction < 0.0)
  {
    heightFraction = 0.0;
  }
  else if (heightFraction > 1.0)
  {
    heightFraction = 1.0;
  }
  
  UIInterfaceOrientation orientation =
  [[UIApplication sharedApplication] statusBarOrientation];
  if (orientation == UIInterfaceOrientationPortrait ||
      orientation == UIInterfaceOrientationPortraitUpsideDown)
  {
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
  }
  else
  {
    animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
  }
  
  CGRect viewFrame = self.view.frame;
  viewFrame.origin.y -= animatedDistance;
  
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationBeginsFromCurrentState:YES];
  [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
  
  [self.view setFrame:viewFrame];
  
  [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
  CGRect viewFrame = self.view.frame;
  viewFrame.origin.y += animatedDistance;
  
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationBeginsFromCurrentState:YES];
  [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
  
  [self.view setFrame:viewFrame];
  
  [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
  
  
  if ([textField isEqual:self.emailTextField]) {
    [self.passwordTextField becomeFirstResponder];
  }
  else{
    [textField resignFirstResponder];
  }
  
  return YES;
}




@end
