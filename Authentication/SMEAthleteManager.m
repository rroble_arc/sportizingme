//
//  SMEAthleteManager.m
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/10/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEAthleteManager.h"
static SMEAthleteManager *instance;

@implementation SMEAthleteManager

#pragma mark - init and shared instance
- (id)init{
  self = [super init];
  if (self) {
    // custom init here
  }
  return self;
}

+ (SMEAthleteManager *)shareManager
{
  if(instance == nil){
    instance = [[SMEAthleteManager alloc] init];
  }
  return instance;
}

@end
