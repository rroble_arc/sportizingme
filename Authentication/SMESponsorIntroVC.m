//
//  SMESponsorIntroVC.m
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMESponsorIntroVC.h"
#import "SMESponsorLoginVC.h"
#import "SMESponsorSignupVC.h"
#import "SMELanguage.h"

@interface SMESponsorIntroVC ()

@end

@implementation SMESponsorIntroVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
     [self.navigationController.navigationBar setTintColor:[UIColor lightGrayColor]];
    [self updateLanguage];
}
- (void)updateLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.lblSpons setText:[lang sponsorIntroBtnAthlete]];
    [self.btnLogin setTitle:[lang sponsorIntroBtnLogin] forState:UIControlStateNormal];
    [self.lblOr setText:[lang sponsorIntroBtnOr]];
    [self.btnSignup setTitle:[lang sponsorIntroBtnSignup] forState:UIControlStateNormal];
    [self.btnTake setTitle:[lang athleteTakeMeBackBtnSignup] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginAction:(id)sender {
    SMESponsorLoginVC *lvc = [[SMESponsorLoginVC alloc]initWithNibName:@"SMESponsorLoginVC" bundle:nil];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)signupAction:(id)sender {
    SMESponsorSignupVC *lvc = [[SMESponsorSignupVC alloc]initWithNibName:@"SMESponsorSignupVC"  bundle:nil];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)takeMeBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
