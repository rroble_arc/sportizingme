//
//  SMEAthleteLoginVC.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEAthleteLoginVC : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblOr;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginWithFacebookBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetBtn;

- (IBAction)loginWithFacebookAction:(UIButton*)sender;
- (IBAction)loginAction:(id)sender;
- (IBAction)forgotMyPasswordAction:(id)sender;




@end
