//
//  SMESponsorLoginVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "SVProgressHUD.h"
#import "SMESponsorLoginVC.h"
#import "SMEAppDelegate.h"
#import "IIViewDeckController.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"
#import "SMESponsorUserManager.h"
#import "Reachability.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface SMESponsorLoginVC ()
{
    CGFloat animatedDistance;
}

@end

@implementation SMESponsorLoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self setupNavigationBar];
    [self updateLanguage];
}
- (void)updateLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.emailTextField setPlaceholder:[lang sponsorLoginTxtEmail]];
    [self.passwordTextField setPlaceholder:[lang sponsorLoginTxtPassword]];
       [self.btnLogin setTitle:[lang sponsorLoginBtnLogin] forState:UIControlStateNormal];
    [self.btnForgot setTitle:[lang sponsorLoginBtnForgetPassword] forState:UIControlStateNormal];
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  // this resign any keyboard
  [self.view endEditing:YES];
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if ([textField isEqual:self.emailTextField]) {
        [self.passwordTextField becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (IBAction)loginAction:(id)sender {
   
    if([[Reachability reachabilityForInternetConnection] isReachable]){
    
    [SVProgressHUD show];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTextField.text,@"email",self.passwordTextField.text,@"password", nil];
     NSString * urlString = @"http://192.168.254.20/sportizingme/api/login/sponsor";
    
    
    ARCFetcher *fetch = [[ARCFetcher alloc]init];
    
    [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
        [SVProgressHUD dismiss];
        if(error){
            
        }else{
            NSLog(@"result %@",result);
            SMESponsorUserManager *userMgr = [SMESponsorUserManager sharedManager];
            
            [userMgr setToken:[result objectForKey:@"token"]];
            userMgr.userID = [result objectForKey:@"id"];
            userMgr.company_name = [result objectForKey:@"company_name"];
            userMgr.email = [result objectForKey:@"email"];
            userMgr.industry = [result objectForKey:@"industry"];
            userMgr.location = [result objectForKey:@"location"];
            userMgr.no_of_employees = [result objectForKey:@"no_of_employees"];
            userMgr.revenue = [result objectForKey:@"revenue"];
            userMgr.paypal = [result objectForKey:@"paypal"];
            [self loginAndCreateDeck];
        }
    } progressCallback:nil];
}
    else{
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}
#pragma mark - login and create deck
- (void)loginAndCreateDeck{
    SMEAppDelegate *appDelegate = (SMEAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate createDeckSponsor];
    appDelegate.window.rootViewController = appDelegate.deckSponsor;
    [appDelegate.window makeKeyAndVisible];
}
- (IBAction)forgotAction:(id)sender {
}
@end
