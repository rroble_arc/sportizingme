//
//  SMEHowItWorksVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/14/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEHowItWorksVC.h"
#import "SMELanguage.h"
@interface SMEHowItWorksVC ()

@end

@implementation SMEHowItWorksVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    NSURL *res = [[NSBundle mainBundle]resourceURL];
    if(res){
        NSURL *urlToLoad = [res URLByAppendingPathComponent:@"HowItWorks.html"];
        if(urlToLoad){
            NSURLRequest *req = [NSURLRequest requestWithURL:urlToLoad];
            [self.webView loadRequest:req];
        }
    }
    
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
   
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
 
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
