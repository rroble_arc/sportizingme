//
//  SMEAthleteIntroVC.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SMEAthleteIntroVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *signupBtn;
@property (weak, nonatomic) IBOutlet UIButton *takemebackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblAthlete;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;


@end
