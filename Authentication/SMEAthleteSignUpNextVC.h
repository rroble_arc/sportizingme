//
//  SMEAthleteSignUpNextVC.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEAthleteSignUpNextVC : UIViewController <UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSString *facebookIDString;
@property (nonatomic) BOOL FBsignup;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *trainingLocationTextField;
@property (weak, nonatomic) IBOutlet UITextField *sportTextField;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UITextField *paypalTextField;


//
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblTL;
@property (weak, nonatomic) IBOutlet UILabel *lblSports;
@property (weak, nonatomic) IBOutlet UILabel *lblHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblPaypal;
@property (weak, nonatomic) IBOutlet UILabel *lblTellus;

//!

//
@property (strong, nonatomic) NSString *emailString;
@property (strong, nonatomic) NSString *passwordString;
@property (strong, nonatomic) NSString *nameString;
@property (strong, nonatomic) NSString *ageString;
@property (strong, nonatomic) NSString *locationString;
@property (strong, nonatomic) NSMutableArray *sportsArray;
@property (strong, nonatomic) NSString *heightString;
@property (strong, nonatomic) NSString *weightString;
@property (strong, nonatomic) NSString *paypalString;
@property (strong, nonatomic) NSString *descriptionString;
//!
//!
@property (weak, nonatomic) IBOutlet UIButton *btnGym;
@property (weak, nonatomic) IBOutlet UIButton *btnRun;
@property (weak, nonatomic) IBOutlet UIButton *btnCyc;
@property (weak, nonatomic) IBOutlet UIButton *btnMMA;
@property (weak, nonatomic) IBOutlet UIButton *btnSoc;


//!
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;

- (IBAction)doneAction:(id)sender;

- (IBAction)sportsAction:(id)sender;


@end
