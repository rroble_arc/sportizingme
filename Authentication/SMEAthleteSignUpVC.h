//
//  SMEAthleteSignUpVC.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEAthleteSignUpVC : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) NSString *facebookIDString;
@property (nonatomic) BOOL FBsignup;

// cells
@property (weak, nonatomic) IBOutlet UIButton *signUpWithFacebookBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField2;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)signUpWithFacebookAction:(id)sender;
- (IBAction)nextAction:(id)sender;
@end
