//
//  SMESponsorIntroVC.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMESponsorIntroVC : UIViewController
- (IBAction)loginAction:(id)sender;
- (IBAction)signupAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblSpons;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UIButton *btnTake;

@end
