//
//  SMEIntroVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/6/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMEIntroVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnAthlete;
@property (weak, nonatomic) IBOutlet UIButton *btnSponsor;
@property (weak, nonatomic) IBOutlet UIButton *btnWorks;

- (IBAction)athleteAction:(id)sender;
- (IBAction)sponsorAction:(id)sender;

- (IBAction)howItWorksAction:(id)sender;

@end
