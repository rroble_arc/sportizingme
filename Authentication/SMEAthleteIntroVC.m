//
//  SMEAthleteIntroVC.m
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/7/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEAthleteIntroVC.h"
#import "SMEAthleteLoginVC.h"
#import "SMEAthleteSignUpVC.h"
#import "SMELanguage.h"

@interface SMEAthleteIntroVC ()

@end

@implementation SMEAthleteIntroVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view from its nib.
  [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar_logo"]
                                                forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - View Life Cycle
- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
    [self updateLanguage];
    [self.navigationItem setHidesBackButton:YES];
  // hide the navigation bar
  //[self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
  [super viewWillDisappear:animated];
  
  // show the navigation bar
  //[self.navigationController setNavigationBarHidden:NO animated:animated];
}
#pragma mark Updates
-(void)updateLanguage{
    [self.loginBtn setTitle:[[SMELanguage sharedManager] athleteIntroBtnLogin] forState:UIControlStateNormal];
    [self.signupBtn setTitle:[[SMELanguage sharedManager] athleteIntroBtnSignup] forState:UIControlStateNormal];
    [self.takemebackBtn setTitle:[[SMELanguage sharedManager] athleteTakeMeBackBtnSignup] forState:UIControlStateNormal];
    [self.lblAthlete setText:[[SMELanguage sharedManager] athleteIntroLblAthlete]];
    [self.lblOr setText:[[SMELanguage sharedManager] athleteIntroLblOr]];
}
#pragma mark - Button Action
- (IBAction)loginAction:(id)sender {
  NSLog(@"loginAction");
  SMEAthleteLoginVC *vc = [[SMEAthleteLoginVC alloc] initWithNibName:@"SMEAthleteLoginVC" bundle:nil];
  [self.navigationController pushViewController:vc animated:YES];
  
}
- (IBAction)signUpAction:(id)sender {
  NSLog(@"signUpAction");
  SMEAthleteSignUpVC *vc = [[SMEAthleteSignUpVC alloc] initWithNibName:@"SMEAthleteSignUpVC" bundle:nil];
  [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)takeMeBackAction:(id)sender {
    NSLog(@"takeMeBackAction");
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
