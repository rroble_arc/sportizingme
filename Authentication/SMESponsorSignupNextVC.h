//
//  SMESponsorSignupNextVC.h
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMESponsorSignupNextVC : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UITextField *industryTextField;
@property (weak, nonatomic) IBOutlet UITextField *noEmployeeTextField;
@property (weak, nonatomic) IBOutlet UITextField *revenueTextField;
@property (weak, nonatomic) IBOutlet UITextField *paypalTextField;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

- (IBAction)doneAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblTell;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UILabel *lblFindAthletes;
@property (weak, nonatomic) IBOutlet UILabel *lblPaypal;

@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblIndustry;
@property (weak, nonatomic) IBOutlet UILabel *lblRevenue;

@property (weak, nonatomic) IBOutlet UILabel *lblEmployees;

//
@property (strong, nonatomic) NSString *companyString;
@property (strong, nonatomic) NSString *emailString;
@property (strong, nonatomic) NSString *passwordString;
@property (strong, nonatomic) NSString *locationString;
@property (strong, nonatomic) NSString *industryString;
@property (strong, nonatomic) NSString *employeeString;
@property (strong, nonatomic) NSString *revenueString;
@property (strong, nonatomic) NSString *paypalString;
//!

@end
