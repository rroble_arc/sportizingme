//
//  SMESponsorSignupNextVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//
#import "SMESponsorUserManager.h"
#import "SMESponsorSignupNextVC.h"
#import "SMELanguage.h"
#import "ARCFetcher.h"
#import "Reachability.h"
#import "SVProgressHUD.h"


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface SMESponsorSignupNextVC ()
{
  CGFloat animatedDistance;
}
@end

@implementation SMESponsorSignupNextVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self updateLanguage];
}
- (void)updateLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    
    if(lang.type == SMELanguageType_french)
        [self.lblLocation setFont:[UIFont fontWithName:@"Helvetica-Bold" size:8]];
    else [self.lblLocation setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    
    
    
    
    [self.locationTextField setPlaceholder:[lang sponsorSignupNextTxtLocation]];
    [self.industryTextField setPlaceholder:[lang sponsorSignupNextTxtIndustry]];
    [self.noEmployeeTextField setPlaceholder:[lang sponsorSignupNextTxtEmployees]];
    [self.revenueTextField setPlaceholder:[lang sponsorSignupNextTxtRevenue]];
    [self.paypalTextField setPlaceholder:[lang sponsorSignupNextTxtPaypal]];
    
    [self.btnDone setTitle:[lang sponsorSignupNextBtnDone] forState:UIControlStateNormal];
    
    [self.lblLocation setText:[lang sponsorSignupNextLblLocation]];
    [self.lblIndustry setText:[lang sponsorSignupNextLblIndustry]];
    [self.lblEmployees setText:[lang sponsorSignupNextLblEmployees]];
    [self.lblRevenue setText:[lang sponsorSignupNextLblRevenue]];
    [self.lblPaypal setText:[lang sponsorSignupNextLblPaypal]];
    
    
    
    [self.topLabel setText:[lang sponsorSignupNextLblLetsFind]];
    [self.bottomLabel setText:[lang sponsorSignupNextLblLetsFind]];
    
    
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
    
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  // this resign any keyboard
  [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
  CGRect textFieldRect =
  [self.view.window convertRect:textField.bounds fromView:textField];
  CGRect viewRect =
  [self.view.window convertRect:self.view.bounds fromView:self.view];
  
  CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
  CGFloat numerator =
  midline - viewRect.origin.y
  - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
  CGFloat denominator =
  (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
  * viewRect.size.height;
  CGFloat heightFraction = numerator / denominator;
  
  if (heightFraction < 0.0)
  {
    heightFraction = 0.0;
  }
  else if (heightFraction > 1.0)
  {
    heightFraction = 1.0;
  }
  
  UIInterfaceOrientation orientation =
  [[UIApplication sharedApplication] statusBarOrientation];
  if (orientation == UIInterfaceOrientationPortrait ||
      orientation == UIInterfaceOrientationPortraitUpsideDown)
  {
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
  }
  else
  {
    animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
  }
  
  CGRect viewFrame = self.view.frame;
  viewFrame.origin.y -= animatedDistance;
  
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationBeginsFromCurrentState:YES];
  [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
  
  [self.view setFrame:viewFrame];
  
  [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
  CGRect viewFrame = self.view.frame;
  viewFrame.origin.y += animatedDistance;
  
  [UIView beginAnimations:nil context:NULL];
  [UIView setAnimationBeginsFromCurrentState:YES];
  [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
  
  [self.view setFrame:viewFrame];
  
  [UIView commitAnimations];
    
    if ([textField isEqual:self.locationTextField]) {
        self.locationString = self.locationTextField.text;
       
    }
    else if ([textField isEqual:self.industryTextField]){
        self.industryString = self.industryTextField.text;
     
    }
    else if ([textField isEqual:self.noEmployeeTextField]){
        self.employeeString = self.noEmployeeTextField.text;
           }
    else if ([textField isEqual:self.revenueTextField]){
        self.revenueString = self.revenueTextField.text;
  
    }
    else if ([textField isEqual:self.paypalTextField]){
        self.paypalString = self.paypalTextField.text;
    
    }
    

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
  //[textField resignFirstResponder];
  
  if ([textField isEqual:self.locationTextField]) {
    self.locationString = self.locationTextField.text;
    [self.industryTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.industryTextField]){
      self.industryString = self.industryTextField.text;
    [self.noEmployeeTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.noEmployeeTextField]){
      self.employeeString = self.noEmployeeTextField.text;
    [self.revenueTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.revenueTextField]){
      self.revenueString = self.revenueTextField.text;
    [self.paypalTextField becomeFirstResponder];
  }
  else if ([textField isEqual:self.paypalTextField]){
       self.paypalString = self.paypalTextField.text;
      self.industryString = self.industryTextField.text;
    [textField resignFirstResponder];
  }
  
  return YES;
}

#pragma mark - Button Action
- (IBAction)doneAction:(id)sender {
    
    if([[Reachability reachabilityForInternetConnection] isReachable]){
   
     [self.view endEditing:YES];
  
    if([self checkTextFields]){
          [SVProgressHUD show];
        NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:self.companyString,@"companyName",self.emailString,@"email",self.passwordString,@"password",self.locationString,@"location",self.industryString,@"industry",self.employeeString,@"number_of_employees",self.revenueString,@"revenue",self.paypalString,@"paypal", nil];
        NSString * urlString = @"http://192.168.254.20/sportizingme/api/sponsor/signup";
        
        
        ARCFetcher *fetch = [[ARCFetcher alloc]init];
        
        [fetch startFetchWithURLString:urlString postParams:postDict userProperties:nil fetchType:ARCFetcherFetchType_JSON completionHandler:^(id result, NSError *error, NSMutableDictionary *userProperties, ARCFetcherType fetchType) {
            [SVProgressHUD dismiss];
            if(error){
                
            }else{
                if([result objectForKey:@"error"]==nil){
                    SMESponsorUserManager *userMgr = [SMESponsorUserManager sharedManager];
                    
                    [userMgr setToken:[result objectForKey:@"token"]];
                    userMgr.userID = [result objectForKey:@"id"];
                    userMgr.company_name = [result objectForKey:@"company_name"];
                    userMgr.email = [result objectForKey:@"email"];
                    userMgr.industry = [result objectForKey:@"industry"];
                    userMgr.location = [result objectForKey:@"location"];
                    userMgr.no_of_employees = [result objectForKey:@"no_of_employees"];
                    userMgr.revenue = [result objectForKey:@"revenue"];
                    userMgr.paypal = [result objectForKey:@"paypal"];
                }else{
                    NSLog(@"XX%@",result);
                    NSLog(@"XX%@",[result objectForKey:@"fields"]);
                    if([[result objectForKey:@"fields"] objectForKey:@"email"]!=nil){
                        NSString *msg = @"Email is being used";
                        
                        [[[UIAlertView alloc]initWithTitle:[result objectForKey:@"error"] message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show ];
                    }
                    
                }
               
            }
            
        } progressCallback:nil];
        
    }
    }
    else{
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
  NSLog(@"done action");
}
-(BOOL)checkTextFields{
        
    if(self.locationString.length <= 0)  {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input location field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(self.industryString.length <= 0)   {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input industry field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(self.employeeString.length <= 0 )  {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input no. of employees field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    
    return YES;
}
@end
