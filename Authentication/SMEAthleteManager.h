//
//  SMEAthleteManager.h
//  Sportizingme
//
//  Created by Edward Aries Abais on 5/10/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMEAthleteManager : NSObject

+ (SMEAthleteManager *)shareManager;

@end
