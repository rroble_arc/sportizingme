//
//  SMESponsorSignupVC.m
//  Sportizingme
//
//  Created by Shlby Puerto on 5/8/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMESponsorSignupVC.h"
#import "SMESponsorSignupNextVC.h"
#import "SMELanguage.h"
#import "NSString+ARCStringUtil.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
@interface SMESponsorSignupVC ()
{
    CGFloat animatedDistance;
}
@end

@implementation SMESponsorSignupVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self updateLanguage];
}
- (void)updateLanguage{
    SMELanguage *lang = [SMELanguage sharedManager];
    [self.companyTextField setPlaceholder:[lang sponsorSignupTxtCompanyName]];
    [self.emailTextField setPlaceholder:[lang sponsorSignupTxtEmail]];
    [self.passwordTextField setPlaceholder:[lang sponsorSignupTxtPassword]];
    [self.passwordTextField2 setPlaceholder:[lang sponsorSignupTxtPassword2]];
    [self.btnNext setTitle:[lang sponsorSignupBtnNext] forState:UIControlStateNormal];
    
}
-(void)setupNavigationBar{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"whiteBG"]]];
   
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 51, 31)];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    [btn setTitle:[[SMELanguage sharedManager] back] forState:UIControlStateNormal];
    NSString *back = [[SMELanguage sharedManager] back];
    int size = 13;
    if(back.length >5){
        size = 11;
    }
    [btn.titleLabel setTextColor:[UIColor grayColor]];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:size]];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnBar = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    
    self.navigationItem.leftBarButtonItem = btnBar;
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  // this resign any keyboard
  [self.view endEditing:YES];
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
  //[textField resignFirstResponder];
  
  if ([textField isEqual:self.companyTextField]) {
    [self.emailTextField becomeFirstResponder];
   
  }
  else if ([textField isEqual:self.emailTextField]){
    [self.passwordTextField becomeFirstResponder];
     }
  else if ([textField isEqual:self.passwordTextField]){
    [self.passwordTextField2 becomeFirstResponder];
   
  }
  else if ([textField isEqual:self.passwordTextField2]){
    [textField resignFirstResponder];
  }
  
  
  return YES;
}

#pragma mark - Button action

- (IBAction)nextAction:(id)sender {
    if([self checkTextFields]){
        [self.view endEditing:YES];
    SMESponsorSignupNextVC *vc = [[SMESponsorSignupNextVC alloc]initWithNibName:@"SMESponsorSignupNextVC" bundle:nil];
        vc.passwordString = self.passwordTextField.text;
        vc.companyString = self.companyTextField.text;
        vc.emailString = self.emailTextField.text;
    [self.navigationController pushViewController:vc animated:YES];
    }
}
-(BOOL)checkTextFields{
    
    NSString *pass1 = self.passwordTextField.text;
    NSString *pass2 = self.passwordTextField2.text;
    NSString *company = self.companyTextField.text;
    NSString *email = self.emailTextField.text;
    
    if(company.length <= 0)  {NSLog(@"NO NAME");
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input company name field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
        return NO;
    }
    
    if(email.length <= 0)   {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input email field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(![email isEmail])    {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Not a valid email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    if(pass1.length <= 0 )  {[[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input password field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    //if(pass2.length <= 0)   {NSLog(@"NO PASS2"); return NO;}
    if(![pass1 isEqualToString:pass2]) { [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Passwords do not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show]; return NO;}
    
    
    return YES;
}
@end
