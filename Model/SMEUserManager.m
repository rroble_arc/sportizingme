//
//  SMEUserManager.m
//  Sportizingme
//
//  Created by Shlby Puerto on 6/24/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMEUserManager.h"

NSString * const SLFUSERMANAGER_TOKEN = @"SMEUSERMANAGER_TOKEN";
NSString * const SLFUSERMANAGER_USER = @"SMEUSERMANAGER_USER";

static SMEUserManager *instance;

@implementation SMEUserManager
+ (SMEUserManager *)sharedManager
{
    if (instance == nil) {
        instance = [[SMEUserManager alloc] init];
    }
    return instance;
}

#pragma mark super init
- (id)init{
    self = [super init];
    if (self) {
        // init here
        // load from user defaults
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        self.token = [ud stringForKey:SLFUSERMANAGER_TOKEN];
        self.user = [ud dictionaryForKey:SLFUSERMANAGER_USER];
    }
    return self;
}
#pragma mark - token
- (void)setToken:(NSString *)token
{
    if ([_token isEqualToString:token] == NO) {
        _token = token;
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:token forKey:SLFUSERMANAGER_TOKEN];
        [ud synchronize];
    }
}
- (void)setUser:(NSDictionary *)user
{
    if ([_user isEqualToDictionary:user] == NO) {
        _user = user;
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:_user forKey:SLFUSERMANAGER_USER];
        [ud synchronize];
    }
}

#pragma mark action
- (void) logout{
  
    self.token = nil;
    self.username = nil;
    self.password = nil;
    self.userID = nil;
    self.user = nil;
    
}
@end
