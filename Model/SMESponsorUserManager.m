//
//  SMESponsorUserManager.m
//  Sportizingme
//
//  Created by Shlby Puerto on 6/25/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import "SMESponsorUserManager.h"
NSString * const SLFSPONSORUSERMANAGER_TOKEN = @"SMEUSERMANAGER_TOKEN";
NSString * const SLFSPONSORUSERMANAGER_USER = @"SMEUSERMANAGER_USER";

static SMESponsorUserManager *instance;
@implementation SMESponsorUserManager
+ (SMESponsorUserManager *)sharedManager
{
    if (instance == nil) {
        instance = [[SMESponsorUserManager alloc] init];
    }
    return instance;
}
#pragma mark super init
- (id)init{
    self = [super init];
    if (self) {
        // init here
        // load from user defaults
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        self.token = [ud stringForKey:SLFSPONSORUSERMANAGER_TOKEN];
        
    }
    return self;
}
#pragma mark - token
- (void)setToken:(NSString *)token
{
    if ([_token isEqualToString:token] == NO) {
        _token = token;
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:token forKey:SLFSPONSORUSERMANAGER_TOKEN];
        [ud synchronize];
    }
}

#pragma mark action
- (void) logout{
  
    self.token = nil;
    self.company_name = nil;
    self.email = nil;
    self.industry = nil;
    self.location = nil;
    self.no_of_employees = nil;
    self.revenue = nil;
    self.paypal = nil;
    
}
@end
