//
//  SMESponsorUserManager.h
//  Sportizingme
//
//  Created by Shlby Puerto on 6/25/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMESponsorUserManager : NSObject

+ (SMESponsorUserManager *)sharedManager;
- (void) logout;

@property (nonatomic, strong)NSString *token;

@property (nonatomic, strong)NSString *userID;
@property (nonatomic, strong)NSString *company_name;
@property (nonatomic, strong)NSString *email;
@property (nonatomic, strong)NSString *industry;
@property (nonatomic, strong)NSString *location;
@property (nonatomic, strong)NSString *no_of_employees;
@property (nonatomic, strong)NSString *revenue;
@property (nonatomic, strong)NSString *paypal;

@end
