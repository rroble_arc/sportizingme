//
//  SMEUserManager.h
//  Sportizingme
//
//  Created by Shlby Puerto on 6/24/13.
//  Copyright (c) 2013 Arcanys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMEUserManager : NSObject

+ (SMEUserManager *)sharedManager;
- (void) logout;

@property (nonatomic, strong)NSString *token;

@property (nonatomic, strong)NSString *userID;
@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSString *email;
@property (nonatomic, strong)NSString *age;
@property (nonatomic, strong)NSString *location;
@property (nonatomic, strong)NSString *sport;
@property (nonatomic, strong)NSString *height;
@property (nonatomic, strong)NSString *weight;
@property (nonatomic, strong)NSString *paypal;


@property (nonatomic, strong)NSString *username;
@property (nonatomic, strong)NSString *password;

@property (nonatomic, strong)NSDictionary *user;

@end
